/**
 * (2019) Heidelberg University, Institute of Medical Biometry and Informatics (IMBI), Germany
 * 
 * This file is part of myCbrBuilder.
 *  
 * myCbrBuilder is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * myCbrBuilder is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with vivaGen. If not, see <http://www.gnu.org/licenses/>.
 * 
 **/
package validation;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import org.cliommics.vivagen.Instances;

import application.javafx.MyCBRBuilderFxElements;
import core.CaseBaseUtils;
import core.MyCbrUtils;
import core.Utils;
import de.dfki.mycbr.core.DefaultCaseBase;
import de.dfki.mycbr.core.casebase.Attribute;
import de.dfki.mycbr.core.casebase.DoubleAttribute;
import de.dfki.mycbr.core.casebase.Instance;
import de.dfki.mycbr.core.model.AttributeDesc;
import de.dfki.mycbr.core.model.Concept;
import de.dfki.mycbr.core.model.SymbolDesc;
import de.dfki.mycbr.core.retrieval.Retrieval;
import de.dfki.mycbr.core.retrieval.Retrieval.RetrievalMethod;
import de.dfki.mycbr.core.similarity.AmalgamationFct;
import de.dfki.mycbr.core.similarity.Similarity;
import de.dfki.mycbr.util.Pair;
import exceptions.CaseBaseException;
import exceptions.ValidationException;
import similarity.computation.Computation;
import similarity.computation.ComputationDiscretize;
import similarity.computation.ComputationKM;
import similarity.computation.config.KaplanMeierTargetConfig;
import similarity.computation.config.KaplanMeierTargetConfig.SURVIVALTARGET;

/**
 * Utilities for performing a validation.
 * 
 * @author Christian Karmen
 *
 */
public class ValidationBackend {

	final static String itsCsvDelimitor = ";";
	final static String itsNewLineChar = System.getProperty("line.separator");
	
	private DefaultCaseBase itsValidationCaseBase;
	private String itsTherapyArmAttributeLabel;
	private KaplanMeierTargetConfig itsSurvivalTargetConfig;
	
	/**
	 * Utilities for performing a validation.
	 * 
	 * @param theValidationCaseBase Case base to validate
	 * @param theTherapyArmAttributeLabel Label of the therapy arm attribute in the case base (e.g. "Therapy Arm")
	 * @param theSurvivalTargetConfig Survival Target Configuration for case base
	 */
	public ValidationBackend(
			DefaultCaseBase theValidationCaseBase, 
			String theTherapyArmAttributeLabel,
			KaplanMeierTargetConfig theSurvivalTargetConfig){
		itsValidationCaseBase = theValidationCaseBase;
		itsTherapyArmAttributeLabel = theTherapyArmAttributeLabel;
		itsSurvivalTargetConfig = theSurvivalTargetConfig;
	}
	  
	  
//	  /**
//	   * Creates the CSV file for similarity results and writes also the header line.
//	   * 
//	   * @param theValidationLabel Custom Validation label
//	   * @param theSolutionDescriptionList List of attribute labels for solution description
//	   * @param anOutputClassAllowedValuesList List of attribute values of the output class (e.g. therapy arm)
//	   * 
//	   * @return file for similarity results
//	   * 
//	   * @throws IOException Error while creating file.
//	   */
//	  private static File createSimilarityResultsHeader(
//			  String theValidationLabel,
//			  ArrayList<String> theSolutionDescriptionList, 
//			  ArrayList<String> anOutputClassAllowedValuesList) throws IOException{
//		  
//		  BufferedWriter aResultsWriter = null;
//		  File aResultsFile = new File(
//				  MyCBRBuilderFxApplication.getCustomDataPath(CUSTOM_DATAPATH.TEMP) 
//				  + theValidationLabel  
//				  + "_similarity_results.csv");
//		  System.out.println("Output file for similarity results: " + aResultsFile.getAbsolutePath());
//		  
//		  // writing csv: header
//		  String aCsvHeader = "TestCaseID" + itsCsvDelimitor;
//		  for (String aSolutionAttribute : theSolutionDescriptionList) {
//			  aCsvHeader += aSolutionAttribute + "(TestCase)" + itsCsvDelimitor;
//		  }
//		  aCsvHeader += "TrainingCaseID" + itsCsvDelimitor;
//		  for (String aSolutionAttribute : theSolutionDescriptionList) {
//			  aCsvHeader += aSolutionAttribute + "(TrainingCase)" + itsCsvDelimitor;
//		  }
//		  aCsvHeader += "Similarity" + itsCsvDelimitor;
//		  
////		  for (String anAllowedValue : anOutputClassAllowedValuesList) {
////			  aCsvHeader += anAllowedValue + " rate" + itsCsvDelimitor;
////			  aCsvHeader += anAllowedValue + " avg Survival" + itsCsvDelimitor;
////			  aCsvHeader += anAllowedValue + " median Survival" + itsCsvDelimitor;
////			  aCsvHeader += anAllowedValue + " lts rate" + itsCsvDelimitor;
////			  aCsvHeader += anAllowedValue + " sts rate" + itsCsvDelimitor;
////			  aCsvHeader += anAllowedValue + " lts avg similarity" + itsCsvDelimitor;
////			  aCsvHeader += anAllowedValue + " sts avg similarity" + itsCsvDelimitor;
////		  }
////		  aCsvHeader += "rate" + itsCsvDelimitor;
////		  aCsvHeader += "avg Survival" + itsCsvDelimitor;
////		  aCsvHeader += "median Survival" + itsCsvDelimitor;
////		  aCsvHeader += "lts rate" + itsCsvDelimitor;
////		  aCsvHeader += "sts rate" + itsCsvDelimitor;
////		  aCsvHeader += "lts avg similarity" + itsCsvDelimitor;
////		  aCsvHeader += "sts avg similarity" + itsCsvDelimitor;
//		  
//		  aCsvHeader += itsNewLineChar;
//		  
//    		// writing header
////    	System.out.print("Writing CSV header: " + aCsvHeader);
//		  	aResultsWriter = new BufferedWriter(new FileWriter(aResultsFile));
//			aResultsWriter.write(aCsvHeader);
//			
//			aResultsWriter.close();
//		  
//		  return aResultsFile;
//	  }
	  
	  
	  
	  /**
	   * Runs a leave-one-out validation. The k most/least similar results are kept.
	   * 
	   * @param theTherapyArmAttributeLabel Label of attribute for the therapy arm 
	   * @param theKvalue Number of similarity results
	   * @param theReverseResultsFlag true=sorts result by least similar patients; false=sorts result by most similar patients
	   * 
	   * @return List of similarity set results of each including its outcome information
	   * @throws Exception General error occurred
	   * 
	   */
	  public ArrayList<CohortAnalysis> performLeaveOneOutValidation(
			  boolean theRandomFlag,
			  String theTherapyArmAttributeLabel,
			  int theKvalue,
			  boolean theReverseResultsFlag) 
					  throws Exception {
		  
		  Utils.printTitle("PERFORMING LEAVE-ONE-OUT VALIDATION");
		  
		  DefaultCaseBase aTrainingCB;	  
		  CohortAnalysis aCohortAnalysis;
		  ArrayList<CohortAnalysis> aCohortAnalysisList = new ArrayList<>();		
		  
		  HashMap<String, Boolean> aBiomarkerMap = buildBiomarkerMap(itsValidationCaseBase, itsSurvivalTargetConfig);
  		     
		  // LEAVE ONE OUT: Process each case of validation case base and test it against all others 
		  for (Instance aTestCase : itsValidationCaseBase.getCases()) {	
			  // creating a case base clone, so we can remove the testing case
			  aTrainingCB = CaseBaseUtils.cloneCaseBase(itsValidationCaseBase);
			  if (!aTrainingCB.removeCase(aTestCase)){
				  throw new CaseBaseException("Error while removing test case from training case base: " + aTestCase.getName());
			  }
	  		  
	  		  aCohortAnalysis = getRetrievalResults(
	  				  aTestCase, 
	  				  aTrainingCB, 
	  				  theKvalue, 
	  				  theReverseResultsFlag,
	  				  aBiomarkerMap,
	  				  theRandomFlag);

	  		  aCohortAnalysisList.add(aCohortAnalysis);
	  		} 
				  
		  
//		  System.out.println("Validation results - done.");
		
		  return aCohortAnalysisList;
	  }
	  
	  
	  /**
	   * Runs a leave-one-out validation for each outcome variable value (e.g. outcome "therapy arm": arm A, arm B). 
	   * For each outcome value (e.g. arm A) the k most (un)similar results are kept.
	   * 
	   * @param theTherapyArmAttributeLabel Label of attribute for the therapy arm 
	   * @param theKvalue Number of similar cases
	   * @param theReverseResultsFlag true=sorts result by least similar patients; false=sorts result by most similar patients
	   * @param theSameArmOnlyFlag true=test case must be in same arm as training case; false=test case may have any arm
	   * 
	   * @return List of similarity results including its outcome information
	   * @throws Exception General error occurred
	   * 
	   */
	  @Deprecated
	  public ArrayList<CohortAnalysis> performLeaveOneOutValidation( 
			  boolean theRandomFlag,
			  String theTherapyArmAttributeLabel,
			  int theKvalue,
			  boolean theReverseResultsFlag,
			  boolean theSameArmOnlyFlag) 
					  throws Exception {
		  
		  Utils.printTitle("PERFORMING LEAVE-ONE-OUT VALIDATION");
	
		  DefaultCaseBase aTrainingCB;	  
		  CohortAnalysis aCohortAnalysis = null;
		  ArrayList<CohortAnalysis> aCohortAnalysisList = new ArrayList<>();
		
	
		  // Getting list of allowed therapy arms
		  ArrayList<String> aTherapyArmValueList = new ArrayList<>(	
				  ((SymbolDesc)CaseBaseUtils.getCaseBaseAttributeDescByLabel(itsValidationCaseBase, theTherapyArmAttributeLabel)).getAllowedValues());
  		     
		  // LEAVE ONE OUT: Process each case of validation case base and test it against all others 
		  for (Instance aTestCase : itsValidationCaseBase.getCases()) {	  
			  
			  // creating a case base clone, so we can remove the testing case
			  aTrainingCB = CaseBaseUtils.cloneCaseBase(itsValidationCaseBase);
			  if (!aTrainingCB.removeCase(aTestCase)){
				  throw new CaseBaseException("Error while removing test case from training case base: " + aTestCase.getName());
			  }
			  
			  // Filter CaseBase with for a specific therapy (e.g. arm A, etc)		
			  for (String aTherapyArmValue : aTherapyArmValueList) {			  
				  
				  // check if test case has same arm, and skip if required
				  if (theSameArmOnlyFlag){
					  String aTestCaseArmValue = CaseBaseUtils.getCaseAttributeValue(aTestCase, theTherapyArmAttributeLabel).getValueAsString();
					  if (!aTestCaseArmValue.equals(aTherapyArmValue)){
						  continue;
					  }
				  }
				  
				  ArrayList<String> aTherapyArmFilter = new ArrayList<>(); 
	        	  aTherapyArmFilter.add(aTherapyArmValue);
		  		  DefaultCaseBase aFilteredCB = CaseBaseUtils.filterCaseBase(aTrainingCB, theTherapyArmAttributeLabel, aTherapyArmFilter, true);
		  		  
		  		  /////////////////////////////////////////////////////////////////////////////////
		  		  // RETRIEVE
		  		  /////////////////////////////////////////////////////////////////////////////////
		  		  aFilteredCB.addCase(aTestCase);;
		  		  HashMap<String, Boolean> aBiomarkerMap = buildBiomarkerMap(aFilteredCB, itsSurvivalTargetConfig);
		  		  aFilteredCB.removeCase(aTestCase);
		  		  
		  		  aCohortAnalysis = getRetrievalResults(
		  				  aTestCase, 
		  				  aFilteredCB, 
		  				  theKvalue, 
		  				  theReverseResultsFlag,
		  				  aBiomarkerMap,
		  				  theRandomFlag);
	  		  

			  	} // for (String aTherapyArmValue : aTherapyArmValueList) 
			  aCohortAnalysisList.add(aCohortAnalysis);
	  		}	// for (Instance aTestCase : theValidationCaseBase.getCases()) 
				  
		System.out.println("Validation results - done.");
		
		return aCohortAnalysisList;
	  }
	  
	  
	  

	  
	  
	  
	  private CohortAnalysis getRetrievalResults(
			  Instance theTestCase, 
			  DefaultCaseBase theCaseBase, 
			  int theKvalue, 
			  boolean theReverseResultsFlag, 
			  HashMap<String, Boolean> theBiomarkerMap,
			  boolean theRandomFlag) 
					  throws ParseException, CaseBaseException, ValidationException{
		  
		  HashMap<String, String> aQueryFilter = new HashMap<String, String>();
		  Instance aTrainingCase;
		  Similarity aSimilarity;
		  Concept aConcept = theTestCase.getConcept();
		  
		  // Collecting Test Case Informations
		  boolean aNumBioArmA = theBiomarkerMap.get(theTestCase.getName()+"_"+getGeneratedBiomarkerAttributeLabel(true, true)); 
		  boolean aNumBioArmB = theBiomarkerMap.get(theTestCase.getName()+"_"+getGeneratedBiomarkerAttributeLabel(true, false));
		  boolean aNomBioArmA = theBiomarkerMap.get(theTestCase.getName()+"_"+getGeneratedBiomarkerAttributeLabel(false, true));
		  boolean aNomBioArmB = theBiomarkerMap.get(theTestCase.getName()+"_"+getGeneratedBiomarkerAttributeLabel(false, false));
		  CaseDescription aTestCaseDescription = new CaseDescription(theTestCase.getName(), aNumBioArmA, aNumBioArmB, aNomBioArmA, aNomBioArmB);
		  
		  String aTestTherapyArm = CaseBaseUtils.getCaseAttributeValue(theTestCase, itsTherapyArmAttributeLabel).getValueAsString();
		  SurvivalOutcome aTestSurvivalOutcome = getSurvivalOutcome(theTestCase, itsSurvivalTargetConfig, true);
		  CaseOutcomeDescription aTestCaseOutcomeDescription = new CaseOutcomeDescription(theTestCase.getName(), aTestTherapyArm, aTestSurvivalOutcome);
		  
		  
		  CohortAnalysis aCohortAnalysis = new CohortAnalysis(theTestCase.getName(), aTestCaseDescription, aTestCaseOutcomeDescription);
		  
		  
		  if (theRandomFlag){
			  Instance aRandomTrainingCase;
			  Similarity aRandomSimilarity;
			  for (int i = 0; i < theKvalue; i++) {
				  aRandomTrainingCase = CaseBaseUtils.getRandomCase(theCaseBase);
				  aRandomSimilarity = Similarity.get(Computation.getRandomNumber(1));
				  
					// Collecting Training Case Informations
					String aTrainingTherapyArm = CaseBaseUtils.getCaseAttributeValue(aRandomTrainingCase, itsTherapyArmAttributeLabel).getValueAsString();
					SurvivalOutcome aTrainingSurvivalOutcome = getSurvivalOutcome(aRandomTrainingCase, itsSurvivalTargetConfig, true);

					aNumBioArmA = theBiomarkerMap.get(aRandomTrainingCase.getName()+"_"+getGeneratedBiomarkerAttributeLabel(true, true)); 
					aNumBioArmB = theBiomarkerMap.get(aRandomTrainingCase.getName()+"_"+getGeneratedBiomarkerAttributeLabel(true, false));
					aNomBioArmA = theBiomarkerMap.get(aRandomTrainingCase.getName()+"_"+getGeneratedBiomarkerAttributeLabel(false, true));
					aNomBioArmB = theBiomarkerMap.get(aRandomTrainingCase.getName()+"_"+getGeneratedBiomarkerAttributeLabel(false, false));
					
					CaseDescription aTrainingCaseDescription = new CaseDescription(aRandomTrainingCase.getName(), aNumBioArmA, aNumBioArmB, aNomBioArmA, aNomBioArmB);
					CaseOutcomeDescription aTrainingCaseOutcomeDescription = new CaseOutcomeDescription(aRandomTrainingCase.getName(), aTrainingTherapyArm, aTrainingSurvivalOutcome); 
					
					
					// Add each retrieval result to sub-cohort of most/least similar cases
					aCohortAnalysis.addSimilarCaseOutcome(
//							aTestCaseDescription,
//							aTestCaseOutcomeDescription,
							aTrainingCaseDescription,
							aTrainingCaseOutcomeDescription,
							aRandomSimilarity.getValue());
			  }
			  
			  return aCohortAnalysis;
		  }
		  
		  
//		  // detect biomarkers per arm
//		  // test cast must be added temporary in order to detect its biomarker settings
//		  aFilteredCB.addCase(aTestCase);
//		  HashMap<String, Boolean> aBiomarkerMap = buildBiomarkerMap(aFilteredCB, aKMTargetConfig);
//		  aFilteredCB.removeCase(aTestCase);

		  // Set up retrieval 
		  Retrieval aRetrieval = new Retrieval(aConcept, theCaseBase);
		  aRetrieval.setRetrievalMethod(RetrievalMethod.RETRIEVE_SORTED);			  
		  // Setting retrieval query with all attributes of test case
		  aQueryFilter.clear();
		  String aQueryAttributeName, aQueryAttributeValue;
		  for (Entry<AttributeDesc, Attribute> anTestAttributeValue : theTestCase.getAttributes().entrySet()) {
			  aQueryAttributeName = anTestAttributeValue.getKey().getName();
			  aQueryAttributeValue = anTestAttributeValue.getValue().getValueAsString();
			  
			  if (GeneratedDataset.getCaseDescriptionList().contains(aQueryAttributeName)){
				  aQueryFilter.put(aQueryAttributeName, aQueryAttributeValue);
			  }
		  }
		  
		  // if needed, align query attribute values to its allowed range
		  aQueryFilter = MyCbrUtils.getRestrictedValuesList(aConcept, aQueryFilter);
		  
		  // Perform retrieval
		  MyCbrUtils.performRetrieval(
				  aConcept,
				  aRetrieval, 
				  aQueryFilter);
		  // System.out.println("Number of retrieval results: " + aRetrieval.getResult().size());
	
			// Collect retrieval results and, if needed, reverse sorting 
			List<Pair<Instance, Similarity>> aRetrievalResult = aRetrieval.getResult();
			if (theReverseResultsFlag){
				Collections.reverse(aRetrievalResult);
			}
			
			
			
			// process the first k retrieval results
			int aResultCounter = 1;
			
			for (Pair<Instance, Similarity> aResult : aRetrievalResult) {
				if (aResultCounter++ > theKvalue){
					break;
				}
				aTrainingCase = aResult.getFirst();
				aSimilarity = aResult.getSecond();
				
				
				  
				
				  
				// Collecting Training Case Informations
				String aTrainingTherapyArm = CaseBaseUtils.getCaseAttributeValue(aTrainingCase, itsTherapyArmAttributeLabel).getValueAsString();
				SurvivalOutcome aTrainingSurvivalOutcome = getSurvivalOutcome(aTrainingCase, itsSurvivalTargetConfig, true);

				aNumBioArmA = theBiomarkerMap.get(aTrainingCase.getName()+"_"+getGeneratedBiomarkerAttributeLabel(true, true)); 
				aNumBioArmB = theBiomarkerMap.get(aTrainingCase.getName()+"_"+getGeneratedBiomarkerAttributeLabel(true, false));
				aNomBioArmA = theBiomarkerMap.get(aTrainingCase.getName()+"_"+getGeneratedBiomarkerAttributeLabel(false, true));
				aNomBioArmB = theBiomarkerMap.get(aTrainingCase.getName()+"_"+getGeneratedBiomarkerAttributeLabel(false, false));
				
				CaseDescription aTrainingCaseDescription = new CaseDescription(aTrainingCase.getName(), aNumBioArmA, aNumBioArmB, aNomBioArmA, aNomBioArmB);
				CaseOutcomeDescription aTrainingCaseOutcomeDescription = new CaseOutcomeDescription(aTrainingCase.getName(), aTrainingTherapyArm, aTrainingSurvivalOutcome); 
				
				
				// Add each retrieval result to sub-cohort of most/least similar cases
				aCohortAnalysis.addSimilarCaseOutcome(
//						aTestCaseDescription,
//						aTestCaseOutcomeDescription,
						aTrainingCaseDescription,
						aTrainingCaseOutcomeDescription,
						aSimilarity.getValue());
				
			} 

			return aCohortAnalysis;
	  }
	  
	  
	  
	  /**
	   * Creates a CSV file containing all similarity results: <validation_label>_similarity results.csv;
	   * 
	   * @param theResultsFile File for the csv output
	   * @param theCohortAnalysisList Similarity results
	   * @throws IOException Error while writing csv file
	   */
	  public static void writeSimilarityResults(			  
			  File theResultsFile,
			  ArrayList<CohortAnalysis> theCohortAnalysisList 
			  ) throws IOException{
		  
			  System.out.println("DEBUG: Output file for similarity results: " + theResultsFile.getAbsolutePath());
			  BufferedWriter aResultsWriter = new BufferedWriter(new FileWriter(theResultsFile));
			  
			  // writing header row
			  String aCsvHeader = "TestCaseID" + itsCsvDelimitor;
			  aCsvHeader += "NomBiomarkerA (TestCase)" + itsCsvDelimitor;
			  aCsvHeader += "NumBiomarkerA (TestCase)" + itsCsvDelimitor;
			  aCsvHeader += "NomBiomarkerB (TestCase)" + itsCsvDelimitor;
			  aCsvHeader += "NumBiomarkerB (TestCase)" + itsCsvDelimitor;
			  aCsvHeader += "RandomArm (TestCase)" + itsCsvDelimitor;
			  aCsvHeader += "SurvivalTime (TestCase)" + itsCsvDelimitor;
			  aCsvHeader += "SurvivalStatus (TestCase)" + itsCsvDelimitor;
			  aCsvHeader += "SurvivalType (TestCase)" + itsCsvDelimitor;

			  aCsvHeader += "TrainingCaseID" + itsCsvDelimitor;
			  aCsvHeader += "NomBiomarkerA (TrainingCase)" + itsCsvDelimitor;
			  aCsvHeader += "NumBiomarkerA (TrainingCase)" + itsCsvDelimitor;
			  aCsvHeader += "NomBiomarkerB (TrainingCase)" + itsCsvDelimitor;
			  aCsvHeader += "NumBiomarkerB (TrainingCase)" + itsCsvDelimitor;
			  aCsvHeader += "RandomArm (TrainingCase)" + itsCsvDelimitor;
			  aCsvHeader += "SurvivalTime (TrainingCase)" + itsCsvDelimitor;
			  aCsvHeader += "SurvivalStatus (TrainingCase)" + itsCsvDelimitor;
			  aCsvHeader += "SurvivalType (TrainingCase)" + itsCsvDelimitor;

			  aCsvHeader += "Similarity" + itsCsvDelimitor;
			  
			  aCsvHeader += itsNewLineChar;			  	
			  aResultsWriter.write(aCsvHeader);
			  
		    
			  for (CohortAnalysis anOutcomeAnalysis : theCohortAnalysisList) {
				  for (SimilarCaseComparison aSimilarCaseComparison : anOutcomeAnalysis.getSimilarCaseOutcomeList()) {
					  // Writing Test Case Informations
					  aResultsWriter.write(aSimilarCaseComparison.TestCaseDescription.CaseID + itsCsvDelimitor);	// Case ID
					  aResultsWriter.write(aSimilarCaseComparison.TestCaseDescription.NomBiomarkerA + itsCsvDelimitor);
					  aResultsWriter.write(aSimilarCaseComparison.TestCaseDescription.NumBiomarkerA + itsCsvDelimitor);
					  aResultsWriter.write(aSimilarCaseComparison.TestCaseDescription.NomBiomarkerB + itsCsvDelimitor);
					  aResultsWriter.write(aSimilarCaseComparison.TestCaseDescription.NumBiomarkerB + itsCsvDelimitor);
					  aResultsWriter.write(aSimilarCaseComparison.TestCaseOutcomeDescription.TherapyArmOutcome + itsCsvDelimitor);	// Therapy Arm
					  aResultsWriter.write(aSimilarCaseComparison.TestCaseOutcomeDescription.SurvivalOutcome.getSurvivalTime() + itsCsvDelimitor);	// Survival Time
					  aResultsWriter.write(aSimilarCaseComparison.TestCaseOutcomeDescription.SurvivalOutcome.getSurvivalStatus() + itsCsvDelimitor);	// Survival Status
					  aResultsWriter.write(aSimilarCaseComparison.TestCaseOutcomeDescription.SurvivalOutcome.getSurvivalType() + itsCsvDelimitor);	// Survival Type
					
					  // Writing Training Case Informations
					  aResultsWriter.write(aSimilarCaseComparison.TrainingCaseDescription.CaseID + itsCsvDelimitor);	// Case ID
					  aResultsWriter.write(aSimilarCaseComparison.TrainingCaseDescription.NomBiomarkerA + itsCsvDelimitor);
					  aResultsWriter.write(aSimilarCaseComparison.TrainingCaseDescription.NumBiomarkerA + itsCsvDelimitor);
					  aResultsWriter.write(aSimilarCaseComparison.TrainingCaseDescription.NomBiomarkerB + itsCsvDelimitor);
					  aResultsWriter.write(aSimilarCaseComparison.TrainingCaseDescription.NumBiomarkerB + itsCsvDelimitor);
					  aResultsWriter.write(aSimilarCaseComparison.TrainingCaseOutcomeDescription.TherapyArmOutcome + itsCsvDelimitor);	// Therapy Arm
					  aResultsWriter.write(aSimilarCaseComparison.TrainingCaseOutcomeDescription.SurvivalOutcome.getSurvivalTime() + itsCsvDelimitor);	// Survival Time
					  aResultsWriter.write(aSimilarCaseComparison.TrainingCaseOutcomeDescription.SurvivalOutcome.getSurvivalStatus() + itsCsvDelimitor);	// Survival Status
					  aResultsWriter.write(aSimilarCaseComparison.TrainingCaseOutcomeDescription.SurvivalOutcome.getSurvivalType() + itsCsvDelimitor);	// Survival Type
					
					  // Write Similarity Information
					  aResultsWriter.write(Double.toString(aSimilarCaseComparison.Similarity));

					  aResultsWriter.write(itsNewLineChar);
				  }
			  }
		    
			  aResultsWriter.close();	
	  }
	  

	  
	  /**
	   * Reads the Survival Outcome Parameters from a case base instance.
	   * 
	   * @param theCaseInstance Case base instance
	   * @param theKMTargetConfig KM Target config
	   * @param theSurvivalTypeFlag true=add survival type (only available in generated dataset); false=omit survival type
	   * @return Survival Outcome Parameters from a case base instance
	   * 
	   * @throws CaseBaseException Error while access case base
	   * @throws NumberFormatException Error while parsing the Survival Time
	   */
	  public static SurvivalOutcome getSurvivalOutcome(
			  Instance theCaseInstance, 
			  KaplanMeierTargetConfig theKMTargetConfig,
			  boolean theSurvivalTypeFlag) throws CaseBaseException, NumberFormatException{
		  
		  	String anAttributeValue;	

			// Getting Survival Time (e.g. in months)
		  	double aSurvivalTimeDouble;
		  	anAttributeValue = CaseBaseUtils.getCaseAttributeValue(theCaseInstance, theKMTargetConfig.TargetSurvivalColumn).getValueAsString();
			try {
				aSurvivalTimeDouble = Double.parseDouble(anAttributeValue);
			} catch (NumberFormatException e) {
				throw new NumberFormatException(""
						+ "NumberFormatException occurred (" + theCaseInstance.getName() + "): " + 
						theKMTargetConfig.TargetSurvivalColumn + "=" + anAttributeValue);
			}
			
			// Getting Survival Status (e.g. dead/alive)
			SurvivalStatus aTrainingSurvivalStatus;
			anAttributeValue = CaseBaseUtils.getCaseAttributeValue(theCaseInstance, theKMTargetConfig.TargetStatusColumn).getValueAsString();
			if (anAttributeValue.equalsIgnoreCase(theKMTargetConfig.TargetStatusIndicator)){
				aTrainingSurvivalStatus = SurvivalStatus.DECEASED;
			} else if (anAttributeValue.trim().isEmpty()){
				aTrainingSurvivalStatus = SurvivalStatus.UNKNOWN;
			} else {
				aTrainingSurvivalStatus = SurvivalStatus.ALIVE;
			}

			// Getting Survival Type (e.g. LTS/STS): only available in generated datasets
			SurvivalType aTrainingSurvivalType;
			if (!theSurvivalTypeFlag){
				aTrainingSurvivalType = SurvivalType.UNKNOWN;
			} else {
				anAttributeValue = CaseBaseUtils.getCaseAttributeValue(theCaseInstance, "SurvivalType").getValueAsString();
				switch (anAttributeValue) {
				case "STS":
					aTrainingSurvivalType = SurvivalType.STS;
					break;
				case "LTS":
					aTrainingSurvivalType = SurvivalType.LTS;
					break;
				default:
					aTrainingSurvivalType = SurvivalType.UNKNOWN;
					break;
			}
		}
			return new SurvivalOutcome(aSurvivalTimeDouble, aTrainingSurvivalStatus, aTrainingSurvivalType);
	  }
	  
	  
	  
	  /**
	   * Performs a validation overview and writes results into a csv file. 
	   * 
	   * @param theOutputFile File to write output
	   * @param theCohortAnalysisList Outcome Analysis of all cohorts (k similar cases)
	   * @param theTherapyArmValuesList List of values of the therapy arm
	   * 
	   * @throws IOException Error while writing to output file 
	   */
	  public static void writeSubcohortOverview(
			  File theOutputFile, 
			  ArrayList<CohortAnalysis> theCohortAnalysisList, 
			  ArrayList<String> theTherapyArmValuesList) 
					  throws IOException{
		  
		  System.out.println("Cohort overview - creating file " + theOutputFile.getAbsolutePath());
		  BufferedWriter anOverviewWriter = new BufferedWriter(new FileWriter(theOutputFile));
		  
		  CohortAnalysis anOverallAnalysis = new CohortAnalysis(null, null, null);
		  for (CohortAnalysis aSubcohortAnalysis : theCohortAnalysisList) {
				anOverallAnalysis.addSimilarCaseOutcomeList(aSubcohortAnalysis.getSimilarCaseOutcomeList());
		  }
				  
		  // write header row
		  anOverviewWriter.write(
				  "Therapy Arm" + itsCsvDelimitor +
				  "Analysis Type" + itsCsvDelimitor +
				  "Analysis Result" + itsCsvDelimitor +
				  itsNewLineChar);
						
		  for (String aTherapyArmValue : theTherapyArmValuesList) {
			  anOverviewWriter.write(
					  aTherapyArmValue + itsCsvDelimitor +
					  "Number of Cases" + itsCsvDelimitor +
					  anOverallAnalysis.getNumberOfCasesWithArmLabel(aTherapyArmValue) + itsNewLineChar);
			  
			  anOverviewWriter.write(
					  aTherapyArmValue + itsCsvDelimitor +
					  "Therapy Rate" + itsCsvDelimitor +
					  anOverallAnalysis.getArmRate(aTherapyArmValue) + itsNewLineChar);
			  
			  anOverviewWriter.write(
					  aTherapyArmValue + itsCsvDelimitor +
					  "Survivor Rate" + itsCsvDelimitor +
					  anOverallAnalysis.getSurvivalStatusRate(aTherapyArmValue, SurvivalStatus.ALIVE) + itsNewLineChar);
			  
			  anOverviewWriter.write(
					  aTherapyArmValue + itsCsvDelimitor +
					  "LTS Rate" + itsCsvDelimitor +
					  anOverallAnalysis.getSurvivalTypeRate(aTherapyArmValue, SurvivalType.LTS) + itsNewLineChar);
			  
			  anOverviewWriter.write(
					  aTherapyArmValue + itsCsvDelimitor +
					  "STS Rate" + itsCsvDelimitor +
					  anOverallAnalysis.getSurvivalTypeRate(aTherapyArmValue, SurvivalType.STS) + itsNewLineChar);
			  
			  anOverviewWriter.write(
					  aTherapyArmValue + itsCsvDelimitor +
					  "Average Similarity (LTS)" + itsCsvDelimitor +
					  anOverallAnalysis.getAverageSimilarity(aTherapyArmValue, SurvivalType.LTS) + itsNewLineChar);
			  
			  anOverviewWriter.write(
					  aTherapyArmValue + itsCsvDelimitor +
					  "Average Similarity (STS)" + itsCsvDelimitor +
					  anOverallAnalysis.getAverageSimilarity(aTherapyArmValue, SurvivalType.STS) + itsNewLineChar);
			  
//					  anOverviewWriter.write(
//							  anAllowedOutputValue + itsCsvDelimitor +
//							  "Confidence Interval 90% (SurvivalTime)" + itsCsvDelimitor +
//							  theOverallAnalysis.getConfidenceIntervalSurvivalTime(anAllowedOutputValue, 0.90) + itsNewLineChar); 
//					  
//					  anOverviewWriter.write(
//							  anAllowedOutputValue + itsCsvDelimitor +
//							  "Confidence Interval 95% (SurvivalTime)" + itsCsvDelimitor +
//							  theOverallAnalysis.getConfidenceIntervalSurvivalTime(anAllowedOutputValue, 0.95) + itsNewLineChar);
//					  
//					  anOverviewWriter.write(
//							  anAllowedOutputValue + itsCsvDelimitor +
//							  "Confidence Interval 99% (SurvivalTime)" + itsCsvDelimitor +
//							  theOverallAnalysis.getConfidenceIntervalSurvivalTime(anAllowedOutputValue, 0.99) + itsNewLineChar);
			  
			  anOverviewWriter.write(
					  aTherapyArmValue + itsCsvDelimitor +
					  "Survival Time - Mean" + itsCsvDelimitor +
					  anOverallAnalysis.getAverageSurvivalTime(aTherapyArmValue) + itsNewLineChar);
			  
			  anOverviewWriter.write(
					  aTherapyArmValue + itsCsvDelimitor +
					  "Survival Time - Median" + itsCsvDelimitor +
					  anOverallAnalysis.getMedianSurvivalTime(aTherapyArmValue) + itsNewLineChar);
			  
			  anOverviewWriter.write(
					  aTherapyArmValue + itsCsvDelimitor +
					  "SurvivalTime - Variance" + itsCsvDelimitor +
					  anOverallAnalysis.getVarianceSurvivalTime(aTherapyArmValue) + itsNewLineChar);
			  
			  anOverviewWriter.write(
					  aTherapyArmValue + itsCsvDelimitor +
					  "SurvivalTime - Standard Deviation" + itsCsvDelimitor +
					  anOverallAnalysis.getStandardDeviationSurvivalTime(aTherapyArmValue) + itsNewLineChar);
		  }
				 
		  anOverviewWriter.close();
		  System.out.println("Subcohort overview - done.");
	}
	  
	  
	  /**
	   * Creates the CSV file for the weights overview and writes also the header line. 
	   * CAUTION: If file already exists, it will not be touched. 
	   * 
	   * @param theOutputFile File to write output 
	   * @param theAmalgamFct AmalgamFct containing the attributes for the global similarity
	   * 
	   * @throws IOException Error occurred while creating the CSV file
	   */
	  public static void createWeightsOverviewHeader(File theOutputFile, AmalgamationFct theAmalgamFct) throws IOException{
		  if (theOutputFile.exists()){
			  return;
		  }
		  
		  // writing validation overview
		  BufferedWriter anOverviewWriter = new BufferedWriter(new FileWriter(theOutputFile));				  
		  anOverviewWriter.write("RowID" + itsCsvDelimitor);		  
		  
		  for (Entry<String, AttributeDesc> anEntry : theAmalgamFct.getConcept().getAllAttributeDescs().entrySet()) {
			  if (theAmalgamFct.isActive(anEntry.getValue())){
				  anOverviewWriter.write(anEntry.getKey() + itsCsvDelimitor);
			  }
		  }

		  anOverviewWriter.write(itsNewLineChar);
		  anOverviewWriter.close();
	  }
	  
	  
	  /**
	   * Adds a line in the weight's overview file with the calculated weights a specific outcome analysis.
	   * 
	   * @param theHeaderFile File for the weight's overview
	   * @param theValidationLabel Custom label for validation
	   * @param theAmalgamFct AmalgamFct containing the attribute's weights for the global similarity.
	   */
	  public static void addWeightsOverview(File theHeaderFile, String theValidationLabel, AmalgamationFct theAmalgamFct){
		  // File for weights overview
		  BufferedWriter aWriter = null;
		  File aFile = theHeaderFile;
		  double aWeight;
		  
		  AttributeDesc anAttributeDesc;
		  
		  	// writing validation overview
			try {
				  aWriter = new BufferedWriter(new FileWriter(aFile, true));
				  aWriter.write(theValidationLabel + itsCsvDelimitor);	
				  
				  for (Entry<String, AttributeDesc> anEntry : theAmalgamFct.getConcept().getAllAttributeDescs().entrySet()) {
					  anAttributeDesc = anEntry.getValue();
					  if (theAmalgamFct.isActive(anAttributeDesc)){					  
						  aWeight = (theAmalgamFct.getWeight(anAttributeDesc)).doubleValue();
						  aWriter.write(Double.toString(aWeight) + itsCsvDelimitor);
					  }
				  }
				  aWriter.write(itsNewLineChar);			  
			} catch (Exception e) {
				MyCBRBuilderFxElements.showExceptionDialog(e);
			} finally {
				// in any case: try closing the writer when work is done
				 try {					 
					 aWriter.close();
		            } catch (Exception e) {
						MyCBRBuilderFxElements.showExceptionDialog(e);
		            }
			}
	  }
	  
	  
	  
	  
	  /**
	   * Creates the CSV file for the sub-cohort analysis and writes also the header line.
	   * 
	   * @param theOutputFile File to write the CSV header to
	   * @param theTherapyArmValuesList List of values of the therapy arm
	   * 
	   * @throws IOException Error occurred while creating the CSV file
	   */
	  private static void createSubCohortAnalysisHeader(File theOutputFile, ArrayList<String> theTherapyArmValuesList) throws IOException{
		  String aCsvHeader;
		  System.out.println("Output file for subcohort analysis: " + theOutputFile.getAbsolutePath());
		  
		  aCsvHeader = "TestCaseID" + itsCsvDelimitor;
		  aCsvHeader += "Therapy Arm (TestCase)" + itsCsvDelimitor;
		  aCsvHeader += "Survival Status (TestCase)" + itsCsvDelimitor;
		  aCsvHeader += "Survival Time (TestCase)" + itsCsvDelimitor;
		  aCsvHeader += "Survival Type (TestCase)" + itsCsvDelimitor;
		  aCsvHeader += "NomBiomarkerA (TestCase)" + itsCsvDelimitor;
		  aCsvHeader += "NumBiomarkerA (TestCase)" + itsCsvDelimitor;
		  aCsvHeader += "NomBiomarkerB (TestCase)" + itsCsvDelimitor;
		  aCsvHeader += "NumBiomarkerB (TestCase)" + itsCsvDelimitor;
		  aCsvHeader += "CohortSize" + itsCsvDelimitor;
				
		  for (String aTherapyArm : theTherapyArmValuesList) {
			  aCsvHeader += aTherapyArm + " rate" + itsCsvDelimitor;
			  aCsvHeader += aTherapyArm + " survivor rate" + itsCsvDelimitor;
			  aCsvHeader += aTherapyArm + " avg Survival" + itsCsvDelimitor;
			  aCsvHeader += aTherapyArm + " median Survival" + itsCsvDelimitor;
			  aCsvHeader += aTherapyArm + " lts rate" + itsCsvDelimitor;
			  aCsvHeader += aTherapyArm + " sts rate" + itsCsvDelimitor;
			  aCsvHeader += aTherapyArm + " lts avg similarity" + itsCsvDelimitor;
			  aCsvHeader += aTherapyArm + " sts avg similarity" + itsCsvDelimitor;
		  }
			  
		  // writing csv header
		  BufferedWriter aWriter = new BufferedWriter(new FileWriter(theOutputFile));
		  aWriter.write(aCsvHeader + itsNewLineChar);
		  aWriter.close();
	  }
	  
	  
	  /**
	   * Adds a line with a sub-cohort analysis of the given outcome analysis in the sub-cohort analysis file.
	   * 
	   * @param theHeaderFile File to add the Sub-cohort analysis
	   * @param theCohortAnalysis Outcome Analysis of the sub-cohort analysis
	   * @param theTherapyArmValuesList List of values for the therapy arm
	   * @throws IOException Error occurred while adding data to the file
	   */
	  private static void addSubCohortAnalysis(
			  File theHeaderFile,
			  CohortAnalysis theCohortAnalysis,
			  ArrayList<String> theTherapyArmValuesList) 
					  throws IOException{
		  
		  double anArmRate, aSurvivalRate, anAverageST, aMedianST, aLtsRate, aStsRate, aLtsPredictionRate, aStsPredictionRate;
		  BufferedWriter aWriter = new BufferedWriter(new FileWriter(theHeaderFile, true));
		  
		  aWriter.write(theCohortAnalysis.getLabel() + itsCsvDelimitor);
		  		aWriter.write(theCohortAnalysis.getTestCaseOutcomeDescription().TherapyArmOutcome + itsCsvDelimitor);
		  		aWriter.write(theCohortAnalysis.getTestCaseOutcomeDescription().SurvivalOutcome.getSurvivalStatus() + itsCsvDelimitor);
		  		aWriter.write(theCohortAnalysis.getTestCaseOutcomeDescription().SurvivalOutcome.getSurvivalTime() + itsCsvDelimitor);
		  		aWriter.write(theCohortAnalysis.getTestCaseOutcomeDescription().SurvivalOutcome.getSurvivalType() + itsCsvDelimitor);
		  		aWriter.write(theCohortAnalysis.getTestCaseDescription().NomBiomarkerA + itsCsvDelimitor);
		  		aWriter.write(theCohortAnalysis.getTestCaseDescription().NumBiomarkerA + itsCsvDelimitor);
		  		aWriter.write(theCohortAnalysis.getTestCaseDescription().NomBiomarkerB + itsCsvDelimitor);
		  		aWriter.write(theCohortAnalysis.getTestCaseDescription().NumBiomarkerB + itsCsvDelimitor);
		  aWriter.write(theCohortAnalysis.getNumberOfCases() + itsCsvDelimitor);
		  
		  for (String aTherapyArm : theTherapyArmValuesList) {
			  anArmRate = theCohortAnalysis.getArmRate(aTherapyArm) ;
			  aWriter.write(anArmRate + itsCsvDelimitor);
			  
			  aSurvivalRate = theCohortAnalysis.getSurvivalStatusRate(aTherapyArm, SurvivalStatus.ALIVE);
			  aWriter.write(aSurvivalRate + itsCsvDelimitor);
			  
			  anAverageST = theCohortAnalysis.getAverageSurvivalTime(aTherapyArm);
			  aWriter.write(anAverageST + itsCsvDelimitor);
			  
			  aMedianST = theCohortAnalysis.getMedianSurvivalTime(aTherapyArm);
			  aWriter.write(aMedianST + itsCsvDelimitor);
			  
			  aLtsRate = theCohortAnalysis.getSurvivalTypeRate(aTherapyArm, SurvivalType.LTS);
			  aWriter.write(aLtsRate + itsCsvDelimitor);
			  
			  aStsRate = theCohortAnalysis.getSurvivalTypeRate(aTherapyArm, SurvivalType.STS);
			  aWriter.write(aStsRate + itsCsvDelimitor);
			  
			  aLtsPredictionRate = theCohortAnalysis.getAverageSimilarity(aTherapyArm, SurvivalType.LTS);
			  aWriter.write(aLtsPredictionRate + itsCsvDelimitor);
			  
			  aStsPredictionRate = theCohortAnalysis.getAverageSimilarity(aTherapyArm, SurvivalType.STS);
			  aWriter.write(aStsPredictionRate + itsCsvDelimitor);
		  }
		  
		  aWriter.write(itsNewLineChar);
		  aWriter.close();
	  }
	  
	  
	  
	  /**
	   * Creates the CSV file for the sub-cohort analysis and adds a line with a sub-cohort analysis of the given list of outcome analysis.
	   * 
	   * @param theOutputFile File to write output
	   * @param theCohortAnalysisList List of all cohort analysis
	   * @param theTherapyArmValueList List of values of the therapy arm
	   * 
	   * @throws IOException Error while writing to output file 
	   */
	  public static void writeSubcohortAnalysis(
			  File theOutputFile, 
			  ArrayList<CohortAnalysis> theCohortAnalysisList, 
			  ArrayList<String> theTherapyArmValueList) 
					  throws IOException{
		  
		  // create header
		  ValidationBackend.createSubCohortAnalysisHeader(theOutputFile, theTherapyArmValueList);
		  
		  // add line for each sub-cohort analysis
		  for (CohortAnalysis aSubcohortAnalysis : theCohortAnalysisList) {
			  ValidationBackend.addSubCohortAnalysis(theOutputFile, aSubcohortAnalysis, theTherapyArmValueList);
			}
	  }
	  
	  
	  
	  /**
	   * Checks if a case in a given case base contains a specific biomarker.
	   * Input case base must contain specific attribute labels for biomarkers. CAUTION: This is probably only provided by the dataset generator. 
	   * 
	   * @param theCaseId Unique identifier of the case in the given case base
	   * @param theCaseBase Case base that contains the case ID
	   * @param theBiomarkerTypeFlag true=numeric; false=nominal;
	   * @param theBiomarkerTherapyFlag true=armA; false=armB
	   * @param theSurvivaltarget Survival Target Configuration
	   *  
	   * @return true if case contains the specific biomarker; else false
	   *  
	   * @throws CaseBaseException Error while getting specific case from case base
	   * @throws ValidationException Error while determine cut-off value for numeric biomarker
	   */
	  @Deprecated
	  public static boolean hasBiomarker(
			  String theCaseId, 
			  DefaultCaseBase theCaseBase, 
			  boolean theBiomarkerTypeFlag, 
			  boolean theBiomarkerTherapyFlag, 
			  SURVIVALTARGET theSurvivaltarget) throws CaseBaseException, ValidationException {
		  
		  // Determinate the full label of the biomarker (CAUTION: dataset generator data only)
		  String aBiomarkerAttributeLabel = getGeneratedBiomarkerAttributeLabel(theBiomarkerTypeFlag, theBiomarkerTherapyFlag);
		  
		  // Check if corresponding AttributeDesc exist for that biomarker label
		  AttributeDesc aBiomarkerAttributeDesc = CaseBaseUtils.getCaseBaseAttributeDescByLabel(theCaseBase, aBiomarkerAttributeLabel);
		  if (aBiomarkerAttributeDesc == null){
			  return false;
		  }
		  
		  // Value which marks the setting of the biomarker
		  String aBiomarkerAttributeValue = ComputationKM.getValueWithHighestKMCurve(theCaseBase, aBiomarkerAttributeLabel, theSurvivaltarget);		  

		  // Getting the case
		  Instance aCaseInstance = CaseBaseUtils.getCaseBaseInstanceById(theCaseBase, theCaseId);
		  
		  // Check if biomarker is present
		  if (theBiomarkerTypeFlag){	// numeric
			  
			  // determine cut-off
//			  KaplanMeierTargetConfig aKMTargetConfig = new KaplanMeierTargetConfig(theSurvivaltarget);
//			  Instances aWekaInstances = ComputationKM.getWekaInstancesFromCaseBase(theCaseBase, aKMTargetConfig);
//			  double aCutOff = ComputationDiscretize.calculateCutOff(aWekaInstances, aKMTargetConfig, aBiomarkerAttributeLabel);
			  double aCutOff = ComputationDiscretize.parseCutOffValue(aBiomarkerAttributeValue);

			  // check if nominalized attribute value contains cut-off value
			  if (!aBiomarkerAttributeValue.contains(Double.toString(aCutOff))){
				  throw new ValidationException("Cut-Off value (" + aCutOff + ") not found in discretized label (" + aBiomarkerAttributeValue + ").");
			  }
			  
			  // check is numeric biomarker label describes values above or below cut-off
			  boolean isAbove = ComputationDiscretize.isRangeAboveCutOff(aBiomarkerAttributeValue);
			  double aNumericValue = Double.parseDouble(aCaseInstance.getAttForDesc(aBiomarkerAttributeDesc).getValueAsString());
			  if (isAbove){
				  return (aNumericValue > aCutOff);
			  } else {
				  return (aNumericValue < aCutOff); 
			  }
			  
			  
		  } else{	// nominal			  
			  if (aCaseInstance.getAttForDesc(aBiomarkerAttributeDesc).getValueAsString().equalsIgnoreCase(aBiomarkerAttributeValue)){
				  return true;
			  }
		  }
		  
		  // Biomarker value not set
		  return false;
	  }
	  
	  

	  /**
	   * Creates a HashMap with the information if a specific biomarker is set for a specific case.  
	   * CAUTION: Works only for generated datasets!!
	   * 
	   * @param theCaseBase Case base to analyse
	   * @param theKMTargetConfig Survival Target Configuration
	   * @return HashMap where KEY=\<caseid\>_\<biomarkerLabel\> and VALUE=true if biomarker is set and false otherwise
	   * @throws CaseBaseException Error while access the case base
	   * @throws ValidationException Any other error occurred 
	   */
	  public static HashMap<String, Boolean> buildBiomarkerMap(DefaultCaseBase theCaseBase, KaplanMeierTargetConfig theKMTargetConfig) throws CaseBaseException, ValidationException{
		  HashMap<String, Boolean> aBiomarkerMap = new HashMap<>();
		  
		  Instances aWekaInstances = ComputationKM.getWekaInstancesFromCaseBase(theCaseBase, theKMTargetConfig);
		  Instances aNominalizedWekaInstances = ComputationDiscretize.makeNominal(aWekaInstances, theKMTargetConfig);
		  
		  ArrayList<String> aBiomarkerLabelList = new ArrayList<>();
		  aBiomarkerLabelList.add(getGeneratedBiomarkerAttributeLabel(true, true));
		  aBiomarkerLabelList.add(getGeneratedBiomarkerAttributeLabel(true, false));
		  aBiomarkerLabelList.add(getGeneratedBiomarkerAttributeLabel(false, true));
		  aBiomarkerLabelList.add(getGeneratedBiomarkerAttributeLabel(false, false));
		  
		  String aBiomarkerValue;
//		  String aNonBiomarkerValue;
		  double anABC;
		  ArrayList<String> anAllowedValueList = new ArrayList<>();
		  for (String aBiomarkerAttributeLabel : aBiomarkerLabelList) {
			  anAllowedValueList.clear();
			  HashMap<String, Integer> map = aNominalizedWekaInstances.getAttributeLabels(aBiomarkerAttributeLabel);
			  for (String aLabel : map.keySet()) {
				anAllowedValueList.add(aLabel);
			  }
			  
			  if (anAllowedValueList.size() > 2 || anAllowedValueList.isEmpty()){
				  throw new ValidationException("Only exactly 2 different allowed values are supported.");
			  }
//			  System.out.println("Allowed Values: " + Utils.toString(anAllowedValueList));
			  
			  anABC = ComputationKM.getAreaBetweenValueAndNotValue(aNominalizedWekaInstances, true, theKMTargetConfig, aBiomarkerAttributeLabel, anAllowedValueList.get(0));
			  if (anABC > 0){
				  aBiomarkerValue = anAllowedValueList.get(0);
//				  aNonBiomarkerValue = anAllowedValueList.get(1);
			  } else {
				  aBiomarkerValue = anAllowedValueList.get(1);
//				  aNonBiomarkerValue = anAllowedValueList.get(0);
			  }
			  System.out.println("DEBUG: Biomarker value for '" + aBiomarkerAttributeLabel + "' is: " + aBiomarkerValue);
			  
			  String aKey;
			  Boolean aBiomarkerFlag = false;
			  double aNumericValue;
			  String aNominalValue;
			  boolean aNumericFlag = CaseBaseUtils.isCaseBaseAttributeNumeric(theCaseBase, aBiomarkerAttributeLabel);
			  double aCutOffValue;
			  for (Instance aCase : theCaseBase.getCases()) {
				  aKey = aCase.getName()+"_"+aBiomarkerAttributeLabel;
				  if (aNumericFlag){
					  aCutOffValue = ComputationDiscretize.parseCutOffValue(aBiomarkerValue);
//					  System.out.println("DEBUG: found cut-off= " + aCutOffValue);
					  aNumericValue = ((DoubleAttribute)CaseBaseUtils.getCaseAttributeValue(aCase, aBiomarkerAttributeLabel)).getValue();
					  if (ComputationDiscretize.isRangeAboveCutOff(aBiomarkerValue)){
						  aBiomarkerFlag = aNumericValue > aCutOffValue;
					  } else {
						  aBiomarkerFlag = aNumericValue <= aCutOffValue;
					  }
					   
				  } else {
					  aNominalValue = CaseBaseUtils.getCaseAttributeValue(aCase, aBiomarkerAttributeLabel).getValueAsString();
					  aBiomarkerFlag = aNominalValue.equals(aBiomarkerValue);
				  }
//				  System.out.println("CaseAttribute=" + CaseBaseUtils.getCaseAttributeValue(aCase, aNomArmA) + "; Biomarker=" + aNomArmA_BiomarkerLabel);
//				  System.out.println(aKey + "=" + aValue);
				  aBiomarkerMap.put(aKey, aBiomarkerFlag);
			  }
		  }
		  
			  
		  
		  return aBiomarkerMap;
	  }
	  
	  

	  /**
	   * Generates the correct label for a biomarker in a generated dataset.
	   * 
	   * @param theBiomarkerTypeFlag true=numeric; false=nominal;
	   * @param theBiomarkerTherapyFlag true=armA; false=armB
	   * @return correct label for a biomarker in a generated dataset
	   */
	  public static String getGeneratedBiomarkerAttributeLabel(boolean theBiomarkerTypeFlag, boolean theBiomarkerTherapyFlag){
		  String aBiomarkerAttributeLabel = "Biomarker";
		  
		  // Add Type prefix
		  if (theBiomarkerTypeFlag){	// true=numeric
			  aBiomarkerAttributeLabel = "Num" + aBiomarkerAttributeLabel;
		  } else {	// false=nominal
			  aBiomarkerAttributeLabel = "Nom" + aBiomarkerAttributeLabel;
		  }
		  
		  // Add Arm suffix
		  if (theBiomarkerTherapyFlag){	// true=ArmA
			  aBiomarkerAttributeLabel += "ArmA";
		  } else{	// false=armB
			  aBiomarkerAttributeLabel += "ArmB";
		  }
		  
		  return aBiomarkerAttributeLabel;
	  }
	  
	  
	  
	  /**
	   * Counts the number of specific Biomarker in the BiomarkerMap.
	   * 
	   * @param theBiomarkerMap Map with biomarker settings
	   * @param theBiomarkerTypeFlag true=numeric; false=nominal;
	   * @param theBiomarkerTherapyFlag true=armA; false=armB
	   * @return number of specific Biomarker
	   */
	  public static int countBiomarkerOccurences(
			  HashMap<String, Boolean> theBiomarkerMap, 
			  boolean theBiomarkerTypeFlag, 
			  boolean theBiomarkerTherapyFlag){

		  int aCounter=0;
		  
		  System.out.println("DEBUG: BioMarkerMap size = " + theBiomarkerMap.size());
		  
		  for (Entry<String, Boolean> anEntry : theBiomarkerMap.entrySet()) {
			  if (anEntry.getKey().contains(ValidationBackend.getGeneratedBiomarkerAttributeLabel(theBiomarkerTypeFlag, theBiomarkerTherapyFlag))){
				  if (anEntry.getValue() == true)
					  aCounter++;
			  }
		  }
		  
		  return aCounter;
	  }
	  
}
