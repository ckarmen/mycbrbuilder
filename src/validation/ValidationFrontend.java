/**
 * (2019) Heidelberg University, Institute of Medical Biometry and Informatics (IMBI), Germany
 * 
 * This file is part of myCbrBuilder.
 *  
 * myCbrBuilder is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * myCbrBuilder is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with vivaGen. If not, see <http://www.gnu.org/licenses/>.
 * 
 **/
package validation;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;

import org.cliommics.vivagen.configuration.Configuration;

import application.javafx.IMyCBRBuilderListener;
import application.javafx.MyCBRBuilderFxApplication;
import application.javafx.MyCBRBuilderFxElements;
import core.MyCbrBuilderSession;
import core.Utils;
import core.Utils.CUSTOM_DATAPATH;
import de.dfki.mycbr.core.similarity.AmalgamationFct;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.Separator;
import javafx.scene.image.Image;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import similarity.computation.config.KaplanMeierTargetConfig;
import similarity.computation.config.KaplanMeierTargetConfig.SURVIVALTARGET;
import similarity.config.SimilarityMeasure;

/**
 * Simple UI to perform validation sets for a specific similarity measure.
 * 
 * @author Christian Karmen
 *
 */
public class ValidationFrontend extends Application implements IMyCBRBuilderListener {
	MyCBRBuilderFxElements itsFxElements;

	 final static SimilarityMeasure itsSimilarityMeasure = SimilarityMeasure.RANDOM;
//	 final static SimilarityMeasure itsSimilarityMeasure = SimilarityMeasure.HEOM;
//	 final static SimilarityMeasure itsSimilarityMeasure = SimilarityMeasure.HVDM1;
//	 final static SimilarityMeasure itsSimilarityMeasure = SimilarityMeasure.DVDM;
//	 final static SimilarityMeasure itsSimilarityMeasure = SimilarityMeasure.IVDM;
//	 final static SimilarityMeasure itsSimilarityMeasure = SimilarityMeasure.WVDM;
//	 final static SimilarityMeasure itsSimilarityMeasure = SimilarityMeasure.KAPLANMEIER;
	
	 final private static int itsKValue = 10;
	
	
	

	/**
	 * Main Application. Starts this JavaFX Application.
	 * 
	 * @param args
	 *            Program parameters
	 */
	public static void main(String[] args) {
		launch(args);
	}

	/**
	 * JavaFX Application Start.
	 * 
	 * @param stage
	 *            Given JavaFX Stage
	 */
	@Override
	public void start(Stage stage) {
		Stage itsStage = stage;
		itsStage.setTitle("myCBR Builder (" + itsSimilarityMeasure + ") - Validation Frontend (k=" + itsKValue + ")");
		itsStage.setResizable(false);

		// read config file of related project: AutoSurvivalPredictor
		Configuration.getConfiguration(MyCBRBuilderFxApplication.CONFIG_FILENAME);

		// initialize myCBR with myCBRBuilder
		itsFxElements = new MyCBRBuilderFxElements(itsStage, this);

		// create button matrix wit all available test sets
		HBox aGeneratedMatrixBox = new HBox();
		aGeneratedMatrixBox.getChildren().addAll(
				// aHarmonizedBox,
				// new Separator(),
				addGeneratedValidationBox(itsFxElements, 250), new Separator(),
				addGeneratedValidationBox(itsFxElements, 500), new Separator(),
				addGeneratedValidationBox(itsFxElements, 1000), new Separator(),
				addGeneratedValidationBox(itsFxElements, 2500), new Separator(),
				addGeneratedValidationBox(itsFxElements, 5000), new Separator(),
				addGeneratedValidationBox(itsFxElements, 10000));

		BorderPane aPane = new BorderPane();
		aPane.setCenter(aGeneratedMatrixBox);

		// create scene and add UI
		Scene aScene = new Scene(new Group());
		Group aRoot = (Group) aScene.getRoot();
		aRoot.getChildren().add(aPane);

		// App Icon
		itsStage.getIcons().add(new Image("/application/resources/java8.png"));

		// Show UI
		itsStage.setScene(aScene);
		itsStage.show();

		// DEBUG ONLY
		// CaseBaseUtils.printCaseBaseData(itsFxElements.getMyCbrBuilderSession().getCaseBase());
	}

	@Override
	public void updateUI() {
		// No UI
	}

	/**
	 * Creates a set of Buttons to perform validation test sets with a specific
	 * number of cases in the validation case base. First button (on top) is
	 * used for a validation tests with a newly created validation case base.
	 * The other buttons trigger validation tests with predefined validation
	 * case bases (10 iterations available).
	 * 
	 * @param theFxElements
	 *            myCBR back-end
	 * @param theNumberOfCases
	 *            number of cases in the validation case base
	 * @return Set of Buttons to perform validation test sets with
	 */
	private VBox addGeneratedValidationBox(MyCBRBuilderFxElements theFxElements, int theNumberOfCases) {
		VBox aGeneratedBox = new VBox();
		aGeneratedBox.getChildren().add(addGeneratedValidationButton(theFxElements, theNumberOfCases, 0, false));
		aGeneratedBox.getChildren().addAll(new Separator(), new Separator(), new Separator());
		for (int i = 1; i <= 10; i++) {
			aGeneratedBox.getChildren().add(addGeneratedValidationButton(theFxElements, theNumberOfCases, i, true));
		}
		return aGeneratedBox;
	}

	/**
	 * Creates a button that triggers a execution of a single validation set.
	 * 
	 * @param theFxElements
	 *            myCBR back-end
	 * @param theNumberOfCases
	 *            number of cases in the validation case base
	 * @param theIteration
	 *            number of predefined validation case base (usually 1-10). Only
	 *            used when preset flag is set to false.
	 * @param thePresetFlag
	 *            true = if a predefined validation case base should be used. A
	 *            valid iteration number must be set. false = a new validation
	 *            test set will be created.
	 * @return button that triggers a execution of a single validation set
	 */
	private Button addGeneratedValidationButton(MyCBRBuilderFxElements theFxElements, int theNumberOfCases,
			int theIteration, boolean thePresetFlag) {

		String aButtonLabel = "GENERATED" + "\n#cases=" + theNumberOfCases;
		if (thePresetFlag) {
			aButtonLabel += "\n#iteration=" + theIteration;
		} else {
			aButtonLabel += "\n(random)";
		}

		Button aButton = new Button(aButtonLabel);
		aButton.setMinWidth(110);
		aButton.setMaxWidth(100);
		aButton.setWrapText(true);
		aButton.setContentDisplay(ContentDisplay.TOP);
		aButton.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent e) {

				HashMap<String, String> aFilter;
				String aValidationLabel;

				// deleting not needed temporary files (from nominalization)
				Utils.removeFiles(new File(Utils.getCustomDataPath(CUSTOM_DATAPATH.TEMP)),
						"nominalized_" + ".*" + "\\.csv");

				// Perform validation: arm A only
				try {
					aFilter = new HashMap<>();
					aFilter.put("RandomArm", "'arm A'");
					aValidationLabel = "armAonly";
					performValidationSet(theFxElements, aValidationLabel, itsSimilarityMeasure, itsKValue, theNumberOfCases,
							theIteration, aFilter, thePresetFlag);
				} catch (Exception e1) {
					MyCBRBuilderFxElements.showExceptionDialog(e1);
				}

				// Perform validation: arm B only
				try {
					aFilter = new HashMap<>();
					aFilter.put("RandomArm", "'arm B'");
					aValidationLabel = "armBonly";
					performValidationSet(theFxElements, aValidationLabel, itsSimilarityMeasure, itsKValue, theNumberOfCases,
							theIteration, aFilter, thePresetFlag);
				} catch (Exception e1) {
					MyCBRBuilderFxElements.showExceptionDialog(e1);
				}

				// Perform validation: all data (arm A+B)
				try {
					aFilter = new HashMap<>();
					aValidationLabel = "mergedArms";
					performValidationSet(theFxElements, aValidationLabel, itsSimilarityMeasure, itsKValue, theNumberOfCases,
							theIteration, aFilter, thePresetFlag);
				} catch (Exception e1) {
					MyCBRBuilderFxElements.showExceptionDialog(e1);
				}


				// Archive validation results
				String anArchiveFileName = "";
				anArchiveFileName += "k" + itsKValue;
				anArchiveFileName += "_" + Integer.toString(theNumberOfCases) + "cases";
				
				if (thePresetFlag) {
					anArchiveFileName += "_it" + Integer.toString(theIteration);
				} else {
					anArchiveFileName += "_random";
				}
				Utils.archiveFiles(Utils.getCustomDataPath(CUSTOM_DATAPATH.TEMP),
						anArchiveFileName);
				System.out.println("DEBUG: ARCHIVING - DONE.");

				theFxElements.updateLayoutElements();
			}
		});
		return aButton;
	}

	
	/**
	 * Performs a validation test set. Predefined validation case bases
	 * (iterations) can be used, but can also be created on-the-go.
	 * 
	 * @param theFxElements
	 *            myCBR back-end
	 * @param theValidationLabel
	 *            label used for this validation test set
	 * @param theSimilarityMeasure
	 *            similarity measure to use
	 * @param theNumberOfCases
	 *            number of cases in the validation case base
	 * @param theIteration
	 *            number of predefined validation case base (usually 1-10). Only
	 *            used when preset flag is set to false.
	 * @param theFilter
	 *            Filter rules to define an excerp from the validation case base
	 *            (e.g. only armA)
	 * @param thePresetFlag
	 *            true = if a predefined validation case base should be used. A
	 *            valid iteration number must be set. false = a new validation
	 *            test set will be created.
	 * 
	 * @throws Exception
	 *             General error occurred
	 */
	private static void performValidationSet(
			MyCBRBuilderFxElements theFxElements, 
			String theValidationLabel,
			SimilarityMeasure theSimilarityMeasure,
			int theKValue,
			int theNumberOfCases, 
			int theIteration,
			HashMap<String, String> theFilter, 
			boolean thePresetFlag) 
					throws Exception {

//		final boolean SAME_ARM_FLAG = true;

		final String anOutputPath = Utils.getCustomDataPath(CUSTOM_DATAPATH.TEMP);

		// Load generated data set
		MyCbrBuilderSession aSession = MyCBRBuilderFxApplication.setupSession_Generated(theFxElements,
				theSimilarityMeasure, theNumberOfCases, theIteration, theFilter, thePresetFlag);
		
		KaplanMeierTargetConfig aKMTargetConfig = new KaplanMeierTargetConfig(SURVIVALTARGET.GENERATED); 
		
		ValidationBackend aValidationBackend = new ValidationBackend(
				aSession.getCaseBase(), 
				aSession.getOutputClassLabel(), 
				aKMTargetConfig);
		
		
		/////// write calculated weights ///////
		File aWeightOverviewFile = new File(anOutputPath + "weights_overview.csv");		
		AmalgamationFct aGlobalSim = aSession.getConcept().getActiveAmalgamFct();
		ValidationBackend.createWeightsOverviewHeader(aWeightOverviewFile, aGlobalSim);
		ValidationBackend.addWeightsOverview(aWeightOverviewFile, theValidationLabel, aGlobalSim);

		
		/////// write results ///////
		ArrayList<CohortAnalysis> anSubcohortAnalysisList = aValidationBackend.performLeaveOneOutValidation(
				itsSimilarityMeasure==SimilarityMeasure.RANDOM,
				aSession.getOutputClassLabel(), 
				theKValue, 
				false 
//				SAME_ARM_FLAG
				);
		ValidationBackend.writeSimilarityResults(
				new File(anOutputPath + theValidationLabel + "_results.csv"), 
				anSubcohortAnalysisList);
		/////// write results (reverse results) ///////
		ArrayList<CohortAnalysis> anSubcohortAnalysisList_ReverseResults = aValidationBackend.performLeaveOneOutValidation(
				itsSimilarityMeasure==SimilarityMeasure.RANDOM,
				aSession.getOutputClassLabel(), 
				theKValue, 
				true
//				SAME_ARM_FLAG
				);
		ValidationBackend.writeSimilarityResults(
				new File(anOutputPath + theValidationLabel + "_results_unsimilar.csv"), 
				anSubcohortAnalysisList_ReverseResults);

		
		/////// write subcohort analysis ///////
		ValidationBackend.writeSubcohortAnalysis(
				new File(anOutputPath + theValidationLabel + "_subcohortAnalysis.csv"), 
				anSubcohortAnalysisList, 
				aSession.getAllowedOutputClassLabels());
		/////// write subcohort analysis (reverse results) ///////
		ValidationBackend.writeSubcohortAnalysis(
				new File(anOutputPath + theValidationLabel + "_subcohortAnalysis_unsimilar.csv"), 
				anSubcohortAnalysisList_ReverseResults, 
				aSession.getAllowedOutputClassLabels());
		
		
		/////// write subcohort overview ///////
		ValidationBackend.writeSubcohortOverview(
				new File(anOutputPath + theValidationLabel + "_subcohortOverview.csv"), 
				anSubcohortAnalysisList,
				aSession.getAllowedOutputClassLabels());
		/////// write subcohort overview (reverse results) ///////
		ValidationBackend.writeSubcohortOverview(
				new File(anOutputPath + theValidationLabel + "_subcohortOverview_unsimilar.csv"), 
				anSubcohortAnalysisList_ReverseResults,
				aSession.getAllowedOutputClassLabels());

		
		// deleting not needed temporary files (from nominalization)
		Utils.removeFiles(new File(Utils.getCustomDataPath(CUSTOM_DATAPATH.TEMP)),
				"nominalized_" + ".*" + "\\.csv");

		System.out.println("DEBUG: VALIDATION SET - DONE.");
		
		System.out.print("DEBUG: RUNNING GC...");
		System.gc();
		System.out.println("DONE.");
		
	}

}
