/**
 * (2019) Heidelberg University, Institute of Medical Biometry and Informatics (IMBI), Germany
 * 
 * This file is part of myCbrBuilder.
 *  
 * myCbrBuilder is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * myCbrBuilder is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with vivaGen. If not, see <http://www.gnu.org/licenses/>.
 * 
 **/
package validation;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;

import org.cliommics.vivagen.Instances;
import org.cliommics.vivagen.configuration.Configuration;
import org.cliommics.vivagen.datasetgeneration.DataSetGenerator;

import core.Utils;
import core.Utils.CUSTOM_DATAPATH;
import similarity.computation.ComputationDiscretize;
import similarity.computation.config.KaplanMeierTargetConfig;
import similarity.computation.config.KaplanMeierTargetConfig.SURVIVALTARGET;
import weka.core.converters.CSVSaver;

/**
 * Class to control and execute the "Dataset Generator". 
 * CAUTION: Configuration parameters for the dataset generator must be initialized BEFORE dataset generation is performed! 
 * This can be done with "Configuration.getConfiguration(CONFIG_FILENAME);"
 * 
 * @author Christian Karmen
 *
 */
public class GeneratedDataset {
	
	
	private final static KaplanMeierTargetConfig TARGET_CONFIG = new KaplanMeierTargetConfig(SURVIVALTARGET.GENERATED);  
	
	/**
	 * Attribute value that represents survival status for "alive".
	 */
	public final static String STATUS_ALIVE = "alive";
	
	/**
	 * Attribute value that represents survival status for "dead".
	 */
	public final static String STATUS_DEAD = TARGET_CONFIG.TargetStatusIndicator;
	
	/**
	 * Attribute label that indicates the therapy arm.
	 */
	public final static String THERAPY_LABEL = "RandomArm";
	
	/**
	 * Attribute value that represents a specific therapy A.
	 */
	public final static String THERAPY_ARM_A = "Arm A";
	
	/**
	 * Attribute value that represents a specific therapy B.
	 */
	public final static String THERAPY_ARM_B = "Arm B";
	
	/**
	 * Location of the configuration file of the dataset generat 
	 */
	//final public static String CONFIG_FILENAME = ".." + File.separator +"AutoSurvivalPredictor" + File.separator + "config" + File.separator + "generatedData.json";
	final public static String CONFIG_FILENAME = "datasets" + File.separator + "generatedData.json";
	
	
	/**
	 * Returns a list of the case description labels.
	 * 
	 * @return list of the case description labels
	 */
	public static ArrayList<String> getCaseDescriptionList(){
		ArrayList<String> aCaseDescriptionList = new ArrayList<>();
		aCaseDescriptionList.add("NormDistr1");
		aCaseDescriptionList.add("NormDistr2");
		aCaseDescriptionList.add("NormDistr3");
		aCaseDescriptionList.add("ExpDistr1");
		aCaseDescriptionList.add("ExpDistr2");
		aCaseDescriptionList.add("ExpDistr3");
		aCaseDescriptionList.add("WeibullDistr1");
		aCaseDescriptionList.add("WeibullDistr2");
		aCaseDescriptionList.add("WeibullDistr3");
		aCaseDescriptionList.add("UniformDistr1");
		aCaseDescriptionList.add("UniformDistr2");
		aCaseDescriptionList.add("UniformDistr3");
		aCaseDescriptionList.add("UniformStrings1");		// debug.kaputt testing
		aCaseDescriptionList.add("UniformStrings2");
		aCaseDescriptionList.add("UniformStrings3");
		aCaseDescriptionList.add("UniformStrings3");
		aCaseDescriptionList.add("UniformStrings4");
		aCaseDescriptionList.add("UniformStrings5");
		aCaseDescriptionList.add("UniformStrings6");
		aCaseDescriptionList.add("UniformStrings7");
		aCaseDescriptionList.add("UniformStrings8");
		aCaseDescriptionList.add("UniformStrings9");
		aCaseDescriptionList.add("UniformStrings10");
		aCaseDescriptionList.add("UniformStrings11");
		aCaseDescriptionList.add("UniformStrings12");
		aCaseDescriptionList.add("NumBiomarkerArmA");
		aCaseDescriptionList.add("NomBiomarkerArmA");
		aCaseDescriptionList.add("NumBiomarkerArmB");
		aCaseDescriptionList.add("NomBiomarkerArmB");
//		aCaseDescriptionList.add("ShowAll");	// FIXME: remove
//		aCaseDescriptionList.add("SurvivalType");	// FIXME: remove	
//		aCaseDescriptionList.add("SurvivalStatus");	// FIXME: remove
		Collections.sort(aCaseDescriptionList);
		
		return aCaseDescriptionList;
	}
	  
	
	/**
	 * Returns a list of the case solution labels (outcome attribute labels).
	 * 
	 * @return list of the case description labels
	 */
	public static ArrayList<String> getSolutionDescriptionList(){
		ArrayList<String> aSolutionDescriptionList = new ArrayList<>();
		aSolutionDescriptionList.add(THERAPY_LABEL);
		aSolutionDescriptionList.add(TARGET_CONFIG.TargetSurvivalColumn);
		aSolutionDescriptionList.add(TARGET_CONFIG.TargetStatusColumn);
		
		aSolutionDescriptionList.add("SurvivalType");
		
		return aSolutionDescriptionList;
	}
	
	
	/**
	 * Returns a list of the case description and solution labels (outcome attribute labels).
	 * 
	 * @return list of the case description labels
	 */
	public static ArrayList<String> getCaseAndSolutionDescriptionList(){
		ArrayList<String> aCaseAndSolutionDescriptionList = getCaseDescriptionList();
		aCaseAndSolutionDescriptionList.addAll(getSolutionDescriptionList());
		
		return aCaseAndSolutionDescriptionList;
	}
	
	
	/**
	 * Gets a defined file for a specific predefined dataset. The location is static and cannot be configured.
	 * 
	 * @param theCaseBaseSize Number of cases for the predefined dataset
	 * @param theIteration number of the dataset iteration
	 * @return file for a specific predefined dataset
	 */
	public static File getPredefinedDatasetFile(int theCaseBaseSize, int theIteration){
		String aCBFileName = Utils.getCustomDataPath(CUSTOM_DATAPATH.GENERATED) +
				theCaseBaseSize + "cases" + File.separator + 
	  			"generatedData" + theCaseBaseSize + "-" + String.format("%02d", theIteration) + ".csv";
	  	return new File(aCBFileName);
	}
	
	
	/**
	 * Generates a specific dataset and writes the result into a given file. STS/LTS rate can be configured.  
	 * CAUTION: Existing output file will be overwritten!
	 * 
	 * @param theCaseBaseSize Number of cases for the predefined dataset; if null then default value is used
	 * @param theStsPercentage Ratio of STS and LTS patients. Value must be between 0.0 and 1.0; if null then default value is used
	 * @param theOutputFileName File to write the generated dataset to
	 * @return File with results of dataset generation
	 * @throws IOException I/O error occurred
	 */
	public static File generateAndWriteDatasetFile(Integer theCaseBaseSize, Double theStsPercentage, String theOutputFileName) throws IOException{
  		DataSetGenerator aDatasetGenerator = new DataSetGenerator();
  		if (theCaseBaseSize != null){
  			aDatasetGenerator.setNumberOfSubjects(theCaseBaseSize);
  		} else {
  			aDatasetGenerator.setNumberOfSubjects(Configuration.DATAGEN_NUMBER_OF_SUBJECTS);
  			System.out.println("Configuration.DATAGEN_NUMBER_OF_SUBJECTS: " + Configuration.DATAGEN_NUMBER_OF_SUBJECTS);
  		}
  		
  		if (theStsPercentage != null){
  			aDatasetGenerator.setPercentageSTS(theStsPercentage);
  		} else {
  			aDatasetGenerator.setPercentageSTS(Configuration.DATAGEN_PERCENTAGE_STS);
  			System.out.println("Configuration.DATAGEN_PERCENTAGE_STS: " + Configuration.DATAGEN_PERCENTAGE_STS);
  		}
  		
  		// create output path if required
  		File anOutputFile = new File(theOutputFileName); 
  		if (!anOutputFile.exists()){
  			new File(anOutputFile.getParent()).mkdirs();
  			anOutputFile.createNewFile();
  		}
  		
  		File aDataSetFile = aDatasetGenerator.generateAndWrite(theOutputFileName);
  		System.out.println("DEBUG: Dataset generated -> " + aDataSetFile.getAbsolutePath());
  		
  		return aDataSetFile;
	}
	
	
	/**
	 * Created a defined set of generated datasets. 
	 * CAUTION: Existing output files will be overwritten!
	 * 
	 * @param theIterationSize Number of datasets
	 * @param theCaseBaseSize Number of generated cases per iteration; if null then default value is used
	 * @param theStsPercentage Ratio of STS and LTS patients. Value must be between 0.0 and 1.0; if null then default value is used
	 * @param theFilePath Path to store the generated datasets
	 * @return List of newly created nominalized files
	 * @throws IOException I/O error occurred
	 */
	public static ArrayList<File> createSetOfDataSetFiles(int theIterationSize, Integer theCaseBaseSize, Double theStsPercentage, String theFilePath) throws IOException{
		String aFileNamePrefix = theFilePath + "generatedData";
		String aOutputFileName;
		String anIterationString;
		File aNominalizedFile;
		ArrayList<File> aNominalizedFileList = new ArrayList<>();
		
		Integer aCaseBaseSize;
		if (theCaseBaseSize != null){
			aCaseBaseSize = theCaseBaseSize;
		} else {
			aCaseBaseSize = Configuration.DATAGEN_NUMBER_OF_SUBJECTS;
		}
		
		for (int i = 1; i <= theIterationSize; i++) {
			anIterationString = String.format("%02d", i);	// iteration with leading zero
			aOutputFileName = aFileNamePrefix + aCaseBaseSize + "-" + anIterationString + ".csv";
			aNominalizedFile = generateAndWriteDatasetFile(aCaseBaseSize, theStsPercentage, aOutputFileName);
			aNominalizedFileList.add(aNominalizedFile);
		}
		
		return aNominalizedFileList;
	}
	
	
	/**
	 * Nominalizes a given dataset and writes results into a newly created file. 
	 * CAUTION: The created file will be placed in the same folder as the input file. File name is 
	 * 
	 * @param theDataSetFile input dataset (not normalized)
	 * @param theDelimiter delimiter of input and output file
	 * @return Newly created file with nominalization results
	 */
	public static File nominalizeDataSetFile(File theDataSetFile, String theDelimiter){		
		try{		
			System.out.print("DEBUG: Reading dataset file \"" + theDataSetFile.getName() + "\"... ");
			Instances asWekaInstances = Instances.readCSV(theDataSetFile.getAbsolutePath(), theDelimiter);
			System.out.println("done.");
			
			Instances aDiscretizedInstances = ComputationDiscretize.makeNominal(asWekaInstances, TARGET_CONFIG);
			
			File aNominalFile = new File(
					theDataSetFile.getAbsolutePath().substring(0,theDataSetFile.getAbsolutePath().length()-4) + ".nominalized.csv");
			
			System.out.println("DEBUG: Storing nominalization results to \"" + aNominalFile + "\".");
	        CSVSaver saver = new CSVSaver();
	        saver.setInstances(aDiscretizedInstances);
	        saver.setFile(aNominalFile);
	        saver.setFieldSeparator(theDelimiter);
	        saver.writeBatch();
	        
	        return aNominalFile;
        
		} catch(Exception e){
        	System.err.println("ERROR: Nominalization failed!");
        	e.printStackTrace();
        	
        	return null;
        }		
	}
	
	/**
	 * Nominalizes a given dataset and writes results into a newly created file. 
	 * CAUTION: The created file will be placed in the same folder as the input file. File name is 
	 * 
	 * @param theDataSetFile input dataset (not normalized)
	 * @return Newly created file with nominalization results
	 */
	public static File nominalizeDataSetFile(File theDataSetFile){
		return nominalizeDataSetFile(theDataSetFile, ",");
	}
	
	
	/**
	 * Creates predefined datasets used for validation.
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		// read config file of related project: AutoSurvivalPredictor
		Configuration.getConfiguration(CONFIG_FILENAME);
		
		int aNrOfCases =0;
		Double aStsPercentage = null; //0.2;
		
		
//		// Create predefined datasets
//		int aNrOfIterations = 10;						
//		try {
//			ArrayList<File> aDataSetFileList = createSetOfDataSetFiles(
//					aNrOfIterations, 
//					aNrOfCases, 
//					aStsPercentage, 
//					"datasets" + File.separatorChar + aNrOfCases + "cases/");
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
////		// Nominalize 
////		for (File aDataSetFile : aDataSetFileList) {
////			nominalizeDataSetFile(aDataSetFile);
////		}
		
		
		// Create single dataset
		File aDataSetFile;
		try {
			aDataSetFile = generateAndWriteDatasetFile(
					aNrOfCases, 
					aStsPercentage, 
					"tmp" + File.separator + "debug.generatedDataset_" + aNrOfCases + "cases.csv");
//			nominalizeDataSetFile(aDataSetFile);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}

}
