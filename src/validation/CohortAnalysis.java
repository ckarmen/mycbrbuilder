/**
 * (2019) Heidelberg University, Institute of Medical Biometry and Informatics (IMBI), Germany
 * 
 * This file is part of myCbrBuilder.
 *  
 * myCbrBuilder is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * myCbrBuilder is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with vivaGen. If not, see <http://www.gnu.org/licenses/>.
 * 
 **/
package validation;

import java.util.ArrayList;
import java.util.Comparator;

import similarity.computation.Computation;

/**
 * Class to analyze the outcomes of a set of similar cases (cohort).  
 * 
 * @author Christian Karmen
 *
 */
public class CohortAnalysis{
	

	  
	private ArrayList<SimilarCaseComparison> itsSimilarCaseOutcomeList = new ArrayList<>();
	private String itsAnalysisLabel;
	
	private CaseDescription itsTestCaseDescription;
	private CaseOutcomeDescription itsTestCaseOutcomeDescription;
	

	/**
	* Class to analyze the outcomes of a set of similar cases (cohort). 
	* 
	* @param theAnalysisLabel Label for the analysis
	* @param theTestCaseDescription Description of the Test Case
	* @param theCaseOutcomeDescription Outcome description of the Test Case
	*/
	public CohortAnalysis(String theAnalysisLabel, CaseDescription theTestCaseDescription, CaseOutcomeDescription theCaseOutcomeDescription){
		itsAnalysisLabel = theAnalysisLabel;
		itsTestCaseDescription = theTestCaseDescription;
		itsTestCaseOutcomeDescription = theCaseOutcomeDescription;
	}
	  
	/**
	 * Adds the outcomes between two similar cases.
	 * 
	 * @param theTestCaseDescription descriptions for the testing case (new case)
	 * @param theTestCaseOutcomeDescription Outcome descriptions for the testing case (new case)
	 * @param theTrainingCaseDescription descriptions for the training case (similar to new case)
	 * @param theTrainingCaseOutcomeDescription Outcome descriptions for the training case (similar to new case)
	 * @param theSimilarity Calculated similarity between testing and training case
	 */
	  public void addSimilarCaseOutcome(
//			  CaseDescription theTestCaseDescription,
//			  CaseOutcomeDescription theTestCaseOutcomeDescription,
			  CaseDescription theTrainingCaseDescription,
			  CaseOutcomeDescription theTrainingCaseOutcomeDescription,
			  double theSimilarity){
		  
		  itsSimilarCaseOutcomeList.add(
				  new SimilarCaseComparison(
//						  theTestCaseDescription,
//						  theTestCaseOutcomeDescription,
						  itsTestCaseDescription, 
						  itsTestCaseOutcomeDescription, 
						  theTrainingCaseDescription, 
						  theTrainingCaseOutcomeDescription, 
						  theSimilarity));
	  }
	
	  /**
	   * Adds the outcomes between two similar cases.
	   * 
	   * @param theSimilarCaseOutcome outcomes of two similar cases
	   */
	  public void addSimilarCaseOutcome(SimilarCaseComparison theSimilarCaseOutcome){
		  itsSimilarCaseOutcomeList.add(theSimilarCaseOutcome);
	  }
	  
	  
	  /**
	   * Adds a list of the outcomes between two similar cases.
	   * 
	   * @param theSimilarCaseOutcomeList list of the outcomes of two similar cases
	   */
	  public void addSimilarCaseOutcomeList(ArrayList<SimilarCaseComparison> theSimilarCaseOutcomeList){
		  for (SimilarCaseComparison aSimilarCaseOutcome : theSimilarCaseOutcomeList) {
			  itsSimilarCaseOutcomeList.add(aSimilarCaseOutcome);
		}		  
	  }
	  
	  
	  /**
	   * Gets the label of this analysis.
	   * 
	   * @return label of this analysis
	   */
	public String getLabel() {
		return itsAnalysisLabel;
	}

	/**
	 * Sets the label of this analysis.
	 * 
	 * @param theAnalysisLabel label of this analysis
	 */
	public void setLabel(String theAnalysisLabel) {
		itsAnalysisLabel = theAnalysisLabel;
	}
	  

	/**
	 * Gets the number of cases (in training case base).
	 * 
	 * @return number of cases
	 */
	  public int getNumberOfCases(){
		  return itsSimilarCaseOutcomeList.size();
	  }
	  
	  
	/**
	 * Gets the number of cases (in training case base) with a specific therapy arm.
	 * 
	 * @param theArmLabel label of the therapy arm
	 * @return number of cases of a specific therapy arm
	 */
	  public int getNumberOfCasesWithArmLabel(String theArmLabel){
		  return getSimilarCaseOutcomeList(theArmLabel).size();
	  }
	  
	  
	  /**
	   * Gets a list of all outcomes between two similar cases.
	   * 
	   * @return list of all outcomes
	   */
	  public ArrayList<SimilarCaseComparison> getSimilarCaseOutcomeList(){
		  return itsSimilarCaseOutcomeList;
	  }
	  
	  
	  /**
	   * Gets a list of all outcomes between two similar cases with a specific therapy arm.
	   * 
	   * @param theArmLabel label of the therapy arm
	   * @return  list of all outcomes
	   */
	  public ArrayList<SimilarCaseComparison> getSimilarCaseOutcomeList(String theArmLabel){
		  ArrayList<SimilarCaseComparison> anArmCaseList = new ArrayList<>(); 
		  
		  for (SimilarCaseComparison aCase : itsSimilarCaseOutcomeList) {
			if (aCase.TrainingCaseOutcomeDescription.TherapyArmOutcome.equals(theArmLabel)){
				anArmCaseList.add(aCase);
			}
		  }
		  return anArmCaseList;
	  }
	  
	  

	  
	  

	  
	  
	  /**
	   * Gets the rate of cases with a specific therapy arm and all cases available.
	   * 
	   * @param theArmLabel label of the therapy arm
	   * @return specific therapy arm rate 
	   */
	  public double getArmRate(String theArmLabel){
		  int i = getNumberOfCasesWithArmLabel(theArmLabel);
		  
//		  System.out.println("rate of '" + theArmLabel + "' = " + (i / (double)itsCases.size()));
		  return i / (double)itsSimilarCaseOutcomeList.size();
	  }
	  
	  
	  /**
	   * Gets the rate of cases with a specific therapy arm and survival type, and all cases available.
	   * 
	   * @param theArmLabel label of the therapy arm
	   * @param theSurvivalType specific survival type
	   * @return specific survival type rate 
	   */
	  public double getSurvivalTypeRate(String theArmLabel, SurvivalType theSurvivalType){
		  ArrayList<SimilarCaseComparison> anArmCaseList = getSimilarCaseOutcomeList(theArmLabel);
		  int anArmListLength = anArmCaseList.size();
		  
		  int aCounter = 0;
		  for (SimilarCaseComparison anArmCase : anArmCaseList) {
			  if (anArmCase.TrainingCaseOutcomeDescription.SurvivalOutcome.getSurvivalType() == theSurvivalType){
				  aCounter++;
			  }
		  }	
		  
		  double aSurvivalTypeRate = (double)aCounter / (double)anArmListLength;	// casting so a double value is returned 
//		  System.out.println("SurvivalType rate of '" + theArmLabel + "' = " + aSurvivalTypeRate);
		  
		  return aSurvivalTypeRate;
	  }
	  
	  
	  
	  /**
	   * Gets the rate of cases with a specific therapy arm and survival status, and all cases available.
	   * 
	   * @param theArmLabel label of the therapy arm
	   * @param theSurvivalStatus specific survival status
	   * @return specific survival type rate 
	   */
	  public double getSurvivalStatusRate(String theArmLabel, SurvivalStatus theSurvivalStatus){
		  ArrayList<SimilarCaseComparison> anArmCaseList = getSimilarCaseOutcomeList(theArmLabel);
		  int anArmListLength = anArmCaseList.size();
		  
		  int aCounter = 0;
		  for (SimilarCaseComparison anArmCase : anArmCaseList) {
			  if (anArmCase.TrainingCaseOutcomeDescription.SurvivalOutcome.getSurvivalStatus() == theSurvivalStatus){
				  aCounter++;
			  }
		  }	
		  
		  double aSurvivalStatusRate = (double)aCounter / (double)anArmListLength;	// casting so a double value is returned 
//		  System.out.println("SurvivalStatus rate of '" + theArmLabel + "' = " + aSurvivalStatusRate);
		  
		  return aSurvivalStatusRate;
	  }
	  
	  
	  /**
	   * Calculates the average similarity for the given output class value and survival type.
	   * <br/>
	   * CAUTION: if no instance of the survival type is found, the return value is Double.NaN!
	   * 
	   * @param theArmLabel label of the therapy arm
	   * @param theSurvivalType survival type of interest
	   * @return the average similarity
	   */
	  public double getAverageSimilarity(String theArmLabel, SurvivalType theSurvivalType){
		  ArrayList<SimilarCaseComparison> anArmCaseList = getSimilarCaseOutcomeList(theArmLabel);

		  int aCounter = 0;		  
		  double aSimSum = 0d;
		  double aSim;
		  for (SimilarCaseComparison anArmCase : anArmCaseList) {
			if (anArmCase.TrainingCaseOutcomeDescription.SurvivalOutcome.getSurvivalType() == theSurvivalType){	
				aCounter++;
				aSim = anArmCase.Similarity;
				aSimSum += aSim;
			}
		  }	
		   
		  double aRate = (double)aSimSum / (double)aCounter;	// casting so a double value is returned
//		  System.out.println(theSurvivalType.name() + " rate of '" + theArmLabel + "' = " + aRate);
		  
		  return aRate;
	  }
	  
	  
	  /**
	   * Gets the average survival time for a specific therapy arm.
	   * 
	   * @param theArmLabel label of the therapy arm
	   * @return average survival time
	   */
	  public double getAverageSurvivalTime(String theArmLabel){
		  ArrayList<SimilarCaseComparison> aArmCaseList = getSimilarCaseOutcomeList(theArmLabel);
		  
		  double aSurvivalTimeSum = 0d;
		  for (SimilarCaseComparison aCase : aArmCaseList) {
				aSurvivalTimeSum += aCase.TrainingCaseOutcomeDescription.SurvivalOutcome.getSurvivalTime();
		  }	
		  
		  // prevent division by zero 
		  if (aArmCaseList.size() == 0){
			  return 0d;
		  }
		  
		  return aSurvivalTimeSum / aArmCaseList.size();
	  }
	  
	  
	  /**
	   * Gets the median survival time for a specific therapy arm.
	   * 
	   * @param theArmLabel label of the therapy arm
	   * @return median survival time
	   */
	  public double getMedianSurvivalTime(String theArmLabel){
		  // FIXME Berechnung des Median sollte auslagert werden. (Zu Klasse "Computation" ?!)
		  ArrayList<SimilarCaseComparison> anArmCaseList = getSimilarCaseOutcomeList(theArmLabel);
		  anArmCaseList.sort(new TrainingSurvivalTimeComperator());
		  
		  if (anArmCaseList.size() == 0){
				 return 0d; 
			  }
		  
		  if (anArmCaseList.size() % 2 == 0){
			  int aUpperMedianIndex = anArmCaseList.size() / 2;
			  int aLowerMedianIndex = aUpperMedianIndex - 1;
			  double anAvgMedian = (anArmCaseList.get(aUpperMedianIndex).TrainingCaseOutcomeDescription.SurvivalOutcome.getSurvivalTime() 
							  + anArmCaseList.get(aLowerMedianIndex).TrainingCaseOutcomeDescription.SurvivalOutcome.getSurvivalTime()) / 2;   
			  return anAvgMedian;
		  } else{
			  int aMedianIndex = anArmCaseList.size() / 2;
			  return anArmCaseList.get(aMedianIndex).TrainingCaseOutcomeDescription.SurvivalOutcome.getSurvivalTime();
		  }
	  }
	  
	  
	  /**
	   * Gets the mean survival time for a specific therapy arm.
	   * 
	   * @param theArmLabel label of the therapy arm
	   * @return mean survival time
	   */
	  public double getMeanSurvivalTime(String theArmLabel){
		  ArrayList<SimilarCaseComparison> anArmCaseList = getSimilarCaseOutcomeList(theArmLabel);
		  double aSurvivalTimeSum = 0;
		  
		  for (SimilarCaseComparison anCase : anArmCaseList) {
			  aSurvivalTimeSum += anCase.TrainingCaseOutcomeDescription.SurvivalOutcome.getSurvivalTime();
		  }
		  
		  return aSurvivalTimeSum / anArmCaseList.size();
	  }
	  
	  
	  public double getStandardDeviationSurvivalTime(String theArmLabel){
		  return Computation.getStandardDeviation(getSurvivalTimeList(theArmLabel));
	  }
	  
//	  public double getMeanSurvivalTime(String theArmLabel){
//		  return Computation.getMeanValue(getSurvivalTimeList(theArmLabel));
//	  }
//	  
//	  public double getMedianSurvivalTime(String theArmLabel){
//		  return Computation.getMedianValue(getSurvivalTimeList(theArmLabel));
//	  }
	  
	  public double getVarianceSurvivalTime(String theArmLabel){
		  return Computation.getVariance(getSurvivalTimeList(theArmLabel));
	  }
	  
	  public double getConfidenceIntervalSurvivalTime(String theArmLabel, double theConfidenceLevel){
		  return Computation.getConfidenceInterval(getSurvivalTimeList(theArmLabel), theConfidenceLevel);
	  }
	  	  
	  public ArrayList<Double> getSurvivalTimeList(String theArmLabel){
		  ArrayList<SimilarCaseComparison> anArmCaseList = getSimilarCaseOutcomeList(theArmLabel);
		  
		  ArrayList<Double> aSurvivalTimeList = new ArrayList<>();
		  for (SimilarCaseComparison aCase : anArmCaseList) {
			aSurvivalTimeList.add(aCase.TrainingCaseOutcomeDescription.SurvivalOutcome.getSurvivalTime());
		  }
		  
		  return aSurvivalTimeList;
	  }
	  
	  
	  /**
	   * Prints details of all similar case outcomes to console.
	   */
	  public void printSimilarCaseOutcomeList(){
		  for (SimilarCaseComparison aSimilarCaseOutcome : itsSimilarCaseOutcomeList) {
			aSimilarCaseOutcome.printDetails();
		}
	  }
	  


	  
	  protected class TrainingSurvivalTimeComperator implements Comparator<SimilarCaseComparison>{
		@Override
		public int compare(SimilarCaseComparison o1, SimilarCaseComparison o2) {
			return Double.compare(
					o1.TrainingCaseOutcomeDescription.SurvivalOutcome.getSurvivalTime(), 
					o2.TrainingCaseOutcomeDescription.SurvivalOutcome.getSurvivalTime());
		}
	  }
	  protected class SimilarityComperator implements Comparator<SimilarCaseComparison>{
		@Override
		public int compare(SimilarCaseComparison o1, SimilarCaseComparison o2) {
			return Double.compare(o1.Similarity, o2.Similarity);
		}
	  }
	  
	  
	  
	  
		public CaseDescription getTestCaseDescription() {
			return itsTestCaseDescription;
		}



		public CaseOutcomeDescription getTestCaseOutcomeDescription() {
			return itsTestCaseOutcomeDescription;
		}


	  
	  
	  
	  
	  

	  
//		/**
//		 * Main program. Very short functionality tests.
//		 * 
//		 * @param args Console arguments
//		 */
//		public static void main(String[] args) {
//			final String armA = "arm A";
//			final String armB = "arm B";
//			
//			// test dataset
//			OutcomeAnalysis aSA = new OutcomeAnalysis("TestAnalysis");
//			aSA.addSimilarCaseOutcome(
//					null,
//					new CaseOutcomeDescription("0", "arm foo", null),
//					null,
//					new CaseOutcomeDescription("1", armA, 
//							new SurvivalOutcome(10, SurvivalStatus.ALIVE, SurvivalType.STS)), 0.9);
//			aSA.addSimilarCaseOutcome(
//					null,
//					new CaseOutcomeDescription("0", "arm foo", null),
//					null,
//					new CaseOutcomeDescription("2", armB, 
//							new SurvivalOutcome(10, SurvivalStatus.DECEASED, SurvivalType.LTS)), 0.8);
//			aSA.addSimilarCaseOutcome(
//					null,
//					new CaseOutcomeDescription("0", "arm foo", null),
//					null,
//					new CaseOutcomeDescription("3", armA, 
//							new SurvivalOutcome(10, SurvivalStatus.ALIVE, SurvivalType.STS)), 0.7);
//			aSA.addSimilarCaseOutcome(
//					null,
//					new CaseOutcomeDescription("0", "arm foo", null),
//					null,
//					new CaseOutcomeDescription("4", armB, 
//							new SurvivalOutcome(10, SurvivalStatus.ALIVE, SurvivalType.LTS)), 0.6);
//			aSA.addSimilarCaseOutcome(
//					null,
//					new CaseOutcomeDescription("0", "arm foo", null),
//					null,
//					new CaseOutcomeDescription("5", armA, 
//							new SurvivalOutcome(10, SurvivalStatus.DECEASED, SurvivalType.LTS)), 0.5);
//			aSA.addSimilarCaseOutcome(
//					null,
//					new CaseOutcomeDescription("0", "arm foo", null),
//					null,
//					new CaseOutcomeDescription("6", armA, 
//							new SurvivalOutcome(10, SurvivalStatus.ALIVE, SurvivalType.STS)), 0.4);
//			aSA.addSimilarCaseOutcome(
//					null,
//					new CaseOutcomeDescription("0", "arm foo", null),
//					null,
//					new CaseOutcomeDescription("7", armA, 
//							new SurvivalOutcome(10, SurvivalStatus.DECEASED, SurvivalType.LTS)), 0.3);
//			aSA.addSimilarCaseOutcome(
//					null,
//					new CaseOutcomeDescription("0", "arm foo", null),
//					null,
//					new CaseOutcomeDescription("8", armA, 
//							new SurvivalOutcome(10, SurvivalStatus.ALIVE, SurvivalType.LTS)), 0.2);
//			aSA.addSimilarCaseOutcome(
//					null,
//					new CaseOutcomeDescription("0", "arm foo", null),
//					null,
//					new CaseOutcomeDescription("9", armB, 
//							new SurvivalOutcome(10, SurvivalStatus.ALIVE, SurvivalType.LTS)), 0.1);
//			aSA.addSimilarCaseOutcome(
//					null,
//					new CaseOutcomeDescription("0", "arm foo", null),
//					null,
//					new CaseOutcomeDescription("10", armB, 
//							new SurvivalOutcome(10, SurvivalStatus.ALIVE, SurvivalType.STS)), 0.0);
//			
////			aSA.addCase("0", "1", 0.9, armA, 10, "alive", sts);
////			aSA.addCase("0", "2", 0.8, armB, 40, "dead", lts);
////			aSA.addCase("0", "3", 0.7, armA, 5, "alive", sts);
////			aSA.addCase("0", "4", 0.6, armB, 10, "alive", lts);
////			aSA.addCase("0", "5", 0.5, armA, 90, "dead", lts);
////			aSA.addCase("0", "6", 0.4, armA, 1, "alive", sts);
////			aSA.addCase("0", "7", 0.3, armA, 100, "dead", lts);
////			aSA.addCase("0", "8", 0.2, armA, 50, "alive", lts);
////			aSA.addCase("0", "9", 0.1, armB, 0, "alive", lts);
////			aSA.addCase("0", "10",0.0, armB, 99, "alive", sts);
//			
//			System.out.println("getArmRate(armA) = " + aSA.getArmRate(armA));
//			System.out.println("getArmRate(armB) = " + aSA.getArmRate(armB));
//			System.out.println();
//			System.out.println("getAverageSurvivalTime(armA) = " + aSA.getAverageSurvivalTime(armA));
//			System.out.println("getAverageSurvivalTime(armB) = " + aSA.getAverageSurvivalTime(armB));
//			System.out.println();
//			System.out.println("getMedianSurvivalTime(armA) = " + aSA.getMedianSurvivalTime(armA));
//			System.out.println("getMedianSurvivalTime(armB) = " + aSA.getMedianSurvivalTime(armB));
//			System.out.println();
//			System.out.println("gettSurvivalTypeRate(armA, lts) = " + aSA.getSurvivalTypeRate(armA, SurvivalType.LTS));
//			System.out.println("gettSurvivalTypeRate(armB, lts) = " + aSA.getSurvivalTypeRate(armB, SurvivalType.LTS));
//			System.out.println("gettSurvivalTypeRate(armA, sts) = " + aSA.getSurvivalTypeRate(armA, SurvivalType.STS));
//			System.out.println("gettSurvivalTypeRate(armB, sts) = " + aSA.getSurvivalTypeRate(armB, SurvivalType.STS));
//			System.out.println();
//			System.out.println("getAverageSimilarity(armA, lts) = " + aSA.getAverageSimilarity(armA, SurvivalType.LTS));
//			System.out.println("getAverageSimilarity(armB, lts) = " + aSA.getAverageSimilarity(armB, SurvivalType.LTS));
//			System.out.println("getAverageSimilarity(armA, sts) = " + aSA.getAverageSimilarity(armA, SurvivalType.STS));
//			System.out.println("getAverageSimilarity(armB, sts) = " + aSA.getAverageSimilarity(armB, SurvivalType.STS));
//		}
}

