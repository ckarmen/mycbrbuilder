/**
 * (2019) Heidelberg University, Institute of Medical Biometry and Informatics (IMBI), Germany
 * 
 * This file is part of myCbrBuilder.
 *  
 * myCbrBuilder is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * myCbrBuilder is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with vivaGen. If not, see <http://www.gnu.org/licenses/>.
 * 
 **/
package validation;

/**
 * Class to describe the Attributes (that are relevant for validation) of a
 * case.
 * 
 * @author Christian Karmen
 *
 */
public class CaseDescription {
	/**
	 * Unique identifier of the Case.
	 */
	public final String CaseID;

	/**
	 * Case has numerical Biomarker for therapy arm A.
	 */
	public boolean NumBiomarkerA;

	/**
	 * Case has numerical Biomarker for therapy arm B.
	 */
	public boolean NumBiomarkerB;

	/**
	 * Case has nominal Biomarker for therapy arm A.
	 */
	public boolean NomBiomarkerA;

	/**
	 * Case has nominal Biomarker for therapy arm B.
	 */
	public boolean NomBiomarkerB;

	/**
	 * Class to describe the Attributes (that are relevant for validation) of a
	 * case.
	 * 
	 * @param theCaseID
	 *            Unique identifier of the Case
	 * @param theNumBioArmA
	 *            Case has numerical Biomarker for therapy arm A
	 * @param theNumBioArmB
	 *            Case has numerical Biomarker for therapy arm B
	 * @param theNomBioArmA
	 *            Case has nominal Biomarker for therapy arm A
	 * @param theNomBioArmB
	 *            Case has nominal Biomarker for therapy arm B
	 */
	public CaseDescription(String theCaseID, boolean theNumBioArmA, boolean theNumBioArmB, boolean theNomBioArmA,
			boolean theNomBioArmB) {
		CaseID = theCaseID;
		NumBiomarkerA = theNumBioArmA;
		NumBiomarkerB = theNumBioArmB;
		NomBiomarkerA = theNomBioArmA;
		NomBiomarkerB = theNomBioArmB;
	}

	/**
	 * Prints details of the case description to console.
	 */
	public void printDetails() {
		System.out.println("-------- Case Description --------");
		System.out.println("CaseID: " + CaseID);
		System.out.println("NumBiomarkerA = " + NumBiomarkerA);
		System.out.println("NumBiomarkerB = " + NumBiomarkerB);
		System.out.println("NomBiomarkerA = " + NomBiomarkerA);
		System.out.println("NomBiomarkerB = " + NomBiomarkerB);
	}

}
