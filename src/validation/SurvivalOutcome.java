/**
 * (2019) Heidelberg University, Institute of Medical Biometry and Informatics (IMBI), Germany
 * 
 * This file is part of myCbrBuilder.
 *  
 * myCbrBuilder is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * myCbrBuilder is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with vivaGen. If not, see <http://www.gnu.org/licenses/>.
 * 
 **/
package validation;

/**
 * Class to describe the Survival Outcome.
 * 
 * @author Christian Karmen
 *
 */
public class SurvivalOutcome {

	final private double itsSurvivalTime;
	final private SurvivalStatus itsSurvivalStatus;
	final private SurvivalType itsSurvivalType;

	/**
	 * Class to describe the Survival Outcome.
	 * 
	 * @param theSurvivalTime
	 * @param theSurvivalStatus
	 * @param theSurvivalType
	 */
	public SurvivalOutcome(double theSurvivalTime, SurvivalStatus theSurvivalStatus, SurvivalType theSurvivalType) {
		itsSurvivalTime = theSurvivalTime;
		itsSurvivalStatus = theSurvivalStatus;
		itsSurvivalType = theSurvivalType;
	}

	/**
	 * Class to describe the Survival Outcome.
	 * 
	 * @param theSurvivalTime
	 * @param theSurvivalStatus
	 */
	public SurvivalOutcome(double theSurvivalTime, SurvivalStatus theSurvivalStatus) {
		itsSurvivalTime = theSurvivalTime;
		itsSurvivalStatus = theSurvivalStatus;
		itsSurvivalType = SurvivalType.UNKNOWN;
	}

	/**
	 * Gets the number of time units (e.g. months) when the last observed
	 * survival status was checked.
	 * 
	 * @return Last observed survival time.
	 */
	public double getSurvivalTime() {
		return itsSurvivalTime;
	}

	/**
	 * Gets the last observed survival status (e.g. "alive" or "deceased")
	 * 
	 * @return Last observed survival status
	 */
	public SurvivalStatus getSurvivalStatus() {
		return itsSurvivalStatus;
	}

	/**
	 * Gets the classification of the survival types "long-term survivor" and
	 * "short-term survivor". Optional attribute only used for generated case
	 * bases.
	 * 
	 * @return classification of the survival type
	 */
	public SurvivalType getSurvivalType() {
		return itsSurvivalType;
	}
	
	/**
	 * Prints details of survival outcome to console.
	 */
	public void printDetails(){
		System.out.println("--- Survival Outcome ---");
		System.out.println("Survival Time: " + itsSurvivalTime);
		System.out.println("Survival Status: " + itsSurvivalStatus);
		System.out.println("Survival Type: " + itsSurvivalType.name());
	}

}
