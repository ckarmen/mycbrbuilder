/**
 * (2019) Heidelberg University, Institute of Medical Biometry and Informatics (IMBI), Germany
 * 
 * This file is part of myCbrBuilder.
 *  
 * myCbrBuilder is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * myCbrBuilder is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with vivaGen. If not, see <http://www.gnu.org/licenses/>.
 * 
 **/
package validation;

/**
 * Class to describe the Outcome of a case.
 * 
 * @author Christian Karmen
 *
 */
public class CaseOutcomeDescription {
	/**
	 * Unique identifier of the Case.
	 */
	final public String CaseID;

	/**
	 * Label of the received therapy arm.
	 */
	final public String TherapyArmOutcome;

	/**
	 * Outcome of survival.
	 */
	final public SurvivalOutcome SurvivalOutcome;

	/**
	 * Class to describe the Outcome of a case.
	 * 
	 * @param theCaseID
	 *            Case ID
	 * @param theTherapyArmOutcome
	 *            Label of the received therapy arm
	 * @param theSurvivalOutcome
	 *            Outcome of survival.
	 */
	public CaseOutcomeDescription(String theCaseID, String theTherapyArmOutcome, SurvivalOutcome theSurvivalOutcome) {
		CaseID = theCaseID;
		TherapyArmOutcome = theTherapyArmOutcome;
		SurvivalOutcome = theSurvivalOutcome;
	}
	
	/**
	 * Prints details of the case outcome to console.
	 */
	public void printDetails(){
		System.out.println("-------- Case Outcome --------");
		System.out.println("CaseID: " + CaseID);
		System.out.println("Therapy Arm: " + TherapyArmOutcome);
		SurvivalOutcome.printDetails();
	}

}
