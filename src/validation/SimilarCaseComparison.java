/**
 * (2019) Heidelberg University, Institute of Medical Biometry and Informatics (IMBI), Germany
 * 
 * This file is part of myCbrBuilder.
 *  
 * myCbrBuilder is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * myCbrBuilder is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with vivaGen. If not, see <http://www.gnu.org/licenses/>.
 * 
 **/
package validation;

/**
 * Class to encapsulate all data (that is relevant for validation) of the test
 * and the training case.
 * 
 * @author Christian Karmen
 *
 */
public class SimilarCaseComparison {

	/**
	 * Description of the testing case.
	 */
	final public CaseDescription TestCaseDescription;

	/**
	 * Description of the outcome of the testing case.
	 */
	final public CaseOutcomeDescription TestCaseOutcomeDescription;

	/**
	 * Description of the training case.
	 */
	final public CaseDescription TrainingCaseDescription;

	/**
	 * Description of the outcome of the training case.
	 */
	final public CaseOutcomeDescription TrainingCaseOutcomeDescription;

	/**
	 * Calculated similarity between test and training case.
	 */
	final public double Similarity;

	/**
	 * Class to encapsulate all data (that is relevant for validation) of the
	 * test and the training case.
	 * 
	 * @param theTestCaseDescription
	 *            Description of the testing case
	 * @param theTestCaseOutcomeDescription
	 *            Description of the outcome of the testing case
	 * @param theTrainingCaseDescription
	 *            Description of the training case
	 * @param theTrainingCaseOutcomeDescription
	 *            Description of the outcome of the training case
	 * @param theSimilarity
	 *            Calculated similarity between test and training case
	 */
	public SimilarCaseComparison(CaseDescription theTestCaseDescription,
			CaseOutcomeDescription theTestCaseOutcomeDescription, CaseDescription theTrainingCaseDescription,
			CaseOutcomeDescription theTrainingCaseOutcomeDescription, double theSimilarity) {

		TestCaseDescription = theTestCaseDescription;
		TestCaseOutcomeDescription = theTestCaseOutcomeDescription;

		TrainingCaseDescription = theTrainingCaseDescription;
		TrainingCaseOutcomeDescription = theTrainingCaseOutcomeDescription;

		Similarity = theSimilarity;
	}

	/**
	 * Prints details of the similar case outcomes to console.
	 */
	public void printDetails() {
		System.out.println("------------- Similar Case Comparison -------------");
		System.out.println("Similarity between Test and Training Case: " + Similarity);
		System.out.println("TEST CASE:");
		TestCaseDescription.printDetails();
		TestCaseOutcomeDescription.printDetails();
		System.out.println("TRAINING CASE:");
		TrainingCaseDescription.printDetails();
		TrainingCaseOutcomeDescription.printDetails();
		System.out.println("---------------------------------------------------");
	}

}
