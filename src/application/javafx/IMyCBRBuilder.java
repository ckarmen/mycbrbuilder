/**
 * (2019) Heidelberg University, Institute of Medical Biometry and Informatics (IMBI), Germany
 * 
 * This file is part of myCbrBuilder.
 *  
 * myCbrBuilder is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * myCbrBuilder is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with vivaGen. If not, see <http://www.gnu.org/licenses/>.
 * 
 **/
package application.javafx;

import java.util.ArrayList;
import java.util.Map;

import core.MyCbrBuilderSession;
import core.connectors.AbstractConnector;
import exceptions.ApplicationException;
import exceptions.CaseBaseException;
import exceptions.SimilarityMeasureException;
import javafx.scene.control.MenuBar;
import javafx.scene.control.TableView;
import javafx.scene.layout.VBox;
import similarity.computation.config.AbstractSimilarityMeasureConfiguration;
import similarity.config.SimilarityMeasure;

/**
 * Interface to define a MyCBRBuilder Application. 
 * 
 * @author Christian Karmen
 *
 */
public interface IMyCBRBuilder {
	
	/**
	 * Enum for a Operating System.
	 * 
	 * @author Christian Karmen
	 *
	 */
	public enum OS{
	/**
	 * Windows  
	 */
	WINDOWS, 
	/**
	 * MacOS
	 */
	MAC, 
	/**
	 * Linux
	 */
	LINUX, 
	/**
	 * Other OS
	 */
	OTHER, 
	/**
	 * Unknown OS
	 */
	UNKNOWN;}
	
	/**
	 * Tries to detect the running Operating System. 
	 * 
	 * @return Currently running OS
	 */
	public static OS detectClientOS(){
		OS aClientOS = OS.UNKNOWN;
		
		String anOSname = System.getProperty("os.name");
//		  System.out.println("Detected OS: " + anOSname);
		  
		  if (anOSname.toLowerCase().contains("windows")){
			  aClientOS = OS.WINDOWS;
		  } else if (anOSname.toLowerCase().contains("mac os")){
			  aClientOS = OS.MAC;
		  } else if (anOSname.toLowerCase().contains("linux")){
			  aClientOS = OS.LINUX;
		  } else {
			  aClientOS = OS.OTHER;
		  }
		  
		  return aClientOS;
	}
	

	/**
	 * Gets the connector to the case base.
	 * 
	 * @return connector to the case base
	 */
	public AbstractConnector getConnector();
	
	/**
	 * Sets the connector to the case base.
	 * 
	 * @param theConnector connector to the case base
	 */
	public void setConnector(AbstractConnector theConnector);
	
	
	/**
	 * Gets the MyCBRBuilder session.
	 * 
	 * @return MyCBRBuilder session
	 */
	public MyCbrBuilderSession getMyCbrBuilderSession();
	
	
	/**
	 * Sets the MyCBRBuilder session.
	 * 
	 * @param theMyCbrBuilderSession MyCBRBuilder session
	 * @throws ApplicationException Error occurred while creating session 
	 * 
	 */
	public void setMyCbrBuilderSession(MyCbrBuilderSession theMyCbrBuilderSession) throws ApplicationException;
	
	/**
	 * Gets the list of attribute labels that describes a case.
	 * 
	 * @return list of attribute labels that describes a case
	 */
	public ArrayList<String> getCaseDescriptionList();
	
	/**
	 * Sets the list of attribute labels that describes a case.
	 * 
	 * @param theCaseDescriptionList list of attribute labels that describes a case
	 */
	public void setCaseDescriptionList(ArrayList<String> theCaseDescriptionList);

	/**
	 * Gets the list of attribute labels that describes a case solution.
	 * 
	 * @return list of attribute labels that describes a case solution
	 */
	public ArrayList<String> getSolutionDescriptionList();
	
	/**
	 * Sets the list of attribute labels that describes a case solution.
	 * 
	 * @param theSolutionDescriptionList list of attribute labels that describes a case solution
	 */
	public void setSolutionDescriptionList(ArrayList<String> theSolutionDescriptionList);
	
	/**
	 * Gets the similarity measure.
	 * 
	 * @return similarity measure
	 */
	public SimilarityMeasure getSimilarityMeasure();
	
	/**
	 * Sets the similarity measure.
	 * 
	 * @param theSimilarityMeasure Similarity Measure
	 * @param theSimilarityMeasureConfig Similarity Measure specific configuration object 
	 */
	public void setSimilarityMeasure(SimilarityMeasure theSimilarityMeasure, AbstractSimilarityMeasureConfiguration theSimilarityMeasureConfig);
	
	/**
	 * Gets the similarity measure specific configuration object.
	 * 
	 * @return similarity measure specific configuration object
	 */
	public AbstractSimilarityMeasureConfiguration getSimilarityMeasureConfig();
	
//	/**
//	 * Sets the similarity measure specific configuration object.
//	 * 
//	 * @param theSimilarityMeasureConfig similarity measure specific configuration object
//	 */
//	public void setSimilarityMeasureConfig(AbstractSimilarityMeasureConfiguration theSimilarityMeasureConfig);
	
	/**
	 * Gets the layout element that contains the Case Descriptions. 
	 * 
	 * @return layout element that contains the Case Descriptions
	 */
	public VBox getLAYOUT_CaseDescriptionPane();
	
	/**
	 * Gets the layout element that contains the Menu Bar. 
	 * 
	 * @return layout element that contains the Menu Bar
	 */
	public MenuBar getLAYOUT_MenuBar();
	
	/**
	 * Gets the layout element that contains the Load/Save pane. 
	 * 
	 * @return layout element that contains the Load/Save pane
	 */
	public VBox getLAYOUT_LoadSavePane();
	
	/**
	 * Gets the layout element that contains the Configuration pane. 
	 * 
	 * @return layout element that contains the Configuration pane
	 */
	public VBox getLAYOUT_ConfigPane();
	
	/**
	 * Gets the layout element that contains the CBR Result pane. 
	 * 
	 * @return layout element that contains the CBR Result pane
	 */
	public TableView<Map<String, String>> getLAYOUT_CurrentResultPane();
	
	
	/**
	 * Generates the global similarity measure. 
	 * 
	 * @param theSimilarityMeasure Similarity measure to be used
	 * @throws CaseBaseException Error while access the case base
	 * @throws SimilarityMeasureException Error while preparing similarity measure
	 */
	public void generateAmalgamFct(SimilarityMeasure theSimilarityMeasure) throws CaseBaseException, SimilarityMeasureException;

}
