/**
 * (2019) Heidelberg University, Institute of Medical Biometry and Informatics (IMBI), Germany
 * 
 * This file is part of myCbrBuilder.
 *  
 * myCbrBuilder is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * myCbrBuilder is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with vivaGen. If not, see <http://www.gnu.org/licenses/>.
 * 
 **/
package application.javafx;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Set;
import java.util.TreeSet;

import org.cliommics.vivagen.configuration.Configuration;

import core.MyCbrBuilderSession;
import core.MyCbrUtils;
import core.Utils;
import core.Utils.CUSTOM_DATAPATH;
import core.connectors.WekaCsvConnector;
import exceptions.ApplicationException;
import exceptions.ConnectorException;
import exceptions.SimilarityMeasureException;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.Separator;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import similarity.computation.config.AbstractSimilarityMeasureConfiguration;
import similarity.computation.config.KaplanMeierConfig;
import similarity.computation.config.KaplanMeierTargetConfig;
import similarity.computation.config.KaplanMeierTargetConfig.SURVIVALTARGET;
import similarity.computation.config.VDMConfig;
import similarity.config.SimilarityMeasure;
import validation.GeneratedDataset;



/**
 * myCBR Builder Main Application for a JavaFX based GUI. 
 * 
 * @author Christian Karmen
 *
 */
public class MyCBRBuilderFxApplication extends Application implements IMyCBRBuilderListener{
	
	final protected static String APPLICATION_TITLE = "myCBR Builder";
	final protected int itsWindowHeight = 780;//740;
    //final public static String CONFIG_FILENAME = ".." + File.separator +"AutoSurvivalPredictor" + File.separator + "config" + File.separator + "generatedData.json";
    final public static String CONFIG_FILENAME = "datasets" + File.separator + "generatedData.json";

	protected Stage itsStage = null;
//	protected MyCbrBuilderSession itsMyCBRBuilderSession;
	protected MyCBRBuilderFxElements itsFxElements;
	protected BorderPane itsBorderPane = new BorderPane();
	
//	final protected static SimilarityMeasure itsSimilarityMeasure = SimilarityMeasure.RANDOM;
//	final protected static SimilarityMeasure itsSimilarityMeasure = SimilarityMeasure.HEOM;
//	final protected static SimilarityMeasure itsSimilarityMeasure = SimilarityMeasure.HVDM1;
//	final protected static SimilarityMeasure itsSimilarityMeasure = SimilarityMeasure.DVDM;
	final protected static SimilarityMeasure itsSimilarityMeasure = SimilarityMeasure.IVDM;
//	final protected static SimilarityMeasure itsSimilarityMeasure = SimilarityMeasure.WVDM;
	
//	final protected SimilarityMeasure itsSimilarityMeasure = SimilarityMeasure.KAPLANMEIER;
	
//	final protected static CUSTOM_DATASET itsCustomDataset = CUSTOM_DATASET.NONE;
//	final protected static CUSTOM_DATASET itsCustomDataset = CUSTOM_DATASET.HARMONIZED;
//	final protected static CUSTOM_DATAPATH itsCustomDataset = CUSTOM_DATAPATH.GENERATED;



	
	
	/**
	 * Main Application. Starts this JavaFX Application.
	 * 
	 * @param args Program parameters
	 */
  public static void main(String[] args) {
    launch(args);
  }
  
  

  
	/**
	 * JavaFX Application Start.
	 * 
	 * @param stage Given JavaFX Stage
	 */
  @Override
  public void start(Stage stage) {
	  itsStage = stage;
	  itsStage.setTitle(APPLICATION_TITLE);
	  itsStage.setResizable(false);
	  
	  // read config file of related project: AutoSurvivalPredictor
	  Configuration.getConfiguration(CONFIG_FILENAME);

	  itsFxElements = new MyCBRBuilderFxElements(itsStage, this);

	  // TOP is static: menu bar
	  itsBorderPane.setTop(itsFxElements.getLAYOUT_MenuBar());
	  
	  // LEFT is static: load/save pane
	  VBox aLoadSavePane = itsFxElements.getLAYOUT_LoadSavePane();
	  Button aHarmonizedButton = addHarmonizedScenarioButton(itsFxElements, itsSimilarityMeasure);
	  Button aGeneratedButton = addGeneratedScenarioButton(itsFxElements, itsSimilarityMeasure, 500, 1, false);
//	  Button aGeneratedButton = addGeneratedScenarioButton(itsFxElements, itsSimilarityMeasure, 100, 1, true);
//	  Button aGeneratedButton = addGeneratedScenarioButton(itsFxElements, itsSimilarityMeasure, 1000, 1, false);
//	  Button aGeneratedButton = addGeneratedScenarioButton(itsFxElements, itsSimilarityMeasure, 250, 1, true);
//	  Button aGeneratedButton = addGeneratedScenarioButton(itsFxElements, itsSimilarityMeasure, 1000, 1, true);
//	  Button aValidationButton = addValidationButton(itsFxElements, "250_1");
	  aLoadSavePane.getChildren().addAll(
			  new Separator(), 
			  new Separator(), 
			  aHarmonizedButton,
			  aGeneratedButton);
//			  aValidationButton);
	  itsBorderPane.setLeft(aLoadSavePane);
	  
	  int aWindowWidth = itsFxElements.getWindowWidth();
	  Scene scene = new Scene(new Group(), aWindowWidth, itsWindowHeight);
	  Group root = (Group) scene.getRoot();
	  root.getChildren().add(itsBorderPane);
    
	  itsStage.getIcons().add(new Image("/application/resources/java8.png"));
	  itsStage.setScene(scene);
	  itsStage.show();
	  
	  itsFxElements.updateLayoutElements();

	// for debug purpose: load test dataset immediately on start
//	  aHarmonizedButton.fire();
//	  aGeneratedButton.fire();
//	  aValidationButton.fire();
	  
	  // DEBUG ONLY
//	  CaseBaseUtils.printCaseBaseData(itsFxElements.getMyCbrBuilderSession().getCaseBase());
  }

  
  
  
	@Override
	public void updateUI() {
		  itsBorderPane.setCenter(itsFxElements.getLAYOUT_CaseDescriptionPane());
		  itsFxElements.updateCurrentRetrievalResultsTable();
		  itsBorderPane.setBottom(itsFxElements.getLAYOUT_CurrentResultPane());
		  itsBorderPane.setRight(itsFxElements.getLAYOUT_ConfigPane());
		  
		  updateApplicationTitle();
	}
	
	
	
  
  private void updateApplicationTitle(){
	  itsStage.setTitle(APPLICATION_TITLE + " (" + itsSimilarityMeasure.getName() + ")" + " - " + itsFxElements.getConceptLabel());
  }
  
  
  
  /**
   * Creates a custom button to trigger for loading the MM CBR setup.
   * 
   * @return custom button for MM data load 
   */
  public static Button addHarmonizedScenarioButton( 
		  MyCBRBuilderFxElements theFxElements, 
		  SimilarityMeasure theSimilarityMeasure){
	  
	    Image anImage = new Image("/application/resources/1446494263_icon-93.png");
	    Image anImage_Loading = new Image("/application/resources/loading36.gif");
	    
	    ImageView anImageView = new ImageView(anImage);
	    
	    Button aButton = new Button("Harmonized", anImageView);
	    aButton.setMinWidth(100);
	    aButton.setMaxWidth(100);
	    aButton.setWrapText(true);
	    aButton.setContentDisplay(ContentDisplay.TOP);
	    aButton.setOnAction(new EventHandler<ActionEvent>() {
	    	@Override public void handle(ActionEvent e) {
	    		
	    		// FIXME loading gif has bad timing (not during loading time)
	    		aButton.setGraphic(new ImageView(anImage_Loading));	    		
	    		HashMap<String, String> aFilter = new HashMap<>();
	    		try {
					setupSession_Harmonized(theFxElements, theSimilarityMeasure, aFilter);
				} catch (Exception e1) {
					MyCBRBuilderFxElements.showExceptionDialog(e1);
				}
	    		aButton.setGraphic(anImageView);
	    		
	    		theFxElements.updateLayoutElements();
//	    		updateUI();
	    	}
		});
	    
	    return aButton;
  }
  
  
  
  /**
   * Creates a custom button to trigger for loading the MM CBR setup.
   * 
   * @return custom button for MM data load 
   */
  public static Button addGeneratedScenarioButton(
		  MyCBRBuilderFxElements theFxElements, 
		  SimilarityMeasure theSimilarityMeasure,
		  int theNumberOfCases, 
		  int theIteration,
		  boolean thePresetFlag){
	  
	    Image anImage = new Image("/application/resources/1446494263_icon-93.png");
	    Image anImage_Loading = new Image("/application/resources/loading36.gif");
	    
	    ImageView anImageView = new ImageView(anImage);
	    
	    String aButtonLabel = "GENERATED" + "\n#cases=" + theNumberOfCases; 
	    if (thePresetFlag){
	    	aButtonLabel += "\n#iteration=" + theIteration;
	    } else {
	    	aButtonLabel += "\n(random)";
	    }
	    
	    Button aButton = new Button(aButtonLabel, anImageView);
	    aButton.setMinWidth(100);
	    aButton.setMaxWidth(100);
	    aButton.setWrapText(true);
	    aButton.setContentDisplay(ContentDisplay.TOP);
	    aButton.setOnAction(new EventHandler<ActionEvent>() {
	    	@Override public void handle(ActionEvent e) {
	    		
	    		// FIXME loading gif has bad timing (not during loading time)
	    		aButton.setGraphic(new ImageView(anImage_Loading));	    		
	    		HashMap<String, String> aFilter = new HashMap<>();
	    		
	    		try {
					setupSession_Generated(theFxElements, theSimilarityMeasure, theNumberOfCases, theIteration, aFilter, thePresetFlag);
				} catch (Exception e1) {
					MyCBRBuilderFxElements.showExceptionDialog(e1);
				}
	    		aButton.setGraphic(anImageView);
	    		
	    		theFxElements.updateLayoutElements();
	    	}
		});
	    return aButton;
  }

  
  
//  public static Button addValidationButton(MyCBRBuilderFxElements theFxElements, String theValidationLabel){
////	  Image anImage = new Image(getClass().getResourceAsStream("/application/resources/1446494263_icon-93.png"));
////	    Image anImage_Loading = new Image(getClass().getResourceAsStream("/application/resources/loading36.gif"));	    
////	    ImageView anImageView = new ImageView(anImage);
//	    
////	    Button aButton = new Button("Validation", anImageView);
//	  Button aButton = new Button("Validation");
//	    aButton.setMinWidth(100);
//	    aButton.setMaxWidth(100);
//	    aButton.setWrapText(true);
//	    aButton.setContentDisplay(ContentDisplay.TOP);
//	    aButton.setOnAction(new EventHandler<ActionEvent>() {
//	    	@Override public void handle(ActionEvent e) {
//	    		
//	    		// FIXME loading gif has bad timing (not during loading time)
////	    		aButton.setGraphic(new ImageView(anImage_Loading));	    		
//
//	    		SurvivalAnalysis.performValidation(
//	    				theFxElements.getMyCbrBuilderSession().getConcept(),
//	    				theFxElements.getMyCbrBuilderSession().getCaseBase(), 
//	    				theFxElements.getSolutionDescriptionList(),
//	    				theFxElements.getMyCbrBuilderSession().getOutputClassLabel());
//	    		
//	    		MyCbrUtils.archiveFiles(getCustomDataPath(CUSTOM_DATAPATH.TEMP), theValidationLabel);
//	    		
////	    		File aZipFile = new File(CUSTOM_DATAPATH.TEMP + "");
////	    		MyCbrUtils.zipFile(theFileList, theZipFile);
//	    		
////	    		aButton.setGraphic(anImageView);
//	    		
//	    		theFxElements.updateLayoutElements();
////	    		updateUI();
//	    	}
//		});
//	    return aButton;
//  }
  
  


	
  
//  public static void initializeDatasetSession(
//		  CUSTOM_DATAPATH theCustomDataset, 
//		  MyCBRBuilderFxElements theFxElements, 
//		  SimilarityMeasure theSimilarityMeasure,
//		  int theNumberOfCases, 
//		  int theIteration){
//	 
//	  MyCbrBuilderSession aSession;
//	  
//	switch (theCustomDataset) {
//		case HARMONIZED:
//			aSession = createSession_HARMONIZED(theSimilarityMeasure);
//			break;
//		case GENERATED:
//			aSession = createSession_GENERATED(theSimilarityMeasure, theNumberOfCases, theIteration);
//			break;
//		case BASE:
//		case CURRENT:
//			aSession = new MyCbrBuilderSession();
//			break;
//		default:
//			System.err.println("ERROR: Unsupported Custom dataset: " + theCustomDataset.name());
//			aSession = new MyCbrBuilderSession();
//			break;
//	}
//
//	theFxElements.setMyCbrBuilderSession(aSession);
//	theFxElements.setWorkingDirectory(getCustomDataPath(theCustomDataset));	
//	  
//	// Some debug info
//	MyCbrUtils.printConceptDescription(aSession.getConcept());
//	
////	return aSession;
//  }	
  
  
  		public static MyCbrBuilderSession setupSession_Harmonized(
  				MyCBRBuilderFxElements theFxElements, 
  				SimilarityMeasure theSimilarityMeasure,
  				HashMap<String, String> theFilter) throws ApplicationException, ConnectorException{
  			
  			theFxElements.setMyCbrBuilderSession(createSession_HARMONIZED(theSimilarityMeasure, theFilter));
  			theFxElements.setWorkingDirectory(Utils.getCustomDataPath(CUSTOM_DATAPATH.HARMONIZED));	
  			
  			// Some debug info
  			MyCbrUtils.printConceptDescription(theFxElements.getMyCbrBuilderSession().getConcept());
  			
  			return theFxElements.getMyCbrBuilderSession();
  		}
  		
  		
  		/**
  		 * @param theFxElements
  		 * @param theSimilarityMeasure
  		 * @param theNumberOfCases
  		 * @param theIteration
  		 * @param theFilter
  		 * @param thePresetFlag
  		 * @return
  		 * @throws ApplicationException
  		 * @throws SimilarityMeasureException
  		 * @throws ConnectorException
  		 * @throws IOException DataGenerator Exception
  		 */
  		public static MyCbrBuilderSession setupSession_Generated(
  				MyCBRBuilderFxElements theFxElements, 
  				SimilarityMeasure theSimilarityMeasure,
  				int theNumberOfCases, 
  				int theIteration,
  				HashMap<String, String> theFilter,
  				boolean thePresetFlag) throws ApplicationException, SimilarityMeasureException, ConnectorException, IOException{
  			
  			if (!thePresetFlag){
  	  			// 	deleting not needed temporary files (from nominalization)
  	    		Utils.removeFiles(
  	    				new File (Utils.getCustomDataPath(CUSTOM_DATAPATH.TEMP)), 
  	    				"nominalized_" + ".*" + "\\.csv");
  	  			}
  			
  			theFxElements.setMyCbrBuilderSession(createSession_GENERATED(theSimilarityMeasure, theNumberOfCases, theIteration, theFilter, thePresetFlag));
  			theFxElements.setWorkingDirectory(Utils.getCustomDataPath(CUSTOM_DATAPATH.GENERATED));	
  			
  			// Some debug info
  			MyCbrUtils.printConceptDescription(theFxElements.getMyCbrBuilderSession().getConcept());
  			
  			return theFxElements.getMyCbrBuilderSession();
  		}
  		
  		
  		public static MyCbrBuilderSession setupSession_Empty(
  				MyCBRBuilderFxElements theFxElements) throws ApplicationException{
  			
  			theFxElements.setMyCbrBuilderSession(new MyCbrBuilderSession());
  			theFxElements.setWorkingDirectory(Utils.getCustomDataPath(CUSTOM_DATAPATH.CURRENT));	
  			
  			// Some debug info
  			MyCbrUtils.printConceptDescription(theFxElements.getMyCbrBuilderSession().getConcept());
  			
  			return theFxElements.getMyCbrBuilderSession();
  		}

	  
	  /**
	   * Generates HD4-specific myCBRBuilder session from i2b2-exported de-lined CSV.  
	 * @throws ConnectorException 
	   * 
	   */
  	private static MyCbrBuilderSession createSession_HARMONIZED(SimilarityMeasure theSimilarityMeasure, HashMap<String, String> theFilter) throws ConnectorException{
  		CUSTOM_DATAPATH aDataSet = CUSTOM_DATAPATH.HARMONIZED;
	  	String aDataPath = Utils.getCustomDataPath(aDataSet);
		  	
	  	String aCaseBaseDataFile = aDataPath;
	  	aCaseBaseDataFile += "HARMONIZED main t2 delined dot selected HD4 mini231 relevant5.csv";
//	  	aCaseBaseDataFile += "HARMONIZED main t2 delined dot selected HD4 mini231 relevant3.csv";
//	  	aCaseBaseDataFile += "HARMONIZED main t2 delined dot selected HD4 mini231.csv";
//	  	aCaseBaseDataFile += "HARMONIZED main t2 delined dot selected HD4 null.csv";
	  	
	  	// FIXME CSV Import mit "null" Werten sollte ebenfalls moeglich sein			
//	  	aCaseBaseDataFile +=  "HARMONIZED main t2 delined dot selected HD4 null clean.csv");
		
	  	File aCsvFile = new File(aCaseBaseDataFile);
	  	String aCaseBaseDataFileDelimitor = ";";
						

		ArrayList<String> aCaseDescriptionList = new ArrayList<>();
		aCaseDescriptionList.add("00 - Main Data \\ Age");					// 5er	// 3er
		aCaseDescriptionList.add("00 - Main Data \\ Sex");					// 5er	// 3er				
		aCaseDescriptionList.add("02 - ON STUDY \\ Weight [kg]");			// 5er	// 3er				
		aCaseDescriptionList.add("00 - Main Data \\ ISS");					// 5er
		aCaseDescriptionList.add("00 - Main Data \\ Stage of disease");		// 5er
//		aCaseDescriptionList.add("02 - ON STUDY \\ Height [cm]");
//		aCaseDescriptionList.add("02 - ON STUDY \\ Surface area [m^2]");	
//		aCaseDescriptionList.add("02 - ON STUDY \\ ALAT [U/l]");
//		aCaseDescriptionList.add("02 - ON STUDY \\ Albumin [g/l]");
//		aCaseDescriptionList.add("02 - ON STUDY \\ Calcium [mmol/l] to");
//		aCaseDescriptionList.add("02 - ON STUDY \\ Creatinine [mg/dl]");
//		aCaseDescriptionList.add("02 - ON STUDY \\ CRP [mg/l]");
//		aCaseDescriptionList.add("02 - ON STUDY \\ LDH [U/l]");
//		aCaseDescriptionList.add("02 - ON STUDY \\ Total proteins [g/l]");
//		aCaseDescriptionList.add("02 - ON STUDY \\ Hemoglobin [mmol/l]");
//		aCaseDescriptionList.add("02 - ON STUDY \\ Platelets [x10^9/l]");
//		aCaseDescriptionList.add("02 - ON STUDY \\ Immunofixation serum");
//		aCaseDescriptionList.add("02 - ON STUDY \\ Immunofixation urine");
//		aCaseDescriptionList.add("02 - ON STUDY \\ Serum M-protein [g/l]");
//		aCaseDescriptionList.add("02 - ON STUDY \\ M-protein heavy chain");
//		aCaseDescriptionList.add("02 - ON STUDY \\ M-protein light chain");
//		aCaseDescriptionList.add("02 - ON STUDY \\ Number of bone plasmacytoma");
//		aCaseDescriptionList.add("02 - ON STUDY \\ Diffuse osteoporosis");
//		aCaseDescriptionList.add("02 - ON STUDY \\ Number of lytic bone lesions");
		Collections.sort(aCaseDescriptionList);
		

		ArrayList<String> aSolutionDescriptionList = new ArrayList<>();
		aSolutionDescriptionList.add("00 - Main Data \\ Randomization arm");
		aSolutionDescriptionList.add("00 - Main Data \\ Overall survival [m]");
		aSolutionDescriptionList.add("00 - Main Data \\ Survival status indicator");
//		aSolutionDescriptionList.add("00 - Main Data \\ RESPONSE AFTER 1ST INDUCTION");
//		aSolutionDescriptionList.add("00 - Main Data \\ RESPONSE AFTER 2ND INDUCTION");
//		aSolutionDescriptionList.add("00 - Main Data \\ RESPONSE FOLLOW-UP");
//		aSolutionDescriptionList.add("00 - Main Data \\ PFS indicator (censored at alloSCT)");
//		aSolutionDescriptionList.add("00 - Main Data \\ PFS. [mo] (censored at alloSCT)");
//		aSolutionDescriptionList.add("00 - Main Data \\ Registration hospital");
		
//		ArrayList<String> anAttributeBlackList = I2b2Connector.getI2b2FactsMetaDataList();
		
		String aOutputClassLabel = "00 - Main Data \\ Randomization arm";
		
		// Setting similarity measure specific parameters
		AbstractSimilarityMeasureConfiguration aSimilarityMeasureConfig;
		switch (theSimilarityMeasure){
		case RANDOM:
		case HEOM:
			aSimilarityMeasureConfig = null;
			break;
		case DVDM:
		case HVDM1:
		case HVDM2:
		case HVDM3:
		case IVDM:
		case WVDM:
			Set<String> aOutputClassAllowedValueList = new TreeSet<>();
			aOutputClassAllowedValueList.add("VAD");
			aOutputClassAllowedValueList.add("Pad");
			aSimilarityMeasureConfig = new VDMConfig(aOutputClassLabel, aOutputClassAllowedValueList);
			break;
		case KAPLANMEIER:
			boolean aStepShapeFlag = true;
			boolean aTicksFlag = true;
			KaplanMeierTargetConfig aKMConfig = new KaplanMeierTargetConfig(SURVIVALTARGET.HD4_OVERALL_SURVIVAL);
			aSimilarityMeasureConfig = new KaplanMeierConfig(aKMConfig, aStepShapeFlag, aTicksFlag);
			break;

		default:
			aSimilarityMeasureConfig = null;
			break;
		}

  		return createCustomCsvSession(
  				aDataSet.name(), 
  				theSimilarityMeasure, 
  				aSimilarityMeasureConfig,
  				aCsvFile, 
  				aCaseBaseDataFileDelimitor, 
  				aCaseDescriptionList, 
  				aSolutionDescriptionList,
  				aOutputClassLabel,
  				theFilter);
  	}
  
  	
  	
  	
	  /**
	   * Generates a myCBRBuilder session from generated CSV.
	   * @throws SimilarityMeasureException Error while preparing similarity measure
	 * @throws ConnectorException 
	 * @throws IOException 
	   * 
	   */
	private static MyCbrBuilderSession createSession_GENERATED(
			SimilarityMeasure theSimilarityMeasure, 
			int theNumberOfCases, 
			int theIteration,
			HashMap<String, String> theFilter,
			boolean thePresetFlag) throws SimilarityMeasureException, ConnectorException, IOException{
		
		
		final Double aSTSrate = null; //0.2;  // 20% STS; 80% LTS
		
		final String aCaseBaseDataFileDelimitor = ",";
		
		File aCsvFile;
	  	String aCBFileName;
	  	
	  	if (!thePresetFlag){
	   		aCBFileName = Utils.getCustomDataPath(CUSTOM_DATAPATH.TEMP) + 
	 		Integer.toString(theNumberOfCases) + "cases_generatedDataset.csv";
	  		
	  		System.out.print("DEBUG: Generating Dataset...");
	  		
	  		aCsvFile = GeneratedDataset.generateAndWriteDatasetFile(theNumberOfCases, aSTSrate, aCBFileName);
	  		System.out.println("done.");
	  	} else {		  			  	
		  	aCsvFile = GeneratedDataset.getPredefinedDatasetFile(theNumberOfCases, theIteration);
	  	}
	  	
	  	System.out.println("DEBUG: Generated Dataset file: " + aCsvFile.getAbsolutePath());
		
		String aTherapyLabel = GeneratedDataset.THERAPY_LABEL;
		
//		ArrayList<String> anAttributeBlackList = I2b2Connector.getI2b2FactsMetaDataList();		
		
		// Setting similarity measure specific parameters
		AbstractSimilarityMeasureConfiguration aSimilarityMeasureConfig;
		switch (theSimilarityMeasure){
		case RANDOM:
		case HEOM:
			aSimilarityMeasureConfig = null;
			break;
		case DVDM:
		case HVDM1:
		case HVDM2:
		case HVDM3:
		case IVDM:
		case WVDM:
			Set<String> aTherapyAllowedValueList = new TreeSet<>();
			aTherapyAllowedValueList.add("arm A");
			aTherapyAllowedValueList.add("arm B");
			aSimilarityMeasureConfig = new VDMConfig(aTherapyLabel, aTherapyAllowedValueList);
			break;
		case KAPLANMEIER:
			boolean aStepShapeFlag = true;
			boolean aTicksFlag = true;
			KaplanMeierTargetConfig aKMConfig = new KaplanMeierTargetConfig(SURVIVALTARGET.GENERATED);
			aSimilarityMeasureConfig = new KaplanMeierConfig(aKMConfig, aStepShapeFlag, aTicksFlag);
			break;
			
		default:
			throw new SimilarityMeasureException("Unknown Similarity Measure: " + theSimilarityMeasure.getName());
//			aSimilarityMeasureConfig = null;
//			break;
		}

  		return createCustomCsvSession(
  				CUSTOM_DATAPATH.GENERATED.name(), 
  				theSimilarityMeasure, 
  				aSimilarityMeasureConfig,
  				aCsvFile, 
  				aCaseBaseDataFileDelimitor, 
  				GeneratedDataset.getCaseDescriptionList(), 
  				GeneratedDataset.getSolutionDescriptionList(),
  				aTherapyLabel,
  				theFilter);
		}
	
  
  
	  private static MyCbrBuilderSession createCustomCsvSession(
		  String aSessionLabel, 
		  SimilarityMeasure theSimilarityMeasure, 
		  AbstractSimilarityMeasureConfiguration theSimilarityMeasureConfig,
		  File theCsvFile, 
		  String theCsvDelimitor,
		  ArrayList<String> theCaseDescriptionList,
		  ArrayList<String> theSolutionDescriptionList,
		  String theOutputClassLabel,
		  HashMap<String, String> theFilter) throws ConnectorException{	
		  
		  	Utils.printTitle("--- GENERATING CUSTOM SESSION (CSV based Import): '" + aSessionLabel + "' ---");
		  	
		  	System.out.println("DEBUG: CSV File for Import: " + theCsvFile.getName());
			// Check if file exists
			if (!theCsvFile.exists()){
				System.err.println("File not found: " + theCsvFile.getAbsolutePath());
				MyCBRBuilderFxElements.showExceptionDialog(new FileNotFoundException("File not found: " + theCsvFile.getAbsolutePath()));
				return null;
			} 
			
			MyCbrBuilderSession aSession = new MyCbrBuilderSession(aSessionLabel);
			aSession.setCaseDescriptionList(theCaseDescriptionList);
			aSession.setSolutionDescriptionList(theSolutionDescriptionList);
			aSession.setSimilarityMeasure(theSimilarityMeasure, theSimilarityMeasureConfig);
			
			// creating white-list for csv import
			ArrayList<String> aWhiteList = new ArrayList<String>();
			aWhiteList.addAll(theCaseDescriptionList); 
			aWhiteList.addAll(theSolutionDescriptionList);

//			CsvConnector aConnector = new CsvConnector(aSession.getConcept(), theCsvFile, theCsvDelimitor, aWhiteList);
			WekaCsvConnector aConnector = new WekaCsvConnector(aSession.getConcept(), theCsvFile, theCsvDelimitor, aWhiteList);
			aSession.setConnector(aConnector);
			aConnector.fetchCaseBase(theFilter);
			
			MyCbrUtils.printConceptDescription(aSession.getConcept());
			
			aSession.setOutputClassLabel(theOutputClassLabel);	

			// Some debug info
//			MyCbrUtils.printConceptDescription(aSession.getConcept());
//			CaseBaseUtils.printCaseBaseData(aConnector.getCaseBase());
				
			return aSession;
	  }



		  
		  

}