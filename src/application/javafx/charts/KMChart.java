/**
 * (2019) Heidelberg University, Institute of Medical Biometry and Informatics (IMBI), Germany
 * 
 * This file is part of myCbrBuilder.
 *  
 * myCbrBuilder is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * myCbrBuilder is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with vivaGen. If not, see <http://www.gnu.org/licenses/>.
 * 
 **/
package application.javafx.charts;

import java.awt.BorderLayout;
import java.awt.Color;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.UIManager;

import org.cliommics.vivagen.Instances;
import org.cliommics.vivagen.gui.SurvivalDiagram;
import org.cliommics.vivagen.survivalanalysis.SurvivalList;

import similarity.computation.ComputationKM;
import similarity.computation.NominalAttribute;
import similarity.computation.config.KaplanMeierTargetConfig;


/**
 * Class to visualize the Kaplan-Meier Diagrams of attribute (nominal) values. 
 * 
 * @author Christian Karmen
 *
 */
public class KMChart {
	protected SurvivalDiagram itsSurvivalDiagram;
	protected KaplanMeierTargetConfig itsKMTargetConfig;
	protected Instances itsInstances;
	
	//private boolean itsStepShapeFlag = SurvivalDiagram.LINEAR_SHAPE;
	private boolean itsStepShapeFlag = SurvivalDiagram.STEP_SHAPE;
	private boolean itsTicksFlag = SurvivalDiagram.SUPPRESS_TICKS;
//	private boolean itsTicksFlag = SurvivalDiagram.DRAW_TICKS;
	
	
	
	/**
	 * Creates a Kaplan-Meier Diagram.
	 * 
	 * @param theInstances Case Base in form of weka Instances
	 * @param theKMTargetConfig Kaplan-Meier configuration
	 */
	public KMChart(Instances theInstances, KaplanMeierTargetConfig theKMTargetConfig){
		itsInstances = theInstances;
		itsSurvivalDiagram = new SurvivalDiagram(itsStepShapeFlag, itsTicksFlag);
		itsKMTargetConfig = theKMTargetConfig;
	}

	
	protected SurvivalDiagram getSurvivalDiagram(){
		return itsSurvivalDiagram;
	}
	
    /**
     * Adds the Survival List for the given nominal attribute. 
     * 
     * @param theNominalAttribute
     */
    public void addSurvivalList(NominalAttribute theNominalAttribute){
    	SurvivalList surv = ComputationKM.getSurvivalList(itsInstances, itsKMTargetConfig, theNominalAttribute);
    	//System.out.println(theNominalAttribute + ": " + surv.getLength());
        itsSurvivalDiagram.addSurvivalList(surv);
    }
    
    
    /**
     * Clears the current Survival list.
     * 
     */
    public void clearSurvivalList(){
        itsSurvivalDiagram.clearSurvivalList();
    }
    
    
    
	
    /**
     * Draws a KM diagram of the given parameter with all given values.
     * 
     * @param theParameter the parameter of interest
     * @param theNominalAttributeList
     */
	public void drawKM(String theParameter, ArrayList<NominalAttribute> theNominalAttributeList){	

		for (NominalAttribute aNA : theNominalAttributeList) {
			addSurvivalList(aNA);
		}
		
		drawKMFrame(theParameter);
	}
	
	
	/**
	 * Draws a KM diagram of the given parameter with all possible values in sorted order.
	 * 
	 * @param theParameter the parameter of interest
	 */
	public void drawKM(String theParameter){
		ArrayList<NominalAttribute> aNominalAttributeList = new ArrayList<>();
		
		for (String anAttributelabel : ComputationKM.getAttributeLabels(itsInstances, theParameter, true)) {
			aNominalAttributeList.add(new NominalAttribute(theParameter, anAttributelabel, true));
		}
		
		drawKM(theParameter, aNominalAttributeList);
	}
	
	
	
	/**
	 * Draws a KM diagram with the parameter "survival" and values "dead" and not "dead".
	 * Useful as a reference area for defining attribute weights.
	 */
	public void drawKM__SurvivalTarget_ReferenceArea(){		
	    drawKM__oneAgainstOthers(itsKMTargetConfig.TargetStatusColumn, itsKMTargetConfig.TargetStatusIndicator);
	}
	

	
	/**
	 * Draws a KM diagram with the given parameter and the given value against the other values. 
	 * 
	 * @param theParameter the parameter of interest
	 * @param theParameterValue	the parameter value to draw itself and all others
	 */
	public void drawKM__oneAgainstOthers(String theParameter, String theParameterValue){		
	    ArrayList<NominalAttribute> aNominalAttributeList = new ArrayList<>();
	    aNominalAttributeList.add(new NominalAttribute(theParameter, theParameterValue, true));
	    aNominalAttributeList.add(new NominalAttribute(theParameter, theParameterValue, false));	
		
		for (NominalAttribute aNA : aNominalAttributeList) {
			addSurvivalList(aNA);
		}
		
		drawKMFrame(theParameter);
		
		
//		NominalAttribute NA1 = aNominalAttributeList.get(0);
//		NominalAttribute NA2 = aNominalAttributeList.get(1);
//		System.out.println("DEBUG: Area between Survival Curves: " + getAreaBetweenSurvivalCurves(NA1, NA2));
	}
	
    
    /**
     * Draws all available KM diagrams in a GUI frame.  
     * 
     * @param theTitle Title of the frame
     */
    private void drawKMFrame(String theTitle){
    	JFrame aFrame;
    	JPanel aPanel = new JPanel(new BorderLayout());
    	
//    	aPanel.setAutoscrolls(true);
    	setLookAndFeel();
    	
    	try {
    		aFrame = new JFrame(theTitle);
    		aFrame.setSize(700, 600);
    		//aFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    		aFrame.getContentPane().setBackground(Color.WHITE);
    		aFrame.setResizable(true);
    		JFrame.setDefaultLookAndFeelDecorated(false);
    		
    		//aFrame.add(itsSurvivalDiagram);
    		aPanel.add(itsSurvivalDiagram);
    		aPanel.setBackground(Color.WHITE);
    		aFrame.add(aPanel);

    		aFrame.setVisible(true);
    		aFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE); //JFrame.EXIT_ON_CLOSE
    		
		} catch (Exception e) {
			e.printStackTrace();
		}
    	
    }
    
    /**
     * Sets the 'Look and Feel' of the GUI.
     */
    private void setLookAndFeel(){
    	String aLookAndFeel;
//    	aLookAndFeel = UIManager.getSystemLookAndFeelClassName();
//    	aLookAndFeel = UIManager.getCrossPlatformLookAndFeelClassName();
//    	aLookAndFeel = "com.sun.java.swing.plaf.motif.MotifLookAndFeel";
//    	aLookAndFeel = "com.sun.java.swing.plaf.gtk.GTKLookAndFeel";	// geht nicht
    	aLookAndFeel = "javax.swing.plaf.metal.MetalLookAndFeel";
    	try {
    		UIManager.setLookAndFeel(aLookAndFeel);
    	} catch (Exception e) {
            System.err.println("Couldn't get specified look and feel (" + aLookAndFeel + "), for some reason.");
            System.err.println("Using the default look and feel.");
            e.printStackTrace();
    	} 
    }

}
