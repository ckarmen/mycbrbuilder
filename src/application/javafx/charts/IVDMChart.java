/**
 * (2019) Heidelberg University, Institute of Medical Biometry and Informatics (IMBI), Germany
 * 
 * This file is part of myCbrBuilder.
 *  
 * myCbrBuilder is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * myCbrBuilder is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with vivaGen. If not, see <http://www.gnu.org/licenses/>.
 * 
 **/
package application.javafx.charts;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Map.Entry;
import java.util.TreeMap;

import core.MyCbrUtils;
import de.dfki.mycbr.core.DefaultCaseBase;
import de.dfki.mycbr.core.model.SimpleAttDesc;
import de.dfki.mycbr.core.model.SymbolDesc;
import de.dfki.mycbr.core.similarity.SymbolFct;
import exceptions.CaseBaseException;
import javafx.scene.Scene;
import javafx.scene.chart.AreaChart;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Tooltip;
import javafx.stage.Stage;
import similarity.computation.ComputationVDM;
import similarity.computation.config.VDMConfig;
import similarity.fct.IVDMDiscretizedFct;

/**
 * Class to visualize the IVDM's probabilities of an attribute value to be in a output class. 
 * 
 * @author Christian Karmen
 *
 */
public class IVDMChart extends AreaChart<String,Number>{
	
    protected SimpleAttDesc itsAttributeDesc;    
    protected DefaultCaseBase itsTrainingCaseBase;
    protected VDMConfig itsVDMConfig;
    
    protected String itsAttributeDescLabel;

	
    /**
     * Creates a IVDM chart of probabilities.
     * 
     * @param theAttributeDesc Attribute if interest 
     * @param theVDMConfig VDM specific configuration
     * @param theTrainingCaseBase CaseBase containing the training data
     */
    public IVDMChart(SimpleAttDesc theAttributeDesc, VDMConfig theVDMConfig, DefaultCaseBase theTrainingCaseBase){
    	super(new CategoryAxis(), new NumberAxis());
    	  	
    	itsAttributeDesc = theAttributeDesc;
    	itsAttributeDescLabel = itsAttributeDesc.getName();
    	itsVDMConfig = theVDMConfig;
    	itsTrainingCaseBase = theTrainingCaseBase;
    	
    	this.setTitle(itsAttributeDescLabel);
        setAxisLayout();
	}
	
	
	
    /**
     * Sets the area chart axis layout.
     */
    protected void setAxisLayout(){
    	CategoryAxis xAxis = (CategoryAxis) getXAxis();
        NumberAxis yAxis = (NumberAxis) getYAxis();
                        
        yAxis.setLabel("Probability of Class");
        
        yAxis.setAutoRanging(false);
        yAxis.setLowerBound(0.0);
        yAxis.setUpperBound(1.0);
        yAxis.setForceZeroInRange(false);
//        yAxis.setPadding(new Insets(50, 0, 0, 0));
        
        yAxis.setMinorTickVisible(true);
        yAxis.setTickUnit(0.1);                
        yAxis.setTickLabelsVisible(true);
        yAxis.setTickMarkVisible(true);
        
        // Set up fancy animations
        yAxis.setAnimated(true);
        xAxis.setAnimated(false);        
        this.setAnimated(false);
    }
	
	
	
    /**
     * Adds output class data for a specific output value. 
     * Also sets some extra meta data about number of cases and occurrences.  
     * 
     * @param theOutputClassAttributeValue specific output value
     * @param thePaxcTreeMap  Paxc data to process
     * @param theNaxcTreeMap Naxc data to process
     * @param theNaxTreeMap Nax data to process
     */
    @SuppressWarnings({"unchecked", "rawtypes"})
	protected void addOutputClass(
			String theOutputClassAttributeValue, 
			TreeMap<String, Double> thePaxcTreeMap, 
			TreeMap<String, Integer> theNaxcTreeMap, 
			TreeMap<String, Integer> theNaxTreeMap){
		XYChart.Series anOutputClass = new XYChart.Series();
        anOutputClass.setName(theOutputClassAttributeValue);
        
        Data aData;
        
        for (Entry<String, Double> aPaxcTreeEntry : thePaxcTreeMap.entrySet()) {
        	aData = new XYChart.Data(aPaxcTreeEntry.getKey(), aPaxcTreeEntry.getValue());
        	aData.setExtraValue(new String(
        			"occured " + theNaxcTreeMap.get(aPaxcTreeEntry.getKey()) + " time(s) in " + theNaxTreeMap.get(aPaxcTreeEntry.getKey()) + " case(s)"));
        	anOutputClass.getData().add(aData);
		}

        this.getData().add(anOutputClass);
    }
    
    
    
    /**
     * Creates tool tips on bullets with some extra information, e.g. number of cases. 
     * 
     */
    protected void createTooltips(){
    	for (final Series<String, Number> series : getData()) {
    	     for (final Data<String, Number> data : series.getData()) {
    	          Tooltip tooltip = new Tooltip();
//    	          tooltip.setText(data.getYValue().toString());
    	          tooltip.setText((String)data.getExtraValue());
    	          Tooltip.install(data.getNode(), tooltip);                    
    	     }
    	}
    }
	
    
    
    
    /**
     * Calculates the probability values for the DVDM bar chart. 
     * @throws CaseBaseException Error while access to case base
     * 
     */
    protected void calculatePropabilities() throws CaseBaseException{
		   int anIntervalIndex;
			String aDiscreteLabel;
			String aLeftBound = "", aRightBound = "";
			double aPaxc;
			int aNax, aNaxc;
			DefaultCaseBase aCB;
			SymbolFct aDiscretizedSymbolFct;
			
			TreeMap<String, Double> aPaxcTreeMap = new TreeMap<String, Double>();
			TreeMap<String, Integer> aNaxcTreeMap = new TreeMap<String, Integer>();
			TreeMap<String, Integer> aNaxTreeMap = new TreeMap<String, Integer>();
							
			IVDMDiscretizedFct anIVDMFct = null;
			Object aSimFct = MyCbrUtils.getActiveFct(itsAttributeDesc.getOwner(), itsAttributeDescLabel);
			
			// Allowed Input Attributes types for DVDM are Numbers and (regular) Symbols
			if (aSimFct instanceof IVDMDiscretizedFct){
				anIVDMFct = ((IVDMDiscretizedFct)aSimFct);
				aDiscretizedSymbolFct = anIVDMFct.getDiscretizedSymbolFct();
				aCB = anIVDMFct.getDiscretizedMiniCaseBase();
			} else if (aSimFct instanceof SymbolFct){
				aDiscretizedSymbolFct = (SymbolFct)aSimFct;
				aCB = itsTrainingCaseBase;
			} else {
				System.err.println("ERROR: No compatible Fct found for: " + itsAttributeDescLabel);
				return;
			}	
			
			SymbolDesc anInputClassDesc  = aDiscretizedSymbolFct.getDesc(); // identical to variable anAttributeDesc 

			DecimalFormat aDF = new DecimalFormat("###.##");
			aDF.setDecimalSeparatorAlwaysShown(false);

			ArrayList<String> myInputList = new ArrayList<String>(anInputClassDesc.getAllowedValues());
//			ArrayList<String> myOutputList = new ArrayList<String>(anOutputClassDesc.getAllowedValues());
			for (String anAllowedOutputClassValue : itsVDMConfig.OutputClassValueList) {
				for (String anIntervallLabel : myInputList) {	
					aDiscreteLabel = anIntervallLabel;
					anIntervalIndex = Integer.parseInt(anIntervallLabel.substring(1))-1;
					
					if (anIVDMFct != null){
					
//					// debug infos
//					System.out.println("aDiscreteFct: " + aDiscreteFct.getName() + 
//							"\n\t=>[Number of intervals]: " + aDiscreteFct.getNumberOfDiscreteIntervals() +
//							"\n\t=>[Width(1)]: " + aDiscreteFct.getIntervalWidth(1) + 
//							"\n\t=>[IntervalIndex]: " + anIntervalIndex +
//							"\n\t=>[MinInterval]:" + aDiscreteFct.getMinIntervalValue(anIntervalIndex) +
//							"\n\t=>[MaxInterval]: " + aDiscreteFct.getMaxIntervalValue(anIntervalIndex));
					
					// add range to bar label
					aDiscreteLabel += "(" 
							+ aDF.format(anIVDMFct.getMinIntervalValue(anIntervalIndex)) 
							+ "-" 
							+ aDF.format(anIVDMFct.getMaxIntervalValue(anIntervalIndex)) 
							+ ")";
					aLeftBound = "D0(" 
							+ aDF.format(anIVDMFct.getMinIntervalValue(0) - anIVDMFct.getIntervalWidth(1) / 2) 
							+ ")";
					int aMaxIndex = anIVDMFct.getNumberOfDiscreteIntervals() + 1;
					aRightBound = "D" + new Integer(aMaxIndex) + "("
							+ aDF.format(anIVDMFct.getMinIntervalValue(aMaxIndex) - anIVDMFct.getIntervalWidth(1) / 2) 
							+ ")";
					
					}
	
					// get probability that attribute a has value x and is in class c
					aPaxc = ComputationVDM.getPaxc(aCB, anInputClassDesc, anIntervallLabel, itsVDMConfig, anAllowedOutputClassValue);
					aPaxcTreeMap.put(aDiscreteLabel, aPaxc);
					
					// get number of cases (overall and per output-class)
					aNax = ComputationVDM.getNax(aCB, anInputClassDesc, anIntervallLabel, itsVDMConfig);
					aNaxc = ComputationVDM.getNaxc(aCB, anInputClassDesc, anIntervallLabel, itsVDMConfig, anAllowedOutputClassValue);
					aNaxTreeMap.put(aDiscreteLabel, aNax);
					aNaxcTreeMap.put(aDiscreteLabel, aNaxc);
					
				}
				aPaxcTreeMap.put(aLeftBound, 0d);
				aPaxcTreeMap.put(aRightBound, 0d);
				addOutputClass(anAllowedOutputClassValue, aPaxcTreeMap, aNaxcTreeMap, aNaxTreeMap);
			}	
    }
    
    
	    /**
	     * Calculates the IVDM probabilities and shows the result in a area chart.
	     * @throws CaseBaseException Error while access to case base
	     * 
	     */
    	public void showIVDMAreaChart() throws CaseBaseException{    		
            Scene scene  = new Scene(this,600,300);
    		Stage aStage = new Stage();
    		
    		aStage.setTitle("IVDM Area Chart");
            aStage.setScene(scene);
            aStage.show();

    		calculatePropabilities();
    		
    		createTooltips();
	   }

}
