/**
 * (2019) Heidelberg University, Institute of Medical Biometry and Informatics (IMBI), Germany
 * 
 * This file is part of myCbrBuilder.
 *  
 * myCbrBuilder is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * myCbrBuilder is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with vivaGen. If not, see <http://www.gnu.org/licenses/>.
 * 
 **/
package application.javafx.charts;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

import core.MyCbrUtils;
import de.dfki.mycbr.core.DefaultCaseBase;
import de.dfki.mycbr.core.model.SimpleAttDesc;
import de.dfki.mycbr.core.model.SymbolDesc;
import de.dfki.mycbr.core.similarity.SymbolFct;
import exceptions.CaseBaseException;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Tooltip;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import similarity.computation.ComputationVDM;
import similarity.computation.config.VDMConfig;
import similarity.fct.DVDMDiscretizedFct;


/**
 * Class to visualize the DVDM's probabilities of an attribute value to be in a output class. 
 * 
 * @author Christian Karmen
 *
 */
public class DVDMChart extends BarChart<String, Number> {      
    /**
     * Registry for text nodes of the bars
     */
    protected Map<Node, Node> nodeMap = new HashMap<>();
    
    protected SimpleAttDesc itsAttributeDesc;    
    protected DefaultCaseBase itsTrainingCaseBase;
    protected VDMConfig itsVDMConfig;
    
    protected String itsAttributeDescLabel;
    
    
    
    /**
     * Creates a DVDM chart of probabilities.
     * 
     * @param theAttributeDesc Attribute if interest 
     * @param theVDMConfig VDM specific configuration
     * @param theTrainingCaseBase CaseBase containing the training data
     */
    public DVDMChart(SimpleAttDesc theAttributeDesc, VDMConfig theVDMConfig, DefaultCaseBase theTrainingCaseBase){
    	super(new CategoryAxis(), new NumberAxis());
    	
    	itsAttributeDesc = theAttributeDesc;
    	itsAttributeDescLabel = itsAttributeDesc.getName();
    	itsVDMConfig = theVDMConfig;
    	itsTrainingCaseBase = theTrainingCaseBase;
    	
    	this.setTitle(itsAttributeDescLabel);
        setAxisLayout();
	}
    
    
    /**
     * Sets the bar chart axis layout.
     */
    protected void setAxisLayout(){
    	CategoryAxis xAxis = (CategoryAxis) getXAxis();
        NumberAxis yAxis = (NumberAxis) getYAxis();
        
        yAxis.setLabel("Probability of Class");
        
        yAxis.setAutoRanging(false);
        yAxis.setLowerBound(0.0);
        yAxis.setUpperBound(1.0);
        yAxis.setForceZeroInRange(false);
        yAxis.setPadding(new Insets(50, 0, 0, 0));
        
        yAxis.setMinorTickVisible(true);
        yAxis.setTickUnit(0.1);                
        yAxis.setTickLabelsVisible(true);
        yAxis.setTickMarkVisible(true);
//        yAxis.setTickLabelFormatter(new NumberAxis.DefaultFormatter(yAxis, "", ""));
        
        
//      xAxis.setLabel(itsAttributeLabel);
        xAxis.setAutoRanging(true);
        xAxis.setTickLabelsVisible(true);
        xAxis.setTickMarkVisible(false);       
        
        
        // Set up fancy animations
        yAxis.setAnimated(true);
        xAxis.setAnimated(false);        
        this.setAnimated(true);


        setVerticalGridLinesVisible(false);
//        setAlternativeColumnFillVisible(true);
//        setAlternativeRowFillVisible(true);
    }
 

    
    /**
     * Adjust text of bars, position them on top
     */
    @Override
    protected void layoutPlotChildren() {

        super.layoutPlotChildren();
        Text aTextNode;
        
        for (Node bar : nodeMap.keySet()) {
        	aTextNode = (Text)nodeMap.get(bar);
//        	aTextNode.setTextAlignment(TextAlignment.CENTER);
        	double barLength = bar.getBoundsInParent().getMaxX() - bar.getBoundsInParent().getMinX();
        	double textLength = aTextNode.getBoundsInParent().getMaxX() - aTextNode.getBoundsInParent().getMinX();
        	double x = bar.getBoundsInParent().getMinX() + (barLength / 2) - (textLength / 2);
        	double y = bar.getBoundsInParent().getMinY()-15;
        	if (y < 0) {
        		y = 0;
        	}
        	aTextNode.relocate(x, y);
        }

    }
    
    /**
     * Add text for bars
     */
    @Override
    protected void seriesAdded(Series<String, Number> series, int seriesIndex) {

        super.seriesAdded(series, seriesIndex);

        for (int j = 0; j < series.getData().size(); j++) {

            Data<String, Number> item = series.getData().get(j);
            
            DecimalFormat aDF = new DecimalFormat(".000");
            aDF.setDecimalFormatSymbols(new DecimalFormatSymbols(Locale.ENGLISH));
            aDF.setDecimalSeparatorAlwaysShown(true);

            Node text = new Text(aDF.format(item.getYValue()));
            nodeMap.put(item.getNode(), text);
            getPlotChildren().add(text);

        }

    }
    
    /**
     * Remove text of bars
     */
    @Override
    protected void seriesRemoved(final Series<String, Number> series) {

        for (Node bar : nodeMap.keySet()) {

            Node text = nodeMap.get(bar);
            getPlotChildren().remove(text);

        }

        nodeMap.clear();

        super.seriesRemoved(series);
    }
    
    
    /**
     * Adds output class data for a specific output value. 
     * Also sets some extra meta data about number of cases and occurrences.  
     * 
     * @param theOutputClassAttributeValue specific output value
     * @param thePaxcTreeMap  Paxc data to process
     * @param theNaxcTreeMap Naxc data to process
     * @param theNaxTreeMap Nax data to process
     */
    @SuppressWarnings({"unchecked", "rawtypes"})
	protected void addOutputClass(
			String theOutputClassAttributeValue, 
			TreeMap<String, Double> thePaxcTreeMap, 
			TreeMap<String, Integer> theNaxcTreeMap, 
			TreeMap<String, Integer> theNaxTreeMap){
    	
		XYChart.Series anOutputClass = new XYChart.Series();
        anOutputClass.setName(theOutputClassAttributeValue);
        
        Data aData;
        
        for (Entry<String, Double> aPaxcTreeEntry : thePaxcTreeMap.entrySet()) {
        	aData = new XYChart.Data(aPaxcTreeEntry.getKey(), aPaxcTreeEntry.getValue());
        	aData.setExtraValue(new String("occured " + theNaxcTreeMap.get(aPaxcTreeEntry.getKey()) + " time(s) in " + theNaxTreeMap.get(aPaxcTreeEntry.getKey()) + " case(s)"));
        	anOutputClass.getData().add(aData);
		}

        getData().add(anOutputClass);
    }
    

    
    /**
     * Creates tool tips on bars with some extra information, e.g. number of cases. 
     * 
     */
    protected void createTooltips(){
    	for (final Series<String, Number> series : getData()) {
    	     for (final Data<String, Number> data : series.getData()) {
    	          Tooltip tooltip = new Tooltip();
//    	          tooltip.setText(data.getYValue().toString());
    	          tooltip.setText((String)data.getExtraValue());
    	          Tooltip.install(data.getNode(), tooltip);                    
    	     }
    	}
    }
    


    /**
     * Calculates the probability values for the DVDM bar chart. 
     * @throws CaseBaseException Error while access to case base
     * 
     */
    protected void calculatePropabilities() throws CaseBaseException{
	   double aPaxc;
	   int aNax, aNaxc;
	   
	   int anIntervalIndex;
	   String aDiscreteLabel;
	   SymbolFct aDiscretizedSymbolFct;
	   DefaultCaseBase aTrainingCaseBase;
	   
	   TreeMap<String, Double> aPaxcTreeMap = new TreeMap<String, Double>();
	   TreeMap<String, Integer> aNaxcTreeMap = new TreeMap<String, Integer>();
	   TreeMap<String, Integer> aNaxTreeMap = new TreeMap<String, Integer>();

	   DVDMDiscretizedFct aDVDMFct = null;
	   Object aSimFct = MyCbrUtils.getActiveFct(itsAttributeDesc.getOwner(), itsAttributeDescLabel);
	   
		// Allowed Input Attributes types for DVDM are Numbers and (regular) Symbols
		if (aSimFct instanceof DVDMDiscretizedFct){
			aDVDMFct = ((DVDMDiscretizedFct)aSimFct);
			aDiscretizedSymbolFct = aDVDMFct.getDiscretizedSymbolFct();
			aTrainingCaseBase = aDVDMFct.getDiscretizedMiniCaseBase();
		} else if (aSimFct instanceof SymbolFct){
			aDiscretizedSymbolFct = (SymbolFct)aSimFct;
			aTrainingCaseBase = itsTrainingCaseBase;
		} else {
			System.err.println("ERROR: No compatible Fct found for: " + itsAttributeDescLabel);
			return;
		}	
		
		SymbolDesc anInputClassDesc  = aDiscretizedSymbolFct.getDesc(); // identical to variable anAttributeDesc			
		ArrayList<String> myInputList = new ArrayList<String>(anInputClassDesc.getAllowedValues());
	   
		
		DecimalFormat aDF = new DecimalFormat("###.##");
		aDF.setDecimalSeparatorAlwaysShown(false);
		
		//for (String anAllowedOutputClassValue : myOutputList) {
		for (String anAllowedOutputClassValue : itsVDMConfig.OutputClassValueList) {
			for (String anIntervalLabel : myInputList) {	
				
				// in case of numbers: calculate and show the interval ranges
				aDiscreteLabel = anIntervalLabel;
				//if (aDiscretizedSymbolFct instanceof DiscretizedSymbolFct){
				if (aDVDMFct != null){
				
					anIntervalIndex = Integer.parseInt(anIntervalLabel.substring(1))-1;

//					// debug infos
//					System.out.println("aDiscretFct: " + aDiscretFct.getName() + 
//							"\n\t=>[Number of classes]: " + aDiscretFct.getNumberOfClasses() +
//							"\n\t=>[Width]: " + aDiscretFct.getWidth() + 
//							"\n\t=>[IntervalIndex]: " + anIntervalIndex +
//							"\n\t=>[MinInterval]:" + aDiscretFct.getMinIntervalValue(anIntervalIndex) +
//							"\n\t=>[MaxInterval]: " + aDiscretFct.getMaxIntervalValue(anIntervalIndex));
					
					// add range to bar label
					aDiscreteLabel += "(" 
					+ aDF.format(aDVDMFct.getMinIntervalValue(anIntervalIndex)) 
					+ "-" 
					+ aDF.format(aDVDMFct.getMaxIntervalValue(anIntervalIndex)) 
					+ ")";
				}
				
				aPaxc = ComputationVDM.getPaxc(aTrainingCaseBase, anInputClassDesc, anIntervalLabel, itsVDMConfig, anAllowedOutputClassValue);
				aPaxcTreeMap.put(aDiscreteLabel, aPaxc);
				
				// get number of cases 
				aNax = ComputationVDM.getNax(aTrainingCaseBase, anInputClassDesc, anIntervalLabel, itsVDMConfig);
				aNaxc = ComputationVDM.getNaxc(aTrainingCaseBase, anInputClassDesc, anIntervalLabel, itsVDMConfig, anAllowedOutputClassValue);
				aNaxTreeMap.put(aDiscreteLabel, aNax);
				aNaxcTreeMap.put(aDiscreteLabel, aNaxc);
			}
			addOutputClass(anAllowedOutputClassValue, aPaxcTreeMap, aNaxcTreeMap, aNaxTreeMap);
		}
    }
    
    

    /**
     * Calculates the DVDM probabilities and shows the result in a bar chart.
     * @throws CaseBaseException Error while access to case base
     * 
     */
    public void showDVDMBarChart() throws CaseBaseException{		
        Scene scene  = new Scene(this,600,300);
		Stage aStage = new Stage();
		
		aStage.setTitle("DVDM Bar Chart");
        aStage.setScene(scene);
        aStage.show();

		calculatePropabilities();
		
		createTooltips();
	   }

}
