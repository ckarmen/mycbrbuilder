/**
 * (2019) Heidelberg University, Institute of Medical Biometry and Informatics (IMBI), Germany
 * 
 * This file is part of myCbrBuilder.
 *  
 * myCbrBuilder is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * myCbrBuilder is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with vivaGen. If not, see <http://www.gnu.org/licenses/>.
 * 
 **/
package application.javafx.charts;

import de.dfki.mycbr.core.DefaultCaseBase;
import de.dfki.mycbr.core.model.SimpleAttDesc;
import javafx.scene.Scene;
import javafx.scene.chart.AreaChart;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.NumberAxis;
import javafx.scene.control.Tooltip;
import javafx.stage.Stage;
import similarity.computation.config.VDMConfig;

/**
 * Class to visualize the WVDM's probabilities of an attribute value to be in a output class. 
 * 
 * @author Christian Karmen
 *
 */
public class WVDMChart extends AreaChart<String, Number> {
	
    protected SimpleAttDesc itsAttributeDesc;    
    protected DefaultCaseBase itsTrainingCaseBase;
    protected VDMConfig itsVDMConfig;
    
    protected String itsAttributeDescLabel;
    
    
    /**
     * Creates a WVDM chart of probabilities.
     * 
     * @param theAttributeDesc Attribute if interest 
     * @param theVDMConfig VDM specific configuration
     * @param theTrainingCaseBase CaseBase containing the training data
     */
	public WVDMChart(SimpleAttDesc theAttributeDesc, VDMConfig theVDMConfig, DefaultCaseBase theTrainingCaseBase) {
		super(new CategoryAxis(), new NumberAxis());
		
    	itsAttributeDesc = theAttributeDesc;
    	itsAttributeDescLabel = itsAttributeDesc.getName();
    	itsVDMConfig = theVDMConfig;
    	itsTrainingCaseBase = theTrainingCaseBase;
    	
    	this.setTitle(itsAttributeDescLabel);
        setAxisLayout();
	}
	
	
    /**
     * Sets the area chart axis layout.
     */
    protected void setAxisLayout(){
    	CategoryAxis xAxis = (CategoryAxis) getXAxis();
        NumberAxis yAxis = (NumberAxis) getYAxis();
                        
        yAxis.setLabel("Probability of Class");
        
        yAxis.setAutoRanging(false);
        yAxis.setLowerBound(0.0);
        yAxis.setUpperBound(1.0);
        yAxis.setForceZeroInRange(false);
//        yAxis.setPadding(new Insets(50, 0, 0, 0));
        
        yAxis.setMinorTickVisible(true);
        yAxis.setTickUnit(0.1);                
        yAxis.setTickLabelsVisible(true);
        yAxis.setTickMarkVisible(true);
        
        // Set up fancy animations
        yAxis.setAnimated(true);
        xAxis.setAnimated(false);        
        this.setAnimated(false);
    }
    
    
    /**
     * Creates tool tips on bullets with some extra information, e.g. number of cases. 
     * 
     */
    protected void createTooltips(){
    	for (final Series<String, Number> series : getData()) {
    	     for (final Data<String, Number> data : series.getData()) {
    	          Tooltip tooltip = new Tooltip();
//    	          tooltip.setText(data.getYValue().toString());
    	          tooltip.setText((String)data.getExtraValue());
    	          Tooltip.install(data.getNode(), tooltip);                    
    	     }
    	}
    }
    
    
    /**
     * Calculates the probability values for the DVDM bar chart. 
     * 
     */
    protected void calculatePropabilities(){
    	// TODO
    }
	
    /**
     * Calculates the IVDM probabilities and shows the result in a area chart.
     * 
     */
	public void showWVDMAreaChart(){    		
       Scene scene  = new Scene(this,600,300);
       Stage aStage = new Stage();
		
       aStage.setTitle("WVDM Area Chart");
       aStage.setScene(scene);
       aStage.show();

		calculatePropabilities();
		
		createTooltips();
  }

}
