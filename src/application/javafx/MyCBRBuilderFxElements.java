/**
 * (2019) Heidelberg University, Institute of Medical Biometry and Informatics (IMBI), Germany
 * 
 * This file is part of myCbrBuilder.
 *  
 * myCbrBuilder is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * myCbrBuilder is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with vivaGen. If not, see <http://www.gnu.org/licenses/>.
 * 
 **/
package application.javafx;

import java.io.File;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import org.cliommics.vivagen.Instances;

import application.javafx.charts.DVDMChart;
import application.javafx.charts.IVDMChart;
import application.javafx.charts.KMChart;
import application.javafx.charts.WVDMChart;
import core.CaseBaseUtils;
import core.MyCbrBuilderSession;
import core.MyCbrUtils;
import core.Utils;
import core.connectors.AbstractConnector;
import de.dfki.mycbr.core.Project;
import de.dfki.mycbr.core.casebase.Attribute;
import de.dfki.mycbr.core.casebase.Instance;
import de.dfki.mycbr.core.model.AttributeDesc;
import de.dfki.mycbr.core.model.DoubleDesc;
import de.dfki.mycbr.core.model.FloatDesc;
import de.dfki.mycbr.core.model.IntegerDesc;
import de.dfki.mycbr.core.model.SimpleAttDesc;
import de.dfki.mycbr.core.model.StringDesc;
import de.dfki.mycbr.core.model.SymbolDesc;
import de.dfki.mycbr.core.retrieval.Retrieval;
import de.dfki.mycbr.core.retrieval.Retrieval.RetrievalMethod;
import de.dfki.mycbr.core.similarity.ISimFct;
import de.dfki.mycbr.core.similarity.Similarity;
import de.dfki.mycbr.core.similarity.config.AmalgamationConfig;
import de.dfki.mycbr.util.Pair;
import exceptions.ApplicationException;
import exceptions.CaseBaseException;
import exceptions.SimilarityMeasureException;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.Control;
import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.OverrunStyle;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Separator;
import javafx.scene.control.SeparatorMenuItem;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.Toggle;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.Tooltip;
import javafx.scene.control.cell.MapValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCombination;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import javafx.stage.Stage;
import javafx.util.Callback;
import similarity.computation.Computation;
import similarity.computation.ComputationDiscretize;
import similarity.computation.ComputationKM;
import similarity.computation.ComputationVDM;
import similarity.computation.config.AbstractSimilarityMeasureConfiguration;
import similarity.computation.config.KaplanMeierConfig;
import similarity.computation.config.VDMConfig;
import similarity.config.SimilarityMeasure;
import similarity.desc.ExtendedDoubleDesc;
import similarity.desc.ExtendedIntegerDesc;
import similarity.desc.ExtendedSymbolDesc;


/**
 * JavaFX GUI elements for myCBR Builder.
 * 
 * @author Christian Karmen
 *
 */
public class MyCBRBuilderFxElements implements IMyCBRBuilder {
	private IMyCBRBuilderListener itsUIListener;	
	private Stage itsStage;
	
	private OS itsOS = OS.UNKNOWN;
	/**
	 * Gets the running Operation System.
	 * 
	 * @return Operation System
	 */
	public OS getOS() {
		return itsOS;
	}
	private String itsHostname = "";
	/**
	 * Gets the hostname.
	 * 
	 * @return hostname
	 */
	public String getHostname() {
		return itsHostname; 
	}

	protected RetrievalMethod itsCurrentRetrievalMethod = RetrievalMethod.RETRIEVE_SORTED;
	private ScrollPane itsCaseDescriptionScrollPane = null;
	private Retrieval itsCurrentRetrieval = null;
	private ObservableList<Map<String, String>> itsCurrentResultData = FXCollections.observableArrayList();
	private ToggleGroup itsToogleGroup_RF = new ToggleGroup();
	
	protected MyCbrBuilderSession itsMyCbrBuilderSession;
	@Override
	public MyCbrBuilderSession getMyCbrBuilderSession() {
		return itsMyCbrBuilderSession;
	}
	public void setMyCbrBuilderSession(MyCbrBuilderSession theMyCbrBuilderSession) throws ApplicationException {
		initUIConfiguration();
		itsMyCbrBuilderSession = theMyCbrBuilderSession;  
		try {
			generateAmalgamFct(theMyCbrBuilderSession.getSimilarityMeasure());
		} catch (Exception e) {
			throw new ApplicationException(e);
		}				
	}

	protected int itsKvalue = 10;
	protected int itsThresholdValue = 85;
	protected int itsDefaultSpacing = 8;
	protected int itsWindowWidthLeft = 100;
	protected int itsWindowWidthCenter = 780; //850;	//800
	protected int itsWindowWidthRight = 100;
	protected int itsWindowHeightBottom = 275; //245;
	protected int itsWindowWidth;
	
	private String itsWorkingDirectory;
	/**
	 * Gets the working directory.
	 * 
	 * @return working directory
	 */
	public String getWorkingDirectory() {
		return itsWorkingDirectory;
	}
	/**
	 * Sets the working directory.
	 * 
	 * @param theWorkingDirectory working directory
	 */
	public void setWorkingDirectory(String theWorkingDirectory) {
		this.itsWorkingDirectory = theWorkingDirectory;
	}
	
	
	/**
	 * Class for getting UI elements for myCBRBuilder, here based on JavaFX.
	 * 
	 * @param theStage Stage to fill UI classes
	 * @param theUIListener Listener for UI changes
	 * @param theMyCBRBuilderSession MyCBRBuilder session parameters
	 */
	public MyCBRBuilderFxElements(Stage theStage, IMyCBRBuilderListener theUIListener, MyCbrBuilderSession theMyCBRBuilderSession){
		itsStage = theStage;
		
		itsOS = IMyCBRBuilder.detectClientOS();
		System.out.println("Detected OS: " + itsOS);
		itsWorkingDirectory = ".";	
		
		try {
			itsHostname = InetAddress.getLocalHost().getHostName();
			System.out.println("Detected Hostname: " + itsHostname);
		} catch (UnknownHostException e) {
			MyCBRBuilderFxElements.showExceptionDialog(e);
		}
		
		itsMyCbrBuilderSession = theMyCBRBuilderSession;
		
		itsLAYOUT_MenuBar = addMenuBar();
		itsLAYOUT_LoadSavePane = addLoadSavePane();
		itsLAYOUT_ConfigPane = addConfigPane();
		itsLAYOUT_CaseDescriptionPane = addCaseDescriptorPane();
		
		updateCurrentRetrievalResultsTable();
		itsUIListener = theUIListener;
		
		initializeWindowSizes();
	}
	
	
	/**
	 * Class for getting UI elements for myCBRBuilder, here based on JavaFX.
	 * 
	 * @param theStage Stage to fill UI classes
	 * @param theUIListener Listener for UI changes
	 */
	public MyCBRBuilderFxElements(Stage theStage, IMyCBRBuilderListener theUIListener){
		this(theStage, theUIListener, new MyCbrBuilderSession());
	}
	


	@Override
	public ArrayList<String> getCaseDescriptionList() {
		return itsMyCbrBuilderSession.getCaseDescriptionList();
	}
	@Override
	public void setCaseDescriptionList(ArrayList<String> theCaseDescriptionList) {
		itsMyCbrBuilderSession.setCaseDescriptionList(theCaseDescriptionList);
	}


	@Override
	public ArrayList<String> getSolutionDescriptionList() {
		return itsMyCbrBuilderSession.getSolutionDescriptionList();
	}
	@Override
	public void setSolutionDescriptionList(ArrayList<String> theSolutionDescriptionList) {
		itsMyCbrBuilderSession.setSolutionDescriptionList(theSolutionDescriptionList);
	}
	
	@Override
	public AbstractSimilarityMeasureConfiguration getSimilarityMeasureConfig() {
		return itsMyCbrBuilderSession.getSimilarityMeasureConfig();
	}
//	@Override
//	public void setSimilarityMeasureConfig(AbstractSimilarityMeasureConfiguration theSimilarityMeasureConfig) {
//		itsMyCbrBuilderSession.setSimilarityMeasureConfig(theSimilarityMeasureConfig);
//	}
	
	@Override
	public SimilarityMeasure getSimilarityMeasure() {
		return itsMyCbrBuilderSession.getSimilarityMeasure();
	}
	@Override
	public void setSimilarityMeasure(SimilarityMeasure theSimilarityMeasure, AbstractSimilarityMeasureConfiguration theSimilarityMeasureConfig) {
		itsMyCbrBuilderSession.setSimilarityMeasure(theSimilarityMeasure, theSimilarityMeasureConfig);
	}
	
	
	@Override
	public void setConnector(AbstractConnector theConnector){
		itsMyCbrBuilderSession.setConnector(theConnector);;
	}
	
	
	@Override
	public AbstractConnector getConnector(){
		return itsMyCbrBuilderSession.getConnector();
	}



	// ALL THE LAYOUT ELEMENTS	
	private VBox itsLAYOUT_CaseDescriptionPane = null;
	private MenuBar itsLAYOUT_MenuBar = null;
	private VBox itsLAYOUT_LoadSavePane = null;
	private VBox itsLAYOUT_ConfigPane = null;
	private TableView<Map<String, String>> itsLAYOUT_CurrentResultPane;// = new TableView<>();
	
	@Override
	public VBox getLAYOUT_CaseDescriptionPane() {
		return itsLAYOUT_CaseDescriptionPane;
	}
	@Override
	public MenuBar getLAYOUT_MenuBar() {
		return itsLAYOUT_MenuBar;
	}
	@Override
	public VBox getLAYOUT_LoadSavePane() {
		return itsLAYOUT_LoadSavePane;
	}
	@Override
	public VBox getLAYOUT_ConfigPane() {
		return itsLAYOUT_ConfigPane;
	}
	@Override
	public TableView<Map<String, String>> getLAYOUT_CurrentResultPane() {
		return itsLAYOUT_CurrentResultPane;
	}


	private void initializeWindowSizes(){
		  if (itsOS == OS.MAC){
			  itsWindowWidthLeft += 0;
			  itsWindowWidthRight += 20;
			  itsWindowWidthCenter += 20;
		  }		  
		  itsWindowWidth = itsWindowWidthLeft + itsWindowWidthCenter + itsWindowWidthRight + 4 * itsDefaultSpacing;
	}
	

	protected int getWindowWidth(){
		return itsWindowWidth;
	}
	
	
	public void updateLayoutElements(){
		itsLAYOUT_CaseDescriptionPane = addCaseDescriptorPane();
		itsLAYOUT_ConfigPane = addConfigPane();
		
		// Notify everybody that may be interested.
        itsUIListener.updateUI();
        
        updateCurrentRetrievalResultsTable();
	}
	
	
	
	  /**
	   * Created the main MenuBar for the Application. 
	   * 
	   * @return Main MenuBar for the Application
	   */
	protected MenuBar addMenuBar(){
		  MenuBar aMenuBar = new MenuBar();
		  
		  Menu menuFile = new Menu("File");
		  
		  MenuItem aOpenItem = new MenuItem("Open Project...");
		  aOpenItem.setAccelerator(KeyCombination.keyCombination("Ctrl+O"));
		  aOpenItem.setOnAction((ActionEvent t) -> {
			  loadProjectFile(showOpenProjectDialog());
		  });
		  
		  MenuItem aSaveItem = new MenuItem("Save Project...");
		  aSaveItem.setAccelerator(KeyCombination.keyCombination("Ctrl+S"));
		  aSaveItem.setOnAction((ActionEvent t) -> {
			  showSaveProjectDialog();
		  });
		  
		  MenuItem aCloseItem = new MenuItem("Close Project");
		  aCloseItem.setAccelerator(KeyCombination.keyCombination("Ctrl+W"));
		  aCloseItem.setOnAction((ActionEvent t) -> {
			  initUIConfiguration();
			  updateLayoutElements();
		  });
		  
		  MenuItem anExitItem = new MenuItem("Exit");
		  anExitItem.setOnAction((ActionEvent t) -> {
			  System.exit(0);
		  });
		  
		  menuFile.getItems().addAll(aOpenItem, aSaveItem, aCloseItem, new SeparatorMenuItem(), anExitItem);
		  
		  aMenuBar.getMenus().addAll(menuFile);
		  
		  return aMenuBar;
	  }
	  
	  
	  /**
	   * Opens a file dialog to open and load a myCBR project. 
	   */
	  protected File showOpenProjectDialog(){
			FileChooser fileChooser = new FileChooser();
			fileChooser.setInitialDirectory(new File(getWorkingDirectory()));
			 fileChooser.setTitle("Open myCBR Project");
			 fileChooser.getExtensionFilters().addAll(
					 new ExtensionFilter("Any myCBR Project", "*.prj", "*.zip", "*_CBR_SMF.XML"),
			         new ExtensionFilter("myCBR Project File", "*.prj"),
			         new ExtensionFilter("myCBR Zipped Project File", "*.zip"),
			         new ExtensionFilter("myCBR XML Project File", "*_CBR_SMF.XML"),
			         new ExtensionFilter("All Files", "*.*"));
			 return fileChooser.showOpenDialog(itsStage);		 
	  }
	  
	  
	  /**
	   * Opens a file dialog to save the current myCBR-Project. 
	   */
	  protected void showSaveProjectDialog(){
		  FileChooser fileChooser = new FileChooser();
			fileChooser.setInitialDirectory(new File(getWorkingDirectory()));
			 fileChooser.setTitle("Save myCBR Project");
			 fileChooser.getExtensionFilters().addAll(
			         new ExtensionFilter("myCBR Project File", "*.prj"),
			         new ExtensionFilter("All Files", "*.*"));
			 File selectedFile = fileChooser.showSaveDialog(itsStage);
			
			try {
				if (selectedFile != null){
					// Remove file extension since saveProject() adds it automatically
					int i = selectedFile.getName().lastIndexOf(".");
					String aFileNameWithoutExtension = selectedFile.getName().substring(0, i);
					
					itsMyCbrBuilderSession.saveProject(selectedFile.getParent() + File.separatorChar, aFileNameWithoutExtension);
				}
			} catch (Exception e1) {
				e1.printStackTrace();
				showExceptionDialog(e1);
			}
	  }
	  
	  
	  /**
	   * Shows an Exception StackTrace in a single info window.
	   * 
	   * @param theException Exception to show the trace from
	   * @param theMaxMessageLength Character limit of the Exception Trace
	   */
		public static void showExceptionDialog(Exception theException, int theMaxMessageLength){
			StringWriter sw = new StringWriter();
			theException.printStackTrace(new PrintWriter(sw));
			
			String aStackTraceString = sw.toString();
			//aStackTraceString = theException.getStackTrace()[1].toString();
			
			// cut message length to max value
			int mlength = aStackTraceString.length();
			mlength = mlength > theMaxMessageLength ? theMaxMessageLength : mlength;
			aStackTraceString = aStackTraceString.substring(0, mlength) + " [...]";
			
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("Critical Error occured!");
			alert.setHeaderText(theException.getLocalizedMessage());
			alert.setContentText(aStackTraceString);
			
			alert.setResizable(true);
			alert.getDialogPane().setPrefSize(800, 400);
			
			// show stack trace also on console
			theException.printStackTrace();

			alert.showAndWait();
		}
		
		  /**
		   * Shows an Exception StackTrace in a single info window.
		   * 
		   * @param theException Exception to show the trace from
		   */
		public static void showExceptionDialog(Exception theException){
			//showExceptionDialog(theException, Integer.MAX_VALUE);
			showExceptionDialog(theException, 2000);
		}
		
		
		

	  
		   
		   
		   /**
		    * Creates a ComboBox containing the given allowed Values. Also automatically sets the 1st allowed Value. 
		    * 
		    * @param theAllowedValueList The list of values that are available in ComboBox.
		    * @return ComboBox containing the given allowed Values.
		    */
		   private ComboBox<String> getComboBoxFactoryElement(ArrayList<String> theAllowedValueList){
			  ComboBox<String> aComboBox = new ComboBox<String>();
			  
			  Collections.sort(theAllowedValueList);
			  
			  for (String aValue : theAllowedValueList) {
				  aComboBox.getItems().addAll(aValue);
			}
			    
			  if (!theAllowedValueList.isEmpty()){
			    aComboBox.setValue(theAllowedValueList.get(0));
			  }
			    
			  aComboBox.setId("ValueControlCB");
			    return aComboBox;
		  }
		  
		   
		   
		   /**
		    * Creates a TextArea with a given default Value. 
		    * 
		    * @param theDefaultValue the default text to be shown in the TextArea.
		    * @return TextArea with a given default Value. 
		    */
		   private TextArea getTextAreaFactoryElement(String theDefaultValue){
			  TextArea aTextArea = new TextArea(theDefaultValue);
			  aTextArea.setPrefRowCount(1);
			  
			  aTextArea.setId("ValueControlTA");
			  return aTextArea;
		  }

		   
		   /**
		    * Adds a customized Value Input Field for a Attribute. The actual Input Control can be individual.
		    * <p>
		    * Radio Buttons are defined to represent either the actual value or the states UNKNOWN resp. UNDEFINED.
		    * 
		    * @param theValueControl the individual input control for values
		    * @return Node containing the customized Value Input Field 
		    */
		   private HBox addValueInputField(Control theValueControl){
			  
			  HBox hbox = new HBox();
			    hbox.setPadding(new Insets(0));
			    hbox.setSpacing(itsDefaultSpacing);
			  
				VBox vbox = new VBox();
				vbox.setPadding(new Insets(0));
				vbox.setSpacing(0);
			  
			    ToggleGroup group = new ToggleGroup();
			    group.selectedToggleProperty().addListener(new ChangeListener<Toggle>(){
			        public void changed(ObservableValue<? extends Toggle> ov, Toggle old_toggle, Toggle new_toggle) {
			                if (group.getSelectedToggle() != null) {
			                	if (((RadioButton)group.getSelectedToggle()).getText().equals("")){
			                		theValueControl.setDisable(false);
			                	} else{
			                		theValueControl.setDisable(true);
			                	}
			                }                
			            }
			    });

			    RadioButton rb1 = new RadioButton();
			    rb1.setId("ValueSetRadioButton");
			    rb1.setToggleGroup(group);
			    rb1.setPrefHeight(40);

			    RadioButton rb2 = new RadioButton(Project.UNKNOWN_SPECIAL_VALUE);
			    rb2.setMnemonicParsing(false);
			    rb2.setId("ValueUnknownRadioButton");
			    rb2.setToggleGroup(group);
			    rb2.setSelected(true);
			     
			    RadioButton rb3 = new RadioButton(Project.UNDEFINED_SPECIAL_VALUE);
			    rb3.setMnemonicParsing(false);
			    rb3.setId("ValueUndefinedRadioButton");
			    rb3.setToggleGroup(group);
			    
			    vbox.getChildren().addAll(rb2, rb3);
			    hbox.getChildren().addAll(rb1, theValueControl, vbox);
			    
			    hbox.setId("ValueBox");
			    return hbox;
		  }
		   
		   
		   /**
		    * Adds all Case Description Elements in scrollable Pane. 
		    * 
		    * @return A Pane containing all Case Description Elements.
		    */
		   private ScrollPane addCaseDescriptionElements(){
			VBox vbox = new VBox();
			vbox.setPadding(new Insets(0));
			vbox.setSpacing(0);		// 3
			
			ScrollPane sp = new ScrollPane();
			VBox.setVgrow(sp, Priority.ALWAYS);
			
			sp.setVmax(itsWindowWidthCenter);
		    sp.setPrefSize(itsWindowWidthCenter, 400);
		    sp.setContent(vbox);
		    
			ArrayList<String> aValueList = new ArrayList<>();
			
			double aWeight = 0;
			String aWeightString;
			
			// get the allowed values for each CaseDescription-Attribute
			for (String aCaseDescItem: getCaseDescriptionList()) {
				
				AttributeDesc anAttributeDesc = MyCbrUtils.getAttributeDescByName(itsMyCbrBuilderSession.getConcept(), aCaseDescItem);
				
				if (itsMyCbrBuilderSession != null && MyCbrUtils.getActiveAmalgamFct(itsMyCbrBuilderSession.getConcept()) != null && anAttributeDesc != null){
					aWeight = MyCbrUtils.getActiveAmalgamFct(itsMyCbrBuilderSession.getConcept()).getWeight(anAttributeDesc).doubleValue();
				}
				
				NumberFormat aNF = NumberFormat.getNumberInstance(Locale.UK);
				aNF.setMinimumFractionDigits(1);
				aNF.setMaximumFractionDigits(1);
				aWeightString = aNF.format(aWeight);
				
			
				if (anAttributeDesc instanceof SymbolDesc || anAttributeDesc instanceof ExtendedSymbolDesc){
					SymbolDesc symbolDesc = (SymbolDesc) anAttributeDesc;
					Set<String> allowedValueList = symbolDesc.getAllowedValues();
					
					aValueList.clear();
					for (String allowedValue : allowedValueList) {
						//System.out.print("[" + allowedValue + "]");
						if (!allowedValue.trim().isEmpty()){
							aValueList.add(allowedValue.trim());
						}
					}
					vbox.getChildren().add(addAttributeRow(symbolDesc.getName(), aWeightString, getComboBoxFactoryElement(aValueList)));
				} else if (anAttributeDesc instanceof IntegerDesc || anAttributeDesc instanceof ExtendedIntegerDesc){
					IntegerDesc symbolDesc = (IntegerDesc) anAttributeDesc;
					vbox.getChildren().add(addAttributeRow(symbolDesc.getName(), aWeightString, getTextAreaFactoryElement("0")));
				} else if (anAttributeDesc instanceof FloatDesc || anAttributeDesc instanceof DoubleDesc|| anAttributeDesc instanceof ExtendedDoubleDesc){
					DoubleDesc floatDesc = (DoubleDesc) anAttributeDesc;
					vbox.getChildren().add(addAttributeRow(floatDesc.getName(), aWeightString, getTextAreaFactoryElement("0.0")));
				} else if (anAttributeDesc instanceof StringDesc){
					StringDesc stringDesc = (StringDesc) anAttributeDesc;
					vbox.getChildren().add(addAttributeRow(stringDesc.getName(), aWeightString, getTextAreaFactoryElement("<text>")));
				} else {
					System.err.println("CAUTION: unhandled Value Type: " + anAttributeDesc!=null?anAttributeDesc.getClass():"null");
				}
			}
			  
			return sp;
		  }
		   
		   
		   
		   /**
		    * Adds a Pane which contains the Load/Save Buttons. Also has a button for HD4 data generation.
		    * 
		    * @return VBox containing Load/Save Buttons and some more.
		    */
		   protected VBox addLoadSavePane(){
		 	  VBox vbox = new VBox();
		 	  
		 	    vbox.setPadding(new Insets(30, 10, 10, 10));
		 	    vbox.setSpacing(itsDefaultSpacing);
		 	    
		 	    Image anImage_Load = new Image(getClass().getResourceAsStream("/application/resources/1446486275_load_upload.png"));
		 	    ImageView anImageView_Load = new ImageView(anImage_Load);
		 	    
		 	    Button aButton_Load = new Button("Open Project", anImageView_Load);
		 	    aButton_Load.setMinWidth(itsWindowWidthLeft);
		 	    aButton_Load.setMaxWidth(itsWindowWidthLeft);
		 	    aButton_Load.setWrapText(true);
		 	    aButton_Load.setContentDisplay(ContentDisplay.TOP);
		 	    aButton_Load.setOnAction(new EventHandler<ActionEvent>() {
		 	    	@Override public void handle(ActionEvent e) {
		 	   		 loadProjectFile(showOpenProjectDialog());
		 	    	}
		  		});
		 	    
		 	    
		 	    Image anImage_Save = new Image(getClass().getResourceAsStream("/application/resources/1446486159_load_download.png"));
		 	    ImageView anImageView_Save = new ImageView(anImage_Save);
		 	    
		 	    Button aButton_Save = new Button("Save Project", anImageView_Save);
		 	    aButton_Save.setMinWidth(100);
		 	    aButton_Save.setMaxWidth(100);
		 	    aButton_Save.setWrapText(true);
		 	    aButton_Save.setContentDisplay(ContentDisplay.TOP);
		 	    aButton_Save.setOnAction(new EventHandler<ActionEvent>() {
		 	    	@Override public void handle(ActionEvent e) {
		 	    		showSaveProjectDialog();
		 	    	}
		  		});
	 	    
		 	    vbox.getChildren().addAll(aButton_Load, aButton_Save);
		 	    
		 	    // FIXME disabled for now
		 	    aButton_Load.setDisable(true);
		 	    aButton_Save.setDisable(true);
		 	  
		 	  return vbox;
		   }
		   
		   
		   private void  loadProjectFile(File theSelectedFile){
			   MyCbrBuilderSession anI2b2HandlerBackup = itsMyCbrBuilderSession;
	 			 try {
	 				 if (theSelectedFile != null){
	 					// reset project configuration
	 					 initUIConfiguration();
	 					
	 					itsMyCbrBuilderSession = new MyCbrBuilderSession(theSelectedFile.getAbsolutePath());
	 					
	 					itsWorkingDirectory = itsMyCbrBuilderSession.getProject().getPath();
	 					//itsCaseDescriptionList = MyCbrUtils.getCaseDescList(itsMyCbrBuilderSession.getConcept(), true);
	 					setCaseDescriptionList(MyCbrUtils.getCaseDescList(itsMyCbrBuilderSession.getConcept(), true));
	 		    		Collections.sort(getCaseDescriptionList());
	 		    		
	 		    		setSolutionDescriptionList(MyCbrUtils.getCaseDescList(itsMyCbrBuilderSession.getConcept(), false));
//	 		    		itsSolutionDescriptionList = MyCbrUtils.getCaseDescList(itsMyCbrBuilderSession.getConcept(), false);
	 		    		
	 		    		// FIXME Irgendwann mal, wenn genug Zeit ist...
	 		    		if (itsMyCbrBuilderSession.getSimilarityMeasure() == SimilarityMeasure.KAPLANMEIER){
	 		    			throw new Exception("ERROR: Kaplan-Meier similarity measure not supported in GUI yet!");
	 		    		}
	 				 }
	 			} catch (Exception e) {
	 				itsMyCbrBuilderSession = anI2b2HandlerBackup;
	 				showExceptionDialog(e);
	 			}
	 			 
	 			 updateLayoutElements();
		   }
		   
		   
		   /**
		    * Creates a Header Line to describe the Attribute Pane.
		    * 
		    * @return Header Line to describe the Attribute Pane.
		    */
		   private HBox addAttributeRowHeader(){
			  HBox hbox = new HBox();
			    hbox.setPadding(new Insets(0));
			    hbox.setSpacing(itsDefaultSpacing);
			    
			    Label aLabel_Concept = new Label("CONCEPT");
			    aLabel_Concept.setPrefSize(220, 20);
			    aLabel_Concept.setStyle("-fx-font-weight: bold");
			    //aLabel_Concept.setStyle("-fx-background-color: #336699;");
			    
			    Label aLabel_Value = new Label("VALUE");
			    aLabel_Value.setPrefSize(285, 20);
			    aLabel_Value.setStyle("-fx-font-weight: bold");
			    
			    Label aLabel_Weight = new Label("WEIGHT");
			    aLabel_Weight.setPrefSize(50, 20);
			    aLabel_Weight.setStyle("-fx-font-weight: bold");
			    
			    Label aLabel_FimFct = new Label("LOCAL SIMILARITY");
			    aLabel_FimFct.setPrefSize(120, 20);
			    aLabel_FimFct.setStyle("-fx-font-weight: bold");
			    
			    hbox.getChildren().addAll(aLabel_Concept, aLabel_Value, aLabel_Weight, aLabel_FimFct);
			    
			    
			    //Label aLabel_KM = new Label(itsSimilarityMeasure.getName());
			    Label aLabel_KM = new Label("INFO");
			    aLabel_KM.setPrefSize(40, 20);
			    aLabel_KM.setStyle("-fx-font-weight: bold");			    
			    hbox.getChildren().addAll(aLabel_KM);

			    return hbox;
		  }
		   
		   
		   
		   /**
		    * Creates a Pane that contains configuration options.
		    * 
		    * @return Pane that contains configuration options.
		    */
		   protected VBox addConfigPane(){
			    VBox vbox = new VBox();
			    vbox.setPadding(new Insets(30, 10, 10, 10));
			    vbox.setSpacing(itsDefaultSpacing);
			    
			    Image anImage_Shuffle = new Image(getClass().getResourceAsStream("/application/resources/1446218367_media-shuffle.png"));
			    ImageView anImageView = new ImageView(anImage_Shuffle);
			    
			    Button aButton_RandomPatient = new Button("", anImageView);
			    aButton_RandomPatient.setMinWidth(itsWindowWidthRight - itsDefaultSpacing);
			    aButton_RandomPatient.setMaxWidth(itsWindowWidthRight - itsDefaultSpacing);
			    aButton_RandomPatient.setWrapText(true);
			    aButton_RandomPatient.setOnAction(new EventHandler<ActionEvent>() {
			    	@Override public void handle(ActionEvent e) {
			    		if (itsMyCbrBuilderSession == null){
			    			return;
			    		}
			    			
			    		Instance anInstance = null;
						try {
							anInstance = CaseBaseUtils.getRandomCase(itsMyCbrBuilderSession.getCaseBase());
						} catch (CaseBaseException e1) {
							showExceptionDialog(e1);
						}
			    		for (Map.Entry<AttributeDesc, Attribute> anAttribute : anInstance.getAttributes().entrySet()) {
							//System.out.println(anAttribute.getKey().getName() + " = " + anAttribute.getValue().getValueAsString());
			    			if (getCaseDescriptionList().contains(anAttribute.getKey().getName())){
			    				setAttributeValueById(anAttribute.getKey().getName(), anAttribute.getValue().getValueAsString());
			    			}
						}
			    	}
		   		});
			    
			    Image anImage_Retrieve = new Image(getClass().getResourceAsStream("/application/resources/1446229224_033.png"));
			    ImageView anImageView_Retrieve = new ImageView(anImage_Retrieve);
			    
			    Button aButton_Retrieve = new Button("", anImageView_Retrieve);
			    aButton_Retrieve.setMinWidth(itsWindowWidthRight - itsDefaultSpacing);
			    aButton_Retrieve.setMaxWidth(itsWindowWidthRight - itsDefaultSpacing);
			    aButton_Retrieve.setOnAction(new EventHandler<ActionEvent>() {
			        @Override public void handle(ActionEvent e) {
			        	if (itsMyCbrBuilderSession == null){
			    			return;
			    		}
			        	
			        	itsCurrentRetrieval = doRetrieval();
			        	//itsI2b2CBR.printRetrievalResults(itsRetrieval, -99d);
			        	try {
							generateResultData();
						} catch (CaseBaseException e1) {
							showExceptionDialog(e1);
						}
			        	updateCurrentRetrievalResultsTable();
			        	
			        	// FIXME warum geht Auto-size nicht mehr??
			        	//itsResultTable.setColumnResizePolicy(TableView.UNCONSTRAINED_RESIZE_POLICY);
			        	//itsResultTable.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
			        	//itsResultTable.setFixedCellSize(TableView.USE_COMPUTED_SIZE); 
			        	itsLAYOUT_CurrentResultPane.autosize();
			        	
			        	// DEBUG: immer 0??
//						System.out.println("getTotalNumberOfCases(): " + itsI2b2CBR.getItsProject().getTotalNumberOfCases());
//						System.out.println("getCurrentNumberOfCases(): " + itsI2b2CBR.getItsProject().getCurrentNumberOfCases());
			        	int aResultSize = 0;
			        	if (itsCurrentRetrieval.getResult() != null){
			        		aResultSize = itsCurrentRetrieval.getResult().size();
			        	}
			        	System.out.println("Retrieval Result size: " + aResultSize);
			        }
			    });
			    
			    
			    ToggleGroup aToogleGroup_GS = new ToggleGroup();
			    aToogleGroup_GS.selectedToggleProperty().addListener(
			    	    (ObservableValue<? extends Toggle> ov, Toggle old_toggle, Toggle new_toggle) -> {
			    	        if (aToogleGroup_GS.getSelectedToggle() != null) {    
			    	        	if (itsMyCbrBuilderSession == null){
			    	    			return;
			    	    		}
			    	        	MyCbrUtils.getActiveAmalgamFct(itsMyCbrBuilderSession.getConcept()).setType((AmalgamationConfig)aToogleGroup_GS.getSelectedToggle().getUserData());
			    	    }
			    	});

			    RadioButton rb1 = new RadioButton("Weighted Sum");
			    rb1.setToggleGroup(aToogleGroup_GS);
			    rb1.setUserData(AmalgamationConfig.WEIGHTED_SUM);

			    RadioButton rb2 = new RadioButton("Euclidian");
			    rb2.setToggleGroup(aToogleGroup_GS);
			    rb2.setUserData(AmalgamationConfig.EUCLIDEAN);
			     
			    RadioButton rb3 = new RadioButton("Miniumum");
			    rb3.setToggleGroup(aToogleGroup_GS);
			    rb3.setUserData(AmalgamationConfig.MINIMUM);
			    
			    RadioButton rb4 = new RadioButton("Maximum");
			    rb4.setToggleGroup(aToogleGroup_GS);
			    rb4.setUserData(AmalgamationConfig.MAXIMUM);
			    
			    RadioButton rb5 = new RadioButton("Squared Sum");
			    rb5.setToggleGroup(aToogleGroup_GS);
			    rb5.setUserData(AmalgamationConfig.SQUARED_SUM);
			    
			    if (MyCbrUtils.getActiveAmalgamFct(itsMyCbrBuilderSession.getConcept()) != null){
				    AmalgamationConfig aAmalgamConfig = MyCbrUtils.getActiveAmalgamFct(itsMyCbrBuilderSession.getConcept()).getType();
				    if (aAmalgamConfig == AmalgamationConfig.WEIGHTED_SUM){
				    	rb1.setSelected(true);
				    } else if (aAmalgamConfig == AmalgamationConfig.EUCLIDEAN){
				    	rb2.setSelected(true);
				    }else if (aAmalgamConfig == AmalgamationConfig.MINIMUM){
				    	rb3.setSelected(true);
				    }else if (aAmalgamConfig == AmalgamationConfig.MAXIMUM){
				    	rb4.setSelected(true);
				    }else if (aAmalgamConfig == AmalgamationConfig.SQUARED_SUM){
				    	rb5.setSelected(true);
				    }
			    } 
			    
			    
			    itsToogleGroup_RF.selectedToggleProperty().addListener(
			    	    (ObservableValue<? extends Toggle> ov, Toggle old_toggle, Toggle new_toggle) -> {
			    	        if (itsToogleGroup_RF.getSelectedToggle() != null) {
			    	        	itsCurrentRetrievalMethod = (RetrievalMethod)itsToogleGroup_RF.getSelectedToggle().getUserData();
			    	    }
			    	});

			    
			    RadioButton rb_k = new RadioButton("k nearest");
			    rb_k.setToggleGroup(itsToogleGroup_RF);
			    rb_k.setUserData(RetrievalMethod.RETRIEVE_K_SORTED);
			    rb_k.setSelected(true);
			    TextArea aTextArea_k = new TextArea(Integer.toString(itsKvalue));
			    aTextArea_k.setPrefWidth(40);
			    aTextArea_k.setPrefHeight(20);
			    aTextArea_k.setPrefRowCount(1);
			    aTextArea_k.textProperty().addListener(new ChangeListener<String>() {
					@Override
					public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
						itsKvalue = Integer.parseInt(newValue);
						rb_k.setSelected(true);
					}    	
			    });
			    VBox kbox = new VBox();
			    kbox.getChildren().addAll(rb_k, aTextArea_k);

			    RadioButton rb_t = new RadioButton("Threshold [%]");
			    rb_t.setToggleGroup(itsToogleGroup_RF);
			    rb_t.setUserData(RetrievalMethod.RETRIEVE_SORTED);
			    //rb_t.setSelected(true);
			    TextArea aTextArea_t = new TextArea(Integer.toString(itsThresholdValue));
			    aTextArea_t.setPrefWidth(40);
			    aTextArea_t.setPrefRowCount(1);
			    aTextArea_t.textProperty().addListener(new ChangeListener<String>() {
					@Override
					public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
						itsThresholdValue = Integer.parseInt(newValue);
						rb_t.setSelected(true);
					}    	
			    });
			    VBox tbox = new VBox();
			    tbox.getChildren().addAll(rb_t, aTextArea_t);
			     
			    RadioButton rb_none = new RadioButton("Full");
			    rb_none.setToggleGroup(itsToogleGroup_RF);
			    rb_none.setUserData(RetrievalMethod.RETRIEVE_SORTED);
			    //rb_none.setSelected(true);
			    
			    Label aLabel_GS = new Label("Global Similarity:");
			    aLabel_GS.setStyle("-fx-font-weight: bold");
			    vbox.getChildren().addAll(aButton_RandomPatient);
			    //vbox.getChildren().addAll(aLabel_GS, new Separator(), rb1, rb2, rb3, rb4, new Separator());
			    vbox.getChildren().addAll(aLabel_GS, new Separator(), rb1, rb5, rb2, new Separator());
			    
			    Label aLabel_RF = new Label("Result Filter:");
			    aLabel_RF.setStyle("-fx-font-weight: bold");
			    vbox.getChildren().addAll(aLabel_RF, new Separator(), kbox, tbox, rb_none, new Separator());
			    vbox.getChildren().addAll(aButton_Retrieve);
			  
			  return vbox;
		  }
		   
		   
		   
		   
			  /**
			   * Creates a single Attribute Row for the Case Description List.
			   * 
			   * @param theAttributeLabel Name of the Attribute/Concept.
			   * @param theWeight Default weight value for the given attribute.
			   * @param theValueControl Control element that represents the attribute's value properly.
			   * @return
			   */
			   private HBox addAttributeRow(String theAttributeLabel, String theWeight, Control theValueControl){
				   final int aRowHeight = 40;
				   
				   boolean isNominal = (theValueControl instanceof ComboBox);
				   
				   HBox hbox = new HBox();
				   hbox.setPadding(new Insets(0));
				   hbox.setSpacing(itsDefaultSpacing);
					//hbox.setStyle("-fx-background-color: #336699;");
					hbox.setId(theAttributeLabel.replaceAll("\\s",""));

				    Label aLabel_Concept = new Label(MyCbrUtils.getConceptShortName(theAttributeLabel));
//				    Label aLabel_Concept = new Label(theConceptName);
				    aLabel_Concept.setUserData(theAttributeLabel);
				    aLabel_Concept.setTooltip(new Tooltip(theAttributeLabel));
				    aLabel_Concept.setPrefSize(185, aRowHeight);
				    aLabel_Concept.setId("Concept");
				    aLabel_Concept.setWrapText(true);
				    aLabel_Concept.setTextOverrun(OverrunStyle.CENTER_ELLIPSIS);
				    
				    theValueControl.setPrefSize(200, aRowHeight);
				    theValueControl.setId("Value");
				    
				    TextArea aTextArea_Weight = getTextAreaFactoryElement(theWeight);
				    aTextArea_Weight.setPrefSize(20, aRowHeight);
				    aTextArea_Weight.setId("Weight");
//				    aTextArea_Weight.setDisable(true);
				    aTextArea_Weight.textProperty().addListener(new ChangeListener<String>() {
						@Override
						public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
							MyCbrUtils.getActiveAmalgamFct(itsMyCbrBuilderSession.getConcept()).setWeight(theAttributeLabel, Double.parseDouble(newValue));
						}
					});
				    
				    ComboBox<String> aComboBox_Similarity = new ComboBox<String>();
				    aComboBox_Similarity.setPrefSize(120, aRowHeight);	    
				    AttributeDesc aAttDesc = MyCbrUtils.getAttributeDescByName(itsMyCbrBuilderSession.getConcept(), theAttributeLabel);
				    // FIXME list all SimFcts (including default function)
				    aComboBox_Similarity.getItems().addAll(MyCbrUtils.getFctListOfActiveAmalgamFct(itsMyCbrBuilderSession.getConcept(), aAttDesc));
				    Object f = MyCbrUtils.getActiveAmalgamFct(itsMyCbrBuilderSession.getConcept()).getActiveFct(aAttDesc);
				    String anActiveFctName = ((ISimFct) f).getName();
//				    aComboBox_Similarity.getItems().add(anActiveFctName);
				    
//				    aComboBox_Similarity.setValue(anActiveFctName);
				    aComboBox_Similarity.setValue(anActiveFctName.substring(0, anActiveFctName.lastIndexOf("_")));
				    aComboBox_Similarity.setPrefSize(120, aRowHeight);
				    aComboBox_Similarity.setDisable(true);
				    aComboBox_Similarity.setId("Similarity");
				    
				    Button anAnalysisButton = createAnalysisButton(itsMyCbrBuilderSession.getSimilarityMeasure(), theAttributeLabel, isNominal);
				    
				    hbox.getChildren().addAll(
				    		aLabel_Concept, 
				    		addValueInputField(theValueControl), 
				    		aTextArea_Weight, 
				    		aComboBox_Similarity, 
				    		anAnalysisButton);
				    
				    return hbox;
			  }
			  
			   
			   
			   
			   private Button createAnalysisButton(
					   SimilarityMeasure theSimilarityMeasure, 
					   String theAttributeDescLabel, 
					   boolean theNominalFlag){
				   
				   Image aChartImage = new Image(getClass().getResourceAsStream("/application/resources/1446498911_line_chart.png"));
				   Image anImage = aChartImage;
				   
				   Button anAnalysisButton = new Button("");
				   anAnalysisButton.setPrefSize(50, 40);
				   
				   SimpleAttDesc anAttributeDesc = (SimpleAttDesc)MyCbrUtils.getAttributeDescByName(
						   itsMyCbrBuilderSession.getConcept(), 
						   theAttributeDescLabel);
				   
				   if (!theNominalFlag) { 
					   if (anAttributeDesc instanceof ExtendedIntegerDesc){
						   anAnalysisButton.setTooltip(new Tooltip(
								   "Min=" + ((ExtendedIntegerDesc)anAttributeDesc).getMin() + 
								   "; Max=" + ((ExtendedIntegerDesc)anAttributeDesc).getMax() +
								   "; MedianValue=" + ((ExtendedIntegerDesc)anAttributeDesc).getMedianValue() +
								   "; StandardDeviation=" + ((ExtendedIntegerDesc)anAttributeDesc).getStandardDeviation()
								   ));
					   } else if (anAttributeDesc instanceof ExtendedDoubleDesc){
						   anAnalysisButton.setTooltip(new Tooltip(
								   "Min=" + ((ExtendedDoubleDesc)anAttributeDesc).getMin() + 
								   "; Max=" + ((ExtendedDoubleDesc)anAttributeDesc).getMax() +
								   "; MedianValue=" + ((ExtendedDoubleDesc)anAttributeDesc).getMedianValue() +
								   "; StandardDeviation=" + ((ExtendedDoubleDesc)anAttributeDesc).getStandardDeviation()
								   ));
					   }

				   }
	   
				   anAnalysisButton.setOnAction(new EventHandler<ActionEvent>() {
						@Override
						public void handle(ActionEvent event) {
							try{
								switch (theSimilarityMeasure) {
								case KAPLANMEIER:
									KaplanMeierConfig aKMConfig = (KaplanMeierConfig)getSimilarityMeasureConfig();
									ArrayList<String> anAttributeList = new ArrayList<>();
									anAttributeList.add(theAttributeDescLabel);
									anAttributeList.add(aKMConfig.KMTargetConfig.TargetStatusColumn);
									anAttributeList.add(aKMConfig.KMTargetConfig.TargetSurvivalColumn);
									Instances aMiniInstances = null;
									try {
										aMiniInstances = ComputationKM.getWekaInstancesFromCaseBase(
												itsMyCbrBuilderSession.getCaseBase(), 
												anAttributeList,
												aKMConfig.KMTargetConfig);
									} catch (CaseBaseException e1) {
										throw new ApplicationException("Weka Import from CaseBase failed");
									}
									try {
										aMiniInstances = ComputationDiscretize.makeNominal(
												aMiniInstances, 
												aKMConfig.KMTargetConfig);
	//									aMiniInstances = SurvivalCutOffDiscretizedFct.makeNominal(
	//											aMiniInstances, 
	//											aKMConfig.KMTargetConfig, 
	//											true, 
	//											theAttributeDescLabel, 
	//											new ArrayList<>());
									} catch (Exception e) {
										throw new ApplicationException("Nominalization failed.");
									}
									KMChart aKMChart = new KMChart(aMiniInstances, aKMConfig.KMTargetConfig);
									aKMChart.drawKM(theAttributeDescLabel);
									break;
								case RANDOM:
								case HEOM:
									// TODO a statistics info window would be nice
									break;
								case HVDM1:
								case HVDM2:
								case HVDM3:
									if (theNominalFlag){
										DVDMChart aDVDMChart1 = new DVDMChart(
												anAttributeDesc, 
												(VDMConfig)getSimilarityMeasureConfig(), 
												itsMyCbrBuilderSession.getCaseBase());
											aDVDMChart1.showDVDMBarChart();

									} else{
										// TODO a statistics info window would be nice
									}
									break;
								case DVDM:
									DVDMChart aDVDMChart2 = new DVDMChart(
											anAttributeDesc, 
											(VDMConfig)getSimilarityMeasureConfig(), 
											itsMyCbrBuilderSession.getCaseBase());
									aDVDMChart2.showDVDMBarChart();
									break;
								case IVDM:
									if (theNominalFlag){
										DVDMChart aDVDMChart3 = new DVDMChart(
												anAttributeDesc, 
												(VDMConfig)getSimilarityMeasureConfig(), 
												itsMyCbrBuilderSession.getCaseBase());
										aDVDMChart3.showDVDMBarChart();
									} else{
										IVDMChart anIVDMChart = new IVDMChart(anAttributeDesc, 
												(VDMConfig)getSimilarityMeasureConfig(), 
												itsMyCbrBuilderSession.getCaseBase());
										anIVDMChart.showIVDMAreaChart();
									}
									break;
								case WVDM:
									if (theNominalFlag){
										DVDMChart aDVDMChart4 = new DVDMChart(
												anAttributeDesc, 
												(VDMConfig)getSimilarityMeasureConfig(), 
												itsMyCbrBuilderSession.getCaseBase());
										aDVDMChart4.showDVDMBarChart();
									} else{
										WVDMChart aWVDMChart = new WVDMChart(
												anAttributeDesc, 
												(VDMConfig)getSimilarityMeasureConfig(), 
												itsMyCbrBuilderSession.getCaseBase());
										aWVDMChart.showWVDMAreaChart();
									}
									break;
	
								default:
									throw new SimilarityMeasureException("Unhandled Similarity Measure: " + theSimilarityMeasure.getName());
								}
							} catch (Exception e){
								showExceptionDialog(e);   
							   }
							}	    		
						});
				   
				   
				   anAnalysisButton.setGraphic(new ImageView(anImage));
				   
				   return anAnalysisButton;
			   }
			   
			   
			   
			   
			   /**
			    * Defines the layout of the complete Case Description Pane.
			    * 
			    * @return VBox containing the layout of the complete Case Description Pane
			    */
			   protected VBox addCaseDescriptorPane(){
					VBox vbox = new VBox();
					vbox.setPadding(new Insets(0));
					vbox.setSpacing(itsDefaultSpacing);
					
					itsCaseDescriptionScrollPane = addCaseDescriptionElements();
					
				    vbox.getChildren().add(addAttributeRowHeader());
//				    vbox.getChildren().add(new Separator());
					vbox.getChildren().add(itsCaseDescriptionScrollPane);
					return vbox;
			  }
			   
			   
			   
			   /**
			    * Sets the given Attribute and its Value in the UI.
			    * 
			    * @param theAttribute The Attribute's full name
			    * @param theValue The Attribute's value
			    */
			   @SuppressWarnings("unchecked")
			   private void setAttributeValueById(String theAttribute, String theValue){
			       HBox aHBox = (HBox)itsCaseDescriptionScrollPane.lookup("#" + theAttribute.replaceAll("\\s",""));
			       Control aControl = null;
			       RadioButton aRadioButton = null;
			       
			       if (aHBox == null){
			    	   System.err.println("ERROR: Attribute '" + theAttribute + "' not found!");
			    	   return;
			       }

			       if (theValue.equals(Project.UNDEFINED_SPECIAL_VALUE)){
			    	   aRadioButton = (RadioButton)aHBox.lookup("#ValueUndefinedRadioButton");
			    	   aRadioButton.setSelected(true);
			       } else if (theValue.equals(Project.UNKNOWN_SPECIAL_VALUE)){
			    	   aRadioButton = (RadioButton)aHBox.lookup("#ValueUnknownRadioButton");
			    	   aRadioButton.setSelected(true);
			       } else {
			    	   aRadioButton = (RadioButton)aHBox.lookup("#ValueSetRadioButton");
			    	   aRadioButton.setSelected(true);
			    	   
			    	   aControl = (Control)aHBox.lookup("#Value");
			    	   if (aControl != null){
			    	       if (aControl instanceof TextArea){
			    	       	((TextArea)aControl).setText(theValue);
			    	       } else if (aControl instanceof ComboBox<?>){
			    	       	((ComboBox<String>)aControl).setValue(theValue);
			    	       }
			    	   } else {
			    		   System.err.println("No Control element found!");
			    	   }
			       }
			       

			       

			   }
			  
			   
			   /**
			    * Gets the current Attribute's value in the Case Description Pane.
			    * @param theAttribute The Name of the Attribute
			    * 
			    * @return Current Attribute's value in the Case Description Pane.
			    */
			   private String getAttributeValueById(String theAttribute){
			       HBox aHBox = (HBox)itsCaseDescriptionScrollPane.lookup("#" + theAttribute.replaceAll("\\s",""));
			       Control aControl = null;
			       RadioButton aRadioButton = null;
			       
			       if (aHBox == null){
			    	   System.err.println("ERROR: Attribute '" + theAttribute + "' not found!");
			    	   return null;
			       }
			       
			       aRadioButton = (RadioButton)aHBox.lookup("#ValueSetRadioButton");
			       aRadioButton = (RadioButton)aRadioButton.getToggleGroup().getSelectedToggle();
			       
			       if (aRadioButton.getId().equals("ValueSetRadioButton")){
			    	   aControl = (Control)aHBox.lookup("#Value");
			    	   
			    	   if (aControl != null){
			    	       String aValue = "";
			    	       if (aControl instanceof TextArea){
			    	       	aValue = ((TextArea)aControl).getText();
			    	       } else if (aControl instanceof ComboBox<?>){
			    	       	aValue = ((ComboBox<?>)aControl).getValue().toString();
			    	       }
			    	       return aValue;
			    	   } else {
			    		   System.err.println("No Control element found!");
			    	   }
			    	   
			       } else if (aRadioButton.getId().equals("ValueUnknownRadioButton")){
			    	   return Project.UNKNOWN_SPECIAL_VALUE;
			       } else if (aRadioButton.getId().equals("ValueUndefinedRadioButton")){
			    	   return Project.UNDEFINED_SPECIAL_VALUE;
			       } else {
			    	   System.err.println("Unknown RadioButton ID: " + aRadioButton.getId());
			       }
			       
			       //default
			       return null;
			   }
			   
			   /**
			    * Updates the current Retrieval Results Table (TableView).
			    * 
			    * @param theCurrentResults current Retrieval results
			    */
			   @SuppressWarnings({ "rawtypes", "unchecked" })
			   protected void updateCurrentRetrievalResultsTable(){
				   itsLAYOUT_CurrentResultPane = new TableView<Map<String, String>>(itsCurrentResultData);
				   itsLAYOUT_CurrentResultPane.setEditable(true);
				   itsLAYOUT_CurrentResultPane.setPrefWidth(itsWindowWidth);//500);
				   itsLAYOUT_CurrentResultPane.setPrefHeight(itsWindowHeightBottom);//245
				  
				  TableColumn<Map<String, String>, String> aTableColumn;
				  TableColumn<Map<String, String>, String> aColumn_Similarity = new TableColumn<Map<String, String>, String>("Similarity");
			      TableColumn<Map<String, String>, String> aColumn_CaseID = new TableColumn<Map<String, String>, String>("CaseID");

			      aColumn_Similarity.setCellValueFactory(new MapValueFactory("Similarity"));
			      aColumn_CaseID.setCellValueFactory(new MapValueFactory("CaseID"));

			      itsLAYOUT_CurrentResultPane.getColumns().addAll(aColumn_Similarity, aColumn_CaseID);

			      //for (String aConceptLabel : getConceptLabels()) {
			      ArrayList<String> aResultTableList = new ArrayList<>(getCaseDescriptionList());
			      aResultTableList.addAll(getSolutionDescriptionList());
			      
			      
			      for (String aConceptLongName : aResultTableList) {
			    	  // FIXME Show Concept short name in table column
			    	  String aConceptName = aConceptLongName;
//			    	  String aConceptName = I2b2CBR.getConceptShortName(aConceptLongName);

//			    	  aTableColumn = new TableColumn<Map<String, String>, String>();
			    	  aTableColumn = new TableColumn<Map<String, String>, String>(aConceptName);
//			    	  aTableColumn = new TableColumn<Map<String, String>, String>(I2b2CBR.getConceptShortName(aConceptName));
			    	  
//			    	  Label aConceptLabel = new Label(I2b2CBR.getConceptShortName(aConceptName));
////			    	  Label aConceptLabel = new Label(aConceptName);
//			    	  aConceptLabel.setTooltip(new Tooltip(aConceptLongName));
//			    	  aTableColumn.setGraphic(aConceptLabel);
			    	  
//			    	  System.out.println("aTableColumn.getText(): " + aTableColumn.getText());
			    	  aTableColumn.setText(aConceptName);
			    	  aTableColumn.setStyle("-fx-font-color: transparent;");
			    	  
			    	  aTableColumn.setCellValueFactory(new MapValueFactory(aConceptName));
			    	  
			    	  // Solution Column in bold font
			    	  if (getSolutionDescriptionList().contains(aConceptName)){
			    		  aTableColumn.setStyle("-fx-font-weight: bold");
			    		  aTableColumn.setStyle("-fx-background-color: #99CCFF;");
			    	  }
			    	  
			    	  if (getCaseDescriptionList().contains(aConceptName)){
			    		  //aTableColumn.setStyle("-fx-background-color: #336699;");
			    		  
			    		// Table cell coloring
			    		  aTableColumn.setCellFactory(new Callback<TableColumn<Map<String, String>, String>, TableCell<Map<String, String>, String>>() {
			    	            @Override
			    	            public TableCell<Map<String, String>, String> call(TableColumn<Map<String, String>, String> param) {
			    	                return new TableCell<Map<String, String>, String>() {

			    	                    @Override
			    	                    public void updateItem(String item, boolean empty) {
			    	                        super.updateItem(item, empty);
			    	                        if (!isEmpty() && itsCurrentRetrieval != null) {
			    	                        	//System.out.println("Current item: " + item);


			    	                     	   String anAttributeLabel = param.getText();
			    	                     	   //String aValue = getAttributeValueById(anAttributeLabel);	
			    	                     	   
			    	                     	   
			    	                     	   String aValue = itsCurrentRetrieval.getQueryInstance().getAttForDesc(MyCbrUtils.getAttributeDescByName(itsMyCbrBuilderSession.getConcept(), anAttributeLabel)).getValueAsString(); 
			    	                           Similarity aSimilarity = MyCbrUtils.getLocalSimilarity(itsMyCbrBuilderSession.getConcept(), anAttributeLabel, aValue, item);
			    	                           double aSimilarityValue = aSimilarity.getValue();
			    	                           
			    	                           // initialize default background color    	                           
			    	                           Color aBackgroundColor = Color.TRANSPARENT; //WHITE;
			    	                           
			    	                           // initialize default text color    	                           
			    	                           Color aTextColor = Color.BLACK;
			    	                           
			    	                           String aRGBString;
			    	                           
			    	                           // similarity has a invalid value
			    	                           if (aSimilarity == Similarity.INVALID_SIM || aSimilarityValue > 1.0 || aSimilarityValue < 0){
			    	                        	   aTextColor = Color.WHITE;
//			    	                        	   aBackgroundColor = Color.BLACK; 
			    	                        	   aBackgroundColor = Color.RED;
			    	                        	   
			    	                           } else {	// similarity has a valid value 
//				    	                           aBackgroundColor = getSimilarityColor_GreenYellowRed_PALETTE(aSimilarity);
//				    	                           aTextColor = Color.BLACK;
			    	                        	       	                        	   
			    	                        	   aBackgroundColor = getSimilarityColor_BlackWhite_PALETTE(aSimilarity);
			    	                        	   if (aSimilarityValue < 0.5){
			    	                        		   aTextColor = Color.WHITE;
			    	                        	   }
				    	                           
//				    	                           aTextColor = getSimilarityColor_GreenOrangeRed_PLAIN(aSimilarity);
			    	                           }
			    	                           
			    	                           this.setTextFill(aTextColor);
			    	                           
			    	                           aRGBString = "rgb(" 
			    	                        		   + new Double(aBackgroundColor.getRed()*255).intValue() +"," 
			    	                        		   + new Double(aBackgroundColor.getGreen()*255).intValue() + "," 
			    	                        		   + new Double(aBackgroundColor.getBlue()*255).intValue() + ")";
			    	                           this.setStyle("-fx-background-color:" + aRGBString);
			    	                           
			    	                           this.setTooltip(new Tooltip("Local Similarity: " + getFormattedSimilarity(aSimilarity, 1, 4)      
//			    	                           	+ "\nBackground-Color: " + aRGBString
			    	                        		   ));
			    	                           setText(item);
			    	                        }
			    	                    }
			    	                };
			    	            }    		  
			    	  });
			    		  
			    	  }
			    	  
			    	  itsLAYOUT_CurrentResultPane.getColumns().add(aTableColumn);
			      }
			  }
			   
			   
			   @SuppressWarnings("unused")
			   private Color getSimilarityColor_GreenYellowRed_PALETTE(Similarity theSimilarity){
				   double red=0, green=0, blue=0;
				   
				   double aSimilarityValue = theSimilarity.getValue();
				   
			         if (aSimilarityValue > 0.5){
			      	   red = 2*(1-aSimilarityValue);
			      	   green = 1d;
			         } else {
			      	   red = 1d;
			      	   green = 2*aSimilarityValue;
			         }
			         blue = 0d;
				   
				   return new Color(red, green, blue, 1d);
			   }


			//@SuppressWarnings("unused")
			   private Color getSimilarityColor_BlackWhite_PALETTE(Similarity theSimilarity){
				double aSimilarityValue = theSimilarity.getValue();
				
				return new Color(aSimilarityValue, aSimilarityValue, aSimilarityValue, 1);
			}
			   
			   @SuppressWarnings("unused")
			   private Color getSimilarityColor_GreenOrangeRed_PLAIN(Similarity theSimilarity){
				   double aSimilarityValue = theSimilarity.getValue();
				   
			       if (aSimilarityValue == 0){
			    	   return Color.RED;
			       } else if (aSimilarityValue == 1){
			    	   return Color.GREEN;
			       } else {
			    	   return Color.ORANGE;
			       }
			   }
			    
			   
			   
				 /**
				 * Formats the given Similarity value to a generic format.
				 * 
				 * @param theSimilarity	The similarity
				 * @param theNumberDigits	The max number of digits after the comma
				 * @return	The generic formatted similarity value
				 */
				private String getFormattedSimilarity(Similarity theSimilarity, int theMaximumIntegerDigits, int theMaximumFractionDigits){
					
					if (theSimilarity == Similarity.INVALID_SIM){
						return "INVALID SIMILARITY";
					}
					
					DecimalFormatSymbols aDFS = new DecimalFormatSymbols();
				    aDFS.setDecimalSeparator('.');
				    
				    DecimalFormat aFormat = new DecimalFormat();
				    aFormat.setMinimumIntegerDigits(1);
				    aFormat.setMinimumFractionDigits(1); 
				    aFormat.setMaximumIntegerDigits(theMaximumIntegerDigits);
				    aFormat.setMaximumFractionDigits(theMaximumFractionDigits);       
				    
				    aFormat.setDecimalSeparatorAlwaysShown(true);    	                           
				    aFormat.setDecimalFormatSymbols(aDFS);
				    
				    return aFormat.format(theSimilarity.getValue());
				}
			   
				
				
				
				/**
				 * Perform the actual Retrieval based on the values in the Case Description Pane.
				 * 
				 * @return Retrieval instance containing the results.
				 */
				private Retrieval doRetrieval() {
					Retrieval aRetrieval = itsMyCbrBuilderSession.generateRetrieval();
					aRetrieval.setRetrievalMethod(itsCurrentRetrievalMethod);
					aRetrieval.setK(itsKvalue);
//					aRetrieval.setCaseBase(itsMyCbrBuilderSession.getCaseBase());
					
					HashMap<String, String> aQueryFilter = new HashMap<String, String>();
					
					Label aConceptNode = null;
					String aValue = "";
					
					for (Node anAttributeRow : ((VBox)itsCaseDescriptionScrollPane.getContent()).getChildren()) {
						
						for (Node anAttributeElement : ((HBox)anAttributeRow).getChildren()) {
							//System.out.println("DEBUG: Sub-Node detected: " + anAttributeElement.toString());
							
							if (anAttributeElement.getId() == null){
								continue;
							}
							
							if (anAttributeElement.getId().equals("Concept")){
								aConceptNode = (Label)anAttributeElement;
//								System.out.println("DEBUG: Concept found: " + aConceptNode.getText());
							}

						}
						
						aValue = getAttributeValueById((String)aConceptNode.getUserData());
						aQueryFilter.put((String)aConceptNode.getUserData(), aValue);						
				}
					
					
					Utils.printTitle("DEBUG: Retrieval parameters");
					// verify query attribute values
					aQueryFilter = MyCbrUtils.getRestrictedValuesList(itsMyCbrBuilderSession.getConcept(), aQueryFilter);
					for (String anAttributeLabel : aQueryFilter.keySet()) {
						System.out.println("  -> " + anAttributeLabel + " = " + aQueryFilter.get(anAttributeLabel));
					}
					
					try {
						MyCbrUtils.performRetrieval(itsMyCbrBuilderSession.getConcept(), aRetrieval, aQueryFilter);
					} catch (ParseException e) {
						e.printStackTrace();
						showExceptionDialog(e);
					}
					
					return aRetrieval;
				}
				
				
				

				
				/**
				 * Indicates if Result Filter settings (UI) indicate a threshold filter.  
				 * 
				 * @return
				 */
			  private boolean isThresholdFiltered(){
				  RadioButton aSelectedRB = (RadioButton)itsToogleGroup_RF.getSelectedToggle();
				  if (aSelectedRB.getText().equals("Threshold [%]")){
					  return true;
				  }
				  
				  return false;
			  }
			
			   
			   
			   /**
			    *  Performs the DataMapping between Retrieval Results and the TableView control.
			    * @throws CaseBaseException Error while access case 
			    */
			  private void generateResultData() throws CaseBaseException {
			 
				  itsCurrentResultData.clear();
				  if (itsCurrentRetrieval == null || itsCurrentRetrieval.getResult() == null){ //|| itsCurrentRetrieval.isEmpty()){
					  return;
				  }
				  
				  Map<String, String> dataRow;
				  String aSimilarity, aCaseId;
				  
					for (Pair<Instance, Similarity> anResult: itsCurrentRetrieval.getResult()) {
						if (isThresholdFiltered() && anResult.getSecond().getValue()*100 < itsThresholdValue){
							continue;
						}
						
						dataRow = new HashMap<>();
//						aSimilarity = new DecimalFormat("##.0000").format(anResult.getSecond().getValue());
						aSimilarity = getFormattedSimilarity(anResult.getSecond(), 1, 4);

						aCaseId = anResult.getFirst().getValueAsString();
						dataRow.put("Similarity", aSimilarity);
						
						dataRow.put("CaseID", aCaseId);
						
						Instance aCase = CaseBaseUtils.getCaseBaseInstanceById(itsMyCbrBuilderSession.getCaseBase(), aCaseId);
						
						for (Map.Entry<AttributeDesc, Attribute> anAttribute : aCase.getAttributes().entrySet()) {
							dataRow.put(anAttribute.getKey().getName(), anAttribute.getValue().getValueAsString());
						}
						
						itsCurrentResultData.add(dataRow);
						
						//System.out.println("DEBUG: CaseID/Similarity: " + aCaseId + "/" + aSimilarity);
					}
			  }
			  
			  
			  /**
			   * Sets all project specific internal variables to its default values;
			   */
			  public void initUIConfiguration(){
				  itsMyCbrBuilderSession = new MyCbrBuilderSession("dummy session");
					
				  itsCurrentResultData.clear();
				  itsWorkingDirectory = ".";
				
				  itsKvalue = 10;
				  itsThresholdValue = 85;
				
				  itsLAYOUT_CurrentResultPane = new TableView<>();
			  }
			  

			  
			   /**
			    * Gets the current concept label.
			    * 
			    * @return Concept label, or empty string if no concept is found.
			    */
			  public String getConceptLabel(){
				  if (itsMyCbrBuilderSession != null && itsMyCbrBuilderSession.getConcept() != null){
					  return itsMyCbrBuilderSession.getConcept().getName();  
				  } else {
					  return "";
				  }	
			  }
			  
			  
				
				
//				@Override
//				public void fetchCaseBaseData() {
//					//itsMyCbrBuilderSession.fetchCaseBaseData(theCaseBaseData, theCaseBaseDataDelimitor, theWhiteList);
//					itsConnector.fetchCaseBase();
//				}

				@Override
				public void generateAmalgamFct(SimilarityMeasure theSimilarityMeasure) throws CaseBaseException, SimilarityMeasureException {
					switch (theSimilarityMeasure) {
					case RANDOM:
						Computation.generateAmalgamFct_RANDOM(
								itsMyCbrBuilderSession.getConcept(), 
								getCaseDescriptionList(), 
								getSolutionDescriptionList());
						break;
					case HEOM:
						Computation.generateAmalgamFct_HEOM(
								itsMyCbrBuilderSession.getConcept(), 
								getCaseDescriptionList(), 
								getSolutionDescriptionList());
						break;
					case HVDM1:
						ComputationVDM.generateAmalgamFct_HVDM1(
								itsMyCbrBuilderSession.getConcept(), 
								getCaseDescriptionList(), 
								getSolutionDescriptionList(), 
								itsMyCbrBuilderSession.getCaseBase(), 
								(VDMConfig)getSimilarityMeasureConfig());
						break;
					case HVDM2:
						ComputationVDM.generateAmalgamFct_HVDM2(
								itsMyCbrBuilderSession.getConcept(), 
								getCaseDescriptionList(), 
								getSolutionDescriptionList(), 
								itsMyCbrBuilderSession.getCaseBase(), 
								(VDMConfig)getSimilarityMeasureConfig());
						break;
					case HVDM3:
						ComputationVDM.generateAmalgamFct_HVDM3(
								itsMyCbrBuilderSession.getConcept(), 
								getCaseDescriptionList(), 
								getSolutionDescriptionList(), 
								itsMyCbrBuilderSession.getCaseBase(), 
								(VDMConfig)getSimilarityMeasureConfig());
						break;
					case DVDM:
						ComputationVDM.generateAmalgamFct_DVDM(
								itsMyCbrBuilderSession.getConcept(), 
								getCaseDescriptionList(), 
								getSolutionDescriptionList(), 
								itsMyCbrBuilderSession.getCaseBase(), 
								(VDMConfig)getSimilarityMeasureConfig());
						break;
					case IVDM:
						ComputationVDM.generateAmalgamFct_IVDM(
								itsMyCbrBuilderSession.getConcept(), 
								getCaseDescriptionList(), 
								getSolutionDescriptionList(), 
								itsMyCbrBuilderSession.getCaseBase(), 
								(VDMConfig)getSimilarityMeasureConfig());
						break;
					case WVDM:
						ComputationVDM.generateAmalgamFct_WVDM(
								itsMyCbrBuilderSession.getConcept(), 
								getCaseDescriptionList(), 
								getSolutionDescriptionList(), 
								itsMyCbrBuilderSession.getCaseBase(), 
								(VDMConfig)getSimilarityMeasureConfig());
						break;
					case KAPLANMEIER:
						ComputationKM.generateAmalgamFct_KAPLANMEIER(
								itsMyCbrBuilderSession.getConcept(), 
								getCaseDescriptionList(), 
								getSolutionDescriptionList(), 
								itsMyCbrBuilderSession.getCaseBase(), 
								(KaplanMeierConfig)getSimilarityMeasureConfig());
						break;
					default:
						throw new SimilarityMeasureException("ERROR: Unsupported Similarity Measure: " + theSimilarityMeasure.getName());
//						System.exit(-1);
					}
					
				}
			
}
