/**
 * (2019) Heidelberg University, Institute of Medical Biometry and Informatics (IMBI), Germany
 * 
 * This file is part of myCbrBuilder.
 *  
 * myCbrBuilder is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * myCbrBuilder is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with vivaGen. If not, see <http://www.gnu.org/licenses/>.
 * 
 **/
package core.connectors;

import java.util.ArrayList;

import exceptions.ConnectorException;

/**
 * Implementation for a connector to the i2b2 data warehouse.
 * 
 * @author Christian Karmen
 *
 */
public class I2b2Connector extends AbstractConnector{
	

	/**
	 * Implementation for a connector to the i2b2 data warehouse.
	 */
	public I2b2Connector(){
		
	}
	
	/**
	 * Gets a list of i2b2 specific meta data columns for facts.
	 * 
	 * @return list of i2b2 specific meta data columns
	 */
    public static ArrayList<String> getI2b2FactsMetaDataList(){
    	ArrayList<String> aReturnValue = new ArrayList<>();
    	
    	aReturnValue.add("PATIENT_NUM");
    	aReturnValue.add("ENCOUNTER_NUM");
    	aReturnValue.add("START_DATE");
    	aReturnValue.add("INSTANCE_NUM");
    	aReturnValue.add("END_DATE");
    	aReturnValue.add("PROVIDER");
    	
    	return aReturnValue;
    }

	@Override
	public void fetchCaseBase() throws ConnectorException{
		// TODO Auto-generated method stub
	}

}
