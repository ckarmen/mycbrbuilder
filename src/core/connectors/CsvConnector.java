/**
 * (2019) Heidelberg University, Institute of Medical Biometry and Informatics (IMBI), Germany
 * 
 * This file is part of myCbrBuilder.
 *  
 * myCbrBuilder is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * myCbrBuilder is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with vivaGen. If not, see <http://www.gnu.org/licenses/>.
 * 
 **/
package core.connectors;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;

import core.CaseBaseUtils;
import core.Utils;
import core.connectors.CSVImporterCustom.CSVImportError;
import de.dfki.mycbr.core.model.Concept;
import exceptions.ConnectorException;

/**
 * Class that implements a connector to a CSV file.
 * 
 * @author Christian Karmen
 *
 */
@Deprecated
public class CsvConnector extends AbstractConnector{
	
	protected File itsCsvFile;
	protected String itsCaseBaseFileNameDelimiter;
		
	
	/**
	 * Connector to read a case base from a CSV file.
	 * 
	 * @param theConcept Concept to add the new case base to
	 * @param theCsvFile File of the CSV
	 * @param theCaseBaseFileNameDelimiter Delimiter char of the CSV
	 * @param theWhiteList list of allowd attributes to import
	 */
	public CsvConnector(Concept theConcept, File theCsvFile, String theCaseBaseFileNameDelimiter, ArrayList<String> theWhiteList){
		itsConcept = theConcept;
		itsCsvFile = theCsvFile;
		itsCaseBaseFileNameDelimiter = theCaseBaseFileNameDelimiter;
		itsWhiteList = theWhiteList;
		
		try {
			itsCaseBase = theConcept.getProject().createDefaultCB("CSVConnectorImport");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	@Override
	public void fetchCaseBase() throws ConnectorException{
		try {
			long startTime = System.currentTimeMillis();
			performCSVImport(Integer.MAX_VALUE);
//			performCSVImport(100);
			
			long aTimeInMS = System.currentTimeMillis() - startTime;
	        System.out.println("DEBUG: CsvConnector.fetchCaseBase() time consumption = " + aTimeInMS + "ms");
		} catch (Exception e) {
			throw new ConnectorException(e);
		}
	}
	
	// FIXME import scheint zu funktionieren, allerdings fehler an anderer Stelle
	// FIXME ggf in AbstractConnector aufnehmen
	public boolean fetchCaseBase(int theRowLimit){
		try {
			performCSVImport(theRowLimit);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	
	
	
	
	/**
	 * Performs a CSV based data import of the Case Base. Only white-listed attribute names are imported.
	 * 
	 * @param theCaseBaseFileName File name of the Case Base in CSV format
	 * @param theCaseBaseFileNameDelimiter Delimitor character of CSV file
	 * @param theWhiteList List of attribute names to import
	 * @throws Exception General CSV import error
	 */
	synchronized private void performCSVImport(int theRowLimit) throws Exception{		
		// Check if file exists
		if (!itsCsvFile.exists()){
			throw new FileNotFoundException("File not found: " + itsCsvFile.getAbsolutePath());
		}
		
		// Initialize CSV Import with this code: (data_path: Path to your csv folder + the csv-file itself
//		CSVImporter aCSVImporter = new CSVImporter(theCaseBaseData, itsConcept);
		//CSVImporter aCSVImporter = new CSVImporter(theCaseBaseData, itsCsvConcept);
		CSVImporterCustom aCSVImporter = new CSVImporterCustom(itsCsvFile.getAbsolutePath(), itsConcept);
		aCSVImporter.setRowLimit(theRowLimit);
		
		// To set the separators that are used in the csv file use these codes.
		aCSVImporter.setSeparator(itsCaseBaseFileNameDelimiter); // column separator
		aCSVImporter.setSymbolThreshold(100);
//		aCSVImporter.setSeparatorMultiple(","); // multiple value separator
		aCSVImporter.readData(); // read csv data
		aCSVImporter.checkData(); // check formal validity of the data
		
		aCSVImporter.applyWhiteList(itsWhiteList);
		
//		aCSVImporter.setChangeDescOnMissingValues(true);
		//aCSVImporter.addMissingDescriptions(); // add default descriptions
		aCSVImporter.addMissingDescriptionsCustom(); // custom descriptions
		
		// FIXME use static variable for "_unknown_"
//		aCSVImporter.setSpecialAttForMissingValues("_unknown_");
		aCSVImporter.addMissingValues(); // add missing values with default values

//		aCSVImporter.getColumnsToBeSkipped();
		
		

		try{
			aCSVImporter.doImport(); // Import the data into the project
		}catch(Exception e){
			e.printStackTrace();
		}
		
		String aCsvProgressText = "CSV import in progress.";
		System.out.print(aCsvProgressText);
		int col=1;
		int row=1;
		while (aCSVImporter.isImporting()) {
//			Thread.sleep(10); 
//			System.out.print(".");
//			if (col++ % 80 == 0 || (row==1 && col++ % (80-aCsvProgressText.length()) == 0 )){
//				row++;
//				System.out.println();
//			}
		}
		String anAddingCaseText = "Adding case to casebase.";
		System.out.println();
		System.out.print(anAddingCaseText);
		
		
		col=1;
		row=1;
//		String anIdLabel;
		HashMap<String,String> anInstanceParameterList = new HashMap<String,String>();
		for (String[] aCSVLine : aCSVImporter.getData()) {
			anInstanceParameterList.clear();
			for (int i=0; i<aCSVLine.length; i++) {
				// FIXME should instanceID column be included, or not?
				if (i==0){	
					//itsConcept.addAttributeDesc(itsConcept.getAttributeDesc(aCSVImporter.getHeader()[i]));
//					anIdLabel = aCSVImporter.getHeader()[i];
//					System.out.println("anIdLabel = " + anIdLabel);
//					continue;
				}
				if (itsWhiteList.contains(aCSVImporter.getHeader()[i])){
					double aDouble;
					try {
						aDouble = Double.parseDouble(aCSVLine[i]);
					} catch (NumberFormatException e) {
						aDouble = Double.NaN;
					}
					
					if (Double.isNaN(aDouble)){
						anInstanceParameterList.put(aCSVImporter.getHeader()[i], aCSVLine[i]);
						System.out.println("Adding Instance value: " + aCSVImporter.getHeader()[i] + " = " + aCSVLine[i]);
					} else {
						System.out.println("DEBUG: aDouble = " + aDouble);
						String aDoubleString = Double.toString(aDouble);
//						aDoubleString = "12.58";
						anInstanceParameterList.put(aCSVImporter.getHeader()[i], aDoubleString);
						System.out.println("Adding Instance value: " + aCSVImporter.getHeader()[i] + " = " + aDoubleString);
					}
				} else {
					System.out.println("Skipping Instance value: " + aCSVImporter.getHeader()[i] + " = " + aCSVLine[i]);
				}
			}
			//CaseBaseUtils.addInstanceToCaseBase(itsCaseBase, itsConcept, "case" + aCSVLine[0], anInstanceParameterList);
			CaseBaseUtils.addInstanceToCaseBase(itsCaseBase, itsConcept, "case" + aCSVLine[0], anInstanceParameterList);

			System.out.print(".");
			if (col++ % 80 == 0 || (row==1 && col++ % (80-anAddingCaseText.length()) == 0 )){
				row++;
				System.out.println();
			}
		}
		System.out.println();
		
//		aCSVImporter.reset();
		
//		printTitle("CSV HEADER");
//		for (String aString : aCSVImporter.getHeader()) {
//			System.out.println(aString);
//		}
//		//printLine();
//		System.out.println();
		
		Utils.printTitle("CSV IMPORT STATISTICS");
		CSVImportError aCSVError = aCSVImporter.getError();
		if (aCSVError != null) {
			System.err.println("Error occured: " + aCSVError.getDesc());
		} else {
			System.out.println("Error occured: NO");
		}
		System.out.println("Total # of cases: " + aCSVImporter.getTotalNumberOfCases());
		System.out.println();
		
//		printTitle("CSV DATA");
//		for (String[] aStringArray : aCSVImporter.getData()) {
//			for (String aString : aStringArray) {
//				System.out.print("[" + aString + "]");
//			}
//			System.out.println();
//		}
		
		
		// DEBUG ONLY
		CaseBaseUtils.printCaseBaseData(itsCaseBase);
	}

}
