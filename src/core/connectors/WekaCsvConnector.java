/**
 * (2019) Heidelberg University, Institute of Medical Biometry and Informatics (IMBI), Germany
 * 
 * This file is part of myCbrBuilder.
 *  
 * myCbrBuilder is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * myCbrBuilder is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with vivaGen. If not, see <http://www.gnu.org/licenses/>.
 * 
 **/
package core.connectors;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map.Entry;

import org.cliommics.vivagen.Instances;

import de.dfki.mycbr.core.DefaultCaseBase;
import de.dfki.mycbr.core.Project;
import de.dfki.mycbr.core.casebase.Instance;
import de.dfki.mycbr.core.model.Concept;
import exceptions.ConnectorException;
import similarity.computation.Computation;
import similarity.desc.ExtendedDoubleDesc;
import similarity.desc.ExtendedSymbolDesc;
import weka.core.Attribute;
import weka.core.AttributeStats;


public class WekaCsvConnector extends AbstractConnector{
	
	protected File itsCsvFile;
	protected String itsCaseBaseFileNameDelimiter;
	protected Instances itsWekaInstances  = null;
	
	final int CaseIdColumn = 0;
	
	/**
	 * Connector to read a case base from a CSV file.
	 * 
	 * @param theConcept Concept to add the new case base to
	 * @param theCsvFile File of the CSV
	 * @param theCaseBaseFileNameDelimiter Delimiter char of the CSV
	 * @param theWhiteList list of allowed attributes to import
	 */
	public WekaCsvConnector(Concept theConcept, File theCsvFile, String theCaseBaseFileNameDelimiter, ArrayList<String> theWhiteList){
		itsConcept = theConcept;
		itsCsvFile = theCsvFile;
		itsCaseBaseFileNameDelimiter = theCaseBaseFileNameDelimiter;
		itsWhiteList = theWhiteList;
		
		try {
			itsCaseBase = theConcept.getProject().createDefaultCB("WekaCsvConnector_Import_" + System.currentTimeMillis());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void fetchCaseBase() throws ConnectorException{
		try {
			fetchCaseBase(Integer.MAX_VALUE);
		} catch (IOException e) {
			throw new ConnectorException(e);
		}
	}
	
	// TODO ggf auch in AbstractConnector aufnehmen	
	public void fetchCaseBase(HashMap<String, String> theFilter) throws ConnectorException{
		try {
			long startTime = System.currentTimeMillis();
			itsWekaInstances = readCsv(theFilter);
			createConceptAttributeDesc();
			weka2CaseBase(itsWekaInstances, itsCaseBase, itsConcept);
			long aTimeInMS = System.currentTimeMillis() - startTime;
	        System.out.println("DEBUG: CsvConnector.fetchCaseBase() time consumption = " + aTimeInMS + "ms");
		} catch (Exception e) {
			throw new ConnectorException(e);
		}
	}
	
	
	// TODO ggf auch in AbstractConnector aufnehmen	
	public void fetchCaseBase(int theRowLimit) 
			throws IOException, ConnectorException{	
		
		if (theRowLimit <= 0){
			throw new ConnectorException("Only a positive row limit value is allowed.");
		}
		
		long startTime = System.currentTimeMillis();
		
		itsWekaInstances = readCsv(theRowLimit);
		createConceptAttributeDesc();
		weka2CaseBase(itsWekaInstances, itsCaseBase, itsConcept);
		long aTimeInMS = System.currentTimeMillis() - startTime;
		
        System.out.println("DEBUG: CsvConnector.fetchCaseBase() time consumption = " + aTimeInMS + "ms");
	}
	
	
	
	/**
	 * Performs a CSV based data import of the Case Base. 
	 * 
	 * @param theRowLimit Number of rows to read (starting from first row)
	 * @throws IOException Error while reading CSV file
	 * @throws ConnectorException Negative row limit value
	 */
	synchronized private Instances readCsv(int theRowLimit) 
			throws IOException, ConnectorException{	
		
		if (theRowLimit <= 0){
			throw new ConnectorException("Only a positive row limit value is allowed.");
		}
		
		// Check if file exists
		if (!itsCsvFile.exists()){
			throw new FileNotFoundException("File not found: " + itsCsvFile.getAbsolutePath());
		}		

		Instances aWekaInstances = Instances.readCSV(itsCsvFile.getAbsolutePath(), itsCaseBaseFileNameDelimiter);
		
		// FIXME performance improvement: row limit should apply within import
		// row limit
		while(aWekaInstances.size() > theRowLimit){
			aWekaInstances.remove(aWekaInstances.size()-1);
		}
		
		return aWekaInstances;
	}
	
	
	/**
	 * Performs a CSV based data import of the Case Base. Only white-listed attribute names are imported.
	 * 
	 * @param theFilter
	 * @throws IOException Error while reading CSV file
	 */
	synchronized private Instances readCsv(HashMap<String, String> theFilter) throws IOException{		
		// Check if file exists
		if (!itsCsvFile.exists()){
			throw new FileNotFoundException("File not found: " + itsCsvFile.getAbsolutePath());
		}		

		Instances aWekaInstances = Instances.readCSV(itsCsvFile.getAbsolutePath(), itsCaseBaseFileNameDelimiter);
		int aFilterAttributeIndex;
		String anAttributeValue;
		boolean aDeleteFlag;
		
		// FIXME performance improvement: filter should apply within import
		for (int i = aWekaInstances.size()-1; i >= 0 ; i--) {
			aDeleteFlag = false;
			for (Entry<String, String> aFilterEntry : theFilter.entrySet()) {
				aFilterAttributeIndex = aWekaInstances.getAttributeIndex(aFilterEntry.getKey());
				anAttributeValue = aWekaInstances.getValueByAttributeIndex(i, aFilterAttributeIndex);
//				if (!anAttributeValue.equalsIgnoreCase("'" + aFilterEntry.getValue() + "'") && !aDeleteFlag){
				if (!anAttributeValue.equalsIgnoreCase(aFilterEntry.getValue()) && !aDeleteFlag){
					aWekaInstances.delete(i);
					aDeleteFlag = true;
				}
			}
		}

		return aWekaInstances;
	}
	
	
	private void weka2CaseBase(Instances theWekaInstances, DefaultCaseBase theCaseBase, Concept theConcept) {
		Instance aCase;
		String anCaseId;
		Attribute aWekaAttribute;
		String anAttributeValue = "";

		for (weka.core.Instance aWekaInstance : theWekaInstances) {
			
			// get case ID: must be in 1st column (required)
			if (aWekaInstance.attribute(CaseIdColumn).isNumeric()) {
				anCaseId = (new DecimalFormat("0")).format(aWekaInstance.value(CaseIdColumn));
			} else if (aWekaInstance.attribute(CaseIdColumn).isNominal()) {
				anCaseId = aWekaInstance.stringValue(CaseIdColumn);
			} else {
				System.err.println("ERROR: cannot detect caseID");
				return;
			}
			
			aCase = new Instance(theConcept, "case" + anCaseId);
			
			for (int i = 0; i < aWekaInstance.numAttributes(); i++) {
				aWekaAttribute = aWekaInstance.attribute(i);
				
				// skip 1st column (caseID)
				if (i == CaseIdColumn){
					continue;
				}
				
				// skip non-whitelist elements
				if (!itsWhiteList.contains(aWekaAttribute.name())){
					continue;
				}
				
//				if (aWekaInstance.attribute(i).isNominal()) {
//					System.out.println("stringValue(i) = " + aWekaInstance.stringValue(i));
//				} else if (aWekaInstance.attribute(i).isNumeric()) {
//					System.out.println("value(i) = " + aWekaInstance.value(i));
//				} else {
//					System.err.println("ERROR: unknown data type!");
//				}
				
				
				try {
					
					if (aWekaInstance.attribute(i).isNominal()){
						anAttributeValue = aWekaInstance.stringValue(aWekaAttribute);
						aCase.addAttribute(aWekaAttribute.name(), anAttributeValue);
					} else if (aWekaInstance.attribute(i).isNumeric()){
						anAttributeValue = Double.toString(aWekaInstance.value(aWekaAttribute));
//						aCase.addAttribute(aWekaAttribute.name(), aWekaInstance.value(aWekaAttribute));
						aCase.addAttribute(aWekaAttribute.name(), anAttributeValue);
					} else {
						System.err.println("ERROR: cannot detect attribute's value");
						anAttributeValue = Project.UNKNOWN_SPECIAL_VALUE;
					}
//					System.out.println("DEBUG: " + aWekaAttribute.name() + " = " + anAttributeValue);
				
//					aCase.addAttribute(aWekaAttribute.name(), anAttributeValue);
				} catch (ParseException e) {
					e.printStackTrace();
				}
			}
			theCaseBase.addCase(aCase);
//			System.out.println();
		}
		
//		for (weka.core.Instance aWekaInstance : theWekaInstances) {
//			aCase = new Instance(itsConcept, aWekaInstance.attribute(0).value(0));
//			theCaseBase.addCase(aCase);
//		}
		
	}
	
	
	private void createConceptAttributeDesc() {
		String anWekaAttributeName;
		Attribute aWekaAttribute;
//		ExtendedSymbolDesc aSymbolDesc;
		ExtendedDoubleDesc aDoubleDesc;
		AttributeStats anAttributeStats;
		
		for (int i = 0; i < itsWekaInstances.numAttributes(); i++) {
			
			// skip 1st column (caseID)
			if (i == CaseIdColumn){
				continue;
			}
			
			aWekaAttribute = itsWekaInstances.attribute(i);
			anWekaAttributeName = aWekaAttribute.name();
			anAttributeStats = itsWekaInstances.attributeStats(i);
			
			// skip non-whitelist elements
			if (!itsWhiteList.contains(aWekaAttribute.name())){
				continue;
			}
			
			if (aWekaAttribute.isNominal()) {
				try {
					new ExtendedSymbolDesc(
							itsConcept, 
							anWekaAttributeName, 
							getAllowedNominalValues(aWekaAttribute));
				} catch (Exception e) {
					e.printStackTrace();
				}
			} else if (aWekaAttribute.isNumeric()) {
				try {
					aDoubleDesc = new ExtendedDoubleDesc(
							itsConcept, 
							anWekaAttributeName, 
							anAttributeStats.numericStats.min,
							anAttributeStats.numericStats.max);

					
					// Getting some statistics for numeric values 
					ArrayList<Double> aValueList = new ArrayList<>();
					for (int j=0; j<anAttributeStats.totalCount; j++) {
						aValueList.add(itsWekaInstances.getDoubleValueByAttributeIndex(j, i));
					}
					aDoubleDesc.setMedianValue(Computation.getMedianValue(aValueList)); 
					aDoubleDesc.setMeanValue(anAttributeStats.numericStats.mean);
					aDoubleDesc.setStandardDeviation(anAttributeStats.numericStats.stdDev);
					
				} catch (Exception e) {
					e.printStackTrace();
				}
			} else {
				System.err.println("ERROR: Unsupported attribute type");
			}
		}
	}
	

	
	private HashSet<String> getAllowedNominalValues(Attribute theWekaAttribute){
		HashSet<String> anAllowedValueList = new HashSet<String>();
		
		for (int i=0; i < theWekaAttribute.numValues(); i++) {
			// ignore special values
			if (theWekaAttribute.value(i).equalsIgnoreCase(Project.OTHERS_SPECIAL_VALUE) ||
					theWekaAttribute.value(i).equalsIgnoreCase(Project.UNDEFINED_SPECIAL_VALUE) ||
					theWekaAttribute.value(i).equalsIgnoreCase(Project.UNKNOWN_SPECIAL_VALUE)){
				continue;
			}
			anAllowedValueList.add(theWekaAttribute.value(i));
		}
		
		return anAllowedValueList;
	}
	
	
//	/**
//	 * Main program.
//	 * 
//	 * @param args Console arguments 
//	 */
//	public static void main(String[] args) {
//		Project aProject = null;
//		Concept aConcept = null;
//		try {
//			aProject = new Project();
//			aConcept = aProject.createTopConcept("fooConcept");
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//		
//		File aCsvFile = new File(
//				Utils.getCustomDataPath(CUSTOM_DATAPATH.GENERATED) + 
//				"debug.generatedData250.kaputt.csv");
//		
//		ArrayList<String> aCaseDescriptionList = new ArrayList<>();
//		aCaseDescriptionList.add("UniformStrings1");
//		
//		ArrayList<String> aSolutionDescriptionList = new ArrayList<>();
//		aSolutionDescriptionList.add("RandomArm");
//		aSolutionDescriptionList.add("SurvivalTime");
//		aSolutionDescriptionList.add("SurvivalStatus");	
//		aSolutionDescriptionList.add("SurvivalType");		// for evaluation only
//		
//		ArrayList<String> aWhiteList = new ArrayList<String>();
//		aWhiteList.addAll(aCaseDescriptionList); 
//		aWhiteList.addAll(aSolutionDescriptionList);
//		
//		WekaCsvConnector aConnector = new WekaCsvConnector(aConcept, aCsvFile, ",", aWhiteList);
//		aConnector.fetchCaseBase();
//		
//		CaseBaseUtils.printCaseBaseData(aConnector.getCaseBase());
//		
////		aConnector.createConceptAttributeDesc();
//		MyCbrUtils.printConceptDescription(aConnector.getConcept());
//	}
	
	

}
