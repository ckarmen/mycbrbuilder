/**
 * (2019) Heidelberg University, Institute of Medical Biometry and Informatics (IMBI), Germany
 * 
 * This file is part of myCbrBuilder.
 *  
 * myCbrBuilder is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * myCbrBuilder is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with vivaGen. If not, see <http://www.gnu.org/licenses/>.
 * 
 **/
package core.connectors;

import java.util.ArrayList;

import de.dfki.mycbr.core.DefaultCaseBase;
import de.dfki.mycbr.core.model.Concept;
import exceptions.ConnectorException;

/**
 * Abstract class for connectors to fact data.
 * 
 * @author Christian Karmen
 *
 */
public abstract class AbstractConnector {
	protected static DefaultCaseBase itsCaseBase;
	protected static Concept itsConcept;
	protected static ArrayList<String> itsWhiteList;
	
	/**
	 * Performs the data acquisition for the case base.
	 * 
	 * @throws ConnectorException Error in reading source data
	 */
	public abstract void fetchCaseBase() throws ConnectorException;
	
	
	/**
	 * Gets the case base if fetching was successful.
	 * 
	 * @return fetched case base
	 */
	public DefaultCaseBase getCaseBase(){
		return itsCaseBase;
	}
	
	/**
	 * Gets the concept where the fetched case base was imported
	 * 
	 * @return concept of the fetched case base
	 */
	public Concept getConcept(){
		return itsConcept;
	}
	
	/**
	 * Gets the list of attribute labels for the case base.
	 * 
	 * @return list of attribute labels for the case base
	 */
	public ArrayList<String> getWhiteList(){
		return itsWhiteList;
	}

}
