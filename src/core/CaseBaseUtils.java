/**
 * (2019) Heidelberg University, Institute of Medical Biometry and Informatics (IMBI), Germany
 * 
 * This file is part of myCbrBuilder.
 *  
 * myCbrBuilder is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * myCbrBuilder is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with vivaGen. If not, see <http://www.gnu.org/licenses/>.
 * 
 **/
package core;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;

import de.dfki.mycbr.core.DefaultCaseBase;
import de.dfki.mycbr.core.Project;
import de.dfki.mycbr.core.casebase.Attribute;
import de.dfki.mycbr.core.casebase.Instance;
import de.dfki.mycbr.core.casebase.SpecialAttribute;
import de.dfki.mycbr.core.model.AttributeDesc;
import de.dfki.mycbr.core.model.Concept;
import de.dfki.mycbr.core.model.DoubleDesc;
import de.dfki.mycbr.core.model.FloatDesc;
import de.dfki.mycbr.core.model.IntegerDesc;
import de.dfki.mycbr.core.model.SimpleAttDesc;
import de.dfki.mycbr.core.model.SymbolDesc;
import exceptions.CaseBaseException;
import similarity.desc.ExtendedDoubleDesc;
import similarity.desc.ExtendedIntegerDesc;
import similarity.desc.ExtendedSymbolDesc;

/**
 * Class for some useful CaseBase related static functions.
 * 
 * @author Christian Karmen
 *
 */
public class CaseBaseUtils {
	
	/**
	 * Gets a list of all available attributes within the given case base.
	 * 
	 * @param theCaseBase Case base of interest
	 * @return list of all available attributes
	 */
	public static ArrayList<AttributeDesc> getCaseBaseAttributeDescList(DefaultCaseBase theCaseBase){
		Instance aFirstCbrCase = (Instance)(theCaseBase.getCases().toArray())[0];
		return new ArrayList<>(aFirstCbrCase.getAttributes().keySet());
	}
	
	
	/**
	 * Gets a list of all available attribute labels within the given case base.
	 * 
	 * @param theCaseBase Case base of interest
	 * @return list of all available attribute labels
	 */
	public static ArrayList<String> getCaseBaseAttributeLabelList(DefaultCaseBase theCaseBase){
		ArrayList<String> anAttributeLabelList = new ArrayList<>();
		for (AttributeDesc anAttributeDesc : getCaseBaseAttributeDescList(theCaseBase)) {
			anAttributeLabelList.add(anAttributeDesc.getName());
		}
		return anAttributeLabelList;
	}
	
	
	/**
	 * Gets the attribute with the given label in the given case base. 
	 * 
	 * @param theCaseBase Case base of interest
	 * @param theAttributeLabel Label of the attribute
	 * @return attribute with the given label
	 * @throws CaseBaseException Attribute not found in case base
	 */
	public static AttributeDesc getCaseBaseAttributeDescByLabel(DefaultCaseBase theCaseBase, String theAttributeLabel) throws CaseBaseException{
		Instance aFirstCaseInstance = getFirstCase(theCaseBase);
		for (AttributeDesc anAttributeDesc : aFirstCaseInstance.getAttributes().keySet()) {
			if (anAttributeDesc.getName().equalsIgnoreCase(theAttributeLabel))
				return anAttributeDesc;
		}
		throw new CaseBaseException("In Case Base the attribute with label '" + theAttributeLabel + "' was not found.");
	}
	
	
	
	
	/**
	 * Gets the attribute value with the given attribute label in a given Instance (Case).
	 * 
	 * @param theCaseInstance Case Instance of interest
	 * @param theAttributeLabel Label of the attribute of interest
	 * @return Attribute value with the given Label
	 * 
	 * @throws CaseBaseException Attribute not found in case
	 */
	public static Attribute getCaseAttributeValue(Instance theCaseInstance, String theAttributeLabel) throws CaseBaseException{		
		for (AttributeDesc anAttributeDesc : theCaseInstance.getAttributes().keySet()) {
			if (anAttributeDesc.getName().equalsIgnoreCase(theAttributeLabel)){
				return theCaseInstance.getAttributes().get(anAttributeDesc);
			}					
		}	
		
		throw new CaseBaseException("The attribute '" + theAttributeLabel + "' was not found in case '" + theCaseInstance.getName() + "'.");
	}
	
	
	/**
	 * Checks if a attribute in the case base is numeric.
	 * 
	 * @param theCaseBase case base
	 * @param theAttributeLabel label of the attribute
	 * @return true=attribute is numeric; false=else
	 * @throws CaseBaseException Error while access to the attribute
	 */
	public static boolean isCaseBaseAttributeNumeric(DefaultCaseBase theCaseBase, String theAttributeLabel) throws CaseBaseException{
		AttributeDesc anAttribute = getCaseBaseAttributeDescByLabel(theCaseBase, theAttributeLabel);
		
		if (anAttribute instanceof ExtendedDoubleDesc || 
				anAttribute instanceof ExtendedIntegerDesc ||
				anAttribute instanceof DoubleDesc ||
				anAttribute instanceof FloatDesc ||
				anAttribute instanceof IntegerDesc){
			return true;
		}
		
		return false;
	}
	
	
	/**
	 * Checks if a attribute in the case base is nominal.
	 * 
	 * @param theCaseBase case base
	 * @param theAttributeLabel label of the attribute
	 * @return true=attribute is nominal; false=else
	 * @throws CaseBaseException Error while access to the attribute
	 */
	public static boolean isCaseBaseAttributeNominal(DefaultCaseBase theCaseBase, String theAttributeLabel) throws CaseBaseException{
		AttributeDesc anAttribute = getCaseBaseAttributeDescByLabel(theCaseBase, theAttributeLabel);
		
		if (anAttribute instanceof ExtendedSymbolDesc || 
				anAttribute instanceof SymbolDesc){
			return true;
		}
		
		return false;
	}
	
	/**
	 * Gets the first instance of the given case base.
	 * 
	 * @param theCaseBase Case base of interest
	 * @return first instance
	 */
	public static Instance getFirstCase(DefaultCaseBase theCaseBase){
		 return (Instance)(theCaseBase.getCases().toArray())[0];
	}
	
	
	/**
	 * Gets a list of caseIDs of all available cases in the given case base. 
	 * 
	 * @param theCaseBase Case Base of interest 
	 * @return list of caseIDs
	 */
	public static ArrayList<String> getCaseIdList(DefaultCaseBase theCaseBase){
		ArrayList<String> aCaseIdList = new ArrayList<>();				
		for (Instance anInstance : theCaseBase.getCases()) {
			aCaseIdList.add(anInstance.getName());
		}
		return aCaseIdList;
	}
	
	
	/**
	 * Gets the CB instance with given ID.
	 * 
	 * @param theCaseBase Case Base of interest
	 * @param theInstanceID Instance ID of interest
	 * @return CB instance with given ID
	 * @throws CaseBaseException Error while getting case by id
	 */
	public static Instance getCaseBaseInstanceById(DefaultCaseBase theCaseBase, String theInstanceID) throws CaseBaseException{
		for (Instance anInstance : theCaseBase.getCases()) {
			if (anInstance.getName().equals(theInstanceID)){
				return anInstance;
			}
		}
		throw new CaseBaseException("Case not found with id: " + theInstanceID);
	}
	

	   /**
	    * Gets a list of all CaseBase elements of a given attribute.
	    * 
	    * @param theAttDesc Attribute of interest
	    * @param theSkipSpecialAttributesFlag true=skip special attributes; false=includes special attributes
	    * @return list of all CaseBase elements
	    */
		public static ArrayList<Attribute> getAllAttributes(AttributeDesc theAttDesc, boolean theSkipSpecialAttributesFlag){
			ArrayList<Attribute> anAttributeList = new ArrayList<Attribute>();
			Attribute theAttribute;		
			
			for (Instance anInstance : theAttDesc.getOwner().getAllInstances()) {
				theAttribute = anInstance.getAttributes().get(theAttDesc);
				
				if (theSkipSpecialAttributesFlag && theAttribute instanceof SpecialAttribute){
					continue;
				}
				anAttributeList.add(theAttribute);
			}
			
			return anAttributeList;
		}
		
		
	   /**
	    * Gets a list of all available values of a given attribute description.
	    * 
	    * @param theAttDesc Attribute of interest
	    * @return list of all CaseBase elements
	    */
		public static ArrayList<Attribute> getAllAttributeValues(AttributeDesc theAttDesc){
			return getAllAttributes(theAttDesc, false);
		}
		
		
		/**
		 * Gets a list of all available numeric values of a given attribute description.
		 * 
	     * @param theAttDesc Attribute of interest
		 * @return list of all CaseBase elements
		 */
		public static ArrayList<Double> getAllCaseBaseAttributesAsDouble(SimpleAttDesc theAttDesc){
			ArrayList<Double> aValueList = new ArrayList<Double>();
			double aValue;
			
			for (Attribute anAttribute : getAllAttributes(theAttDesc, true)) {
				aValue = Double.parseDouble(anAttribute.getValueAsString());
				aValueList.add(aValue);
			}
			
			return aValueList;
		}
		
		
		/**
		 * Gets a random case (instance) out of the Default Case Base.
		 * 
		 * @param theCaseBase Case Base to choose from
		 * @return Random case (instance)
		 * @throws CaseBaseException Case base is empty
		 */
		public static Instance getRandomCase(DefaultCaseBase theCaseBase) throws CaseBaseException{
			int aCBsize = theCaseBase.getCases().size();
			if (aCBsize == 0){
				throw new CaseBaseException("Case base is empty");
			}
			int i = (new Random()).nextInt(aCBsize);
			System.out.println("Getting case " + i + " of " + aCBsize + " total cases...");
			
			Object[] anArray = theCaseBase.getCases().toArray();
			
			Instance anInstance = (Instance)anArray[i];
			
			if (anInstance == null){
				System.err.println("ERROR: Cannot get CB Instance!");
			}else{
				System.out.println("\t=> Found case with CaseID: " + anInstance.getName());
			}
			
			return anInstance;
		}
		

		
		/**
		 * Adds an Instance to a Case Base.
		 * 
		 * @param theCaseBase Case base to add the instance
		 * @param theConcept Concept to add the instance
		 * @param theInstanceLabel Name of the Instance
		 * @param theInstanceAttributeValueList List of Attribute values
		 * 
		 * @throws CaseBaseException Error while adding Instance to case base
		 */
		public static void addInstanceToCaseBase(
				DefaultCaseBase theCaseBase, 
				Concept theConcept, 
				String theInstanceLabel, 
				HashMap<String,String> theInstanceAttributeValueList) 
						throws CaseBaseException{
			
			Instance aInstance;
			try {
				aInstance = theConcept.addInstance(theInstanceLabel); // new Instance(itsConcept, theInstanceLabel);
			} catch (Exception e) {
				throw new CaseBaseException(e);
			} 
			AttributeDesc anAttribute;
			Attribute anAttributeValue;
			boolean aSuccessFlag;
			
			for (Map.Entry<String, String> aParameter: theInstanceAttributeValueList.entrySet()) {
				anAttribute = theConcept.getAttributeDesc(aParameter.getKey());
				
				if (anAttribute != null){
					try {
						anAttributeValue = anAttribute.getAttribute(aParameter.getValue());
					} catch (ParseException e) {
						throw new CaseBaseException(e);
					}
					
					aSuccessFlag = aInstance.addAttribute(anAttribute, anAttributeValue);
					if (!aSuccessFlag){
						System.err.println("ERROR: Adding attribute '" + anAttribute + "' = " + aParameter.getValue() + " failed!");
					}
				} else {
					throw new CaseBaseException("ERROR: Cannot detect AttributeDesc of '" + aParameter.getKey() + "'");
				}
			}
			
			theCaseBase.addCase(aInstance);
		}
		

		
		
		/**
		 * Clones a given Case Base.
		 * 
		 * @param theOriginalCaseBase Case Base to clone
		 * @return cloned Case Base
		 * 
		 * @throws CaseBaseException Error while creating new Case Base
		 */
		public static DefaultCaseBase cloneCaseBase(DefaultCaseBase theOriginalCaseBase) throws CaseBaseException{
			String aNewCBLabel = theOriginalCaseBase.getName() + "_cloned_" + Math.random() * Integer.MAX_VALUE;
			DefaultCaseBase aNewCB;
			try {
				aNewCB = new DefaultCaseBase(new Project(), aNewCBLabel, theOriginalCaseBase.getCases().size());
			} catch (Exception e) {
				throw new CaseBaseException(e);
			}
//			Instance anInstance;
			
			for (Instance aCaseInstance : theOriginalCaseBase.getCases()) {
				aNewCB.addCase(aCaseInstance);
			}
		
			return aNewCB;
		}
		
		
		/**
		 * Clones a given Case Base. Filters cases that contain a specific attribute's value.
		 * 
		 * @param theOriginalCaseBase Case Base to clone
		 * @param theAttributeLabel Label of the attribute
		 * @param theAttributeValue
		 * 
		 * @return cloned Case Base
		 * 
		 * @throws CaseBaseException Error occurred while creating new case base
		 */
		public static DefaultCaseBase getClonedCaseBase(DefaultCaseBase theOriginalCaseBase, String theAttributeLabel, String theAttributeValue) throws CaseBaseException{
			AttributeDesc anAttributeDesc = getCaseBaseAttributeDescByLabel(theOriginalCaseBase, theAttributeLabel);
			DefaultCaseBase aClonedCB = cloneCaseBase(theOriginalCaseBase);
			
			for (Instance aCaseInstance : aClonedCB.getCases()) {
				if (aCaseInstance.getAttForDesc(anAttributeDesc).getValueAsString().equalsIgnoreCase(theAttributeValue)){
					aClonedCB.removeCase(aCaseInstance);
				}
			}
			
			return aClonedCB;
		}
		
		
		/**
		 * Clones a given Case Base and filters it for allowed values of a given nominal attribute.
		 * <br/>
		 * CAUTION: Only filtering of a nominal attribute is allowed
		 * 
		 * @param theOriginalCaseBase Case Base to clone
		 * @param theAttributeLabel Label of filtered attribute (only nominal attribute is allowed)
		 * @param theValueLabelList List of filtered values for given attribute
		 * @param theConditionFlag true=cases with found values are added; false=cases without found values are added
		 * @return filtered Case Base
		 * 
		 * @throws CaseBaseException Error occurred while creating new Case Base
		 */
		public static DefaultCaseBase filterCaseBase(
				DefaultCaseBase theOriginalCaseBase, 
				String theAttributeLabel, 
				ArrayList<String> theValueLabelList,
				boolean theConditionFlag) 
						throws CaseBaseException{
			
			String aNewCBLabel = theOriginalCaseBase.getName() + "_filtered_" + Math.random() * Integer.MAX_VALUE;
			DefaultCaseBase aNewCB;
			try {
				aNewCB = new DefaultCaseBase(new Project(), aNewCBLabel, theOriginalCaseBase.getCases().size());
			} catch (Exception e) {
				throw new CaseBaseException(e);
			}
			
			AttributeDesc anAttribute = getCaseBaseAttributeDescByLabel(theOriginalCaseBase, theAttributeLabel);
			String anAttributeValue;
			boolean aFoundFlag;
			
			for (Instance aCaseInstance : theOriginalCaseBase.getCases()) {
				anAttributeValue = aCaseInstance.getAttForDesc(anAttribute).getValueAsString();
				aFoundFlag = false;
				for (String aValueLabel : theValueLabelList) {					
					if (anAttributeValue.equalsIgnoreCase(aValueLabel)){
						aFoundFlag = true;
					} 
				}
				if ((aFoundFlag && theConditionFlag)
						||
						(!aFoundFlag && !theConditionFlag)){
					aNewCB.addCase(aCaseInstance);
				}
			}
			
			return aNewCB;
		}
		
		
		/**
		 * Prints Case Base details to console.
		 * 
		 * @param theCaseBase Case Base of interest
		 */
		public static void printCaseBaseData(DefaultCaseBase theCaseBase){
			Utils.printTitle("CASE BASE DATA");
			
			for (Instance anInstance : theCaseBase.getCases()) {
				System.out.print(anInstance.getName() + ": ");
				for (Entry<AttributeDesc, Attribute> anEntry : anInstance.getAttributes().entrySet()) {
					System.out.print("[" + anEntry.getKey() + ":" + anEntry.getValue().getValueAsString() + "]");
				}
				System.out.println();
			}
			
			System.out.println();
		}
		
		
		public static void printCaseAttributes(Instance theInstance){
			Utils.printTitle("Print Case attributes");
			for (Entry<AttributeDesc, Attribute> anAttribute : theInstance.getAttributes().entrySet()) {
				System.out.println(anAttribute.getKey() + " = " + anAttribute.getValue());
			}
		}
		
		public static void printCase(DefaultCaseBase theCaseBase, String theInstanceID){
			for (Instance anInstance : theCaseBase.getCases()) {
				if (anInstance.getName().equalsIgnoreCase(theInstanceID)){
					printCaseAttributes(anInstance);
				}
			}
		}

}
