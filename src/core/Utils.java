/**
 * (2019) Heidelberg University, Institute of Medical Biometry and Informatics (IMBI), Germany
 * 
 * This file is part of myCbrBuilder.
 *  
 * myCbrBuilder is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * myCbrBuilder is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with vivaGen. If not, see <http://www.gnu.org/licenses/>.
 * 
 **/
package core;

import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.regex.Pattern;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import application.javafx.IMyCBRBuilder;
import application.javafx.MyCBRBuilderFxElements;

/**
 * General purpose utilities.
 * 
 * @author Christian Karmen
 *
 */
public class Utils {
	
	
	/**
	 * Prints a doubled line of defined length (=80) to console. 
	 */
	static public void printLine(){
		printLine("=", 80);
	}
	
	
	/**
	 * Prints a line of given length and given char element to console. 
	 * 
	 * @param theLineChar character element for the line
	 * @param theLineLength length of the line
	 */
	static public void printLine(String theLineChar, int theLineLength){
		StringBuilder aSB = new StringBuilder();
		while (theLineLength-- > 0) {
			aSB.append(theLineChar);
		}
		System.out.println(aSB);
	}
	
	
	/**
	 * Prints a designed header with a given title to console. 
	 * 
	 * @param theTitle Text to show as title.
	 */
	static public void printTitle(String theTitle){
		String aTitleLine =  " >>> " + theTitle + " <<< ";
		
		printLine("=", aTitleLine.length());
		System.out.println(aTitleLine);
		printLine("=", aTitleLine.length());
	}
	
	
	
	
	
	
	  /**
	   * Compress a list of files into one zip file.
	   * 
	   * @param theFileList Files to zip
	   * @param theZipFile Target zip file
	   * @throws IOException Error while creating zip
	   * @throws FileNotFoundException Error while reading files from file list
	   */
	  public static void zipFile(
			  final ArrayList<File> theFileList, 
			  final File theZipFile) 
					  throws IOException, FileNotFoundException {
		  
	      FileOutputStream aZipFileOut = new FileOutputStream(theZipFile);
	      ZipOutputStream aZipOut = new ZipOutputStream(aZipFileOut);
	      byte[] buffer = new byte[128];
	      int read;
	      ZipEntry aZipEntry;
	      FileInputStream aFileIn;
	      
	      for (File aCurrentFile : theFileList) {
	        if (!aCurrentFile.isDirectory()) {
	          aZipEntry = new ZipEntry(aCurrentFile.getName());
	          aFileIn = new FileInputStream(aCurrentFile);
	          aZipOut.putNextEntry(aZipEntry);
	          read = 0;
	          while ((read = aFileIn.read(buffer)) != -1) {
	            aZipOut.write(buffer, 0, read);
	          }
	          aZipOut.closeEntry();
	          aFileIn.close();
	        }
	      }
	      aZipOut.close();
	      aZipFileOut.close();
	  }
	  
	  
	  /**
	   * Moves all files in given given directory (sub-directories not included) in to a newly created sub-folder with given archive label. 
	   * 
	   * @param theSourcePath Path to archive
	   * @param theValidationLabel Label of archive
	   */
	  public static void archiveFiles(String theSourcePath, String theValidationLabel){
			// Archiving the validation results
			Date aCurrentTime = Calendar.getInstance().getTime();
			DateFormat aDateFormat = new SimpleDateFormat("yyyMMdd_HHmmss");
			File aValidationDirectory = new File(theSourcePath);
//			File anArchiveDirectory = new File(aValidationDirectory, theValidationLabel + "_" + aDateFormat.format(aCurrentTime) + File.separator);
			File anArchiveDirectory = new File(aValidationDirectory, aDateFormat.format(aCurrentTime) + "_" + theValidationLabel + File.separator);
			try{
				// create archive directory
	  		if(anArchiveDirectory.mkdirs()) {		    			
	  		    for (File aFile : aValidationDirectory.listFiles()) {
	  		    	// move all files to archive directory
						if (!aFile.isDirectory()){
							aFile.renameTo(new File(anArchiveDirectory, aFile.getName()));
						}
					}
	  		}
			} catch (Exception exception) {
				MyCBRBuilderFxElements.showExceptionDialog(exception);
			}
	  }
	  
	  
	  
	/**
	 * Returns all files in a given directory that matches a regular expression.
	 * 
	 * @param theDirectory Directory of interest
	 * @param theRegex Regular expression of interest
	 * @return all files that matches the regular expression
	 */
	public static File[] getFiles(File theDirectory, String theRegex) {
		    if(!theDirectory.isDirectory()) {
		        throw new IllegalArgumentException(theDirectory+" is no directory.");
		    }
		    final Pattern aPattern = Pattern.compile(theRegex); 
		    return theDirectory.listFiles(new FileFilter(){
		        @Override
		        public boolean accept(File file) {
		            return aPattern.matcher(file.getName()).matches();
		        }
		    });
		}
	
	
	/**
	 * Deletes all files in a given directory that matches a regular expression.
	 * 
	 * @param theDirectory Directory of interest
	 * @param theRegex Regular expression of interest
	 */
	public static void removeFiles(File theDirectory, String theRegex){
		for (File aFile : getFiles(theDirectory, theRegex)) {
			aFile.delete();
		}
	}
	
	
	
	
	/**
	 * Converts any collection to a human-readable string in the form [element1, element2, ...].
	 * 
	 * @param theCollection Collection Any Object that uses the Collection Interface
	 * @return human-readable string in the form [element1, element2, ...]
	 */
	public static String toString(Collection<?> theCollection){
		String anOutput = "[";
		for (int i = 0; i < theCollection.size(); i++) {
			anOutput += theCollection.toArray()[i].toString();
			if (i != theCollection.size()-1){
				anOutput += ", ";
			}
		}
		anOutput += "]";
		
		return anOutput;
	}
	
	
	
	/**
	 * Gets the customized Data path.
	 * 
	 * @param theCustomDataset Dataset Configuration 
	 * @return Path of dataset
	 */
  	public static String getCustomDataPath(CUSTOM_DATAPATH theCustomDataset){	
//  		String theClientHostname = "";
//		try {
//			theClientHostname = InetAddress.getLocalHost().getHostName();
//		} catch (UnknownHostException e) {					
//			MyCBRBuilderFxElements.showExceptionDialog(e);
//		}
  		
  		String aDataPath;
  		switch (IMyCBRBuilder.detectClientOS()) {
		case WINDOWS:
			aDataPath = "Z:\\CBR\\";
			break;
		case MAC:
			aDataPath = "/Users/rikerck/CBR/";
//			aDataPath = "/Volumes/NO NAME/CBR/";
			break;
		case LINUX:
		case OTHER:
		default:
			aDataPath = "/home/rikerck/CBR/";
			break;
		}
  		
  		switch (theCustomDataset) {
		case HD4:
			aDataPath += "HD4";
			break;
		case HARMONIZED:
			aDataPath += "Harmonized";
			break;
		case GENERATED:
			aDataPath = "." + File.separatorChar + "datasets";
			//aDataPath += File.separatorChar + "generatedSet";
			break;
		case TEMP:
			aDataPath = "." + File.separatorChar + "tmp";
			break;
		case CURRENT:
		default:
			aDataPath = ".";
			break;
		}
  		
  		return aDataPath + File.separatorChar;
	}


  	/**
  	 * List of predefined custom dataset (-directory).
  	 * 
  	 * @author Christian Karmen
  	 *
  	 */
	public enum CUSTOM_DATAPATH{
		/**
		 * current path (where application starts)
		 */
		CURRENT, 
		
		/**
		 * Base dataset path
		 */
		BASE, 
		
		/**
		 * HD4 dataset
		 */
		HD4, 
		
		/**
		 * (HD4) harmonized path
		 */
		HARMONIZED, 
		
		/**
		 * generated path
		 */
		GENERATED,
		
		/**
		 * path for temporary files
		 */
		TEMP
	}
  
	


}
