/**
 * (2019) Heidelberg University, Institute of Medical Biometry and Informatics (IMBI), Germany
 * 
 * This file is part of myCbrBuilder.
 *  
 * myCbrBuilder is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * myCbrBuilder is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with vivaGen. If not, see <http://www.gnu.org/licenses/>.
 * 
 **/
package core;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import de.dfki.mycbr.core.Project;
import de.dfki.mycbr.core.casebase.Instance;
import de.dfki.mycbr.core.casebase.StringAttribute;
import de.dfki.mycbr.core.model.AttributeDesc;
import de.dfki.mycbr.core.model.Concept;
import de.dfki.mycbr.core.model.DoubleDesc;
import de.dfki.mycbr.core.model.FloatDesc;
import de.dfki.mycbr.core.model.IntegerDesc;
import de.dfki.mycbr.core.model.SymbolDesc;
import de.dfki.mycbr.core.retrieval.Retrieval;
import de.dfki.mycbr.core.similarity.AdvancedDoubleFct;
import de.dfki.mycbr.core.similarity.AdvancedFloatFct;
import de.dfki.mycbr.core.similarity.AdvancedIntegerFct;
import de.dfki.mycbr.core.similarity.AmalgamationFct;
import de.dfki.mycbr.core.similarity.DoubleFct;
import de.dfki.mycbr.core.similarity.FloatFct;
import de.dfki.mycbr.core.similarity.ISimFct;
import de.dfki.mycbr.core.similarity.IntegerFct;
import de.dfki.mycbr.core.similarity.Similarity;
import de.dfki.mycbr.core.similarity.StringFct;
import de.dfki.mycbr.core.similarity.SymbolFct;
import de.dfki.mycbr.util.Pair;
import exceptions.SimilarityMeasureException;
import similarity.computation.Computation;
import similarity.desc.ExtendedDoubleDesc;
import similarity.desc.ExtendedIntegerDesc;
import similarity.fct.AbstractDiscretizedNumberFct;
import similarity.fct.NormalizedNumberFct;
import similarity.fct.RandomNumberFct;


/**
 * Common utilities for handling the myCBR framework.
 * 
 * @author Christian Karmen
 *
 */
public class MyCbrUtils {
	
	/**
	 * Prints retrieval results to console.
	 * 
	 * @param theRetrieval retrieval of interest
	 * @param theThreshold number of max. retrieval results
	 */
	public static void printRetrievalResults(Retrieval theRetrieval, double theThreshold) {
		int aResultCounter = 0;  
		
		Utils.printTitle("QUERY RESULTS");
		
		System.out.println("Number of Total Results: " + theRetrieval.entrySet().size());
		System.out.println();
		
		
//		for (Map.Entry<Instance, Similarity> entry: itsRetrieval.entrySet()) {
////			System.out.println("\nSimilarity: " + entry.getValue().getValue()
////					+ " to case: " + entry.getKey().getValueAsString()); //.getID() gibt es nicht ?mehr?
//			
//			System.out.println(entry.getKey().getValueAsString() + ": " + entry.getValue().getValue());
//		}
		
		for (Pair<Instance, Similarity> anResult: theRetrieval.getResult()) {
			//if (anResult.getSecond().getValue() >= theThreshold){
				System.out.println(anResult.getFirst() + ": " + anResult.getSecond().getValue());
				aResultCounter++;
			//}
		}
		System.out.println("Number of Results: " + aResultCounter);
		

		
		System.out.println();
	}
	


	/**
	 * Prints concept details to console.
	 * 
	 * @param theConcept Concept of interest
	 */
	public static void printConceptDescription(Concept theConcept) {
		// get all attributes of the CBR case model
		HashMap<String, AttributeDesc> aValueMap = theConcept.getAllAttributeDescs();
		
//		Computation aComputation = new Computation(theConcept);
		
		DecimalFormat aDF = new DecimalFormat("###.###");
		aDF.setDecimalSeparatorAlwaysShown(false);
		
		Utils.printTitle("CONCEPT DESCRIPTION DETAILS");
		
		// get the allowed values for each Attribute
		for (Map.Entry<String, AttributeDesc> entry: aValueMap.entrySet()) {
			System.out.print(entry.getValue() + ": ");
			AttributeDesc anAttributeDesc = entry.getValue();
			
			String attClass = anAttributeDesc.getClass().getSimpleName(); //.toString();
			System.out.print("[Class=" + attClass + "]");
			
//			System.out.print(entry.getValue() + ": ");
		
//			if (attClass.compareTo("class de.dfki.mycbr.core.model.SymbolDesc")==0){
			if (anAttributeDesc instanceof SymbolDesc){
				SymbolDesc symbolDesc = (SymbolDesc) entry.getValue();
				Set<String> elements = symbolDesc.getAllowedValues();
				
				System.out.print("[Values=");
				for (String allowedValue : elements) {
					System.out.print("(" + allowedValue + ")");
				}
				System.out.print("]");
			} else if (anAttributeDesc instanceof IntegerDesc || anAttributeDesc instanceof FloatDesc || anAttributeDesc instanceof DoubleDesc){
				if (anAttributeDesc instanceof IntegerDesc){
					System.out.print("[Min=" + ((IntegerDesc)anAttributeDesc).getMin() + "]");
					System.out.print("[Max=" + ((IntegerDesc)anAttributeDesc).getMax() + "]");
				} else if (anAttributeDesc instanceof DoubleDesc){
					System.out.print("[Min=" + aDF.format(((DoubleDesc)anAttributeDesc).getMin()) + "]");
					System.out.print("[Max=" + aDF.format(((DoubleDesc)anAttributeDesc).getMax()) + "]");
				}
				
				if (anAttributeDesc instanceof ExtendedDoubleDesc){
					System.out.print("[Median=" + aDF.format(((ExtendedDoubleDesc)anAttributeDesc).getMedianValue()) + "]");
					System.out.print("[Mean=" + aDF.format(((ExtendedDoubleDesc)anAttributeDesc).getMeanValue()) + "]");
					System.out.print("[StandardDeviation=" + aDF.format(((ExtendedDoubleDesc)anAttributeDesc).getStandardDeviation()) + "]");
				} else if (anAttributeDesc instanceof ExtendedIntegerDesc){
					System.out.print("[Median=" + aDF.format(((ExtendedIntegerDesc)anAttributeDesc).getMedianValue()) + "]");
					System.out.print("[Mean=" + aDF.format(((ExtendedIntegerDesc)anAttributeDesc).getMeanValue()) + "]");
					System.out.print("[StandardDeviation=" + aDF.format(((ExtendedIntegerDesc)anAttributeDesc).getStandardDeviation()) + "]");
				}
					
			} else{
				System.err.println("ERROR: Unhandled AttributeDesc: " + anAttributeDesc.getName());
			}
			System.out.println();
		}
		
		System.out.println();
	}
	
	

	
	

	
	
	
	   
	   
		/**
		 * Generates a Function Name for a given Attribute.
		 * 
		 * @param theAttributeName Name of the Attribute
		 * @return Generated a Function Name
		 */
		public static String generateFctName(String theAttributeName){
			return generateFctName(theAttributeName, "", "Fct");
		}
		
		/**
		 * Generates a Function Name for a given Attribute.
		 * 
		 * @param theAttributeName Name of the Attribute
		 * @param thePrefix Prefix for generated name
		 * @return Generated a Function Name
		 */
		public static String generateFctName(String theAttributeName, String thePrefix){
			return generateFctName(theAttributeName, thePrefix, "Fct");
		}
		
		/**
		 * Generates a Function Name for a given Attribute.
		 * 
		 * @param theAttributeName Name of the Attribute
		 * @param thePrefix Prefix for generated name
		 * @param theSuffix Suffix for generated name
		 * @return Generated a Function Name
		 */
		public static String generateFctName(String theAttributeName, String thePrefix, String theSuffix){
			return thePrefix + theAttributeName.trim().toLowerCase().replaceAll("\\s","") + theSuffix;
		}
		
		
		/**
		 * Generates the short version for the concept name by returning the label after the last delimiter ("\").
		 * If no delimiter is found the input string will be returned.
		 * 
		 * @param theConceptLongName Absolute name of concept
		 * @return Label after the last delimiter ("\").
		 */
		public static String getConceptShortName(String theConceptLongName){
			
			int aIndex = theConceptLongName.lastIndexOf("\\");
			
			// no delimiter available
			if (aIndex == -1){
				return theConceptLongName;
			}
			
			return theConceptLongName.substring(aIndex+1).trim();
		}
		
		

		
		

		/**
		 * Gets the AttributeDesc with the given AttributeName.
		 * 
		 * @param theConcept Concept of interest
		 * @param theAttributeName Name of the attribute
		 * @return AttributeDesc with the given AttributeName
		 */
		public static AttributeDesc getAttributeDescByName(Concept theConcept, String theAttributeName){
			return theConcept.getAllAttributeDescs().get(theAttributeName);
		}
		
		

		/**
		 * Gets the AmalgamFct (global similarity function) of the given concept.
		 * 
		 * @param theConcept Concept of interest
		 * @return AmalgamFct of given concept
		 */
		public static AmalgamationFct getActiveAmalgamFct(Concept theConcept){
			if (theConcept == null){
				return null;
			}
			
			return theConcept.getActiveAmalgamFct();
		}
		

		
		/**
		 * Gets the active used AttributeFct for a given Attribute.
		 * 
		 * @param theConcept Concept of interest
		 * @param theAttributeName Name of the attribute
		 * @return active used AttributeFct
		 */
		public static Object getActiveFct(Concept theConcept, String theAttributeName){
			AmalgamationFct anAmalgamFct = getActiveAmalgamFct(theConcept);
			return anAmalgamFct.getActiveFct(getAttributeDescByName(theConcept, theAttributeName));
		}
		
		
		
		/**
		 * Returns a list of all active AmalgamFcts (global similarity functions)
		 * 
		 * @param theConcept Concept of interest
		 * @param theAttributeDesc  AttributeDesc of interest
		 * @return list of all active AmalgamFcts
		 */
		public static ArrayList<String> getFctListOfActiveAmalgamFct(Concept theConcept, AttributeDesc theAttributeDesc){
			ArrayList<String> anAmalgamList = new ArrayList<>();
			
			Object f = getActiveAmalgamFct(theConcept).getActiveFct(theAttributeDesc);
			anAmalgamList.add(((ISimFct) f).getName());
			
			// TODO check if works properly
			
//			for (AmalgamationFct anAmalgamationFct : itsConcept.getAvailableAmalgamFcts()) {
//				Object f = anAmalgamationFct.getActiveFct(theAttributeDesc);
//				anAmalgamList.add(((ISimFct) f).getName());
//			}
			return anAmalgamList;
		}
		
		
		
		
		
		/**
		 * Checks if given attribute is a symbol type (nominal).
		 * 
		 * @param theConcept Concept of interest
		 * @param theAttributeName  Name of the attribute
		 * @return true=nominal; false=not nominal
		 */
		public static boolean isSymbolDesc(Concept theConcept, String theAttributeName){
			AttributeDesc anAttributeDesc = getAttributeDescByName(theConcept, theAttributeName);
			
			return (anAttributeDesc instanceof SymbolDesc); 
		}
		
		
		
		/**
		 * Returns a list of all attribute names used to describe a case.
		 * 
		 * @param theConcept Concept of interest
		 * @param theActiveFlag true=only active attributes; false=unfiltered
		 * @return list of all attribute names used to describe a case
		 */
		public static ArrayList<String> getCaseDescList(Concept theConcept, boolean theActiveFlag){
			ArrayList<String> aList = new ArrayList<>();
			int fcts = theConcept.getAvailableAmalgamFcts().size();
			boolean isAmalgamFctAvailable = fcts != 0;
			boolean isActive;
			
//			System.out.println("Number of detected Amalgam Fcts: " + fcts);

			for (Map.Entry<String, AttributeDesc> anAttribute : theConcept.getAllAttributeDescs().entrySet()) {
				isActive = false;
				if (isAmalgamFctAvailable){
					Object f = theConcept.getAvailableAmalgamFcts().get(0).getActiveFct(anAttribute.getValue());
					String aFctName = ((ISimFct)f).getName();
					try{
						isActive = theConcept.getFct(aFctName).isActive(anAttribute.getValue());
					} catch(NullPointerException e){
						System.err.println("ERROR: Cannot detect activity of Attribute: " + anAttribute.getKey());
						isActive = false;
					}
					
					if (isActive == theActiveFlag){
						aList.add(anAttribute.getKey());
					}
				} else {
					aList.add(anAttribute.getKey());
				}
				
//				System.out.println("Key/Value: " + anAttribute.getKey() + " / " + anAttribute.getValue());
			}
			
			return aList;
		}
		
		

		
		
		   /**
		    * Calculates the local similarity of the given Attributes.
		    * 
		    * @param theConcept Concept of interest
		    * @param theAttributeName Name of the Attribute
		    * @param theFirstValue 1st Value for similarity calculation
		    * @param theSecondValue 2nd Value for similarity calculation
		    * @return The similarity value in the range between 0 and 1.
		    */
		   public static Similarity getLocalSimilarity(Concept theConcept, String theAttributeName, String theFirstValue, String theSecondValue){
		       if (theFirstValue.equals(theSecondValue)){
		    	   return Similarity.get(1.0);
		       }
		       if (theFirstValue.equals(Project.UNDEFINED_SPECIAL_VALUE) || theFirstValue.equals(Project.UNKNOWN_SPECIAL_VALUE) ||
		    		   theSecondValue.equals(Project.UNDEFINED_SPECIAL_VALUE) || theSecondValue.equals(Project.UNKNOWN_SPECIAL_VALUE)
		    		   ){
		    	   return Similarity.get(0.0);
		       }

		       AmalgamationFct anAmalgamFct = getActiveAmalgamFct(theConcept);
		       AttributeDesc anAttributeDesc = getAttributeDescByName(theConcept, theAttributeName);
		       
		       Object f = anAmalgamFct.getActiveFct(anAttributeDesc);
		       Similarity aSimilarity = Similarity.INVALID_SIM;
		       try {
		    	   if (f instanceof SymbolFct){
					aSimilarity = ((SymbolFct)f).calculateSimilarity(theFirstValue, theSecondValue);
		    	   } else if (f instanceof IntegerFct){
		   			aSimilarity = ((IntegerFct)f).calculateSimilarity(Integer.parseInt(theFirstValue), Integer.parseInt(theSecondValue));   			
		       	   } else if (f instanceof AdvancedIntegerFct){
		      		aSimilarity = ((AdvancedIntegerFct)f).calculateSimilarity(Integer.parseInt(theFirstValue), Integer.parseInt(theSecondValue));   			
		           } else if (f instanceof DoubleFct){
		   			aSimilarity = ((DoubleFct)f).calculateSimilarity(Double.parseDouble(theFirstValue), Double.parseDouble(theSecondValue));   			
		       	   } else if (f instanceof AdvancedDoubleFct){
		   			aSimilarity = ((AdvancedDoubleFct)f).calculateSimilarity(Double.parseDouble(theFirstValue), Double.parseDouble(theSecondValue));   			
		       	   } else if (f instanceof FloatFct){
		   			aSimilarity = ((FloatFct)f).calculateSimilarity(Float.parseFloat(theFirstValue), Float.parseFloat(theSecondValue));   			
		       	   } else if (f instanceof AdvancedFloatFct){
		   			aSimilarity = ((AdvancedFloatFct)f).calculateSimilarity(Float.parseFloat(theFirstValue), Float.parseFloat(theSecondValue));   			
		       	   } else if (f instanceof StringFct){	
//		       		   // simple manual similarity check
//		       		   if (theFirstValue.equalsIgnoreCase(theSecondValue)){
//		       			   aSimilarity = Similarity.get(1.0);
//		       		   } else {
//		       			   aSimilarity = Similarity.get(0.0);
//		       		   }
		       		// FIXME Would be nicer to use the calculateSimilarity() function (e.g. with LEVENSTEIN algorithm)	
		       		   StringFct aStringFct = (StringFct)f;
		       		   aStringFct.setCaseSensitive(false);
		       		   StringAttribute aFirstValue = (StringAttribute)((StringFct) f).getDesc().getAttribute(theFirstValue);
		       		   StringAttribute aSecondValue = (StringAttribute)((StringFct) f).getDesc().getAttribute(theSecondValue);
		       		   aSimilarity = aStringFct.calculateSimilarity(aFirstValue , aSecondValue);
		       	   } 
		    	   
			       	else if (f instanceof NormalizedNumberFct){
			       		if (((NormalizedNumberFct) f).getDesc() instanceof ExtendedDoubleDesc){
			       			aSimilarity = ((NormalizedNumberFct)f).calculateSimilarity(Float.parseFloat(theFirstValue), Float.parseFloat(theSecondValue));
			       		}
			       		
			       		else if (((NormalizedNumberFct) f).getDesc() instanceof ExtendedIntegerDesc){
			       			aSimilarity = ((NormalizedNumberFct)f).calculateSimilarity(Integer.parseInt(theFirstValue), Integer.parseInt(theSecondValue));
			       		}
			       		
			       		else {
			       			throw new SimilarityMeasureException("Unknown Linear-Desc class: " + ((NormalizedNumberFct) f).getDesc());
			       		}
			       	}
		    	   
			       	else if (f instanceof AbstractDiscretizedNumberFct){
			       		if (((AbstractDiscretizedNumberFct) f).getDesc() instanceof ExtendedDoubleDesc){
			       			aSimilarity = ((AbstractDiscretizedNumberFct)f).calculateSimilarity(Double.parseDouble(theFirstValue), Double.parseDouble(theSecondValue));
			       		}
			       		
			       		else if (((AbstractDiscretizedNumberFct) f).getDesc() instanceof ExtendedIntegerDesc){
			       			aSimilarity = ((AbstractDiscretizedNumberFct)f).calculateSimilarity(Integer.parseInt(theFirstValue), Integer.parseInt(theSecondValue));
			       		}
			       		
			       		else {
			       			throw new SimilarityMeasureException("Unknown Linear-Desc class: " + ((NormalizedNumberFct) f).getDesc());
			       		}
			       	}
			    	   
//			       	else if (f instanceof KaplanMeierFct){
//			       		aSim = ((KaplanMeierFct)f).calculateSimilarity(theFirstValue, theSecondValue); 			
//			       	}
		    	   
			       	else if (f instanceof RandomNumberFct){
			       		double aRandomSimilarity = Computation.getRandomNumber(1);
			       		aSimilarity = Similarity.get(aRandomSimilarity);
			       	}
		       	   
		       	   else {
		       		   // maybe more types are needed
		       		   throw new SimilarityMeasureException("Unknown Fct-Type: " + f.toString());
		       	   }
		    	   //return aSim.getRoundedValue();
		    	   return aSimilarity;
			} catch (Exception e) {
				e.printStackTrace();
			}
		       
			   return aSimilarity;
		   }

		   

			
			

			
			/**
			 * Executes a retrieval of similar cases with the given case description. 
			 * 
			 * @param theConcept Concept of interest
			 * @param theRetrieval Retrieval configuration
			 * @param theQueryFilter List of Case Description attribute values 
			 * @throws ParseException
			 */
			public static void performRetrieval(Concept theConcept, Retrieval theRetrieval, HashMap<String, String> theQueryFilter) throws ParseException{
				AttributeDesc anAttributeDesc;
				
				Instance aQueryInstance = theRetrieval.getQueryInstance();
				
//				MyCbrUtils.printTitle("QUERY DETAILS");
//				
//				System.out.println("Current AmalgamFct Type: " + theConcept.getActiveAmalgamFct().getType());
//				System.out.println("Current AmalgamFct Name: " + theConcept.getActiveAmalgamFct().getName());
//				System.out.println();
				
				for (Map.Entry<String, String> aEntry : theQueryFilter.entrySet()) {
					anAttributeDesc = theConcept.getAttributeDesc(aEntry.getKey());
					aQueryInstance.addAttribute(anAttributeDesc, anAttributeDesc.getAttribute(aEntry.getValue()));
					
//					System.out.println(aEntry.getKey() + ": " + aEntry.getValue());
				}				
				theRetrieval.start();
//				System.out.println();
			}
			
			
			
			/**
			 * Adds a new SymbolDesc within the given concept with the given attribute values.
			 * 
			 * @param theConcept Concept of interest
			 * @param theAttributeLabel Name of the Attribute
			 * @param theAttributeValueList List of all possible attribute values
			 * @return SymbolFct within newly created SymbolDesc
			 * @throws Exception Error while adding Attribute to Concept
			 */
			public static SymbolFct addSymbolDescription(Concept theConcept, String theAttributeLabel, ArrayList<String> theAttributeValueList) throws Exception{
				// add symbol attribute
				HashSet<String> aHashSet = new HashSet<String>();
				aHashSet.addAll(theAttributeValueList);
				SymbolDesc aSymbolDesc = new SymbolDesc(theConcept, theAttributeLabel, aHashSet);
				
				// add table function
				SymbolFct aSymbolFct = aSymbolDesc.addSymbolFct(theAttributeLabel.toLowerCase() + "Fct", true);
				
				return aSymbolFct;
			}			
			
			
			/**
			 * Adds a new DoubleDesc within the given concept with the given attribute value range.
			 * 
			 * @param theConcept Concept of interest
			 * @param theAttributeLabel Name of the Attribute
			 * @param theMinValue Smallest allowed value 
			 * @param theMaxValue Highest allowed value
			 * @return DoubleFct within newly created DoubleDesc
			 * @throws Exception Error while adding Attribute to Concept
			 */
			public static DoubleFct addDoubleDescription(Concept theConcept, String theAttributeLabel, double theMinValue, double theMaxValue) throws Exception{
				// add symbol attribute
				DoubleDesc aDoubleDesc = new DoubleDesc(theConcept, theAttributeLabel, theMinValue, theMaxValue);
				
				// add table function
				DoubleFct aDoubleFct = aDoubleDesc.addDoubleFct(theAttributeLabel.toLowerCase() + "Fct", true);
				
				return aDoubleFct;
			}
			
			
			/**
			 * Adds a new DoubleDesc within the given concept with the given attribute value range.
			 * 
			 * @param theConcept Concept of interest
			 * @param theAttributeLabel Name of the Attribute
			 * @param theMinValue Smallest allowed value 
			 * @param theMaxValue Highest allowed value
			 * @return AdvancedDoubleFct within newly created DoubleDesc
			 * @throws Exception Error while adding Attribute to Concept
			 */
			public static AdvancedDoubleFct addAdvancedDoubleDescription(Concept theConcept, String theAttributeLabel, double theMinValue, double theMaxValue) throws Exception{
				// add symbol attribute
				DoubleDesc aDoubleDesc = new DoubleDesc(theConcept, theAttributeLabel, theMinValue, theMaxValue);
				
				// add table function
				AdvancedDoubleFct aDoubleFct = aDoubleDesc.addAdvancedDoubleFct(theAttributeLabel.toLowerCase() + "Fct", true);
				
				return aDoubleFct;
			}
			
			
			/**
			 * Adds a new IntegerDesc within the given concept with the given attribute value range.
			 * 
			 * @param theConcept Concept of interest
			 * @param theAttributeLabel Name of the Attribute
			 * @param theMinValue Smallest allowed value 
			 * @param theMaxValue Highest allowed value
			 * @return IntegerFct within newly created IntegerDesc
			 * @throws Exception Error while adding Attribute to Concept
			 */
			public static IntegerFct addIntegerDescription(Concept theConcept, String theAttributeLabel, int theMinValue, int theMaxValue) throws Exception{
				// add symbol attribute
				IntegerDesc anIntegerDesc = new IntegerDesc(theConcept, theAttributeLabel, theMinValue, theMaxValue);
				
				// add table function
				IntegerFct anIntegerFct = anIntegerDesc.addIntegerFct(theAttributeLabel.toLowerCase() + "Fct", true);
				
				return anIntegerFct;
			}
			
			
			/**
			 * Adds a new IntegerDesc within the given concept with the given attribute value range.
			 * 
			 * @param theConcept Concept of interest
			 * @param theAttributeLabel Name of the Attribute
			 * @param theMinValue Smallest allowed value 
			 * @param theMaxValue Highest allowed value
			 * @return AdvancedIntegerFct within newly created IntegerDesc
			 * @throws Exception Error while adding Attribute to Concept
			 */
			public static AdvancedIntegerFct addAdvancedIntegerDescription(Concept theConcept, String theAttributeLabel, int theMinValue, int theMaxValue) throws Exception{
				// add symbol attribute
				IntegerDesc anIntegerDesc = new IntegerDesc(theConcept, theAttributeLabel, theMinValue, theMaxValue);
				
				// add table function
				AdvancedIntegerFct anAdvancedIntegerFct = anIntegerDesc.addAdvancedIntegerFct(theAttributeLabel.toLowerCase() + "Fct", true);
				
				return anAdvancedIntegerFct;
			}
			
			
			

			
			
			
			/**
			 * Enables or disables the activity flag for a given attribute list  in a given Amalgam function.
			 * 
			 * @param theConcept concept of interest
			 * @param theAmalgamFct	amalgam function
			 * @param theAttributeLabelList attribute list
			 * @param theActivityFlag true for enabling all given attributes; false for disabling all given attributes
			 */
			public static void setAttributeActivity(Concept theConcept, AmalgamationFct theAmalgamFct, ArrayList<String> theAttributeLabelList, boolean theActivityFlag){
				AttributeDesc anAttributeDesc;
				
				for (String anAttributeLabel : theAttributeLabelList) {
					anAttributeDesc = MyCbrUtils.getAttributeDescByName(theConcept, anAttributeLabel);
					theAmalgamFct.setActive(anAttributeDesc, theActivityFlag);
					
					// non-active attributes should have no weights (otherwise similarity calculation will be wrong)
					theAmalgamFct.setWeight(anAttributeDesc, 0);
				}
			}
			
			
			/**
			 * Checks for upper/lower attribute boundaries (as defined in concept), and if needed sets all attribute values to boundary limit.
			 * 
			 * @param theConcept concepte of interest
			 * @param theUnrestrictedValueList attribute list with unrestricted values
			 * 
			 * @return restricted values list
			 */
			public static HashMap<String, String> getRestrictedValuesList(Concept theConcept, HashMap<String, String> theUnrestrictedValueList){
				HashMap<String, String> aRestrictedValueList = new HashMap<>();
				AttributeDesc anAttributeDesc;
				ExtendedDoubleDesc aDoubleDesc;
				String aValue;
				String aRestrictedValue;
				double aDouble;
				  
				for (String aCurrentAttributeLabel : theUnrestrictedValueList.keySet()) {
					// check if attribute is available in given concept 
					anAttributeDesc = theConcept.getAttributeDesc(aCurrentAttributeLabel);
					if (anAttributeDesc == null){
						System.err.println("ERROR: Cannot get attribute desc for: " + aCurrentAttributeLabel);
					}
					
					aValue = theUnrestrictedValueList.get(aCurrentAttributeLabel);
					
//					System.out.println("DEBUG: Processing:  " + aCurrentAttributeLabel + " = " + aValue);
					
					// check if attribute is numeric, and if so: parse to double
					if (anAttributeDesc instanceof ExtendedDoubleDesc){
						aDoubleDesc = (ExtendedDoubleDesc)anAttributeDesc;
						try{
							  aDouble = Double.parseDouble(aValue);
							  // check for allowed upper and lower boundaries
							  if (aDouble > aDoubleDesc.getMax()){
								  aRestrictedValue = Double.toString(aDoubleDesc.getMax());
								  System.out.println("DEBUG: Restricting attribute value '" + aValue + "' to '" + aRestrictedValue + "' (" + aCurrentAttributeLabel + ")");
							  } else if (aDouble < aDoubleDesc.getMin()){
								  aRestrictedValue = Double.toString(aDoubleDesc.getMin());
								  System.out.println("DEBUG: Restricting attribute value '" + aValue + "' to '" + aRestrictedValue + "' (" + aCurrentAttributeLabel + ")");
							  } else {
								  aRestrictedValue = aValue;
							  }
						  }catch (Exception e) {
							  aRestrictedValue = Project.UNDEFINED_SPECIAL_VALUE;
						  }
					// check if attribute is nominal, and if so: check if value is allowed
					} else if (anAttributeDesc instanceof SymbolDesc){
						if (!((SymbolDesc)anAttributeDesc).getAllowedValues().contains(aValue)){
							aRestrictedValue = Project.UNDEFINED_SPECIAL_VALUE;
							System.out.println("DEBUG: Restricting attribute value '" + aValue + "' to '" + aRestrictedValue + "' (" + aCurrentAttributeLabel + ")");
						} else {
							aRestrictedValue = aValue;
						}
					} else {
						System.err.println("ERROR: Unhandled attribute desc: " + anAttributeDesc.getClass());
						aRestrictedValue = Project.UNDEFINED_SPECIAL_VALUE;
					}
					aRestrictedValueList.put(aCurrentAttributeLabel, aRestrictedValue);
				}
  
				return aRestrictedValueList;
			}
			
			


}
