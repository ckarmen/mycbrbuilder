/**
 * (2019) Heidelberg University, Institute of Medical Biometry and Informatics (IMBI), Germany
 * 
 * This file is part of myCbrBuilder.
 *  
 * myCbrBuilder is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * myCbrBuilder is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with vivaGen. If not, see <http://www.gnu.org/licenses/>.
 * 
 **/
package core;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;

import core.connectors.AbstractConnector;
import de.dfki.mycbr.core.DefaultCaseBase;
import de.dfki.mycbr.core.ICaseBase;
import de.dfki.mycbr.core.Project;
import de.dfki.mycbr.core.casebase.Instance;
import de.dfki.mycbr.core.model.Concept;
import de.dfki.mycbr.core.model.SymbolDesc;
import de.dfki.mycbr.core.retrieval.Retrieval;
import similarity.computation.config.AbstractSimilarityMeasureConfiguration;
import similarity.config.SimilarityMeasure;



/**
 * Class to handle a myCBRBuilder client session.
 * 
 * @author Christian Karmen
 *
 */
public class MyCbrBuilderSession {

	private Project itsProject;
	private Concept itsConcept = null;
//	private Concept itsCsvConcept = null;
	private DefaultCaseBase itsCaseBase = null;

	private AbstractConnector itsConnector;
	
	ArrayList<String> itsCaseDescriptionList;
	ArrayList<String> itsSolutionDescriptionList;
	String itsOutputClassLabel;
	ArrayList<String> itsAllowedOutputClassLabels;
	
	SimilarityMeasure itsSimilarityMeasure;
	AbstractSimilarityMeasureConfiguration itsSimilarityMeasureConfig;
	
	
	/**
	 * Creates a new myCBRBuilder client session.
	 * 
	 * @param theTopConceptLabel Name of the Concept
	 */
	public MyCbrBuilderSession(String theTopConceptLabel) {
		try{
			itsProject = new Project();
//			itsProject = theProject;
			
			// create concept 
			itsConcept = itsProject.createTopConcept(theTopConceptLabel);
//			itsCsvConcept = itsConcept;
			
			itsCaseDescriptionList = new ArrayList<>();
			itsSolutionDescriptionList = new ArrayList<>();
			itsAllowedOutputClassLabels = new ArrayList<>();
			itsOutputClassLabel = "";
			
			itsSimilarityMeasure = SimilarityMeasure.NONE;
			itsSimilarityMeasureConfig = null;
			
			// add case base
//			itsCaseBase = itsProject.createDefaultCB(theCaseBaseLabel);
//			itsCaseBase = itsConnector.getCaseBase();
			
//			itsConnector = theConnector;
		} catch(Exception e){
			e.printStackTrace();
			System.exit(-1);
		}
		
	}
	
	
	/**
	 * Creates a new myCBRBuilder client session.
	 * 
	 * @param theTopConceptLabel Name of the Concept
	 */
	public MyCbrBuilderSession() {
		this("empty session");
	}
	
	
	/**
	 * Creates a new myCBRBuilder client session.
	 * 
	 * @param thePrjFile myCBR specific project file
	 */
	public MyCbrBuilderSession(File thePrjFile){
		// FIXME testen, geht wohl nicht mehr so
		try {
			//itsProject.delete();
			itsProject = loadProject(thePrjFile.getAbsolutePath());
			
			//itsConcept = itsProject.getConceptByID("");
			itsConcept = ((Instance)(itsProject.getAllInstances().toArray())[0]).getConcept();
//			while (itsConcept.getSuperConcept() != null){
//				itsConcept = itsConcept.getSuperConcept();
//			}
			
			Collection<ICaseBase> aCB = itsProject.getCaseBases().values();
			itsCaseBase = (DefaultCaseBase)(aCB.toArray())[0];
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	

	
	
	/**
	 * Creates a new Retrieval.
	 * 
	 * @return new Retrieval object
	 */
	public Retrieval generateRetrieval(){
		return new Retrieval(itsConcept, itsCaseBase);
	}
	

	/**
	 * Gets access to the Project.
	 * 
	 * @return the Project
	 */
	public Project getProject() {
		return itsProject;
	}

	/**
	 * Gets access to the Concept.
	 * 
	 * @return the itsConcept
	 */
	public Concept getConcept() {
		return itsConcept;
	}


	/**
	 * Gets access to the Case Base.
	 * 
	 * @return the Case Base
	 */
	public DefaultCaseBase getCaseBase() {
		return itsCaseBase;
	}
	
	/**
	 * Gets the Connector for Case Base.
	 * 
	 * @return Connector for Case Base
	 */
	public AbstractConnector getConnector(){
		return itsConnector;
	}
	
	/**
	 * Sets the Connector for Case Base.
	 * 
	 * @param theConnector Connector for Case Base
	 */
	public void setConnector(AbstractConnector theConnector){
		itsConnector = theConnector;
		itsCaseBase = theConnector.getCaseBase();
	}
	
	
	
	public ArrayList<String> getCaseDescriptionList() {
		return itsCaseDescriptionList;
	}


	public void setCaseDescriptionList(ArrayList<String> theCaseDescriptionList) {
		this.itsCaseDescriptionList = theCaseDescriptionList;
	}


	public ArrayList<String> getSolutionDescriptionList() {
		return itsSolutionDescriptionList;
	}


	public void setSolutionDescriptionList(ArrayList<String> theSolutionDescriptionList) {
		this.itsSolutionDescriptionList = theSolutionDescriptionList;
	}
	
	public String getOutputClassLabel(){
		return itsOutputClassLabel;
	}
	
	public void setOutputClassLabel(String theOutputClassLabel){
		itsOutputClassLabel = theOutputClassLabel;
		itsAllowedOutputClassLabels.clear();
		for (String anAllowedValue : ((SymbolDesc)itsConcept.getAttributeDesc(theOutputClassLabel)).getAllowedValues()) {
			itsAllowedOutputClassLabels.add(anAllowedValue);
		}
	}
	
	public ArrayList<String> getAllowedOutputClassLabels(){
		return itsAllowedOutputClassLabels;
	}


	public SimilarityMeasure getSimilarityMeasure() {
		return itsSimilarityMeasure;
	}


	public void setSimilarityMeasure(SimilarityMeasure theSimilarityMeasure, AbstractSimilarityMeasureConfiguration theSimilarityMeasureConfig) {
		this.itsSimilarityMeasure = theSimilarityMeasure;
		this.itsSimilarityMeasureConfig = theSimilarityMeasureConfig;
	}


	public AbstractSimilarityMeasureConfiguration getSimilarityMeasureConfig() {
		return itsSimilarityMeasureConfig;
	}


	public void setSimilarityMeasureConfig(AbstractSimilarityMeasureConfiguration theSimilarityMeasureConfig) {
		this.itsSimilarityMeasureConfig = theSimilarityMeasureConfig;
	}



	/**
	 * Loades a myCBR project from given file. 
	 * 
	 * @param thePrjFile myCBR project file
	 * @return myCBR Project
	 * @throws Exception Error occurred while loading project file
	 */
	public synchronized Project loadProject(String thePrjFile) throws Exception{
		Project p = new Project(thePrjFile);
		
//		System.out.print("Importing Project.");
		while (p.isImporting()){
			Thread.sleep(100); 
			System.out.print(".");
		}
		return p;
	}
	
	
	
	/**
	 * Saves the current project to given path. File name is the given ID with automatically added .prj extension.
	 * 
	 * @param thePath The Path to save into.
	 * @param theID The ID for the file. The file extension .prj will be added automatically.  
	 * 
	 * @throws Exception General Error while trying to save file to given path.
	 */
	public void saveProject(String thePath, String theID) throws Exception{
		itsProject.setPath(thePath);
		itsProject.setName(theID);
		itsProject.save();
	}
	

	
}
