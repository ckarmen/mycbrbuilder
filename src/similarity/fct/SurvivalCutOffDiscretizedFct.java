/**
 * (2019) Heidelberg University, Institute of Medical Biometry and Informatics (IMBI), Germany
 * 
 * This file is part of myCbrBuilder.
 *  
 * myCbrBuilder is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * myCbrBuilder is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with vivaGen. If not, see <http://www.gnu.org/licenses/>.
 * 
 **/
package similarity.fct;

import java.io.File;
import java.util.ArrayList;

import org.cliommics.vivagen.Instances;
import org.cliommics.vivagen.configuration.Configuration;
import org.cliommics.vivagen.survivalanalysis.DiscretizeSurvival;

import application.javafx.MyCBRBuilderFxApplication;
import de.dfki.mycbr.core.DefaultCaseBase;
import de.dfki.mycbr.core.Project;
import de.dfki.mycbr.core.model.AttributeDesc;
import de.dfki.mycbr.core.model.DoubleDesc;
import de.dfki.mycbr.core.similarity.Similarity;
import similarity.computation.ComputationKM;
import similarity.computation.config.KaplanMeierConfig;
import similarity.computation.config.KaplanMeierTargetConfig;
import similarity.desc.ExtendedDoubleDesc;
import similarity.desc.ExtendedIntegerDesc;
import weka.core.converters.CSVSaver;

/**
 * Class to implement the value discretization algorithm "Survival Cut-off". 
 * 
 * @author Christian Karmen
 *
 */
public class SurvivalCutOffDiscretizedFct extends AbstractLookUpDiscretizedNumberFct  {
	
	static {
		itsMinIntervals=2;
	}

	protected KaplanMeierConfig itsKMConfig;	
	protected double itsCutOffValue;
	
	protected double itsWeight;
	
	protected DefaultCaseBase itsMiniCaseBase;
	protected Instances itsWekaTrainingCaseBase;
	protected Instances itsWekaDiscretizedCaseBase;
	
	DiscretizeSurvival itsDiscretizeSurvival;
	
	private final static String itsNominalDelimiter = ";";

	/**
	 * Implementation of the value discretization algorithm "Survival Cut-off". 
	 * 
	 * @param theProject Project of interest
	 * @param theDesc AttributeDesc of interest
	 * @param theName Label for this Fct
	 * @param theTrainingCaseBase CaseBase to learn from
	 * @param itsKMConfig Similarity measure specific parameters
	 */
	public SurvivalCutOffDiscretizedFct(
			Project theProject, 
			ExtendedDoubleDesc theDesc, 
			String theName,
			DefaultCaseBase theTrainingCaseBase, 
			KaplanMeierConfig itsKMConfig
			) {
		super(theProject, theDesc, theName, theTrainingCaseBase, itsKMConfig, 2);
		
		init(itsKMConfig);
	}
	
	/**
	 * Implementation of the value discretization algorithm "Survival Cut-off". 
	 * 
	 * @param theProject Project of interest
	 * @param theDesc AttributeDesc of interest
	 * @param theName Label for this Fct
	 * @param theTrainingCaseBase CaseBase to learn from
	 * @param itsKMConfig Similarity measure specific parameters
	 */
	public SurvivalCutOffDiscretizedFct(
			Project theProject, 
			ExtendedIntegerDesc theDesc, 
			String theName,
			DefaultCaseBase theTrainingCaseBase, 
			KaplanMeierConfig itsKMConfig
			) {
		super(theProject, theDesc, theName, theTrainingCaseBase, itsKMConfig, 2);
		
		init(itsKMConfig);
	}
	
	
	private void init(KaplanMeierConfig theKMConfig){
//		MyCbrUtils.printTitle("Processing Survival Cut-off of attribute '" + desc.getName() + "'");	
		
		itsKMConfig = theKMConfig;
		setNumberOfDiscreteIntervals(2);
		
		ArrayList<String> aMiniCBAttributeList = new ArrayList<>();
		aMiniCBAttributeList.add(desc.getName());
		aMiniCBAttributeList.add(itsKMConfig.KMTargetConfig.TargetStatusColumn);
		aMiniCBAttributeList.add(itsKMConfig.KMTargetConfig.TargetSurvivalColumn);
		
		//String aMiniCBName = desc.getName() + "[MINI][CASEBASE]";
		try {
			itsDiscretizeSurvival = new DiscretizeSurvival(
	        		itsKMConfig.KMTargetConfig.TargetSurvivalColumn, 
	        		itsKMConfig.KMTargetConfig.TargetStatusColumn, 
	        		itsKMConfig.KMTargetConfig.TargetStatusIndicator, 
	        		true);
			
			//itsMiniCaseBase = buildMiniCaseBase(getDiscretizedConcept(), aMiniCBName, aMiniCBAttributeList);
			itsWekaTrainingCaseBase = ComputationKM.getWekaInstancesFromCaseBase(getTrainingCaseBase(), aMiniCBAttributeList, theKMConfig.KMTargetConfig);
//			itsWekaTrainingCaseBase = ComputationKM.getInstancesFromCaseBase(itsMiniCaseBase);
			itsWekaDiscretizedCaseBase = makeNominal(
					itsWekaTrainingCaseBase, 
					itsKMConfig.KMTargetConfig,
					true,
//					false, 
					desc.getName()	
//					I2b2Connector.getI2b2FactsMetaDataList()
					);
		} catch (Exception e) {
			e.printStackTrace();
			return;
		}
		
        
		itsCutOffValue = getCutOffValue();
		
//		itsCutOffValue = parseCutOffValue();  
//		System.out.println("DEBUG: parseCutOffValue() = " + parseCutOffValue());
		
		System.out.println("DEBUG: calulating weight...");
		itsWeight = ComputationKM.calculateImpactKM(
//				 getWekaTrainingCaseBase(),
				getWekaDiscretizedCaseBase(),
				 getKMConfig().StepShapeFlag, 
				 getKMConfig().KMTargetConfig, 
				 desc.getName());;
		
		System.out.println("DEBUG: cut-off = " + getCutOffValue());
		System.out.println("DEBUG: max = " + max);
		System.out.println("DEBUG: min = " + min);
		System.out.println("DEBUG: weight = " + itsWeight);
	}
	
	
    /**
     * Discretize all numeric attributes in the given case base.
     * 
     * @param theInstances	the case base instances
     * @param theKMTargetConfig Kaplan-Meier configuration
     * @param theFileLoaderFlag true=load pre-calculated nominalized case base; false=calculates nominalized case base and stores its result
     * @param theID ID for the nominal list (used for file cache)
     * @param theNominalIgnoreList list of attribute labels to ignore
     * 
     * @return nominalized weka instances
     *
     * @throws Exception whenever an error occurs
     */
    private Instances makeNominal(
    		Instances theInstances, 
    		KaplanMeierTargetConfig theKMTargetConfig, 
    		boolean theFileLoaderFlag, 
    		String theID
    		//ArrayList<String> theNominalIgnoreList
    		) throws Exception{
    	
    	// FIXME: Ggf. eine Filterung von Merkmalen (zur Optimierung) => theNominalIgnoreList
    	
    	File aNominalFile = ComputationKM.getNominalFile(theID);
    	Instances instancesNominal = theInstances; //FIXME:	 besser: theInstances.clone(); ???
    	
    	Configuration.getConfiguration(MyCBRBuilderFxApplication.CONFIG_FILENAME);
    	if (theFileLoaderFlag && aNominalFile.exists()){
    		System.out.print("Loading Discretized Attributes from \"" + aNominalFile.getName() + "\" ...");
    		instancesNominal = Instances.readCSV(aNominalFile.getAbsolutePath(), itsNominalDelimiter);
    	} else {
	    	System.out.print("Discretizing attributes...");

	        instancesNominal = itsDiscretizeSurvival.makeNominal(instancesNominal);
	        itsCutOffValue = itsDiscretizeSurvival.getCutPoints()[1][0];
//	        System.out.println("DEBUG: itsDiscretizeSurvival.getCutPoints()[1][0] = " + itsDiscretizeSurvival.getCutPoints()[1][0]);
//	        System.out.println("DEBUG: desc.getName() = " + desc.getName());
//	        System.out.println("DEBUG: calculateCutOff() = " + calculateCutOff(theInstances, theKMTargetConfig, desc.getName()));
	        
	        if (aNominalFile.exists()){
	        	if (!aNominalFile.delete()){
	        		System.err.print("ERROR: Cannot delete file...");
	        	}
	        }
	        
	        System.out.print("Storing Results to \"" + aNominalFile + "\" ...");
	        CSVSaver saver = new CSVSaver();
	        saver.setInstances(instancesNominal);
	        saver.setFile(aNominalFile);
	        saver.setFieldSeparator(itsNominalDelimiter);
	        saver.writeBatch();
    	}
        
    	System.out.println("done.");
    	
        return instancesNominal;
    }
    
	  


	  
	@Override
	public double getIntervalWidth(int theIntervalIndex) {
		if (theIntervalIndex == 0){
			return getCutOffValue() - min;
		} else if (theIntervalIndex == 1){
			return max - getCutOffValue();
		} else{
			System.err.println("ERROR: Unvalid interval index: " + theIntervalIndex);
			return Double.NaN;
		}
	}




	@Override
	public Similarity calculateDiscretizedSimilarity(double theValue1, double theValue2) {
		String aDiscretizedValue1 = getDiscretizedIndexAsString(theValue1);
		String aDiscretizedValue2 = getDiscretizedIndexAsString(theValue2);
		
		Similarity aSimilarity;
		try {
			aSimilarity = getDiscretizedSymbolFct().calculateSimilarity(aDiscretizedValue1, aDiscretizedValue2);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		
		return aSimilarity;
	}

	@Override
	void setSimilarityMatrix() {
		ComputationKM.setSimilarityMatrix_KM(itsWekaDiscretizedCaseBase, itsKMConfig, getDiscretizedSymbolFct());
	}


	@Override
	public double getMinIntervalValue(int theIntervalIndex) {
		if (theIntervalIndex == 0){
			return min;
		} else if (theIntervalIndex == 1){
			// FIXME Cut-off enthalten??
			return getCutOffValue();
		} else{
			System.err.println("ERROR: Unvalid interval index: " + theIntervalIndex);
			return Double.NaN;
		}
	}

	@Override
	public double getMaxIntervalValue(int theIntervalIndex) {
		if (theIntervalIndex == 0){
			// FIXME Cut-off enthalten??
			return getCutOffValue();
		} else if (theIntervalIndex == 1){
			return max;
		} else{
			System.err.println("ERROR: Unvalid interval index: " + theIntervalIndex);
			return Double.NaN;
		}
	}
	
	
	/**
	 * Gets the Survival Cut-off value.
	 * 
	 * @return Survival Cut-off value
	 */
	public double getCutOffValue(){
		return itsCutOffValue;
	}
	
	
	/**
	 * Gets the training CaseBase as weka instances.
	 * 
	 * @return training CaseBase as weka instances
	 */
	public Instances getWekaTrainingCaseBase(){
		return itsWekaTrainingCaseBase;
	}
	
	
	/**
	 * Gets the discretized CaseBase as weka instances.
	 * 
	 * @return discretized CaseBase as weka instances
	 */
	public Instances getWekaDiscretizedCaseBase(){
		return itsWekaDiscretizedCaseBase;
	}	
	
	/**
	 * Gets the Kaplan-Meier configuration.
	 * 
	 * @return Kaplan-Meier configuration.
	 */
	public KaplanMeierConfig getKMConfig(){
		return itsKMConfig;
	}
	
	
	/**
	 * Gets the calculated weight of the discretized attribute.
	 * 
	 * @return weight of the discretized attribute
	 */
	public double getWeight(){
		return itsWeight;
	}
	
	
	@Override
	public void clone(AttributeDesc descNEW, boolean active) {
		if (descNEW instanceof DoubleDesc && !name.equals(Project.DEFAULT_FCT_NAME)) {
//			LinearDiscretizedFct f = ((LinearContinuousDesc)descNEW).addDVDMFct(name, active, itsTrainingCaseBase, itsOutputClass);
			
			// FIXME Unterscheidung zwischen ExtendedDouble + ExtendedInteger
			SurvivalCutOffDiscretizedFct f = ((ExtendedDoubleDesc)descNEW).addSurvivalCutOffDiscretizedFct(
					name, 
					active, 
					getTrainingCaseBase(), 
					(KaplanMeierConfig)itsSimilarityConfig);
			
			f.isSymmetric = this.isSymmetric;
			f.mc = this.mc;
			f.distanceFunction = this.distanceFunction;
			f.setDiscretizationSymbolConfig(itsDiscretizationSymbolConfig);

			f.itsCutOffValue = this.itsCutOffValue;
			f.itsKMConfig = this.itsKMConfig;				
			f.itsMiniCaseBase = this.itsMiniCaseBase;
			f.itsWekaTrainingCaseBase = this.itsWekaTrainingCaseBase;;
			f.itsWekaDiscretizedCaseBase = this.itsWekaDiscretizedCaseBase;
		}
	}
}
