/**
 * (2019) Heidelberg University, Institute of Medical Biometry and Informatics (IMBI), Germany
 * 
 * This file is part of myCbrBuilder.
 *  
 * myCbrBuilder is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * myCbrBuilder is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with vivaGen. If not, see <http://www.gnu.org/licenses/>.
 * 
 **/
package similarity.fct;

import java.util.Observable;

import de.dfki.mycbr.core.Project;
import de.dfki.mycbr.core.casebase.Attribute;
import de.dfki.mycbr.core.casebase.DoubleAttribute;
import de.dfki.mycbr.core.casebase.FloatRange;
import de.dfki.mycbr.core.casebase.IntegerAttribute;
import de.dfki.mycbr.core.casebase.MultipleAttribute;
import de.dfki.mycbr.core.casebase.SimpleAttribute;
import de.dfki.mycbr.core.casebase.SpecialAttribute;
import de.dfki.mycbr.core.model.AttributeDesc;
import de.dfki.mycbr.core.model.IntegerDesc;
import de.dfki.mycbr.core.model.SimpleAttDesc;
import de.dfki.mycbr.core.similarity.NumberFct;
import de.dfki.mycbr.core.similarity.Similarity;
import similarity.config.NormalizationConfig;
import similarity.desc.ExtendedDoubleDesc;
import similarity.desc.ExtendedIntegerDesc;

/**
 * Class to handle similarity functions for numeric attributes by using a normalization.
 * 
 * @author Christian Karmen
 *
 */
public class NormalizedNumberFct extends NumberFct {
	
	NormalizationConfig itsNormalizationConfig = NormalizationConfig.NONE;
	
	protected FloatRange range;
	
	double itsMeanValue = Double.NaN;
	double itsStandardDeviation = Double.NaN;
		
	
	/**
	 * Class to handle similarity functions for numeric attributes by using a normalization.
	 * 
	 * @param theProject Project of interest
	 * @param theDesc Numeric AttributeDesc of interest
	 * @param theName Name of this Fct
	 * @param theTrainingCaseBase Training Case Base to learn normalization from 
	 * @param theNorminalizationConfig Norminalization Type
	 */
	public NormalizedNumberFct(
			Project theProject, 
			ExtendedDoubleDesc theDesc, 
			String theName, 
//			DefaultCaseBase theTrainingCaseBase,
			NormalizationConfig theNorminalizationConfig) {
		
		super(theProject, theDesc, theName);

		init(theProject, theDesc, theName, theNorminalizationConfig);
		
		max = theDesc.getMax();
		min = theDesc.getMin();
		diff = Math.abs(max-min);
		itsMeanValue = theDesc.getMeanValue();
		itsStandardDeviation = theDesc.getStandardDeviation();
	}
	
	
	/**
	 * Class to handle similarity functions for numeric attributes by using a normalization.
	 * 
	 * @param theProject Project of interest
	 * @param theDesc Numeric AttributeDesc of interest
	 * @param theName Name of this Fct
	 * @param theTrainingCaseBase Training Case Base to learn normalization from 
	 * @param theNorminalizationConfig Norminalization Type
	 */
	public NormalizedNumberFct(
			Project theProject, 
			ExtendedIntegerDesc theDesc, 
			String theName,
//			DefaultCaseBase theTrainingCaseBase,
			NormalizationConfig theNorminalizationConfig) {
		super(theProject, theDesc, theName);
		
		init(theProject, theDesc, theName, theNorminalizationConfig);
		
		max = theDesc.getMax();
		min = theDesc.getMin();
		diff = Math.abs(max-min);
		itsMeanValue = theDesc.getMeanValue();
		itsStandardDeviation = theDesc.getStandardDeviation();
	}
	
	
	private void init(Project theProject, SimpleAttDesc theDesc, String theName, NormalizationConfig theNominalizationConfig){
		this.prj = theProject;
		this.desc = theDesc;

		this.name = theName;
		
		itsNormalizationConfig = theNominalizationConfig;
	}

	
	@Override
	public Similarity calculateSimilarity(Attribute theAttribute1, Attribute theAttribute2) throws Exception {
		Similarity aResult = Similarity.INVALID_SIM;
		
		if (theAttribute1 instanceof SpecialAttribute
				|| theAttribute2 instanceof SpecialAttribute) {
		    aResult = prj.calculateSpecialSimilarity(theAttribute1, theAttribute2);
		} 
		
		else if (theAttribute1 instanceof MultipleAttribute<?> && theAttribute2 instanceof MultipleAttribute<?>) {
			aResult = prj.calculateMultipleAttributeSimilarity(this,((MultipleAttribute<?>)theAttribute1), (MultipleAttribute<?>)theAttribute2);
		} 
		
		else if (theAttribute1 instanceof SimpleAttribute && theAttribute2 instanceof SimpleAttribute) {
			SimpleAttribute aSimpleAttribute1 = (SimpleAttribute)theAttribute1;
			SimpleAttribute aSimpleAttribute2 = (SimpleAttribute)theAttribute2;
			
			
			if ((aSimpleAttribute1 instanceof IntegerAttribute) && (aSimpleAttribute2 instanceof IntegerAttribute)) {
				IntegerAttribute anIntegerAttribute1 = (IntegerAttribute) aSimpleAttribute1;
				IntegerAttribute anIntegerAttribute2 = (IntegerAttribute) aSimpleAttribute2;
	
				// get query and case values
				double aValue1 = anIntegerAttribute1.getValue();
				double aValue2 = anIntegerAttribute2.getValue();
				
				aResult = getNormalizedSimilarity(aValue1, aValue2);
			}
			
//			else if ((aSimpleAttribute1 instanceof FloatAttribute) && (aSimpleAttribute2 instanceof FloatAttribute)) {
//				FloatAttribute aFloatAttribute1 = (FloatAttribute) aSimpleAttribute1;
//				FloatAttribute aFloatAttribute2 = (FloatAttribute) aSimpleAttribute2;
//	
//				// get query and case values
//				double aValue1 = aFloatAttribute1.getValue();
//				double aValue2 = aFloatAttribute2.getValue();
//				
//				aResult = getNormalizedSimilarity(aValue1, aValue2);
//			}
			
			else if ((aSimpleAttribute1 instanceof DoubleAttribute) && (aSimpleAttribute2 instanceof DoubleAttribute)) {
				DoubleAttribute aDoubleAttribute1 = (DoubleAttribute) aSimpleAttribute1;
				DoubleAttribute aDoubleAttribute2 = (DoubleAttribute) aSimpleAttribute2;
	
				// get query and case values
				double aValue1 = aDoubleAttribute1.getValue();
				double aValue2 = aDoubleAttribute2.getValue();
				
				aResult = getNormalizedSimilarity(aValue1, aValue2);
			}
			
		}
		
		return aResult;
	}
	
	
	/**
	 * Calculates the local similarity between two values of this attribute.
	 * 
	 * @param value1 First value
	 * @param value2 Second value
	 * @return local similarity between two values of this attribute
	 * @throws Exception General error occured
	 */
	public Similarity calculateSimilarity(Integer value1, Integer value2) throws Exception {
		return calculateSimilarity(desc.getAttribute(value1), desc.getAttribute(value2));
	}
	
	/**
	 * Calculates the local similarity between two values of this attribute.
	 * 
	 * @param value1 First value
	 * @param value2 Second value
	 * @return local similarity between two values of this attribute
	 * @throws Exception General error occured
	 */
	public Similarity calculateSimilarity(Double value1, Double value2) throws Exception {
		return calculateSimilarity(desc.getAttribute(value1), desc.getAttribute(value2));
	}
	
	/**
	 * Calculates the local similarity between two values of this attribute.
	 * 
	 * @param value1 First value
	 * @param value2 Second value
	 * @return local similarity between two values of this attribute
	 * @throws Exception General error occured
	 */
	public Similarity calculateSimilarity(Float value1, Float value2) throws Exception {
		return calculateSimilarity(desc.getAttribute(value1), desc.getAttribute(value2));
//		return calculateSimilarity(
//				(SimpleAttribute) range.getAttribute(value1),
//				(SimpleAttribute) range.getAttribute(value2));
	}
	
	
	private Similarity getNormalizedSimilarity(double theValue1, double theValue2){
		
		if (theValue1 == theValue2){
			return Similarity.get(1.0);
		} 
		
		double aNormalizedFactor;
		double aValueDifference = Math.abs(theValue1 - theValue2);
		
		switch (itsNormalizationConfig) {
			case NONE:
				aNormalizedFactor = 1;
				break;
			case RANGE:
				aNormalizedFactor = diff;
				break;
			case FOUR_SIGMA:
				aNormalizedFactor = 4 * itsStandardDeviation;
				break;
			case SIGMA:
				aNormalizedFactor = itsStandardDeviation;
				break;
			case MEAN:
				aNormalizedFactor = itsMeanValue;
				break;
	
			default:
				System.err.println("ERROR: Unknown Normalization config: " + itsNormalizationConfig);
				return Similarity.INVALID_SIM;
		}
		
		double aDistanceValue = aValueDifference / aNormalizedFactor;
		
		// FIXME Achtung: Anders berechnen! Theoretisch darf die Distanz > 1 sein...
		double aSimilarityValue = 1 - aDistanceValue;
		if (aSimilarityValue < 0){
			aSimilarityValue = 0;
		}
		
//		System.out.println(desc.getName() + ": " + itsNormalizationConfig.name() + 
//				" (Value1=" + theValue1 + 
//				"; Value2=" + theValue2 + 
//				"; ValueDiff=" + aValueDifference + 
//				"; NormalizedFactor=" + aNormalizedFactor + 
//				"; Distance=" + aDistanceValue +
//				"; Similarity=" + aSimilarityValue + ")");
		
		return Similarity.get(aSimilarityValue);
	}
	

	@Override
	public void clone(AttributeDesc descNEW, boolean active) {
		if (descNEW instanceof IntegerDesc && !name.equals(Project.DEFAULT_FCT_NAME)) {
			NormalizedNumberFct f = ((ExtendedDoubleDesc)descNEW).addNormalizedNumberFct(name, active, itsNormalizationConfig);
			f.isSymmetric = this.isSymmetric;
			f.mc = this.mc;
			f.distanceFunction = this.distanceFunction;
			
			f.setNormalizationConfig(itsNormalizationConfig);
//			f.setMeanValue(itsMeanValue);
//			f.setStandardDeviation(itsStandardDeviation);
			
			
			// TODO ggf. weitere Attribute...
		}
	}

	@Override
	public void update(Observable arg0, Object arg1) {
		if (arg0.equals(desc)) {
//			super.min = desc.getMin();
//			super.max = desc.getMax();
//			super.diff = max-min;
			
//			if (functionTypeL.equals(NumberConfig.SMOOTH_STEP_AT) || functionTypeL.equals(NumberConfig.STEP_AT)) {
//				// check whether parameter still fits domain
//				if (!fitsDistance((int)functionParameterL)) {
//					functionParameterL = 0;
//				}
//			}
//			if (functionTypeR.equals(NumberConfig.SMOOTH_STEP_AT) || functionTypeR.equals(NumberConfig.STEP_AT)) {
//				// check whether parameter still fits domain
//				if (!fitsDistance((int)functionParameterR)) {
//					functionParameterR = 0;
//				}
//			}
		}
		
	}
	
	
	/**
	 * Sets the Normalization Mode
	 * 
	 * @param theNormalizationMode the new Normalization Mode
	 */
	public void setNormalizationConfig(NormalizationConfig theNormalizationMode) {
		this.itsNormalizationConfig = theNormalizationMode;
		setChanged();
		notifyObservers();
	}

	/**
	 * Returns the Normalization Mode 
	 * 
	 * @return the Normalization Mode  
	 */
	public NormalizationConfig getNormalizationConfig() {
		return itsNormalizationConfig;
	}
	
	
	
//	public double getMeanValue() {
//		return itsMeanValue;
//	}
//
//
//	public void setMeanValue(double itsMeanValue) {
//		this.itsMeanValue = itsMeanValue;
//	this.setChanged();
//	notifyObservers();
//	}
//
//
//	public double getStandardDeviation() {
//		return itsStandardDeviation;
//	}
//
//
//	public void setStandardDeviation(double itsStandardDeviation) {
//		this.itsStandardDeviation = itsStandardDeviation;
//	this.setChanged();
//	notifyObservers();
//	}

}
