/**
 * (2019) Heidelberg University, Institute of Medical Biometry and Informatics (IMBI), Germany
 * 
 * This file is part of myCbrBuilder.
 *  
 * myCbrBuilder is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * myCbrBuilder is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with vivaGen. If not, see <http://www.gnu.org/licenses/>.
 * 
 **/
package similarity.fct;

import java.util.Set;

/**
 * Interface for Classes that implement functions to handle discretized values. 
 * 
 * @author Christian Karmen
 *
 */
public interface IDiscretize {
	/**
	 * General purpose prefix for discretized labels. 
	 */
	final static String DISCRETIZED_TAG = "[DISCRETIZED]";
	
	/**
	 * Prefix for interval labels of discretized values.
	 */
	final static String INTERVAL_LABEL_PREFIX = "D";
	
	
	/**
	 * Calculates the discretized index of a given value.
	 * and concatenated a given prefix.
	 * 
	 * @param theValue value to discretize
	 * @return given prefix with concatenated discretized index; an empty string result indicates an error
	 */
	abstract String getDiscretizedIndexAsString(double theValue);
	
	
	/**
	 * Calculates the discretized index of a given value.
	 * The minimum index is 1; the maximum index is equal the number of max intervals.  
	 * 
	 * @param theValue value to discretize
	 * @return discretized index; -1 means an error occurred
	 */
	abstract int getDiscretizedIndex(double theValue);
	
	
	/**
	 * Generates the interval labels (using a prefix) for all discrete intervals.
	 * 
	 * @return set of interval labels
	 */
	abstract Set<String> generateIntervalLabels();
	
	
	/**
	 * Returns the number of discrete intervals.
	 * 
	 * @return Number of discrete intervals
	 */
	abstract int getNumberOfDiscreteIntervals();
	
	/**
	 * Calculates the width of a discrete interval of a given index. 
	 * 
	 * @param theIntervalIndex interval index of interest
	 * @return the width of a discrete interval
	 */
	public double getIntervalWidth(int theIntervalIndex);

}
