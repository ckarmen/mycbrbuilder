/**
 * (2019) Heidelberg University, Institute of Medical Biometry and Informatics (IMBI), Germany
 * 
 * This file is part of myCbrBuilder.
 *  
 * myCbrBuilder is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * myCbrBuilder is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with vivaGen. If not, see <http://www.gnu.org/licenses/>.
 * 
 **/
package similarity.fct;

import java.util.Observable;

import de.dfki.mycbr.core.Project;
import de.dfki.mycbr.core.casebase.Attribute;
import de.dfki.mycbr.core.model.AttributeDesc;
import de.dfki.mycbr.core.similarity.NumberFct;
import de.dfki.mycbr.core.similarity.Similarity;
import similarity.computation.Computation;
import similarity.desc.ExtendedDoubleDesc;
import similarity.desc.ExtendedIntegerDesc;

/**
 * Class to handle similarity functions for numeric attributes by using a randomization.
 * CAUTION: This is useful only for validation or testing purposes!
 * 
 * @author Christian Karmen
 *
 */
public class RandomNumberFct extends NumberFct {

	/**
	 * Class to handle similarity functions for numeric attributes by using a randomization.
	 * CAUTION: This is useful only for validation or testing purposes!
	 * 
	 * @param theProject Project of interest
	 * @param theDesc Numeric AttributeDesc of interest
	 * @param theName Name of this Fct
	 */
	public RandomNumberFct(Project theProject, ExtendedDoubleDesc theDesc, String theName) {
		super(theProject, theDesc, theName);
	}
	
	/**
	 * Class to handle similarity functions for numeric attributes by using a randomization.
	 * CAUTION: This is useful only for validation or testing purposes!
	 * 
	 * @param theProject Project of interest
	 * @param theDesc Numeric AttributeDesc of interest
	 * @param theName Name of this Fct
	 */
	public RandomNumberFct(Project theProject, ExtendedIntegerDesc theDesc, String theName) {
		super(theProject, theDesc, theName);
	}
	

	@Override
	public Similarity calculateSimilarity(Attribute value1, Attribute value2) throws Exception {
		
		double aRandomSimilarity = Computation.getRandomNumber(1);
		
		return Similarity.get(aRandomSimilarity);
	}

	@Override
	public void clone(AttributeDesc descNEW, boolean active) {
		// TODO Auto-generated method stub

	}

	@Override
	public void update(Observable arg0, Object arg1) {
		// TODO Auto-generated method stub

	}

}
