/**
 * (2019) Heidelberg University, Institute of Medical Biometry and Informatics (IMBI), Germany
 * 
 * This file is part of myCbrBuilder.
 *  
 * myCbrBuilder is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * myCbrBuilder is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with vivaGen. If not, see <http://www.gnu.org/licenses/>.
 * 
 **/
package similarity.fct;

import de.dfki.mycbr.core.DefaultCaseBase;
import de.dfki.mycbr.core.Project;
import de.dfki.mycbr.core.model.AttributeDesc;
import de.dfki.mycbr.core.model.DoubleDesc;
import de.dfki.mycbr.core.similarity.Similarity;
import exceptions.CaseBaseException;
import similarity.computation.ComputationVDM;
import similarity.computation.config.VDMConfig;
import similarity.config.SymbolSimilarityConfig;
import similarity.desc.ExtendedDoubleDesc;
import similarity.desc.ExtendedIntegerDesc;

/**
 * Class to handle similarity functions of discretized numeric values of the IVDM. 
 * </p>
 * IVDM = Interpolated Value Difference Metric, Martinez et al, 1997
 * 
 * @author Christian Karmen
 *
 */
public class IVDMDiscretizedFct extends DVDMDiscretizedFct {
	

	/**
	 * Class to handle similarity functions of discretized numeric values of the IVDM.
	 * </p>
	 * Here, the discretized values can be put in a Lookup-Table (LUT) in order to 
	 * pre-calculate all possible similarities of the possible value pairs.
	 * 
	 * @param theProject Project of interest
	 * @param theDesc Numeric AttributeDesc of interest
	 * @param theName Name of this Fct
	 * @param theTrainingCaseBase Training Case Base to learn discretization from 
	 * @param theVDMConfig VDM Configuration Object
	 * @param theNumberOfDiscreteIntervals Number of discretized values of interest
	 * @param theSymbolSimilarityConfig VDM type configuration
	 * @throws CaseBaseException Error while access to case base
	 */
	public IVDMDiscretizedFct(
			Project theProject, 
			ExtendedDoubleDesc theDesc, 
			String theName,
			DefaultCaseBase theTrainingCaseBase, 
			VDMConfig theVDMConfig,
			int theNumberOfDiscreteIntervals, 
			SymbolSimilarityConfig theSymbolSimilarityConfig) throws CaseBaseException {
		
		super(theProject, theDesc, theName, theTrainingCaseBase, theVDMConfig, theNumberOfDiscreteIntervals, theSymbolSimilarityConfig);
	}
	
	
	/**
	 * Class to handle similarity functions of discretized numeric values of the IVDM.
	 * </p>
	 * Here, the discretized values can be put in a Lookup-Table (LUT) in order to 
	 * pre-calculate all possible similarities of the possible value pairs.
	 * 
	 * @param theProject Project of interest
	 * @param theDesc Numeric AttributeDesc of interest
	 * @param theName Name of this Fct
	 * @param theTrainingCaseBase Training Case Base to learn discretization from 
	 * @param theVDMConfig VDM Configuration Object
	 * @param theNumberOfDiscreteIntervals Number of discretized values of interest
	 * @param theSymbolSimilarityConfig VDM type configuration
	 * @throws CaseBaseException Error while access to case base
	 */
	public IVDMDiscretizedFct(
			Project theProject, 
			ExtendedIntegerDesc theDesc, 
			String theName,
			DefaultCaseBase theTrainingCaseBase, 
			VDMConfig theVDMConfig,
			int theNumberOfDiscreteIntervals, 
			SymbolSimilarityConfig theSymbolSimilarityConfig) throws CaseBaseException {
		
		super(theProject, theDesc, theName, theTrainingCaseBase, theVDMConfig, theNumberOfDiscreteIntervals, theSymbolSimilarityConfig);
	}



	@Override
	public Similarity calculateDiscretizedSimilarity(double theValue1, double theValue2) throws CaseBaseException {
		
		double aPacx, aPacy;
		double aDistance = 0d;
		
		for (String anAllowedOutputValue : ((VDMConfig)getSimilarityMeasureConfig()).OutputClassValueList) {
			aPacx = getPac(theValue1, anAllowedOutputValue);
			aPacy = getPac(theValue2, anAllowedOutputValue);
			
			aDistance += Math.pow(Math.abs(aPacx - aPacy) , 2);
		}
		
		// FIXME Achtung: Ggf. anders berechnen! Theoretisch darf die Distanz > 1 sein...
		double aSimilarityValue = 1 - aDistance;
		if (aSimilarityValue < 0){
			aSimilarityValue = 0;
		}
		
		Similarity aIVDMSimilarity = Similarity.get(aSimilarityValue);
		
		return aIVDMSimilarity;
	}
	
	
	
	private double getMid(int theDiscreteIntervall){
		return min + getIntervalWidth(1) * (theDiscreteIntervall + 0.5);
	}
	
	
	
	private double getPac(double theInputValue, String theOutputClassValue) throws CaseBaseException{
		int aDiscreteInterval = calculateDiscreteInterval(theInputValue);
		
		double Pauc = getPauc(addDiscreteIntervalPrefix(aDiscreteInterval), theOutputClassValue);
		double Pau1c = getPauc(addDiscreteIntervalPrefix(aDiscreteInterval+1), theOutputClassValue);
		double MIDau = getMid(aDiscreteInterval);
		double MIDau1 = getMid(aDiscreteInterval + 1);
		
		return Pauc + ( (theInputValue - MIDau) / (MIDau1 - MIDau) ) * (Pau1c - Pauc);
	}
	
	
	
	private double getPauc(String theDiscreteIntervallAsString, String theOutputClassValue) throws CaseBaseException{
		// FIXME: Kann man die Abh�ngigkeit von itsDiscretizedMiniCaseBase etc. l�sen??
		double aPaxc = ComputationVDM.getPaxc(itsDiscretizedMiniCaseBase, itsDiscretizedSymbolDesc, theDiscreteIntervallAsString, itsVDMConfig, theOutputClassValue); 
		return aPaxc;
	}
	
	
	
	private int calculateDiscreteInterval(double theValue){
		int theU = getDiscretizedIndex(theValue);
		
		// FIXME Warum klappt das nicht??
//		if (theU < getMid(theU)){
//			theU--;
//		}
		
//		int tmp = theU;
//		
//		if (theU < getMid(theU)){
//			tmp = theU + 1;
//		} else if (theU > getMid(theU)){
//			tmp = theU - 1;
//		}
		
		return theU;
	}
	

	private String addDiscreteIntervalPrefix(int theDiscreteIntervall){
		return AbstractDiscretizedNumberFct.INTERVAL_LABEL_PREFIX + theDiscreteIntervall;
	}
	



	
	@Override
	public void clone(AttributeDesc descNEW, boolean active) {
		if (descNEW instanceof DoubleDesc && !name.equals(Project.DEFAULT_FCT_NAME)) {
//			LinearDiscretizedFct f = ((LinearContinuousDesc)descNEW).addDVDMFct(name, active, itsTrainingCaseBase, itsOutputClass);
			
			// FIXME reicht das?? Was ist mit LinearDiscreteDesc??
			IVDMDiscretizedFct f = null;
			try {
				f = ((ExtendedDoubleDesc)descNEW).addIVDMFct(name, active, itsTrainingCaseBase, (VDMConfig)itsSimilarityConfig, itsDiscretizationSymbolConfig);
			} catch (CaseBaseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}			
			
			f.isSymmetric = this.isSymmetric;
			f.mc = this.mc;
			f.distanceFunction = this.distanceFunction;
			
//			f.setDiscretizationConfig(itsDiscretizationConfig);
			f.setDiscretizationSymbolConfig(itsDiscretizationSymbolConfig);
//			f.setMeanValue(itsMeanValue);
//			f.setStandardDeviation(itsStandardDeviation);
			
			
			// TODO some more stuff?!
			f.itsTrainingCaseBase = itsTrainingCaseBase;
		}
	}

}
