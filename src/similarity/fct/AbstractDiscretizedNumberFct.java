/**
 * (2019) Heidelberg University, Institute of Medical Biometry and Informatics (IMBI), Germany
 * 
 * This file is part of myCbrBuilder.
 *  
 * myCbrBuilder is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * myCbrBuilder is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with vivaGen. If not, see <http://www.gnu.org/licenses/>.
 * 
 **/
package similarity.fct;

import java.util.Observable;
import java.util.Set;

import de.dfki.mycbr.core.DefaultCaseBase;
import de.dfki.mycbr.core.Project;
import de.dfki.mycbr.core.casebase.Attribute;
import de.dfki.mycbr.core.casebase.DoubleAttribute;
import de.dfki.mycbr.core.casebase.FloatAttribute;
import de.dfki.mycbr.core.casebase.IntegerAttribute;
import de.dfki.mycbr.core.casebase.MultipleAttribute;
import de.dfki.mycbr.core.casebase.SimpleAttribute;
import de.dfki.mycbr.core.casebase.SpecialAttribute;
import de.dfki.mycbr.core.model.Concept;
import de.dfki.mycbr.core.model.SimpleAttDesc;
import de.dfki.mycbr.core.similarity.NumberFct;
import de.dfki.mycbr.core.similarity.Similarity;
import exceptions.CaseBaseException;
import similarity.computation.ComputationDiscretize;
import similarity.computation.config.AbstractSimilarityMeasureConfiguration;
import similarity.config.SymbolSimilarityConfig;
import similarity.desc.ExtendedDoubleDesc;
import similarity.desc.ExtendedIntegerDesc;

/**
 * Abstract class to handle similarity functions of discretized numeric values. 
 * 
 * @author Christian Karmen
 *
 */
public abstract class AbstractDiscretizedNumberFct extends NumberFct implements IDiscretize{
	
	protected SymbolSimilarityConfig itsDiscretizationSymbolConfig;	
	protected int itsNumberOfDiscreteIntervals;		
	protected DefaultCaseBase itsTrainingCaseBase;
	protected AbstractSimilarityMeasureConfiguration itsSimilarityConfig;
	protected Concept itsDiscretizedConcept;
	
	protected static int itsMinIntervals = 2; 
	
	
	/**
	 * Abstract class to handle similarity functions of discretized numeric values.
	 * 
	 * @param theProject Project of interest
	 * @param theDesc Numeric AttributeDesc of interest
	 * @param theName Name of this Fct
	 * @param theTrainingCaseBase Training Case Base to learn discretization from 
	 * @param theSimilarityConfig Similarity Measure Configuration Object
	 * @param theNumberOfDiscreteIntervals Number of discretized values of interest
	 */
	public AbstractDiscretizedNumberFct(
			Project theProject,   
			ExtendedDoubleDesc theDesc, 
			String theName, 
			DefaultCaseBase theTrainingCaseBase, 
			AbstractSimilarityMeasureConfiguration theSimilarityConfig,
			int theNumberOfDiscreteIntervals) {
		super(theProject, theDesc, theName);
		
		init(theProject, theDesc, theName, theTrainingCaseBase, theSimilarityConfig, theNumberOfDiscreteIntervals);
	}
	
	
	/**
	 * Abstract class to handle similarity functions of discretized numeric values.
	 * 
	 * @param theProject Project of interest
	 * @param theDesc Numeric AttributeDesc of interest
	 * @param theName Name of this Fct
	 * @param theTrainingCaseBase Training Case Base to learn discretization from 
	 * @param theSimilarityConfig Similarity Measure Configuration Object
	 * @param theNumberOfDiscreteIntervals Number of discretized values of interest
	 */
	public AbstractDiscretizedNumberFct(
			Project theProject, 
			ExtendedIntegerDesc theDesc, 
			String theName, 
			DefaultCaseBase theTrainingCaseBase,
			AbstractSimilarityMeasureConfiguration theSimilarityConfig,
			int theNumberOfDiscreteIntervals) {
		super(theProject, theDesc, theName);
		
		init(theProject, theDesc, theName, theTrainingCaseBase, theSimilarityConfig, theNumberOfDiscreteIntervals);
	}
	
	
	private void init(
			Project theProject, 
			SimpleAttDesc theDesc, 
			String theName, 
			DefaultCaseBase theTrainingCaseBase, 
			AbstractSimilarityMeasureConfiguration theSimilarityConfig,
			int theNumberOfDiscreteIntervals){
		this.prj = theProject;
		this.desc = theDesc;
		
		itsTrainingCaseBase = theTrainingCaseBase;
		itsSimilarityConfig = theSimilarityConfig;
		itsDiscretizationSymbolConfig = SymbolSimilarityConfig.VDM_N2;

		this.name = theName;
		
		if (theDesc instanceof ExtendedDoubleDesc){
			max = ((ExtendedDoubleDesc)theDesc).getMax();
			min = ((ExtendedDoubleDesc)theDesc).getMin();
		} else if (theDesc instanceof ExtendedIntegerDesc){
			max = ((ExtendedIntegerDesc)theDesc).getMax();
			min = ((ExtendedIntegerDesc)theDesc).getMin();
		} else {
			System.err.println("ERROR: cannot determine Desc-Type of " + theDesc + "!");
		}
		
		diff = Math.abs(max-min);
		
		String aConceptName = DISCRETIZED_TAG + name + "[Concept]";		
		try {
			itsDiscretizedConcept = prj.createTopConcept(aConceptName);

		} catch (Exception e) {
			e.printStackTrace();
		}
		
		setNumberOfDiscreteIntervals(theNumberOfDiscreteIntervals);
	}
	
	
	/**
	 * Gets the current training case base.
	 * 
	 * @return the TrainingCaseBase
	 */
	public DefaultCaseBase getTrainingCaseBase() {
		return itsTrainingCaseBase;
	}
	

	/**
	 * Gets the similarity measure specific configuration object.
	 * 
	 * @return similarity measure specific configuration
	 */
	public AbstractSimilarityMeasureConfiguration getSimilarityMeasureConfig(){
		return itsSimilarityConfig;
	}
	
		
	/**
	 * Generates the interval labels (using a prefix) for all discrete intervals.
	 * 
	 * @return set of interval labels
	 */
	@Override
	public Set<String> generateIntervalLabels(){	
		return ComputationDiscretize.generateIntervalLabels(itsNumberOfDiscreteIntervals, INTERVAL_LABEL_PREFIX);
	}
	

	/**
	 * Gets the DiscretizationSymbolConfig.
	 * 
	 * @return DiscretizationSymbolConfig
	 */
	public SymbolSimilarityConfig getDiscretizationSymbolConfig() {
		return itsDiscretizationSymbolConfig;
	}


	/**
	 * Sets the DiscretizationSymbolConfig.
	 * 
	 * @param theDiscretizationSymbolConfig DiscretizationSymbolConfig to set
	 */
	protected void setDiscretizationSymbolConfig(SymbolSimilarityConfig theDiscretizationSymbolConfig) {
		this.itsDiscretizationSymbolConfig = theDiscretizationSymbolConfig;
	}
	
	
	/**
	 * Returns the number of discrete intervals.
	 * 
	 * @return Number of discrete intervals
	 */
	public int getNumberOfDiscreteIntervals() {
		return itsNumberOfDiscreteIntervals;
	}


	/**
	 * Sets the number of discrete intervals.
	 * 
	 * @param theNumberOfDiscreteIntervals Number Of discrete intervals
	 */
	protected void setNumberOfDiscreteIntervals(int theNumberOfDiscreteIntervals) {
		this.itsNumberOfDiscreteIntervals = theNumberOfDiscreteIntervals;
		
		if (theNumberOfDiscreteIntervals < getNumberOfMinIntervals()){
			this.itsNumberOfDiscreteIntervals = getNumberOfMinIntervals();
		}
	}
	
	
	/**
	 * Gets the minimal number of intervals allowed for dicretization. 
	 * 
	 * @return minimal number of intervals
	 */
	public static int getNumberOfMinIntervals(){
		return itsMinIntervals;
	}
	
	
	/**
	 * Calculates the discretized index of a given value.
	 * and concatenated a given prefix.
	 * 
	 * @param theValue value to discretize
	 * @return given prefix with concatenated discretized index; an empty string result indicates an error
	 */
	@Override
	public String getDiscretizedIndexAsString(double theValue){
		return ComputationDiscretize.getDiscretizedIndexAsString(theValue, min, max, itsNumberOfDiscreteIntervals, INTERVAL_LABEL_PREFIX);
	}
	
	
	/**
	 * Calculates the discretized index of a given value.
	 * The minimum index is 1; the maximum index is equal the number of max intervals.  
	 * 
	 * @param theValue value to discretize
	 * @return discretized index; -1 means an error occurred
	 */
	@Override
	public int getDiscretizedIndex(double theValue){	
		return ComputationDiscretize.getDiscretizedIndex(theValue, min, max, itsNumberOfDiscreteIntervals);
	}
	

	@Override
	public Similarity calculateSimilarity(Attribute theAttribute1, Attribute theAttribute2) throws Exception {
		Similarity aResult = Similarity.INVALID_SIM;
		
		if (theAttribute1 instanceof SpecialAttribute
				|| theAttribute2 instanceof SpecialAttribute) {
		    aResult = prj.calculateSpecialSimilarity(theAttribute1, theAttribute2);
		} 
		
		else if (theAttribute1 instanceof MultipleAttribute<?> && theAttribute2 instanceof MultipleAttribute<?>) {
			aResult = prj.calculateMultipleAttributeSimilarity(this,((MultipleAttribute<?>)theAttribute1), (MultipleAttribute<?>)theAttribute2);
		} 
		
		else if (theAttribute1 instanceof SimpleAttribute && theAttribute2 instanceof SimpleAttribute) {
			SimpleAttribute aSimpleAttribute1 = (SimpleAttribute)theAttribute1;
			SimpleAttribute aSimpleAttribute2 = (SimpleAttribute)theAttribute2;
			double aValue1 = Double.NaN;
			double aValue2 = Double.NaN;;
			
			if ((aSimpleAttribute1 instanceof IntegerAttribute) && (aSimpleAttribute2 instanceof IntegerAttribute)) {
				IntegerAttribute anIntegerAttribute1 = (IntegerAttribute) aSimpleAttribute1;
				IntegerAttribute anIntegerAttribute2 = (IntegerAttribute) aSimpleAttribute2;
	
				// get query and case values
				aValue1 = anIntegerAttribute1.getValue();
				aValue2 = anIntegerAttribute2.getValue();
			}
			
			else if ((aSimpleAttribute1 instanceof FloatAttribute) && (aSimpleAttribute2 instanceof FloatAttribute)) {
				FloatAttribute aFloatAttribute1 = (FloatAttribute) aSimpleAttribute1;
				FloatAttribute aFloatAttribute2 = (FloatAttribute) aSimpleAttribute2;
	
				// get query and case values
				aValue1 = aFloatAttribute1.getValue();
				aValue2 = aFloatAttribute2.getValue();	
			}
			
			else if ((aSimpleAttribute1 instanceof DoubleAttribute) && (aSimpleAttribute2 instanceof DoubleAttribute)) {
				DoubleAttribute aDoubleAttribute1 = (DoubleAttribute) aSimpleAttribute1;
				DoubleAttribute aDoubleAttribute2 = (DoubleAttribute) aSimpleAttribute2;
	
				// get query and case values
				aValue1 = aDoubleAttribute1.getValue();
				aValue2 = aDoubleAttribute2.getValue();	
			}
			
			else{
				System.err.println("ERROR: Unhandled attribute class: " + aSimpleAttribute1.getClass());
				return Similarity.INVALID_SIM;
			}
			
			if (aValue1 == aValue2){
				return Similarity.get(1d);
			}
			aResult = calculateDiscretizedSimilarity(aValue1, aValue2);
			
		}
		
		return aResult;
	}
	
	
	
	/**
	 * Calculates the similarity of two given values based on discretization.
	 * 
	 * @param value1 First value to compare
	 * @param value2 Second value to compare
	 * @return similarity of two given values
	 * @throws Exception
	 */
	public Similarity calculateSimilarity(Integer value1, Integer value2) throws Exception {
		return calculateSimilarity(desc.getAttribute(value1), desc.getAttribute(value2));
	}
	
	
	
	/**
	 * Calculates the similarity of two given values based on discretization.
	 * 
	 * @param value1 First value to compare
	 * @param value2 Second value to compare
	 * @return similarity of two given values
	 * @throws Exception
	 */
	public Similarity calculateSimilarity(Float value1, Float  value2) throws Exception {
		return calculateSimilarity(desc.getAttribute(value1), desc.getAttribute(value2));
	}
	
	
	
	/**
	 * Calculates the similarity of two given values based on discretization.
	 * 
	 * @param value1 First value to compare
	 * @param value2 Second value to compare
	 * @return similarity of two given values
	 * @throws Exception
	 */
	public Similarity calculateSimilarity(Double value1, Double value2) throws Exception {
		return calculateSimilarity(desc.getAttribute(value1), desc.getAttribute(value2));
	}
	
	

	/**
	 * Gets the smallest value of the interval with the given index.
	 * 
	 * @param theIntervalIndex interval index
	 * @return smallest value of the interval
	 */
	abstract public double getMinIntervalValue(int theIntervalIndex);
	
	
	
	/**
	 * Gets the highest value of the interval with the given index.
	 * 
	 * @param theIntervalIndex interval index
	 * @return smallest value of the interval
	 */
	abstract public double getMaxIntervalValue(int theIntervalIndex);	

	
	
	/**
	 * Calculates the similarity two given attribute values based on discretization. 
	 * 
	 * @param theValue1	first value
	 * @param theValue2	second value
	 * @return similarity based on discretization
	 * @throws CaseBaseException 
	 */
	abstract public Similarity calculateDiscretizedSimilarity(double theValue1, double theValue2) throws CaseBaseException;
	
	
	
	/**
	 * Gets the Concept for discretized values 
	 * 
	 * @return Concept for discretized values 
	 */
	public Concept getDiscretizedConcept() {
		return itsDiscretizedConcept;
	}
	
	
	
	@Override
	public void update(Observable arg0, Object arg1) {
		if (arg0.equals(desc)) {
			if (desc instanceof ExtendedDoubleDesc){
				max = ((ExtendedDoubleDesc)desc).getMax();
				min = ((ExtendedDoubleDesc)desc).getMin();
			} else if (desc instanceof ExtendedIntegerDesc){
				max = ((ExtendedIntegerDesc)desc).getMax();
				min = ((ExtendedIntegerDesc)desc).getMin();
			} else {
				System.err.println("ERROR: cannot determine Desc-Type of " + desc + "!");
			}
			super.diff = max-min;
			
			// TODO some more stuff?!
		}
	}
}
