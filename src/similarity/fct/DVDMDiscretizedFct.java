/**
 * (2019) Heidelberg University, Institute of Medical Biometry and Informatics (IMBI), Germany
 * 
 * This file is part of myCbrBuilder.
 *  
 * myCbrBuilder is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * myCbrBuilder is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with vivaGen. If not, see <http://www.gnu.org/licenses/>.
 * 
 **/
package similarity.fct;

import core.CaseBaseUtils;
import core.MyCbrUtils;
import de.dfki.mycbr.core.DefaultCaseBase;
import de.dfki.mycbr.core.Project;
import de.dfki.mycbr.core.casebase.Attribute;
import de.dfki.mycbr.core.casebase.Instance;
import de.dfki.mycbr.core.casebase.SpecialAttribute;
import de.dfki.mycbr.core.model.AttributeDesc;
import de.dfki.mycbr.core.model.Concept;
import de.dfki.mycbr.core.model.DoubleDesc;
import de.dfki.mycbr.core.model.SymbolDesc;
import de.dfki.mycbr.core.similarity.Similarity;
import exceptions.CaseBaseException;
import similarity.computation.ComputationVDM;
import similarity.computation.config.VDMConfig;
import similarity.config.SymbolSimilarityConfig;
import similarity.desc.ExtendedDoubleDesc;
import similarity.desc.ExtendedIntegerDesc;

/**
 * Class to handle similarity functions of discretized numeric values of the DVDM. 
 * </p>
 * DVDM = Discretized Value Difference Metric, Martinez et al, 1997
 * 
 * @author Christian Karmen
 *
 */
public class DVDMDiscretizedFct extends AbstractLookUpDiscretizedNumberFct {
	
	protected DefaultCaseBase itsDiscretizedMiniCaseBase;
	private SymbolSimilarityConfig itsSymbolSimilarityConfig;
	protected SymbolDesc itsDiscretizedOutputDesc;
	protected VDMConfig itsVDMConfig;
	
	static {
		itsMinIntervals=5;
	}

	
	/**
	 * Class to handle similarity functions of discretized numeric values of the DVDM.
	 * </p>
	 * Here, the discretized values can be put in a Lookup-Table (LUT) in order to 
	 * pre-calculate all possible similarities of the possible value pairs.
	 * 
	 * @param theProject Project of interest
	 * @param theDesc Numeric AttributeDesc of interest
	 * @param theName Name of this Fct
	 * @param theTrainingCaseBase Training Case Base to learn discretization from 
	 * @param theVDMConfig VDM Configuration Object
	 * @param theNumberOfDiscreteIntervals Number of discretized values of interest
	 * @param theSymbolSimilarityConfig VDM type configuration
	 * @throws CaseBaseException Error while access to case base
	 */
	public DVDMDiscretizedFct(
			Project theProject, 
			ExtendedDoubleDesc theDesc, 
			String theName,
			DefaultCaseBase theTrainingCaseBase, 
			VDMConfig theVDMConfig,
			int theNumberOfDiscreteIntervals, 
			SymbolSimilarityConfig theSymbolSimilarityConfig) throws CaseBaseException {
		
		super(theProject, theDesc, theName, theTrainingCaseBase, theVDMConfig, theNumberOfDiscreteIntervals);
		
		init(theVDMConfig, theNumberOfDiscreteIntervals, theSymbolSimilarityConfig);
	}
	
	
	/**
	 * Class to handle similarity functions of discretized numeric values of the DVDM.
	 * </p>
	 * Here, the discretized values can be put in a Lookup-Table (LUT) in order to 
	 * pre-calculate all possible similarities of the possible value pairs.
	 * 
	 * @param theProject Project of interest
	 * @param theDesc Numeric AttributeDesc of interest
	 * @param theName Name of this Fct
	 * @param theTrainingCaseBase Training Case Base to learn discretization from 
	 * @param theVDMConfig VDM Configuration Object
	 * @param theNumberOfDiscreteIntervals Number of discretized values of interest
	 * @param theSymbolSimilarityConfig VDM type configuration
	 * @throws CaseBaseException Error while access to case base
	 */
	public DVDMDiscretizedFct(
			Project theProject, 
			ExtendedIntegerDesc theDesc, 
			String theName,
			DefaultCaseBase theTrainingCaseBase, 
			VDMConfig theVDMConfig,
			int theNumberOfDiscreteIntervals, 
			SymbolSimilarityConfig theSymbolSimilarityConfig) throws CaseBaseException {
		super(theProject, theDesc, theName, theTrainingCaseBase, theVDMConfig, theNumberOfDiscreteIntervals);
		
		init(theVDMConfig, theNumberOfDiscreteIntervals, theSymbolSimilarityConfig);
	}
	
	
	
	private void init(
			VDMConfig theVDMConfig, 
			int theNumberOfDiscreteIntervals, 
			SymbolSimilarityConfig theSymbolSimilarityConfig) throws CaseBaseException{
		
//		MyCbrUtils.printTitle("Processing VDM of attribute '" + desc.getName() + "'");
		
		setNumberOfDiscreteIntervals(theNumberOfDiscreteIntervals);
		
		itsVDMConfig = theVDMConfig;
		
		try {
			itsDiscretizedOutputDesc = new SymbolDesc(getProject(), itsVDMConfig.OutputClassLabel, itsVDMConfig.OutputClassValueList);
			getDiscretizedConcept().addAttributeDesc(itsDiscretizedOutputDesc);
		} catch (Exception e) {
			e.printStackTrace();
		} 	
		
		itsSymbolSimilarityConfig = theSymbolSimilarityConfig;

		setSimilarityMatrix();
	}
	

	
	
	/**
	 * Builds a case base (from given CB) in order to process the newly created discretized values.
	 * Similar to original CB, however consists ONLY of a discretized (extended) symbol of a previously numeric attribute.
	 * 
	 * @param theTargetConcept concept where the case base will be created in. 
	 * @return CB with discretized attributes only
	 */
	protected DefaultCaseBase buildDiscretizedMiniCaseBase(Concept theTargetConcept){
		DefaultCaseBase aDiscretizedTrainingCaseBase = null;
		Instance aDiscretizedInstance;
		double aCaseValue = 0;
		String anOutputClassValue;
		boolean aSuccessFlag;
		Attribute aTrainingCaseAttribute;
		
		int i = 0;
		int anOldCaseBaseSize = getTrainingCaseBase().getCases().size();
		
		try {
			aDiscretizedTrainingCaseBase = new DefaultCaseBase(prj, DISCRETIZED_TAG + name + "[CASEBASE]", anOldCaseBaseSize);
			
			for (Instance aTrainingCase : getTrainingCaseBase().getCases()) {
				i++;
				aTrainingCaseAttribute = aTrainingCase.getAttForDesc(MyCbrUtils.getAttributeDescByName(desc.getOwner(), desc.getName()));
				
				aDiscretizedInstance = new Instance(theTargetConcept, aTrainingCase.getName());
				if (aTrainingCaseAttribute instanceof SpecialAttribute){
					aSuccessFlag = aDiscretizedInstance.addAttribute(getDiscretizedSymbolDesc(), (SpecialAttribute)aTrainingCaseAttribute);
				} else {
					aCaseValue = Double.parseDouble(aTrainingCaseAttribute.getValueAsString());
					aSuccessFlag = aDiscretizedInstance.addAttribute(getDiscretizedSymbolDesc(), getDiscretizedIndexAsString(aCaseValue));
				}
				
				if (!aSuccessFlag){
					System.err.println("ERROR: ERROR AT ADDING DISCRETIZED ATTRIBUTE (" + getDiscretizedSymbolDesc().getName() + " = " + aCaseValue + "[Discretized=" + getDiscretizedIndexAsString(aCaseValue) + "]) ==> " + i);
				}
				
				anOutputClassValue = CaseBaseUtils.getCaseAttributeValue(aTrainingCase, itsVDMConfig.OutputClassLabel).getValueAsString();
				aSuccessFlag = aDiscretizedInstance.addAttribute(itsDiscretizedOutputDesc, anOutputClassValue);
				if (!aSuccessFlag){
					System.err.println("ERROR: ERROR AT ADDING OUTPUT CLASS ATTRIBUTE (" + itsDiscretizedOutputDesc.getName() + " = " + anOutputClassValue + ")");
				}

				aDiscretizedTrainingCaseBase.addCase(aDiscretizedInstance);
			}
			
		} catch (Exception e) {
			System.err.println("ERROR: Cannot create Discretized Training-CaseBase!");
			e.printStackTrace();
		}
		
		return aDiscretizedTrainingCaseBase;
	}
	
	
	
	/**
	 * Gets the VDM discretized Mini Case Base. It contains of two elements only: (a) CaseID and (b) Discretized Attribute.
	 * 
	 * @return VDM discretized Mini Case Base
	 */
	public DefaultCaseBase getDiscretizedMiniCaseBase() {
		return itsDiscretizedMiniCaseBase;
	}
	
	
	/**
	 * Gets the VDM similarity configuration.
	 * 
	 * @return VDM similarity configuration
	 */
	public SymbolSimilarityConfig getSymbolSimilarityConfig(){
		return itsSymbolSimilarityConfig;
	}
	
	
	
	@Override
	public Similarity calculateDiscretizedSimilarity(double theValue1, double theValue2) throws CaseBaseException{
		String aDiscretizedValue1 = getDiscretizedIndexAsString(theValue1);
		String aDiscretizedValue2 = getDiscretizedIndexAsString(theValue2);
		
		Similarity aVDMSimilarity;
		try {
			aVDMSimilarity = getDiscretizedSymbolFct().calculateSimilarity(aDiscretizedValue1, aDiscretizedValue2);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		
		return aVDMSimilarity;
	}
	


	@Override
	void setSimilarityMatrix() throws CaseBaseException {
		itsDiscretizedMiniCaseBase = buildDiscretizedMiniCaseBase(getDiscretizedConcept());
		ComputationVDM.setSimilarityMatrix_VDM(getDiscretizedSymbolFct(), itsSymbolSimilarityConfig, itsDiscretizedMiniCaseBase, itsVDMConfig);
	}

	
	@Override
	public double getMinIntervalValue(int theIntervalIndex){
		// FIXME: Interval min/max values must not contain values from another interval
		return min + theIntervalIndex * getIntervalWidth(theIntervalIndex);
	}
	
	
	@Override
	public double getMaxIntervalValue(int theIntervalIndex){
		// FIXME: Interval min/max values must not contain values from another interval
		return getMinIntervalValue(theIntervalIndex) + getIntervalWidth(theIntervalIndex);
	}	
	
	
	@Override
	public double getIntervalWidth(int theIntervalIndex) {
		// no interval index needed, all intervals have same length here
		return diff / itsNumberOfDiscreteIntervals;
	}
	
	
	
	@Override
	public void clone(AttributeDesc descNEW, boolean active) {
		if (descNEW instanceof DoubleDesc && !name.equals(Project.DEFAULT_FCT_NAME)) {
			
			// FIXME Unterscheidung zwischen ExtendedDouble + ExtendedInteger
			DVDMDiscretizedFct f = null;
			try {
				f = ((ExtendedDoubleDesc)descNEW).addDVDMFct(
						name, 
						active, 
						getTrainingCaseBase(), 
						(VDMConfig)itsSimilarityConfig, 
						itsSymbolSimilarityConfig);
			} catch (CaseBaseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			f.isSymmetric = this.isSymmetric;
			f.mc = this.mc;
			f.distanceFunction = this.distanceFunction;
			f.setDiscretizationSymbolConfig(itsDiscretizationSymbolConfig);

			f.itsDiscretizedMiniCaseBase = this.itsDiscretizedMiniCaseBase;
			f.itsSymbolSimilarityConfig = this.itsSymbolSimilarityConfig;
			f.itsDiscretizedOutputDesc = this.itsDiscretizedOutputDesc;
			f.itsVDMConfig = this.itsVDMConfig;
		}
	}

}
