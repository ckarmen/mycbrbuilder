/**
 * (2019) Heidelberg University, Institute of Medical Biometry and Informatics (IMBI), Germany
 * 
 * This file is part of myCbrBuilder.
 *  
 * myCbrBuilder is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * myCbrBuilder is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with vivaGen. If not, see <http://www.gnu.org/licenses/>.
 * 
 **/
package similarity.fct;

import de.dfki.mycbr.core.Project;
import de.dfki.mycbr.core.model.SymbolDesc;
import de.dfki.mycbr.core.similarity.SymbolFct;

/**
 * @author Christian Karmen
 *
 */
public class DiscretizedSymbolFct extends SymbolFct{ //implements IDiscretize{
	
//	private SymbolSimilarityConfig itsDiscretizationSymbolConfig;
//	private DefaultCaseBase itsDiscretizedCaseBase;
//	private SymbolDesc itsOutputClass;
//	protected int itsNumberOfDiscreteIntervals;
	
//	private DiscretizationConfig itsDiscretizationConfig;
//	final static int MIN_INTERVALS = 5;
	
//	private double itsMinValue;
//	private double itsMaxValue;
	
//	private double itsIntervalWidth;
//	private double itsDiff;	
	

	/**
	 * 
	 * @param prj
	 * @param desc
	 * @param name
	 * @param theMinValue
	 * @param theMaxValue
	 */
	public DiscretizedSymbolFct(
			Project prj, 
			SymbolDesc desc, 
			String name, 
			double theMinValue, 
			double theMaxValue
//			SymbolSimilarityConfig theDiscretizationSymbolConfig, 
//			DefaultCaseBase theDiscretizedCaseBase,
//			SymbolDesc theOutputClass
			) {
		
		super(prj, desc, IDiscretize.DISCRETIZED_TAG + name);
		
//		itsDiscretizationSymbolConfig = theDiscretizationSymbolConfig;
//		itsDiscretizedCaseBase = theDiscretizedCaseBase;
//		itsOutputClass = theOutputClass;
		
//		boolean aSuccessFlag = recalculateAllDiscretizedSimilarities();
//		
//		if(!aSuccessFlag){
//			System.err.println("ERROR: Problems occured during function recalculateAllDiscretizedSimilarities()!");
//		}
		
//		itsNumberOfDiscreteIntervals = MIN_INTERVALS;
//		setNumberOfIntervals(theOutputClass.getAllowedValues().size());
		
//		itsMinValue = theMinValue;
//		itsMaxValue = theMaxValue;
		
//		itsDiff = Math.abs(itsMaxValue - itsMinValue);
//		itsIntervalWidth = itsDiff / itsNumberOfDiscreteIntervals;
	}
	
	
	
	
	

//	/**
//	 * Gets the current DiscretizationSymbolConfig.
//	 * 
//	 * @return DiscretizationSymbolConfig
//	 */
//	public SymbolSimilarityConfig getDiscretizationSymbolConfig() {
//		return itsDiscretizationSymbolConfig;
//	}

	
//	/**
//	 * Sets a new DiscretizationSymbolConfig and thus recalculates all possible 
//	 * similarities of the discretized values (in the internal Lookup-Table of SymbolFct).
//	 * 
//	 * @param itsDiscretizationSymbolConfig DiscretizationSymbolConfig to set
//	 */
//	public void setDiscretizationSymbolConfig(
//			SymbolSimilarityConfig itsDiscretizationSymbolConfig) {
//		this.itsDiscretizationSymbolConfig = itsDiscretizationSymbolConfig;
//		
////		recalculateAllDiscretizedSimilarities();
//	}
	
	
//	/**
//	 * Gets the current DiscretizedCaseBase.
//	 * 
//	 * @return DiscretizedCaseBase
//	 */
//	public DefaultCaseBase getDiscretizedCaseBase() {
//		return itsDiscretizedCaseBase;
//	}

	
//	/**
//	 * Gets the current number of intervals.
//	 * 
//	 * @return Number of intervals
//	 */
//	@Override
//	public int getNumberOfDiscreteIntervals() {
//		return itsNumberOfDiscreteIntervals;
//	}

//	/**
//	 * Gets the current output class.
//	 * 
//	 * @return Output class
//	 */
//	@Override
//	public SymbolDesc getOutputClass() {
//		return itsOutputClass;
//	}

	
//	/**
//	 * Sets a new output class and thus recalculates all possible 
//	 * similarities of the discretized values (in the internal Lookup-Table of SymbolFct).
//	 * 
//	 * @param itsOutputClass Output class to set
//	 */
//	public void setOutputClass(SymbolDesc itsOutputClass) {
//		this.itsOutputClass = itsOutputClass;
//		
//		recalculateAllDiscretizedSimilarities();
//	}
	
	
//	/**
//	 * Calculates all possible similarities of the discretized values (in the internal Lookup-Table of SymbolFct).
//	 * 
//	 * @param theDiscretizationSymbolConfig discretization symbol config
//	 * @return true for success; false for error
//	 */
//	private boolean recalculateAllDiscretizedSimilarities(){
//		boolean aLocalSuccessFlag = false;
//		boolean aGlobalSuccessFlag = true;
//		Similarity anCalculatedSimilarity = Similarity.INVALID_SIM;
//		
//		for (String anAllowedValue1 : desc.getAllowedValues()) {
//			for (String anAllowedValue2 : desc.getAllowedValues()) {
//				anCalculatedSimilarity = calculateDiscretizedSimilarity(
//						anAllowedValue1, 
//						anAllowedValue2, 
//						itsDiscretizationSymbolConfig);
//				aLocalSuccessFlag = setSimilarity(
//						anAllowedValue1, 
//						anAllowedValue2, 
//						anCalculatedSimilarity);
//				
//				if (aLocalSuccessFlag == false) {
//					aGlobalSuccessFlag = false;
//				}
//			}
//		}
//		
//		return aGlobalSuccessFlag;
//	}
	
	
//	/**
//	 * Calculates the specific similarity of two given (discretized) attribute's values.
//	 * 
//	 * @param theValue1
//	 * @param theValue2
//	 * @param theSymbolSimilarityConfig
//	 * @return
//	 */
//	private Similarity calculateDiscretizedSimilarity(
//			String theValue1, 
//			String theValue2, 
//			SymbolSimilarityConfig theSymbolSimilarityConfig){
//		double aSimilarity = Double.NaN;
//		double aDistance = Double.NaN;
//		
//		setSymmetric(true);
//		
//		if (theValue1.equalsIgnoreCase(theValue2)){
//			return Similarity.get(1d);
//		} else {
//			switch (theSymbolSimilarityConfig) {
//			case OVERLAP:
//				return Similarity.get(0d);
//			case VDM_N1:
//				aDistance = ComputationVDM.getVDM_N1(
//						itsDiscretizedCaseBase, 
//						desc, theValue1, 
//						theValue2, 
//						itsOutputClass);				
//				break;
//			case VDM_N1_Q2:
//				aDistance = ComputationVDM.getVDM_N1_Q2(
//						itsDiscretizedCaseBase, 
//						desc, 
//						theValue1, 
//						theValue2, 
//						itsOutputClass);
//				break;
//			case VDM_N2:
//				aDistance = ComputationVDM.getVDM_N2(
//						itsDiscretizedCaseBase, 
//						desc, theValue1, 
//						theValue2, 
//						itsOutputClass);
//				break;
//			case VDM_N3:
//				aDistance = ComputationVDM.getVDM_N3(
//						itsDiscretizedCaseBase, 
//						desc, 
//						theValue1, 
//						theValue2, 
//						itsOutputClass);
//				break;
////			case VDM_Q3:
////				aDistance = Computation.getVDM_Q3(
////						itsTrainingCaseBase, 
////						desc, theValue1, 
////						theValue2, 
////						itsOutputClass);
////				break;
//	
//			default:
//				System.err.println("ERROR: Unhandled DiscretizationSymbolConfig: " + theSymbolSimilarityConfig);
//				return Similarity.INVALID_SIM;
//			}
//		}
//		
//		// FIXME Achtung: Ggf. anders berechnen! Theoretisch darf die Distanz > 1 sein...
//		aSimilarity = 1 - aDistance;
//		if (aSimilarity < 0){
//			aSimilarity = 0;
//		}
//		
//		return Similarity.get(aSimilarity);
//	}

	
	
//	/**
//	 * Generates the interval labels (using a prefix) for all discrete intervals.
//	 * 
//	 * @return set of interval labels
//	 */
//	@Override
//	public Set<String> generateIntervalLabels(){
////		HashSet<String> aLabelSet = new HashSet<String>();
////		int i=0;
////		
////		while(i++ < itsNumberOfDiscretIntervalls){
////			aLabelSet.add(INTERVALL_LABEL_PREFIX + new Integer(i).toString());
////		}
//////		System.out.println(aLabelSet);
////		return aLabelSet;
//		
//		return ComputationDiscretize.generateIntervalLabels(itsNumberOfDiscreteIntervals, INTERVAL_LABEL_PREFIX);
//	}
//	
//
//	/**
//	 * Calculates the discretized index of a given value.
//	 * and concatenated a given prefix.
//	 * 
//	 * @param theValue value to discretize
//	 * @return given prefix with concatenated discretized index; an empty string result indicates an error
//	 */
//	@Override
//	public String getDiscretizedIndexAsString(double theValue){
//		return ComputationDiscretize.getDiscretizedIndexAsString(theValue, itsMinValue, itsMaxValue, itsNumberOfDiscreteIntervals, DESC_PREFIX);
//	}
//		
//	
//	/**
//	 * Calculates the discretized index of a given value.
//	 * The minimum index is 1; the maximum index is equal the number of max intervals.  
//	 * 
//	 * @param theValue value to discretize
//	 * @return discretized index; -1 means an error occurred
//	 */
//	@Override
//	public int getDiscretizedIndex(double theValue){	
//		return ComputationDiscretize.getDiscretizedIndex(theValue, itsMinValue, itsMaxValue, itsNumberOfDiscreteIntervals);
//	}
	

//
//	/**
//	 * Sets a new number of intervals and thus recalculates all possible 
//	 * similarities of the discretized values (in the internal Lookup-Table of SymbolFct).
//	 * 
//	 * @param theNumberOfIntervals Number of intervals to set
//	 */
//	public void setNumberOfIntervals(int theNumberOfIntervals) {
//		this.itsNumberOfDiscreteIntervals = theNumberOfIntervals;
//		
//		if (theNumberOfIntervals < MIN_INTERVALS){
//			itsNumberOfDiscreteIntervals = MIN_INTERVALS;
//		}
//		
//		// calculate new interval width
//		itsIntervalWidth = getIntervalWidth(1);
//		
//		recalculateAllDiscretizedSimilarities();
//	}
//
//
//
//	/**
//	 * Gets the current interval width.
//	 * 
//	 * @return the interval width
//	 */
//	public double getIntervalWidth(int theIntervalIndex) {
//		// no interval index needed, all intervals have same length here
//		return itsDiff / itsNumberOfDiscreteIntervals;
//	}
//	
//	
//	/**
//	 * Gets the smallest value of the interval with the given index.
//	 * 
//	 * @param theIntervalIndex interval index
//	 * @return smallest value of the interval
//	 */
//	public double getMinIntervalValue(int theIntervalIndex){
//		// FIXME: Interval min/max values must not contain values from another interval
//		return itsMinValue + theIntervalIndex * itsIntervalWidth;
//	}
//	
//	
//	/**
//	 * Gets the highest value of the interval with the given index.
//	 * 
//	 * @param theIntervalIndex interval index
//	 * @return smallest value of the interval
//	 */
//	public double getMaxIntervalValue(int theIntervalIndex){
//		// FIXME: Interval min/max values must not contain values from another interval
//		return getMinIntervalValue(theIntervalIndex) + itsIntervalWidth;
//	}	

}
