/**
 * (2019) Heidelberg University, Institute of Medical Biometry and Informatics (IMBI), Germany
 * 
 * This file is part of myCbrBuilder.
 *  
 * myCbrBuilder is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * myCbrBuilder is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with vivaGen. If not, see <http://www.gnu.org/licenses/>.
 * 
 **/
package similarity.fct;

import de.dfki.mycbr.core.DefaultCaseBase;
import de.dfki.mycbr.core.Project;
import exceptions.CaseBaseException;
import similarity.computation.config.AbstractSimilarityMeasureConfiguration;
import similarity.desc.ExtendedDoubleDesc;
import similarity.desc.ExtendedIntegerDesc;
import similarity.desc.ExtendedSymbolDesc;


/**
 * Abstract class to handle similarity functions of discretized numeric values. 
 * </p>
 * Here, the discretized values can be put in a Lookup-Table (LUT) in order to 
 * pre-calculate all possible similarities of the possible value pairs.
 * </p>
 * The LUT is implemented as a SymbolFct.
 * 
 * @author Christian Karmen
 *
 */
public abstract class AbstractLookUpDiscretizedNumberFct extends AbstractDiscretizedNumberFct {
	
	protected ExtendedSymbolDesc itsDiscretizedSymbolDesc;
	protected DiscretizedSymbolFct itsDiscretizedSymbolFct;
	
	
	
	/**
	 * Abstract class to handle similarity functions of discretized numeric values.
	 * </p>
	 * Here, the discretized values can be put in a Lookup-Table (LUT) in order to 
	 * pre-calculate all possible similarities of the possible value pairs.
	 * 
	 * @param theProject Project of interest
	 * @param theDesc Numeric AttributeDesc of interest
	 * @param theName Name of this Fct
	 * @param theTrainingCaseBase Training Case Base to learn discretization from 
	 * @param theSimilarityConfig Similarity Measure Configuration Object
	 * @param theNumberOfDiscreteIntervals Number of discretized values of interest
	 */
	public AbstractLookUpDiscretizedNumberFct(
			Project theProject, 
			ExtendedDoubleDesc theDesc, 
			String theName,
			DefaultCaseBase theTrainingCaseBase, 
			AbstractSimilarityMeasureConfiguration theSimilarityConfig,
			int theNumberOfDiscreteIntervals) {
		super(theProject, theDesc, theName, theTrainingCaseBase, theSimilarityConfig, theNumberOfDiscreteIntervals);
		
		init();
	}
	
	/**
	 * Abstract class to handle similarity functions of discretized numeric values.
	 * </p>
	 * Here, the discretized values can be put in a Lookup-Table (LUT) in order to 
	 * pre-calculate all possible similarities of the possible value pairs.
	 * 
	 * @param theProject Project of interest
	 * @param theDesc Numeric AttributeDesc of interest
	 * @param theName Name of this Fct
	 * @param theTrainingCaseBase Training Case Base to learn discretization from 
	 * @param theSimilarityConfig Similarity Measure Configuration Object
	 * @param theNumberOfDiscreteIntervals Number of discretized values of interest
	 */
	public AbstractLookUpDiscretizedNumberFct(
			Project theProject, 
			ExtendedIntegerDesc theDesc, 
			String theName,
			DefaultCaseBase theTrainingCaseBase,
			AbstractSimilarityMeasureConfiguration theSimilarityConfig,
			int theNumberOfDiscreteIntervals) {
		super(theProject, theDesc, theName, theTrainingCaseBase, theSimilarityConfig, theNumberOfDiscreteIntervals);
		
		init();
	}

	
	
	private void init(){		
		// FIXME ggf. (teilweise) nach Klasse AbstractDiscretizedNumberFct verschieben
		
		itsDiscretizedSymbolDesc = createDiscretizedSymbolDesc();		
		itsDiscretizedSymbolFct = itsDiscretizedSymbolDesc.addDiscretizedSymbolFct(name, min, max, true);
	}

	
	
	
	private ExtendedSymbolDesc createDiscretizedSymbolDesc() {
		
		// FIXME ggf. nach init() verschieben
		
		ExtendedSymbolDesc aDiscretizedSymbolDesc;
		String aDescName = DISCRETIZED_TAG + this.getDesc().getName() + "[DESC]";
		
		try {
			aDiscretizedSymbolDesc = new ExtendedSymbolDesc(
					getDiscretizedConcept(), 
					aDescName, 
					generateIntervalLabels());
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}	
		return aDiscretizedSymbolDesc;
	}
		
		
	

	/**
	 * Gets the discretized SymbolFct (LookUp Table)
	 * 
	 * @return discretized SymbolFct (LookUp Table)
	 */
	public DiscretizedSymbolFct getDiscretizedSymbolFct(){
		return itsDiscretizedSymbolFct;
	}
	
	/**
	 * Gets the discretized SymbolDesc of the (LookUp Table)
	 * @return discretized SymbolDesc of the (LookUp Table)
	 */
	public ExtendedSymbolDesc getDiscretizedSymbolDesc() {
		return itsDiscretizedSymbolDesc;
	}
	

//	/**
//	 * Builds a reduced CaseBase ("mini") from the training CaseBase. Only given attributes will be added. 
//	 * 
//	 * @param theTargetConcept Concept to add the CaseBase
//	 * @param theMiniCaseBaseName Name of the new mini CaseBase
//	 * @param theAttributeLabelList List of attribute label that should be added
//	 * @return Reduced (mini) CaseBase
//	 * @throws Exception Error in creating mini CaseBase
//	 */
//	protected DefaultCaseBase buildMiniCaseBase(Concept theTargetConcept, String theMiniCaseBaseName, ArrayList<String> theAttributeLabelList) throws Exception{
//		DefaultCaseBase aMiniCaseBase = new DefaultCaseBase(prj, theMiniCaseBaseName, getTrainingCaseBase().getCases().size());
//		Attribute aTrainingCaseAttribute;
//		AttributeDesc anAttributeDesc;
//		Instance aCaseInstance;
//		boolean aSuccessFlag;
////		String aCaseValue;
//		
//		for (Instance aTrainingCase : getTrainingCaseBase().getCases()) {
//			for (String anAttributeLabel : theAttributeLabelList) {
//				aTrainingCaseAttribute = aTrainingCase.getAttForDesc(MyCbrUtils.getAttributeDescByName(desc.getOwner(), anAttributeLabel));
//				
//				aCaseInstance = new Instance(theTargetConcept, aTrainingCase.getName());
//				anAttributeDesc = MyCbrUtils.getAttributeDescByName(desc.getOwner(), anAttributeLabel);
//				//anAttributeDesc = aTrainingCaseAttribute.
//				
//				aSuccessFlag = aCaseInstance.addAttribute(anAttributeDesc, aTrainingCaseAttribute);
//				
//				if (!aSuccessFlag){
//					System.err.println("ERROR: ERROR AT ADDING ATTRIBUTE TO MINI-CB: " + anAttributeLabel + " = " + aTrainingCaseAttribute.getValueAsString());
//				}
//			}
//		}
//		
//		return aMiniCaseBase;
//	}
	
	
	abstract void setSimilarityMatrix() throws CaseBaseException;
}
