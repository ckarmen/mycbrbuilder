/**
 * (2019) Heidelberg University, Institute of Medical Biometry and Informatics (IMBI), Germany
 * 
 * This file is part of myCbrBuilder.
 *  
 * myCbrBuilder is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * myCbrBuilder is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with vivaGen. If not, see <http://www.gnu.org/licenses/>.
 * 
 **/
package similarity.fct;

import de.dfki.mycbr.core.DefaultCaseBase;
import de.dfki.mycbr.core.Project;
import de.dfki.mycbr.core.model.AttributeDesc;
import de.dfki.mycbr.core.model.DoubleDesc;
import de.dfki.mycbr.core.similarity.Similarity;
import exceptions.CaseBaseException;
import similarity.computation.config.VDMConfig;
import similarity.config.SymbolSimilarityConfig;
import similarity.desc.ExtendedDoubleDesc;
import similarity.desc.ExtendedIntegerDesc;

/**
 * Class to handle similarity functions of discretized numeric values of the WVDM. 
 * </p>
 * IVDM = Windowed Value Difference Metric, Martinez et al, 1997
 * 
 * @author Christian Karmen
 *
 */
public class WVDMDiscretizedFct extends DVDMDiscretizedFct {

	/**
	 * Class to handle similarity functions of discretized numeric values of the WVDM.
	 * </p>
	 * Here, ... TODO
	 * 
	 * @param theProject Project of interest
	 * @param theDesc Numeric AttributeDesc of interest
	 * @param theName Name of this Fct
	 * @param theTrainingCaseBase Training Case Base to learn discretization from 
	 * @param theVDMConfig VDM Configuration Object
	 * @param theNumberOfDiscreteIntervals Number of discretized values of interest
	 * @param theSymbolSimilarityConfig VDM type configuration
	 * @throws CaseBaseException Error while access to case base
	 */
	public WVDMDiscretizedFct(
			Project theProject, 
			ExtendedDoubleDesc theDesc, 
			String theName, 
			DefaultCaseBase theTrainingCaseBase, 
			VDMConfig theVDMConfig,
			int theNumberOfDiscreteIntervals, 
			SymbolSimilarityConfig theSymbolSimilarityConfig) throws CaseBaseException {
		
		super(theProject, theDesc, theName, theTrainingCaseBase, theVDMConfig, theNumberOfDiscreteIntervals, theSymbolSimilarityConfig);
		
		init();
	}
	
	
	/**
	 * Class to handle similarity functions of discretized numeric values of the WVDM.
	 * </p>
	 * Here, ... TODO
	 * 
	 * @param theProject Project of interest
	 * @param theDesc Numeric AttributeDesc of interest
	 * @param theName Name of this Fct
	 * @param theTrainingCaseBase Training Case Base to learn discretization from 
	 * @param theVDMConfig VDM Configuration Object
	 * @param theNumberOfDiscreteIntervals Number of discretized values of interest
	 * @param theSymbolSimilarityConfig VDM type configuration
	 * @throws CaseBaseException Error while access to case base
	 */
	public WVDMDiscretizedFct(
			Project theProject, 
			ExtendedIntegerDesc theDesc, 
			String theName, 
			DefaultCaseBase theTrainingCaseBase,
			VDMConfig theVDMConfig,
			int theNumberOfDiscreteIntervals, 
			SymbolSimilarityConfig theSymbolSimilarityConfig) throws CaseBaseException {
		
		super(theProject, theDesc, theName, theTrainingCaseBase, theVDMConfig, theNumberOfDiscreteIntervals, theSymbolSimilarityConfig);
		
		init();
	}

	
	private void init(){
//		MyCbrUtils.printTitle("Processing WVDM of attribute '" + desc.getName() + "'");
	}

	
	@Override
	public Similarity calculateDiscretizedSimilarity(double theValue1, double theValue2) {
		
		//double aPacx, aPacy;
		double aDistance = 0d;
		
		// TODO WVDM noch nicht implementiert!
		
/*		
		for (String anOutputValue : itsOutputClass.getAllowedValues()) {
			//aPacx = getPac(theValue1, anOutputValue);
			//aPacx = Computation.getPaxc(theCaseBase, theInputDesc, theInputValue, theOutputDesc, theOutputClassValue);
			aPacx = Computation.getPaxc(itsCaseBase, theInputDesc, theInputValue, itsOutputClass, anOutputValue);
			
			aPacy = getPac(theValue2, anOutputValue);
			
			aDistance += Math.pow(Math.abs(aPacx - aPacy) , 2);
		}
*/		
		// FIXME Achtung: Ggf. anders berechnen! Theoretisch darf die Distanz > 1 sein...
		double aSimilarityValue = 1 - aDistance;
		if (aSimilarityValue < 0){
			aSimilarityValue = 0;
		}
		
		return Similarity.get(aSimilarityValue);
	}
	
	
	
	
	@Override
	public void clone(AttributeDesc descNEW, boolean active) {
		if (descNEW instanceof DoubleDesc && !name.equals(Project.DEFAULT_FCT_NAME)) {
//			LinearDiscretizedFct f = ((LinearContinuousDesc)descNEW).addDVDMFct(name, active, itsTrainingCaseBase, itsOutputClass);
			
			// FIXME reicht das?? Was ist mit LinearDiscreteDesc??
			WVDMDiscretizedFct f = null;
			try {
				f = ((ExtendedDoubleDesc)descNEW).addWVDMFct(name, active, itsTrainingCaseBase, (VDMConfig)itsSimilarityConfig, itsDiscretizationSymbolConfig);
			} catch (CaseBaseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}			
			
			f.isSymmetric = this.isSymmetric;
			f.mc = this.mc;
			f.distanceFunction = this.distanceFunction;
			
//			f.setDiscretizationConfig(itsDiscretizationConfig);
			f.setDiscretizationSymbolConfig(itsDiscretizationSymbolConfig);
//			f.setMeanValue(itsMeanValue);
//			f.setStandardDeviation(itsStandardDeviation);
			
			
			// TODO some more stuff?!
			f.itsTrainingCaseBase = itsTrainingCaseBase;
		}
	}


}
