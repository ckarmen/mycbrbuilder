/**
 * (2019) Heidelberg University, Institute of Medical Biometry and Informatics (IMBI), Germany
 * 
 * This file is part of myCbrBuilder.
 *  
 * myCbrBuilder is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * myCbrBuilder is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with vivaGen. If not, see <http://www.gnu.org/licenses/>.
 * 
 **/
package similarity.config;

/**
 * Interface for a Similarity Measure Configuration Enum.
 * 
 * @author Christian Karmen
 *
 */
public interface ISimilarityConfig {
	/** Gets the numeric (enum) value for the configuration.
	 * 
	 * @return numeric (enum) value for the configuration
	 */
	int getValue();
	
	/**
	 * Gets the nominal value for the configuration.
	 * 
	 * @return nominal value for the configuration
	 */
	String getName();
}
