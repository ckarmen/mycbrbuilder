/**
 * (2019) Heidelberg University, Institute of Medical Biometry and Informatics (IMBI), Germany
 * 
 * This file is part of myCbrBuilder.
 *  
 * myCbrBuilder is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * myCbrBuilder is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with vivaGen. If not, see <http://www.gnu.org/licenses/>.
 * 
 **/
package similarity.config;

/**
 * Defined list of similarity measures.
 * 
 * @author Christian Karmen
 *
 */
public enum SimilarityMeasure implements ISimilarityConfig {
	/**
	 * No similarity measure
	 */
	NONE,
	
	/**
	 * Random Case Measure (for validation purpose only) 
	 * 
	 */
	RANDOM,
	
	/**
	 * Heterogeneous Euclidean-Overlap Metric (HEOM)
	 */
	HEOM, 
	
	/**
	 * Heterogeneous Value Difference Metric, N1, Martinez et al, 1997
	 */
	HVDM1, 
	
	/**
	 * Heterogeneous Value Difference Metric, N2, Martinez et al, 1997
	 */
	HVDM2, 
	
	/**
	 * Heterogeneous Value Difference Metric, N3, Martinez et al, 1997
	 */
	HVDM3, 
	
	/**
	 * Interpolated Heterogeneous Value Difference Metric, Martinez et al, 1997
	 */
	IVDM, 
	
	/**
	 * Discretized Heterogeneous Value Difference Metric, Martinez et al, 1997
	 */
	DVDM, 
	
	/**
	 * Windowed Heterogeneous Value Difference Metric, Martinez et al, 1997
	 */
	WVDM, 
	
	/**
	 * Kaplan-Meier based Similarity Measure, Karmen et al, 2016
	 */
	KAPLANMEIER;
	
	  private final int value;

	  SimilarityMeasure() {
	    value = ordinal();
	  }
	
	@Override
	  public int getValue() {
	    return value;
	  }
	
	@Override
	public String getName() {
		return this.name();
	}
}
