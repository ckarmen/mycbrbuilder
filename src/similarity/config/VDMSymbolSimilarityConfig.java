/**
 * (2019) Heidelberg University, Institute of Medical Biometry and Informatics (IMBI), Germany
 * 
 * This file is part of myCbrBuilder.
 *  
 * myCbrBuilder is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * myCbrBuilder is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with vivaGen. If not, see <http://www.gnu.org/licenses/>.
 * 
 **/
package similarity.config;

/**
 * Enumeration for allowed VDM similarity measures types. 
 * 
 *	@author Christian Karmen
 */
public enum VDMSymbolSimilarityConfig implements ISimilarityConfig{
	/**
	 * VDM with N=1
	 */
	VDM_N1, 
	
	/**
	 * VDM with N=1
	 */
	VDM_N2, 
	
	/**
	 * VDM with N=2
	 */
	VDM_N3, 
	
	/**
	 * VDM with Q=1
	 */
	VDM_Q1, 
	
	/**
	 * VDM with N=1 and Q=2
	 */
	VDM_N1_Q2, 
	
	/**
	 * VDM with Q=3
	 */
	VDM_Q3;
	
	  private final int value;

	  VDMSymbolSimilarityConfig() {
	    value = ordinal();
	  }
	
	@Override
	  public int getValue() {
	    return value;
	  }
	
	@Override
	public String getName() {
		return this.name();
	}
}
