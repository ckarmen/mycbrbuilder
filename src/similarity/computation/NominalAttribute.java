/**
 * (2019) Heidelberg University, Institute of Medical Biometry and Informatics (IMBI), Germany
 * 
 * This file is part of myCbrBuilder.
 *  
 * myCbrBuilder is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * myCbrBuilder is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with vivaGen. If not, see <http://www.gnu.org/licenses/>.
 * 
 **/
package similarity.computation;

/**
 * Class to store a key-value pair as a data point for Kaplan-Meier diagrams.
 * 
 * @author Christian Karmen
 *
 */
public class NominalAttribute {
	
	/**
	 * Key of the Attribute.
	 */
	public String KEY;
	
	/**
	 * Value of the nominal Attribute.
	 */
	public String VALUE;
	
	/**
	 * Negation of value representation. Application of logical NOT: meaning all others.
	 */
	public boolean CRITERION;
	
	/**
	 * CONTRUCTOR
	 * 
	 * @param theAttributeLabel attribute label
	 * @param theNominalAttributeValue attribute value
	 * @param isParameterCriterion true=no negation of value; false=negation of value
	 */
	public NominalAttribute(String theAttributeLabel, String theNominalAttributeValue, boolean isParameterCriterion){
		KEY = theAttributeLabel;
		VALUE = theNominalAttributeValue;
		CRITERION = isParameterCriterion;
	}

}
