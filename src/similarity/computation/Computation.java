/**
 * (2019) Heidelberg University, Institute of Medical Biometry and Informatics (IMBI), Germany
 * 
 * This file is part of myCbrBuilder.
 *  
 * myCbrBuilder is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * myCbrBuilder is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with vivaGen. If not, see <http://www.gnu.org/licenses/>.
 * 
 **/
package similarity.computation;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Random;

import org.apache.commons.math3.distribution.TDistribution;
import org.apache.commons.math3.exception.MathIllegalArgumentException;
import org.cliommics.vivagen.Instances;

import core.CaseBaseUtils;
import core.MyCbrUtils;
import de.dfki.mycbr.core.DefaultCaseBase;
import de.dfki.mycbr.core.casebase.Attribute;
import de.dfki.mycbr.core.model.AttributeDesc;
import de.dfki.mycbr.core.model.Concept;
import de.dfki.mycbr.core.model.DoubleDesc;
import de.dfki.mycbr.core.model.IntegerDesc;
import de.dfki.mycbr.core.model.SimpleAttDesc;
import de.dfki.mycbr.core.model.SymbolDesc;
import de.dfki.mycbr.core.similarity.AmalgamationFct;
import de.dfki.mycbr.core.similarity.SymbolFct;
import de.dfki.mycbr.core.similarity.config.AmalgamationConfig;
import exceptions.CaseBaseException;
import exceptions.SimilarityMeasureException;
import similarity.computation.config.AbstractSimilarityMeasureConfiguration;
import similarity.computation.config.KaplanMeierConfig;
import similarity.computation.config.VDMConfig;
import similarity.config.DiscretizationConfig;
import similarity.config.ISimilarityConfig;
import similarity.config.NormalizationConfig;
import similarity.config.SimilarityMeasure;
import similarity.config.SymbolSimilarityConfig;
import similarity.desc.ExtendedDoubleDesc;
import similarity.desc.ExtendedIntegerDesc;
import similarity.fct.AbstractDiscretizedNumberFct;
import similarity.fct.NormalizedNumberFct;
import similarity.fct.RandomNumberFct;
import similarity.fct.SurvivalCutOffDiscretizedFct;


/**
 * General Helper Methods for Attribute based calculations for myCBR. 
 * 
 * @author Christian Karmen
 *
 */
public class Computation {
	
//	/**
//	 * Creates a specific Amalgam Function for testing purposes.
//	 * 
//	 * @param theConcept
//	 * @param theCaseDescList
//	 * @param theCaseSolutionList
//	 * @param theIntervallValue
//	 * @param theRandomWeightFlag
//	 */
//	@Deprecated
//	public static void generateAmalgamFct_TestMe(Concept theConcept, ArrayList<String> theCaseDescList, ArrayList<String> theCaseSolutionList, double theIntervallValue, boolean theRandomWeightFlag){
//		
//		AmalgamationFct anAmalgamFct = theConcept.addAmalgamationFct(AmalgamationConfig.WEIGHTED_SUM, "SimpleIntervallFunc", true);
//
//		SymbolDesc aSymbolDesc;
//		SymbolFct aSymbolFct;
//		
//		IntegerDesc anIntegerDesc;
//		IntegerFct anIntegerFct;
//		
//		FloatDesc aFloatDesc;
//		FloatFct aFloatFct;
//		
//		AttributeDesc anAttributeDesc;
//		
//		double aWeight, aSimValue;
//		
//		String aGeneratedLabel;
//		
//		// set discriminants for amalgam function
//		MyCbrUtils.setAttributeActivity(theConcept, anAmalgamFct, theCaseDescList, true);
//		MyCbrUtils.setAttributeActivity(theConcept, anAmalgamFct, theCaseSolutionList, false);
//		
//		for (String aCaseDescLabel : theCaseDescList) {
//			anAttributeDesc = MyCbrUtils.getAttributeDescByName(theConcept, aCaseDescLabel);
//			//anAttributeDesc = itsCsvConcept.getAttributeDescs().get(aCaseDescLabel);
//			
//			// set default weight
//			aWeight = 1;
//			
//			// set default label
//			aGeneratedLabel = MyCbrUtils.generateFctName(aCaseDescLabel);
//			
//			if (anAttributeDesc instanceof SymbolDesc){
//				aSymbolDesc = (SymbolDesc)anAttributeDesc;
//				aSymbolFct = aSymbolDesc.addSymbolFct(aGeneratedLabel, true);
//				aSymbolFct.setSymmetric(false);
//
//				for (String aValidValue1 : aSymbolDesc.getAllowedValues()) {
//					for (String aValidValue2 : aSymbolDesc.getAllowedValues()) {
//						if (aValidValue1.equals(aValidValue2)){
//							aSimValue = 1.0;
//						} else {
//							aSimValue = 0.0;
//						}
//						aSymbolFct.setSimilarity(aValidValue1, aValidValue2, aSimValue);
//					}
//				}
//				
//				anAmalgamFct.setActiveFct(aSymbolDesc, aSymbolFct);
//			}
//			
//			else if (anAttributeDesc instanceof IntegerDesc){
//				anIntegerDesc = (IntegerDesc)anAttributeDesc;
//				anIntegerFct = anIntegerDesc.addIntegerFct(aGeneratedLabel, true);
//				anIntegerFct.setFunctionTypeL(NumberConfig.SMOOTH_STEP_AT);
//				anIntegerFct.setFunctionTypeR(NumberConfig.SMOOTH_STEP_AT);
//				anIntegerFct.setFunctionParameterL(theIntervallValue);
//				anIntegerFct.setFunctionParameterR(theIntervallValue);
//				anIntegerFct.setSymmetric(false);
//				
//				anAmalgamFct.setActiveFct(anIntegerDesc, anIntegerFct);
//			}
//			
//			else if (anAttributeDesc instanceof FloatDesc){
//				aFloatDesc = (FloatDesc)anAttributeDesc;
//				aFloatFct = aFloatDesc.addFloatFct(MyCbrUtils.generateFctName(aCaseDescLabel), true);
//				aFloatFct.setFunctionTypeL(NumberConfig.SMOOTH_STEP_AT);
//				aFloatFct.setFunctionTypeR(NumberConfig.SMOOTH_STEP_AT);
//				aFloatFct.setFunctionParameterL(theIntervallValue);
//				aFloatFct.setFunctionParameterR(theIntervallValue);
//				aFloatFct.setSymmetric(false);
//				
////				aFloatFct.setDistanceFct();
//				//aFloatFct.setMaxForQuotient(10);
//				
//				anAmalgamFct.setActiveFct(aFloatDesc, aFloatFct);
//			} 
//			
//			else{
//				System.err.println("ERROR: Unhandled Type of AttributeDesc: " + anAttributeDesc);
//				anAmalgamFct.setWeight(anAttributeDesc,0);
//				
//				continue;
//			}
//			
//			if (theRandomWeightFlag){
//				anAmalgamFct.setWeight(anAttributeDesc, getRandomNumber(10));
//			} else{
//				anAmalgamFct.setWeight(anAttributeDesc, aWeight);	
//			}
//
//		}
//	}
	
	


	
	
	/***
	 * Creates a specific Amalgam Function with given parameters.
	 * 
	 * @param anAmalgamFctLabel	Label of AmalgamFct
	 * @param theConcept Concept of interest
	 * @param theCaseDescList	List of attributes that describe the case
	 * @param theCaseSolutionList	List of attributes that describe the solution
	 * @param theNumericSimilarityConfig	The similarity strategy for numeric values 
	 * @param theSymbolSimilarityConfig	The similarity strategy for nominal values
	 * @param theTrainingCaseBase	The Training case base for normalization purposes
	 * @param theAmalgamationConfig	The global similarity calculation 
	 * @param theSimilaritySpecificConfig A similarity measure specific configuration object
	 *  
	 * @throws CaseBaseException Error while access the case base
	 * @throws SimilarityMeasureException Error while preparing similarity measure 
	 */
	protected static void generateAmalgamFct(
			String anAmalgamFctLabel,
			Concept theConcept,
			ArrayList<String> theCaseDescList, 
			ArrayList<String> theCaseSolutionList, 
			ISimilarityConfig theNumericSimilarityConfig, 
			SymbolSimilarityConfig theSymbolSimilarityConfig,
			DefaultCaseBase theTrainingCaseBase,
			AmalgamationConfig theAmalgamationConfig,
			AbstractSimilarityMeasureConfiguration theSimilaritySpecificConfig) throws CaseBaseException, SimilarityMeasureException{
		
		long startTime = System.currentTimeMillis();
		
		AmalgamationFct anAmalgamFct = theConcept.addAmalgamationFct(theAmalgamationConfig, anAmalgamFctLabel, true);
		
		ExtendedDoubleDesc anExtendedDoubleDesc;
		ExtendedIntegerDesc  anExtendedIntegerDesc;
		NormalizedNumberFct aNormalizedNumberFct;
		RandomNumberFct aRandomNumberFct;
		AbstractDiscretizedNumberFct aDiscretizedNumberFct = null; 
		
		SymbolDesc aSymbolDesc;
		SymbolFct aSymbolFct;		
		AttributeDesc anAttributeDesc;
		
		double aWeight;
		String aGeneratedLabel, aFctLabel;
		
		// set discriminants for amalgam function
		MyCbrUtils.setAttributeActivity(theConcept, anAmalgamFct, theCaseDescList, true);
		MyCbrUtils.setAttributeActivity(theConcept, anAmalgamFct, theCaseSolutionList, false);
		
		for (String aCaseDescLabel : theCaseDescList) {
			anAttributeDesc = MyCbrUtils.getAttributeDescByName(theConcept, aCaseDescLabel);
			//anAttributeDesc = itsCsvConcept.getAttributeDescs().get(aCaseDescLabel);
			
			// set default weight
			aWeight = 1;
	
			// set default label
			aGeneratedLabel = MyCbrUtils.generateFctName(aCaseDescLabel);
			
			// set function label
			aFctLabel = theNumericSimilarityConfig.getName() + "_"+ aGeneratedLabel;
			
//			System.out.println(anAttributeDesc.getName() + ": " + anAttributeDesc.getClass());
			
			
			// nominal attributes (Symbols) 
			if (anAttributeDesc instanceof SymbolDesc){
				 aSymbolDesc = (SymbolDesc)anAttributeDesc;
				 
				 // FIXME Prefix notwendig??
				 aSymbolFct = aSymbolDesc.addSymbolFct(theSymbolSimilarityConfig.name() + "_" + aGeneratedLabel, true);
//				 aSymbolFct = aSymbolDesc.addSymbolFct(aFctLabel, true);
//				 aSymbolFct = aSymbolDesc.addSymbolFct(aGeneratedLabel, true);
				 
				 switch (theSymbolSimilarityConfig) {
				 	case RANDOM:
				 		setSimilarityMatrix_RANDOM(aSymbolFct);
				 		break;
					case OVERLAP:
						setSimilarityMatrix_OVERLAP(aSymbolFct);
						break;
					case VDM_N1:
					case VDM_N2:
					case VDM_N3:
					case VDM_N1_Q2:
						ComputationVDM.setSimilarityMatrix_VDM(aSymbolFct, theSymbolSimilarityConfig, theTrainingCaseBase, (VDMConfig)theSimilaritySpecificConfig);
						break;
					case KAPLANMEIER:
						Instances aWekaCaseInstances = ComputationKM.getWekaInstancesFromCaseBase(theTrainingCaseBase, ((KaplanMeierConfig)theSimilaritySpecificConfig).KMTargetConfig);
						ComputationKM.setSimilarityMatrix_KM(aWekaCaseInstances, (KaplanMeierConfig)theSimilaritySpecificConfig, aSymbolFct);
						aWeight = ComputationKM.calculateImpactKM(
								aWekaCaseInstances, 
								((KaplanMeierConfig)theSimilaritySpecificConfig).StepShapeFlag, 
								((KaplanMeierConfig)theSimilaritySpecificConfig).KMTargetConfig, 
								 anAttributeDesc.getName());
						break;
	
					default:
						throw new SimilarityMeasureException("Unsupported SymbolSimilarityConfig: " + theSymbolSimilarityConfig.getName());
						//break;
					}
				 
				 anAmalgamFct.setActiveFct(aSymbolDesc, aSymbolFct);
			}
			
			/////////////////////////// decimal attributes (Double) ///////////////////////////
			else if (anAttributeDesc instanceof ExtendedDoubleDesc){
				 anExtendedDoubleDesc = (ExtendedDoubleDesc)anAttributeDesc;
				 
				 setStatistics(anExtendedDoubleDesc);
//				 anExtendedDoubleDesc.setMeanValue(getMeanValue(anExtendedDoubleDesc));
//				 anExtendedDoubleDesc.setMedianValue(getMedianValue(anExtendedDoubleDesc));
//				 anExtendedDoubleDesc.setStandardDeviation(getStandardDeviation(anExtendedDoubleDesc));
				 
				// Discretization
				 if (theNumericSimilarityConfig instanceof DiscretizationConfig){					 
					switch ((DiscretizationConfig)theNumericSimilarityConfig) {
					case DVDM:
						 aDiscretizedNumberFct = anExtendedDoubleDesc.addDVDMFct(
								 aFctLabel, 
								 true, 
								 theTrainingCaseBase, 
								 (VDMConfig)theSimilaritySpecificConfig, 
								 theSymbolSimilarityConfig);
						break;
					case IVDM:
						 aDiscretizedNumberFct = anExtendedDoubleDesc.addIVDMFct(
								 aFctLabel, 
								 true, 
								 theTrainingCaseBase, 
								 (VDMConfig)theSimilaritySpecificConfig, 
								 theSymbolSimilarityConfig);
						break;
					case WVDM:
						 aDiscretizedNumberFct = anExtendedDoubleDesc.addWVDMFct(
								 aFctLabel, 
								 true, 
								 theTrainingCaseBase, 
								 (VDMConfig)theSimilaritySpecificConfig, 
								 theSymbolSimilarityConfig);
						break;
					case SURVIVAL_CUTOFF:
						 aDiscretizedNumberFct = anExtendedDoubleDesc.addSurvivalCutOffDiscretizedFct(
								 aFctLabel, 
								 true, 
								 theTrainingCaseBase, 
								 (KaplanMeierConfig)theSimilaritySpecificConfig);
						 // FIXME ggf. die gewichtung aus fct heraus bestimmen 
						 SurvivalCutOffDiscretizedFct aFct = (SurvivalCutOffDiscretizedFct)aDiscretizedNumberFct;
						 aWeight = aFct.getWeight();
						break;

					default:
						throw new SimilarityMeasureException("Unhandled DiscretizationConfig: " + (DiscretizationConfig)theNumericSimilarityConfig);
					}
					anAmalgamFct.setActiveFct(anExtendedDoubleDesc, aDiscretizedNumberFct);
					 
				 // Normalization
				 } else if (theNumericSimilarityConfig instanceof NormalizationConfig){
					 aNormalizedNumberFct = anExtendedDoubleDesc.addNormalizedNumberFct(aFctLabel, true, (NormalizationConfig)theNumericSimilarityConfig);
//					 aNormalizedNumberFct.setNormalizationConfig((NormalizationConfig)theNumericSimilarityConfig);
					 
					 anAmalgamFct.setActiveFct(anExtendedDoubleDesc, aNormalizedNumberFct);
				 // Randomize
				 } else if (theNumericSimilarityConfig instanceof SimilarityMeasure && 
						 ((SimilarityMeasure)theNumericSimilarityConfig) == SimilarityMeasure.RANDOM){
					 aRandomNumberFct = anExtendedDoubleDesc.addRandomNumberFct(aFctLabel, true);
					 anAmalgamFct.setActiveFct(anExtendedDoubleDesc, aRandomNumberFct);
				 } else {
					 throw new SimilarityMeasureException("Unhandled NumericSimilarityConfig: " + theNumericSimilarityConfig);
				 } 				 
			}		
			
			/////////////////////////// integer attributes (Integer) ///////////////////////////
			else if (anAttributeDesc instanceof ExtendedIntegerDesc){
				 anExtendedIntegerDesc = (ExtendedIntegerDesc)anAttributeDesc;
				 
				 setStatistics(anExtendedIntegerDesc);
//				 anExtendedIntegerDesc.setMeanValue(getMeanValue(anExtendedIntegerDesc));
//				 anExtendedIntegerDesc.setMedianValue(getMedianValue(anExtendedIntegerDesc));
//				 anExtendedIntegerDesc.setStandardDeviation(getStandardDeviation(anExtendedIntegerDesc));
				 
				 // Discretization
				 if (theNumericSimilarityConfig instanceof DiscretizationConfig){
					 switch ((DiscretizationConfig)theNumericSimilarityConfig) {
					case DVDM:
						 aDiscretizedNumberFct = anExtendedIntegerDesc.addDVDMFct(
								 aFctLabel, 
								 true, 
								 theTrainingCaseBase, 
								 (VDMConfig)theSimilaritySpecificConfig, 
								 theSymbolSimilarityConfig);
						break;
					case IVDM:
						 aDiscretizedNumberFct = anExtendedIntegerDesc.addIVDMFct(
								 aFctLabel, 
								 true, 
								 theTrainingCaseBase, 
								 (VDMConfig)theSimilaritySpecificConfig, 
								 theSymbolSimilarityConfig);
						break;
					case WVDM:
						 aDiscretizedNumberFct = anExtendedIntegerDesc.addWVDMFct(
								 aFctLabel, 
								 true, 
								 theTrainingCaseBase, 
								 (VDMConfig)theSimilaritySpecificConfig, 
								 theSymbolSimilarityConfig);
						break;
						
					case SURVIVAL_CUTOFF:
						 aDiscretizedNumberFct = anExtendedIntegerDesc.addSurvivalCutOffDiscretizedFct(
								 aFctLabel, 
								 true, 
								 theTrainingCaseBase, 
								 (KaplanMeierConfig)theSimilaritySpecificConfig
								 );
						 // FIXME ggf. die gewichtung aus fct heraus bestimmen
						 SurvivalCutOffDiscretizedFct aFct = (SurvivalCutOffDiscretizedFct)aDiscretizedNumberFct;
						 aWeight = aFct.getWeight();
						break;

					default:
						throw new SimilarityMeasureException("Unhandled NumericSimilarityConfig: " + theNumericSimilarityConfig);
					}
					 
				 anAmalgamFct.setActiveFct(anExtendedIntegerDesc, aDiscretizedNumberFct);
					 
				 // Normalization
				 } else if (theNumericSimilarityConfig instanceof NormalizationConfig){
					 aNormalizedNumberFct = anExtendedIntegerDesc.addNormalizedNumberFct(aFctLabel, true, (NormalizationConfig)theNumericSimilarityConfig);
//					 aNormalizedNumberFct.setNormalizationConfig((NormalizationConfig)theNumericSimilarityConfig);
					 
					 anAmalgamFct.setActiveFct(anExtendedIntegerDesc, aNormalizedNumberFct);
				 // Randomize
				 } else if (theNumericSimilarityConfig instanceof SimilarityMeasure && 
						 ((SimilarityMeasure)theNumericSimilarityConfig) == SimilarityMeasure.RANDOM){
					 aRandomNumberFct = anExtendedIntegerDesc.addRandomNumberFct(aFctLabel, true);
					 anAmalgamFct.setActiveFct(anExtendedIntegerDesc, aRandomNumberFct);
				 } else {
					 throw new SimilarityMeasureException("Unhandled NumericSimilarityConfig: " + theNumericSimilarityConfig);
				 } 
				  
			} else{
				throw new SimilarityMeasureException("Unhandled Type of AttributeDesc: " + anAttributeDesc);
			}
			
			// no weight influence (weight=1)
			anAmalgamFct.setWeight(anAttributeDesc, aWeight);
		}
		
		long aTimeInMS = System.currentTimeMillis() - startTime;
        System.out.println("\nDEBUG: generateAmalgamFct() time consumption = " + aTimeInMS + "ms");
	}
	
	
	
	/**
	 * Computes all available statistical values for the given numeric attribute.  
	 * 
	 * @param theExtendedDoubleDesc Attribute of interest
	 */
	public static void setStatistics(ExtendedDoubleDesc theExtendedDoubleDesc){
		theExtendedDoubleDesc.setMin(getMin(theExtendedDoubleDesc));
		theExtendedDoubleDesc.setMax(getMax(theExtendedDoubleDesc));
		theExtendedDoubleDesc.setMeanValue(getMeanValue(theExtendedDoubleDesc));
		theExtendedDoubleDesc.setMedianValue(getMedianValue(theExtendedDoubleDesc));
		theExtendedDoubleDesc.setStandardDeviation(getStandardDeviation(theExtendedDoubleDesc));		
	}
	
	
	/**
	 * Computes all available statistical values for the given numeric attribute.
	 * 
	 * @param theExtendedIntegerDesc Attribute of interest
	 */
	public static void setStatistics(ExtendedIntegerDesc theExtendedIntegerDesc){
		theExtendedIntegerDesc.setMin(new Double(getMin(theExtendedIntegerDesc)).intValue());
		theExtendedIntegerDesc.setMax(new Double(getMax(theExtendedIntegerDesc)).intValue());
		theExtendedIntegerDesc.setMeanValue(getMeanValue(theExtendedIntegerDesc));
		theExtendedIntegerDesc.setMedianValue(getMedianValue(theExtendedIntegerDesc));
		theExtendedIntegerDesc.setStandardDeviation(getStandardDeviation(theExtendedIntegerDesc));
	}
	
	
	/**
	 * Generates an Amalgam function (global similarity function) for the HEOM metric.
	 * 
	 * @param theConcept Concept of interest
	 * @param theCaseDescList	case description list
	 * @param theCaseSolutionList case solution list
	 * @throws CaseBaseException Error while access the case base
	 * @throws SimilarityMeasureException Error while preparing similarity measure
	 */
	public static void generateAmalgamFct_HEOM(
			Concept theConcept, 
			ArrayList<String> theCaseDescList, 
			ArrayList<String> theCaseSolutionList) 
					throws CaseBaseException, SimilarityMeasureException{
		
		generateAmalgamFct(
				"HEOM", 
				theConcept, 
				theCaseDescList, 
				theCaseSolutionList, 
				NormalizationConfig.RANGE, 
//				NormalizationConfig.FOUR_SIGMA,
//				NormalizationConfig.SIGMA,
//				NormalizationConfig.MEAN,
				SymbolSimilarityConfig.OVERLAP, 
				null, 
				AmalgamationConfig.EUCLIDEAN, 
				null);
	}

	
	/**
	 * Generates an Amalgam function (global similarity function) for a RANDOM metric.
	 * CAUTION: Only for validation purposes!
	 * 
	 * @param theConcept Concept of interest
	 * @param theCaseDescList	case description list
	 * @param theCaseSolutionList case solution list
	 * @throws CaseBaseException Error while access the case base
	 * @throws SimilarityMeasureException Error while preparing similarity measure
	 */
	public static void generateAmalgamFct_RANDOM(
			Concept theConcept, 
			ArrayList<String> theCaseDescList, 
			ArrayList<String> theCaseSolutionList) 
					throws CaseBaseException, SimilarityMeasureException{	
		
		generateAmalgamFct(
				"RANDOM", 
				theConcept, 
				theCaseDescList, 
				theCaseSolutionList, 
				SimilarityMeasure.RANDOM,
				SymbolSimilarityConfig.RANDOM, 
				null, 
				AmalgamationConfig.EUCLIDEAN, 
				null);
	}
	
	
	/**
	 * Generates a random value between zero and a given value. 
	 * If upper limit is set to negative value, the random number will be negative as well.
	 * 
	 * @param theUpperLimit the maximum random number
	 * @return random value between zero and a given value
	 */
	public static double getRandomNumber(int theUpperLimit){		
		// random number between 0 and upper limit
		double aRandomNumber = (new Random()).nextDouble() * theUpperLimit;
		
		return aRandomNumber;
	}
		
	
	
	/**
	 * Counts the unique values in a given attribute description. 
	 * 
	 * @param theAttributeDesc attribute description of interest
	 * @return number of unique values
	 * @throws CaseBaseException 
	 */
	public static int getNumberOfUniqueValues(SimpleAttDesc theAttributeDesc) throws CaseBaseException{
		int aNumberOfUniqueValues;
		
		// in case of a categorial attribute
		if (theAttributeDesc instanceof SymbolDesc){
			ArrayList<Attribute> aListOfAllAttributes = CaseBaseUtils.getAllAttributes(theAttributeDesc, false);
			aNumberOfUniqueValues = new HashSet<Attribute>(aListOfAllAttributes).size();
		} 
		
		// in case of a numeric attribute
		else if (theAttributeDesc instanceof DoubleDesc || theAttributeDesc instanceof IntegerDesc){			
			ArrayList<Double> aListOfAllAttributes = CaseBaseUtils.getAllCaseBaseAttributesAsDouble(theAttributeDesc);
			aNumberOfUniqueValues = new HashSet<Double>(aListOfAllAttributes).size();
		} else {
			throw new CaseBaseException("Unhandled attribute desc: " + theAttributeDesc.getName());
		}
		return aNumberOfUniqueValues;
	}
		
	
	/**
	 * Calculates the mean value for the given attribute description.
	 * 
	 * @param theAttributeDesc attribute description
	 * @return mean value
	 * @throws NumberFormatException error occurred while getting attribute values
	 */
	public static double getMeanValue(SimpleAttDesc theAttributeDesc) throws NumberFormatException{
		return getMeanValue(CaseBaseUtils.getAllCaseBaseAttributesAsDouble(theAttributeDesc));
	}
	
	
	/**
	 * Calculates the mean value for the given value list.
	 * 
	 * @param theValueList list of values
	 * @return mean value
	 * @throws NumberFormatException error occurred while getting attribute values
	 */
	public static double getMeanValue(ArrayList<Double> theValueList) throws NumberFormatException{
		double aSum = 0; 
		
		int numberOfValues = theValueList.size();
		
		for (Double aCurrentValue : theValueList) {
			aSum += aCurrentValue;
		}
		
		return aSum / numberOfValues;
	}
	
	
	/**
	 * Calculates the confidence interval (CI) for the given attribute description.
	 * 
	 * @param theAttributeDesc attribute description
	 * @param theConfidenceLevel conficence level between 0.0 and 1.0
	 * @return confidence interval
	 * @throws NumberFormatException error occurred while getting attribute values
	 */
	public static double getConfidenceInterval(SimpleAttDesc theAttributeDesc, double theConfidenceLevel) throws NumberFormatException{	
		return getConfidenceInterval(CaseBaseUtils.getAllCaseBaseAttributesAsDouble(theAttributeDesc), theConfidenceLevel);
	}
	
	
	/**
	 * Calculates the confidence interval (CI) for the given list of values. 
	 * It uses the t-interval for a population mean.
	 * 
	 * @param theValueList list of values
	 * @param theConfidenceLevel confidence level between 0.0 and 1.0 (e.g. 0.95)
	 * @return confidence interval
	 * @throws NumberFormatException error occurred while getting attribute values
	 */
	public static double getConfidenceInterval(ArrayList<Double> theValueList, double theConfidenceLevel) throws NumberFormatException{		
		int aNumberOfValues = theValueList.size();
		double aStandardDeviation = getStandardDeviation(theValueList); 
		
        try {
            // Create T Distribution with N-1 degrees of freedom
            TDistribution tDist = new TDistribution(aNumberOfValues - 1);
            // Calculate critical value
            double critVal = tDist.inverseCumulativeProbability(1.0 - (1 - theConfidenceLevel) / 2);
            // Calculate confidence interval
            return critVal * aStandardDeviation / Math.sqrt(aNumberOfValues);
        } catch (MathIllegalArgumentException e) {
            return Double.NaN;
        }
		
	}

	
	
	
	/**
	 * Determines the minimum value for the given numeric attribute description.
	 * 
	 * @param theAttributeDesc attribute description
	 * @return min value
	 */
	public static double getMin(SimpleAttDesc theAttributeDesc){
		double aMin = Double.POSITIVE_INFINITY; 
		
		ArrayList<Double> aValueList = CaseBaseUtils.getAllCaseBaseAttributesAsDouble(theAttributeDesc);
		
		if (aValueList.size() == 0){
			return Double.NEGATIVE_INFINITY;
		}
		
		for (Double aCurrentValue : aValueList) {
			if (aCurrentValue < aMin){
				aMin = aCurrentValue;
			}
		}
		
		return aMin;
	}
	
	
	/**
	 * Determines the maximum value for the given numeric attribute description.
	 * 
	 * @param theAttributeDesc attribute description
	 * @return max value
	 */
	public static double getMax(SimpleAttDesc theAttributeDesc){
		double aMax = Double.NEGATIVE_INFINITY; 
		
		ArrayList<Double> aValueList = CaseBaseUtils.getAllCaseBaseAttributesAsDouble(theAttributeDesc);
		
		if (aValueList.size() == 0){
			return Double.POSITIVE_INFINITY;
		}
		
		for (Double aCurrentValue : aValueList) {
			if (aCurrentValue > aMax){
				aMax = aCurrentValue;
			}
		}
		
		return aMax;
	}
	
	
	/**
	 * Calculates the variance for the given attribute description.
	 * 
	 * @param theAttributeDesc attribute description
	 * @return variance
	 * @throws NumberFormatException error occurred while getting attribute values
	 */
	public static double getVariance(SimpleAttDesc theAttributeDesc) throws NumberFormatException{	
		return getVariance(CaseBaseUtils.getAllCaseBaseAttributesAsDouble(theAttributeDesc));
	}
	
	
	/**
	 * Calculates the variance for the given value list.
	 * 
	 * @param theValueList list of values
	 * @return variance
	 * @throws NumberFormatException error occurred while getting attribute values
	 */
	public static double getVariance(ArrayList<Double> theValueList) throws NumberFormatException{
		double meanValue = getMeanValue(theValueList);
		double sum = 0; 
		
		int numberOfValues = theValueList.size();
		
		for (Double aCurrentValue : theValueList) {
			sum += Math.pow(meanValue-aCurrentValue, 2);	// == (meanValue - value)^2
		}
		
		return sum / numberOfValues;
	}
	
	
	
	/**
	 * Calculates the standard deviation for the given attribute description.
	 * 
	 * @param theAttributeDesc attribute description
	 * @return standard deviation
	 * @throws NumberFormatException error occurred while getting attribute values
	 */
	public static double getStandardDeviation(SimpleAttDesc theAttributeDesc) throws NumberFormatException{
		return Math.sqrt(getVariance(theAttributeDesc));
	}
	
	
	/**
	 * Calculates the standard deviation for the given list of values.
	 * 
	 * @param theValueList list of values
	 * @return standard deviation
	 * @throws NumberFormatException error occurred while getting attribute values
	 */
	public static double getStandardDeviation(ArrayList<Double> theValueList) throws NumberFormatException{
		return Math.sqrt(getVariance(theValueList));
	}
	
	
	/**
	 * Calculates the median for the given list of values.
	 * 
	 * @param theValueList list of values
	 * @return median
	 * @throws NumberFormatException error occurred while getting attribute values
	 */	
	public static double getMedianValue(ArrayList<Double> theValueList) {
		int numberOfValues = theValueList.size();
		
		Collections.sort(theValueList);

	   if (numberOfValues % 2 == 0) 
	   {
	      return (theValueList.get((numberOfValues / 2) - 1) + theValueList.get(numberOfValues / 2)) / 2.0;
	   } 
	   else 
	   {
	      return theValueList.get(numberOfValues / 2);
	   }
    }
	
	
	/**
	 * Calculates the median for the given attribute description.
	 * 
	 * @param theAttributeDesc attribute description
	 * @return median
	 * @throws NumberFormatException error occurred while getting attribute values
	 */	
	public static double getMedianValue(SimpleAttDesc theAttributeDesc) {		
		return getMedianValue(CaseBaseUtils.getAllCaseBaseAttributesAsDouble(theAttributeDesc));
    }
	
	
	
	/**
	 * Calculates the Look-Up-Table (LUT) for the local similarity metric "OVERLAP".
	 * 
	 * @param theSymbolFct The target LUT for the local similarity metric
	 */
	protected static void setSimilarityMatrix_OVERLAP(SymbolFct theSymbolFct){
		double aCaluclatedSimilarity;
		
		System.out.println("========= DEBUG: Similarities for '" + theSymbolFct.getName() + "' =========");
		
		for (String anAllowedValue1 : theSymbolFct.getDesc().getAllowedValues()) {
			for (String anAllowedValue2 : theSymbolFct.getDesc().getAllowedValues()) {
				
				if (anAllowedValue1.equals(anAllowedValue2)){
					aCaluclatedSimilarity = 1d;
				} else{
					aCaluclatedSimilarity = 0d;
				}
								
				System.out.println("DEBUG: Calculated Similarity (" + anAllowedValue1 + ";" + anAllowedValue2 + ") = " + aCaluclatedSimilarity);
				theSymbolFct.setSimilarity(anAllowedValue1, anAllowedValue2, aCaluclatedSimilarity);
			}
		}
	}
	
	
	/**
	 * Calculates the Look-Up-Table (LUT) for the local similarity metric "RANDOM".
	 * 
	 * @param theSymbolFct The target LUT for the local similarity metric
	 */
	protected static void setSimilarityMatrix_RANDOM(SymbolFct theSymbolFct){
		double aCaluclatedSimilarity;
		
		System.out.println("========= DEBUG: Similarities for '" + theSymbolFct.getName() + "' =========");
		
		for (String anAllowedValue1 : theSymbolFct.getDesc().getAllowedValues()) {
			for (String anAllowedValue2 : theSymbolFct.getDesc().getAllowedValues()) {
				
//				if (anAllowedValue1.equals(anAllowedValue2)){
//					aCaluclatedSimilarity = 1d;
//				} else{
//					aCaluclatedSimilarity = 0d;
//				}
				
				aCaluclatedSimilarity = Computation.getRandomNumber(1);
						
				System.out.println("DEBUG: Calculated Similarity (" + anAllowedValue1 + ";" + anAllowedValue2 + ") = " + aCaluclatedSimilarity);
				theSymbolFct.setSimilarity(anAllowedValue1, anAllowedValue2, aCaluclatedSimilarity);
			}
		}
	}

}
