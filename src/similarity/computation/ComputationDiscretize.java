/**
 * (2019) Heidelberg University, Institute of Medical Biometry and Informatics (IMBI), Germany
 * 
 * This file is part of myCbrBuilder.
 *  
 * myCbrBuilder is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * myCbrBuilder is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with vivaGen. If not, see <http://www.gnu.org/licenses/>.
 * 
 **/
package similarity.computation;

import java.util.HashSet;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.cliommics.vivagen.Instances;
import org.cliommics.vivagen.survivalanalysis.DiscretizeSurvival;

import exceptions.CaseBaseException;
import similarity.computation.config.KaplanMeierTargetConfig;


/**
 * Collection of static functions often used for discretization. 
 * 
 * @author Christian Karmen
 *
 */
public class ComputationDiscretize {
	
	// TODO Ggf. Merge mit Klasse "AbstractDiscretizedNumberFct"
	
	/**
	 * Generates the interval labels (using a given prefix) for a given number of discrete intervals.
	 * 
	 * @param theNumberOfDiscretIntervals number of discrete intervals
	 * @param theIntervalLabelPrefix prefix for generated labels
	 * @return set of interval labels
	 */
	public static Set<String> generateIntervalLabels(int theNumberOfDiscretIntervals, String theIntervalLabelPrefix){
		HashSet<String> aLabelSet = new HashSet<String>();
		int i=0;
		
		while(i++ < theNumberOfDiscretIntervals){
			aLabelSet.add(theIntervalLabelPrefix + new Integer(i).toString());
		}
//		System.out.println(aLabelSet);
		return aLabelSet;
	}

	
	/**
	 * Calculates the index of a given value within an given range (between min and max) in a given number of intervals.
	 * The minimum index is 1; the maximum index is equal the number of given intervals.  
	 * 
	 * @param theValue value to discretize
	 * @param theMin range minimum value
	 * @param theMax range maximum value
	 * @param theNumberOfIntervals number of intervals
	 * @return discretized index; -1 means an error occurred
	 */
	public static int getDiscretizedIndex(double theValue, double theMin, double theMax, int theNumberOfIntervals){
		int aDiscretizedIndex;
		
		if (theValue <= theMin){
			aDiscretizedIndex = 1;
		} else if (theValue >= theMax){
			aDiscretizedIndex = theNumberOfIntervals;
		} else if (theValue <= theMin){
			aDiscretizedIndex = 1;
		} else{
			double aDiff = Math.abs(theMax-theMin);
			double aWidth = aDiff / theNumberOfIntervals;
			
			// FIXME is this really safe, casting a double to int?? and necessary, since decimal values are used??
			aDiscretizedIndex = (int)((theValue-theMin) / aWidth) + 1;
		}
		
		// error handling
		if (aDiscretizedIndex <= 0 || aDiscretizedIndex > theNumberOfIntervals){
			aDiscretizedIndex = -1;
		}
		
		return aDiscretizedIndex;
	}
	
	
	
	
	
	/**
	 * Calculates the index of a given value within an given range (between min and max) in a given number of intervals
	 * and concatenated a given prefix.
	 * 
	 * @param theValue value to discretize
	 * @param theMin range minimum value
	 * @param theMax range maximum value
	 * @param theNumberOfIntervals number of intervals
	 * @param theDiscretizedValuePrefix prefix
	 * @return given prefix with concatenated discretized index; an empty string result indicates an error
	 */
	public static String getDiscretizedIndexAsString(double theValue, double theMin, double theMax, int theNumberOfIntervals, String theDiscretizedValuePrefix){
		int aDiscretizedIndex = getDiscretizedIndex(theValue, theMin, theMax, theNumberOfIntervals);
		
		if (aDiscretizedIndex == -1){
			return "";
		}
		
		return theDiscretizedValuePrefix + new Integer(aDiscretizedIndex).toString();
	}
	
	
	
    /**
     * Nominalizes all numeric columns in the given list of cases.
     * 
     * @param theInstances Cases to nominalize
     * @param theKMTargetConfig Survival Configuration
     * @return Nominalized cases
     * @throws CaseBaseException General error while nominalization
     */
    public static Instances makeNominal(
    		Instances theInstances, 
    		KaplanMeierTargetConfig theKMTargetConfig) 
    				throws CaseBaseException{ 
    	
    	System.out.print("DEBUG: Nominalizing instances... ");

    	DiscretizeSurvival aDiscretizeSurvival = new DiscretizeSurvival(
    		theKMTargetConfig.TargetSurvivalColumn, 
    		theKMTargetConfig.TargetStatusColumn, 
    		theKMTargetConfig.TargetStatusIndicator, 
    	    true);
    	
    	try {
    		Instances aNominalizedInstances = aDiscretizeSurvival.makeNominal(theInstances);
    		System.out.println("done.");
			return aNominalizedInstances;
		} catch (Exception e) {
			throw new CaseBaseException(e);
		}
    }
    
    
	  /**
	   * Gets the nominal cut-off value of nominalization.
	   * 
	   * @param theInstances weka Instances to learn from
	   * @param theKMTargetConfig Kaplan-Meier configuration
	   * @param theAttributeLabel Label of (numeric) attribute of interest 
	   * 
	   * @return cut-off value of nominalization
	   * @throws Exception Error while nominalization 
	   */
	  public static double calculateCutOff(Instances theInstances, KaplanMeierTargetConfig theKMTargetConfig, String theAttributeLabel) throws Exception{
	        DiscretizeSurvival aDiscretize = new DiscretizeSurvival(
	        		theKMTargetConfig.TargetSurvivalColumn, 
	        		theKMTargetConfig.TargetStatusColumn, 
	        		theKMTargetConfig.TargetStatusIndicator, 
	        		true);
 
	        Instances aNominalizedInstances = aDiscretize.makeNominal(theInstances);	        
	        int anAtributeIndex = aNominalizedInstances.getAttributeIndex(theAttributeLabel);
	        
	        return aDiscretize.getCutPoints()[anAtributeIndex][0];	
	  }
	  
	  
	  
		/**
		 * Parses the cut-off value from the nominalized label.
		 * 
		 * @param theNominalizedAttributeValue nominalized label
		 * @return cut-off value
		 */
		public static double parseCutOffValue(String theNominalizedAttributeValue){
			
//			System.out.println("DEBUG: Parsing Cut-Off value of attribute '" + anAttributeName + "'");
			
			// Ggf. auch Kommanotation (de) beachten
			Pattern aPattern = Pattern.compile("[0-9]+(?:.[0-9]+)?");
			Matcher aMatcher = aPattern.matcher(theNominalizedAttributeValue);
			
			if (!aMatcher.find()){
				System.err.println("ERROR: Number Pattern not found in String '" + theNominalizedAttributeValue + "'"); 
				return Double.NaN;
			}
			
//			System.out.println("DEBUG: Number pattern match (" + anAttributeValue  + ") result = " +  aMatcher.group());
			double aCutOffValue = Double.parseDouble(aMatcher.group());
			
			return aCutOffValue;
		}
		
		
		  /**
		   * Checks if the given normalized attribute value describes the value range above or below the cut-off. 
		   * CAUTION: This applies only for previously performed dichtomoized nominalization, especially for the 'SurvivalCutOffDiscretize'.
		   * 
		   * @param theNormalizedAttributeValue normalized attribute value
		   * @return true=above the cut-off; false=below the cut-off
		   */
		  public static boolean isRangeAboveCutOff(String theNormalizedAttributeValue){
			  if (theNormalizedAttributeValue.contains("(-inf-")){
				  return false;
			  }
			  // FIXME positiven fall testen, sonst exception werfen
			  return true;
		  }

}
