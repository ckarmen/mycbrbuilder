/**
 * (2019) Heidelberg University, Institute of Medical Biometry and Informatics (IMBI), Germany
 * 
 * This file is part of myCbrBuilder.
 *  
 * myCbrBuilder is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * myCbrBuilder is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with vivaGen. If not, see <http://www.gnu.org/licenses/>.
 * 
 **/
package similarity.computation.config;

/**
 * Configuration object for the kaplan-meier similarity measure.
 * 
 * @author Christian Karmen
 *
 */
public class KaplanMeierConfig extends AbstractSimilarityMeasureConfiguration{
	
//	public Instances Instances;
	
	/**
	 * Kaplan-Meier Target Configuration for Survival Data Objects.
	 */
	public KaplanMeierTargetConfig KMTargetConfig;
	
	/**
	 * Configures interpolation in calculation of Kaplan-Meier diagram.
	 * </p>
	 * FIXME nachpruefen!!
	 * true=step shape; false=interpolated shape
	 */
	public boolean StepShapeFlag;
	
	/**
	 * Configures if Kaplan-Meier visualization contains ticks for patient exclusions.
	 */
	public boolean TicksFlag;
	
	/**
	 * Configuration object for the kaplan-meier similarity measure.
	 * 
	 * @param theKMTargetConfig
	 * @param theStepShapeFlag
	 * @param theTicksFlag
	 */
	public KaplanMeierConfig(KaplanMeierTargetConfig theKMTargetConfig, boolean theStepShapeFlag, boolean theTicksFlag){
//		Instances = theInstances;
		KMTargetConfig = theKMTargetConfig;
		StepShapeFlag = theStepShapeFlag;
		TicksFlag = theTicksFlag;
	}

	@Override
	public void init(){
//		Instances = null;
		KMTargetConfig = new KaplanMeierTargetConfig("", "", ""); 
		StepShapeFlag = true;
		TicksFlag = true;
	}
}
