/**
 * (2019) Heidelberg University, Institute of Medical Biometry and Informatics (IMBI), Germany
 * 
 * This file is part of myCbrBuilder.
 *  
 * myCbrBuilder is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * myCbrBuilder is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with vivaGen. If not, see <http://www.gnu.org/licenses/>.
 * 
 **/
package similarity.computation.config;

/**
 * Class to keep the Kaplan-Meier Survival Target Configuration.
 * 
 * @author Christian Karmen
 *
 */
public class KaplanMeierTargetConfig extends AbstractSimilarityMeasureConfiguration{

	/**
	 * Definition of custom survival targets.
	 * 
	 * @author Christian Karmen
	 */
	// FIXME Verschieben nach myCBRBuilderApplication
	public static enum SURVIVALTARGET{
		/**
		 * Survival target is Overall Survival (OS) 
		 */
		HD4_OVERALL_SURVIVAL, 
		
		/**
		 * Survival target is Progression Free Survival (PFS)
		 */
		HD4_PROGRESSION_FREE_SURVIVAL,
		
		/**
		 * Survival target is generated
		 */
		GENERATED
	}
	
	/**
	 * Attribute Label containing the survival status (e.g. "Survival status indicator").
	 */
	public String TargetStatusColumn;				//Configuration.OVERALL_SURVIVAL_STATUS_COLUMN
	
	/**
	 * Value that indicates the event (e.g. "death") in TargetStatusColumn.
	 */
	public String TargetStatusIndicator;			//Configuration.DEAD;
	
	/**
	 * Attribute Label containing the survival duration (e.g. "Overall survival [m]").
	 */
	public String TargetSurvivalColumn;				//Configuration.OVERALL_SURVIVAL_COLUMN
	
	
	/**
	 * Class to keep the Kaplan-Meier Survival Target Configuration.
	 * 
	 * @param theTargetStatusColumn Attribute Label containing the survival status (e.g. "Survival status indicator")
	 * @param theTargetStatusIndicator Value that indicates the event (e.g. "death") in TargetStatusColumn
	 * @param theTargetSurvivalColumn Attribute Label containing the survival duration (e.g. "Overall survival [m]")
	 */
	public KaplanMeierTargetConfig(String theTargetStatusColumn, String theTargetStatusIndicator, String theTargetSurvivalColumn){
		TargetStatusColumn = theTargetStatusColumn;
		TargetStatusIndicator = theTargetStatusIndicator;
		TargetSurvivalColumn = theTargetSurvivalColumn;
	}
	
	
	/**
	 * Class to keep the Kaplan-Meier Survival Target Configuration.
	 * 
	 * @param theSurvivalTarget Survival Target predefined configuration
	 */
	// FIXME Verschieben nach myCBRBuilderApplication
	public KaplanMeierTargetConfig(SURVIVALTARGET theSurvivalTarget){
		setSurvivalTarget(theSurvivalTarget);
	}
	
	
	/**
	 * Sets a predefined Survival Target Configuration.
	 * 
	 * @param theSurvivalTarget Survival Target predefined configuration
	 */
	// FIXME Verschieben nach myCBRBuilderApplication
	public void setSurvivalTarget(SURVIVALTARGET theSurvivalTarget){
		switch (theSurvivalTarget) {
		case HD4_OVERALL_SURVIVAL:
			TargetStatusColumn = "00 - Main Data \\ Survival status indicator";				//Configuration.OVERALL_SURVIVAL_STATUS_COLUMN
			TargetStatusIndicator = "dead";													//Configuration.DEAD;
			TargetSurvivalColumn = "00 - Main Data \\ Overall survival [m]";				//Configuration.OVERALL_SURVIVAL_COLUMN
			break;
		case HD4_PROGRESSION_FREE_SURVIVAL:
			TargetStatusColumn = "00 - Main Data \\ PFS indicator (censored at alloSCT)";	//Configuration.PROGRESSION_FREE_SURVIVAL_STATUS_COLUMN
			TargetStatusIndicator = "pd/d";													//Configuration.PROGRESSION
			TargetSurvivalColumn = "00 - Main Data \\ PFS. [mo] (censored at alloSCT)";		//Configuration.PROGRESSION_FREE_SURVIVAL_COLUMN		
			break;
		case GENERATED:
			TargetStatusColumn = "SurvivalStatus";
			TargetStatusIndicator = "dead";	
			TargetSurvivalColumn = "SurvivalTime";	
			break;
		default:
			System.err.println("ERROR: unhandled SURVIVALTARGET");
			break;
		}
	}

	@Override
	public void init() {
		TargetStatusColumn = "";
		TargetStatusIndicator = "";
		TargetSurvivalColumn = "";
	}
		
}


