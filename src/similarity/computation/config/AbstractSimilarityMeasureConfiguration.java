/**
 * (2019) Heidelberg University, Institute of Medical Biometry and Informatics (IMBI), Germany
 * 
 * This file is part of myCbrBuilder.
 *  
 * myCbrBuilder is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * myCbrBuilder is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with vivaGen. If not, see <http://www.gnu.org/licenses/>.
 * 
 **/
package similarity.computation.config;

/**
 * Abstract class for a similarity measure configuration object.
 * 
 * @author Christian Karmen
 *
 */
public abstract class AbstractSimilarityMeasureConfiguration {
	
	/**
	 * Abstract class for a similarity measure configuration object. 
	 */
	public AbstractSimilarityMeasureConfiguration(){
		init();
	}
	
	/**
	 * Initializes all configuration parameters.
	 */
	abstract public void init();

}
