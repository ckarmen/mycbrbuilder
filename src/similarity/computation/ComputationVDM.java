/**
 * (2019) Heidelberg University, Institute of Medical Biometry and Informatics (IMBI), Germany
 * 
 * This file is part of myCbrBuilder.
 *  
 * myCbrBuilder is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * myCbrBuilder is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with vivaGen. If not, see <http://www.gnu.org/licenses/>.
 * 
 **/
package similarity.computation;

import java.util.ArrayList;

import core.CaseBaseUtils;
import de.dfki.mycbr.core.DefaultCaseBase;
import de.dfki.mycbr.core.casebase.Instance;
import de.dfki.mycbr.core.casebase.SymbolAttribute;
import de.dfki.mycbr.core.model.Concept;
import de.dfki.mycbr.core.model.SymbolDesc;
import de.dfki.mycbr.core.similarity.SymbolFct;
import de.dfki.mycbr.core.similarity.config.AmalgamationConfig;
import exceptions.CaseBaseException;
import exceptions.SimilarityMeasureException;
import similarity.computation.config.VDMConfig;
import similarity.config.DiscretizationConfig;
import similarity.config.NormalizationConfig;
import similarity.config.SymbolSimilarityConfig;


/**
 * General Helper Methods for VDM based calculations for myCBR. (VDM = Value Difference Metric, Martinet et al, 2006).  
 * 
 * @author Christian Karmen
 *
 */
public class ComputationVDM {

	
	/**
	 * Generates an Amalgam function (global similarity function) for the HVDM metric. 
	 * Here VDM variant 1 is being used.
	 * 
	 * @param theConcept Concept of interest
	 * @param theCaseDescList	case description list
	 * @param theCaseSolutionList case solution list
	 * @param theTrainingCaseBase training case base
	 * @param theVDMConfig VDM configuration
	 * @throws CaseBaseException Error while access the case base
	 * @throws SimilarityMeasureException Error while preparing similarity measure
	 */
	public static void generateAmalgamFct_HVDM1(
			Concept theConcept,
			ArrayList<String> theCaseDescList, 
			ArrayList<String> theCaseSolutionList, 
			DefaultCaseBase theTrainingCaseBase,
			VDMConfig theVDMConfig) 
					throws CaseBaseException, SimilarityMeasureException{
		
		Computation.generateAmalgamFct(
				"HVDM1", 
				theConcept, 
				theCaseDescList, 
				theCaseSolutionList, 
				NormalizationConfig.FOUR_SIGMA, 
				SymbolSimilarityConfig.VDM_N1, 
				theTrainingCaseBase, 
				AmalgamationConfig.EUCLIDEAN, 
				theVDMConfig);
	}
	
	/**
	 * Generates an Amalgam function (global similarity function) for the HVDM metric. 
	 * Here VDM variant 2 is being used.
	 * 
	 * @param theConcept Concept of interest
	 * @param theCaseDescList	case description list
	 * @param theCaseSolutionList case solution list
	 * @param theTrainingCaseBase training case base
	 * @param theVDMConfig VDM configuration
	 * @throws CaseBaseException Error while access the case base
	 * @throws SimilarityMeasureException Error while preparing similarity measure
	 */
	public static void generateAmalgamFct_HVDM2(
			Concept theConcept,
			ArrayList<String> theCaseDescList, 
			ArrayList<String> theCaseSolutionList, 
			DefaultCaseBase theTrainingCaseBase,
			VDMConfig theVDMConfig) 
					throws CaseBaseException, SimilarityMeasureException{
		
		Computation.generateAmalgamFct(
				"HVDM2", 
				theConcept, 
				theCaseDescList, 
				theCaseSolutionList, 
				NormalizationConfig.FOUR_SIGMA, 
				SymbolSimilarityConfig.VDM_N2, 
				theTrainingCaseBase, 
				AmalgamationConfig.EUCLIDEAN, 
				theVDMConfig);
	}
	
	/**
	 * Generates an Amalgam function (global similarity function) for the HVDM metric. 
	 * Here VDM variant 3 is being used.
	 * 
	 * @param theConcept Concept of interest
	 * @param theCaseDescList	case description list
	 * @param theCaseSolutionList case solution list
	 * @param theTrainingCaseBase training case base
	 * @param theVDMConfig VDM configuration
	 * @throws CaseBaseException Error while access the case base
	 * @throws SimilarityMeasureException Error while preparing similarity measure
	 */
	public static void generateAmalgamFct_HVDM3(
			Concept theConcept,
			ArrayList<String> theCaseDescList, 
			ArrayList<String> theCaseSolutionList, 
			DefaultCaseBase theTrainingCaseBase,
			VDMConfig theVDMConfig) 
					throws CaseBaseException, SimilarityMeasureException{
		
		Computation.generateAmalgamFct("HVDM3", 
				theConcept, 
				theCaseDescList, 
				theCaseSolutionList, 
				NormalizationConfig.FOUR_SIGMA, 
				SymbolSimilarityConfig.VDM_N3, 
				theTrainingCaseBase, 
				AmalgamationConfig.EUCLIDEAN, 
				theVDMConfig);
	}
	
	/**
	 * Generates an Amalgam function (global similarity function) for the DVDM metric. 
	 * 
	 * @param theConcept Concept of interest
	 * @param theCaseDescList	case description list
	 * @param theCaseSolutionList case solution list
	 * @param theTrainingCaseBase training case base
	 * @param theVDMConfig VDM configuration
	 * @throws CaseBaseException Error while access the case base
	 * @throws SimilarityMeasureException Error while preparing similarity measure
	 */
	public static void generateAmalgamFct_DVDM(
			Concept theConcept,
			ArrayList<String> theCaseDescList, 
			ArrayList<String> theCaseSolutionList, 
			DefaultCaseBase theTrainingCaseBase,
			VDMConfig theVDMConfig) 
					throws CaseBaseException, SimilarityMeasureException{
		
		Computation.generateAmalgamFct(
				"DVDM", 
				theConcept, 
				theCaseDescList, 
				theCaseSolutionList, 
				DiscretizationConfig.DVDM, 
				SymbolSimilarityConfig.VDM_N1_Q2, 
				theTrainingCaseBase, 
				AmalgamationConfig.SQUARED_SUM, 
				theVDMConfig);
	}
	
	/**
	 * Generates an Amalgam function (global similarity function) for the IVDM metric. 
	 * 
	 * @param theConcept Concept of interest
	 * @param theCaseDescList	case description list
	 * @param theCaseSolutionList case solution list
	 * @param theTrainingCaseBase training case base
	 * @param theVDMConfig VDM configuration
	 * @throws CaseBaseException Error while access the case base
	 * @throws SimilarityMeasureException Error while preparing similarity measure
	 */
	public static void generateAmalgamFct_IVDM(
			Concept theConcept,
			ArrayList<String> theCaseDescList, 
			ArrayList<String> theCaseSolutionList, 
			DefaultCaseBase theTrainingCaseBase,
			VDMConfig theVDMConfig) 
					throws CaseBaseException, SimilarityMeasureException{
		
		Computation.generateAmalgamFct(
				"IVDM", 
				theConcept, 
				theCaseDescList, 
				theCaseSolutionList, 
				DiscretizationConfig.IVDM, 
				SymbolSimilarityConfig.VDM_N1_Q2, 
				theTrainingCaseBase, 
				AmalgamationConfig.SQUARED_SUM, 
				theVDMConfig);
	}
	
	/**
	 * Generates an Amalgam function (global similarity function) for the WVDM metric. 
	 * 
	 * @param theConcept Concept of interest
	 * @param theCaseDescList	case description list
	 * @param theCaseSolutionList case solution list
	 * @param theTrainingCaseBase training case base
	 * @param theVDMConfig VDM configuration
	 * @throws CaseBaseException Error while access the case base
	 * @throws SimilarityMeasureException Error while preparing similarity measure
	 */
	public static void generateAmalgamFct_WVDM(
			Concept theConcept,
			ArrayList<String> theCaseDescList, 
			ArrayList<String> theCaseSolutionList, 
			DefaultCaseBase theTrainingCaseBase,
			VDMConfig theVDMConfig) 
					throws CaseBaseException, SimilarityMeasureException{
		
		Computation.generateAmalgamFct(
				"WVDM", 
				theConcept, 
				theCaseDescList, 
				theCaseSolutionList, 
				DiscretizationConfig.WVDM, 
				SymbolSimilarityConfig.VDM_N1_Q2, 
				theTrainingCaseBase, 
				AmalgamationConfig.SQUARED_SUM, 
				theVDMConfig);
	}
	
	

	
	
	/**
	 * N(a,x,c) is the number of instances in the training set (given Case Base) 
	 * that have the value x (theInputValue) for attribute a (theInputDesc) 
	 * and output class c (theOutputClassValue) of output attribute C (theOutputClassValue).
	 * 
	 * @param theCaseBase	the training set (T)
	 * @param theInputDesc	the input attribute (a)
	 * @param theInputValue the input value (x)
	 * @param theVDMConfig VDM configuration
	 * @param theOutputClassValue	the output class (c)
	 * @return N(a,x,c)
	 * @throws CaseBaseException Error while access to case base
	 */
	public static int getNaxc(
			DefaultCaseBase theCaseBase,
			SymbolDesc theInputDesc, 
			String theInputValue, 
			VDMConfig theVDMConfig,
			String theOutputClassValue) throws CaseBaseException{
		
		int aMatchCounter = 0;
		SymbolAttribute anInputAttribute;//, anOutputAttribute;
		String aOutputValue;
		
		for (Instance anInstance : theCaseBase.getCases()) {
//			anOutputAttribute = (SymbolAttribute)anInstance.getAttForDesc(theOutputDesc);
			aOutputValue = CaseBaseUtils.getCaseAttributeValue(anInstance, theVDMConfig.OutputClassLabel).getValueAsString();
			//if (!anOutputAttribute.getValue().equalsIgnoreCase(theOutputClassValue)){
			if (!aOutputValue.equalsIgnoreCase(theOutputClassValue)){
				continue;
			}
			
			anInputAttribute = (SymbolAttribute)anInstance.getAttForDesc(theInputDesc);
			if (anInputAttribute.getValue().equalsIgnoreCase(theInputValue)){
				aMatchCounter++;
			}
		} 
		
		return aMatchCounter;
	}
	
	
	
	/**
	 * N(a,x) is the number of instances in the training set (given Case Base) 
	 * that have the value x (theInputValue) for attribute a (theInputDesc) 
	 * of output attribute C (theOutputClassValue).
	 * 
	 * @param theCaseBase	the training set (T)
	 * @param theInputDesc	the input attribute (a)
	 * @param theInputValue the input value (x)
	 * @param theVDMConfig VDM configuration
	 * @return N(a,x)
	 * @throws CaseBaseException Error while access to case base
	 */
	public static int getNax(
			DefaultCaseBase theCaseBase,
			SymbolDesc theInputDesc, 
			String theInputValue, 
			VDMConfig theVDMConfig
			) throws CaseBaseException{
		int aMatchCounter = 0;
		
		for (String anAllowedOutputValue : theVDMConfig.OutputClassValueList) {
			aMatchCounter += getNaxc(theCaseBase, theInputDesc, theInputValue, theVDMConfig, anAllowedOutputValue);
		}
		
		return aMatchCounter;
	}
	
	
	/**
	 * P(a,x,c) is the conditional probability that the output class is c (theOutputClassValue) given  
	 * that attribute a (theInputDesc) has the value x (theInputValue), i.e., P(c|x(a)).
	 * 
	 * number of instances in the training set (given Case Base) 
	 * that have the value x (theInputValue) for attribute a (theInputDesc) 
	 * and output class c (theOutputClassValue) of output attribute C (theOutputClassValue).
	 * 
	 * @param theCaseBase	the training set (T)
	 * @param theInputDesc	the input attribute (a)
	 * @param theInputValue the input value (x)
	 * @param theVDMConfig VDM configuration
	 * @param theOutputClassValue	the output class (c)
	 * @return P(a,x,c)
	 * @throws CaseBaseException Error while access to case base
	 */
	public static double getPaxc(
			DefaultCaseBase theCaseBase,
			SymbolDesc theInputDesc, 
			String theInputValue, 
			VDMConfig theVDMConfig, 
			String theOutputClassValue) throws CaseBaseException{
		
		int Naxc = getNaxc(theCaseBase, theInputDesc, theInputValue, theVDMConfig, theOutputClassValue);
		int Nax = getNax(theCaseBase, theInputDesc, theInputValue, theVDMConfig);
		
		if (Nax == 0){
			return 0;
		}
		
		return new Integer(Naxc).doubleValue() / new Integer(Nax).doubleValue();
	}
	
	
	
	
	/**
	 * The Value Difference Metric (VDM) was introduced by Stanfill and Waltz (1986)
	 * to provide an appropriate distance function for nominal attributes.
	 * 
	 * @param theCaseBase	the training set (T)
	 * @param theInputDesc	the input attribute (a)
	 * @param theInputValue1	the 1st input value (x)
	 * @param theInputValue2	the 2nd input value (y)
	 * @param theVDMConfig VDM configuration
	 * @param theExponent	the exponent q for each probability difference (usually 1 or 2)
	 * @return Normalized distance due to VDM.
	 * @throws CaseBaseException Error while access to case base
	 */
	public static double getVDM(
			DefaultCaseBase theCaseBase,
			SymbolDesc theInputDesc, 
			String theInputValue1, 
			String theInputValue2, 
			VDMConfig theVDMConfig,
			int theExponent) throws CaseBaseException{
		
		double Paxc, Payc;
		double probabilityDiff;
		double probabilityDiffSum = 0;
		
//		int i=1;
		
		for (String anAllowedOutputValue : theVDMConfig.OutputClassValueList) {
			Paxc = getPaxc(theCaseBase, theInputDesc, theInputValue1, theVDMConfig, anAllowedOutputValue);
			Payc = getPaxc(theCaseBase, theInputDesc, theInputValue2, theVDMConfig, anAllowedOutputValue);
			probabilityDiff = Math.abs(Paxc - Payc);
			probabilityDiff = Math.pow(probabilityDiff, theExponent);
			
			probabilityDiffSum += probabilityDiff;
			
//			//DEBUG OUTPUT
//			if (theInputDesc.getName().contains("Sex"))
//			System.out.println(
//					"INPUT DESC: " + theInputDesc.getName() 
////					+ "\n OUTPUT DESC: " + theOutputDesc.getName()
//					+ "\n\t theInputValue1 = \"" + theInputValue1 + "\""
//					+ "\n\t theInputValue2 = \"" + theInputValue2 + "\""
//					
//					+ "\n\t anAllowedOutputValue = " + anAllowedOutputValue + "\""
//					
//					+ "\n\t Paxc = " + Paxc
//					+ "\n\t Payc = " + Payc
//					
//					+ "\n\t probabilityDiff = " + probabilityDiff
//					+ "\n\t probabilityDiffSum = " + probabilityDiffSum
//					+ "\n\t i = " + i + "(" + theInputValue1 + "/" + theInputValue2 + ")"
//					);
//			i++;
		}
		
		return probabilityDiffSum;
	}
	
	
	/**
	 * The Value Difference Metric (VDM) was introduced by Stanfill and Waltz (1986)
	 * to provide an appropriate distance function for nominal attributes. </br>
	 * Here a VDM variant with exponent q=1 is calculated.
	 * 
	 * @param theCaseBase	the training set (T)
	 * @param theInputDesc	the input attribute (a)
	 * @param theInputValue1	the 1st input value (x)
	 * @param theInputValue2	the 2nd input value (y)
	 * @param theVDMConfig VDM configuration
	 * @return VDM variant with exponent q=1
	 * @throws CaseBaseException Error while access to case base
	 */
	public static double getVDM_N1(
			DefaultCaseBase theCaseBase,
			SymbolDesc theInputDesc, 
			String theInputValue1, 
			String theInputValue2, 
			VDMConfig theVDMConfig
			) throws CaseBaseException{
		
		return getVDM(theCaseBase, theInputDesc, theInputValue1, theInputValue2, theVDMConfig, 1);
	}
	
	/**
	 * The Value Difference Metric (VDM) was introduced by Stanfill and Waltz (1986)
	 * to provide an appropriate distance function for nominal attributes. </br>
	 * Here a square-rooted VDM with exponent q=2 is calculated.
	 * 
	 * @param theCaseBase	the training set (T)
	 * @param theInputDesc	the input attribute (a)
	 * @param theInputValue1	the 1st input value (x)
	 * @param theInputValue2	the 2nd input value (y)
	 * @param theVDMConfig VDM configuration
	 * @return Square-rooted VDM with exponent q=2
	 * @throws CaseBaseException Error while access to case base
	 */
	public static double getVDM_N2(
			DefaultCaseBase theCaseBase,
			SymbolDesc theInputDesc, 
			String theInputValue1, 
			String theInputValue2, 
			VDMConfig theVDMConfig
			) throws CaseBaseException{
		
		return Math.sqrt(getVDM(theCaseBase, theInputDesc, theInputValue1, theInputValue2, theVDMConfig, 2));
	}
	
	
	/**
	 * The Value Difference Metric (VDM) was introduced by Stanfill and Waltz (1986)
	 * to provide an appropriate distance function for nominal attributes. </br>
	 * Here a square-rooted VDM with exponent q=2 and a factor of C=NumberOfOutputClasses is calculated.
	 * 
	 * @param theCaseBase	the training set (T)
	 * @param theInputDesc	the input attribute (a)
	 * @param theInputValue1	the 1st input value (x)
	 * @param theInputValue2	the 2nd input value (y)
	 * @param theVDMConfig VDM configuration
	 * @return Square-rooted VDM with exponent q=2 and a factor of C=NumberOfOutputClasses
	 * @throws CaseBaseException Error while access to case base
	 */
	public static double getVDM_N3(
			DefaultCaseBase theCaseBase,
			SymbolDesc theInputDesc, 
			String theInputValue1, 
			String theInputValue2, 
			VDMConfig theVDMConfig
			) throws CaseBaseException{
		
		int numberOfOutputClasses = theVDMConfig.OutputClassValueList.size();
		
		return Math.sqrt(numberOfOutputClasses * getVDM(theCaseBase, theInputDesc, theInputValue1, theInputValue2, theVDMConfig, 2));
	}
	
	
	/**
	 * The Value Difference Metric (VDM) was introduced by Stanfill and Waltz (1986)
	 * to provide an appropriate distance function for nominal attributes. </br>
	 * Here a VDM with exponent q=2 is calculated.
	 * 
	 * @param theCaseBase	the training set (T)
	 * @param theInputDesc	the input attribute (a)
	 * @param theInputValue1	the 1st input value (x)
	 * @param theInputValue2	the 2nd input value (y)
	 * @param theVDMConfig VDM configuration
	 * @return VDM with q=2
	 * @throws CaseBaseException Error while access to case base
	 */
	public static double getVDM_N1_Q2(
			DefaultCaseBase theCaseBase,
			SymbolDesc theInputDesc, 
			String theInputValue1, 
			String theInputValue2, 
			VDMConfig theVDMConfig
			) throws CaseBaseException{
		
		return getVDM(theCaseBase, theInputDesc, theInputValue1, theInputValue2, theVDMConfig, 2);
	}
	
	

	
//	/**
//	 * The Value Difference Metric (VDM) was introduced by Stanfill and Waltz (1986)
//	 * to provide an appropriate distance function for nominal attributes. </br>
//	 * Here a VDM with exponent q=2 and a factor of C=NumberOfOutputClasses is calculated.
//	 * 
//	 * @param theCaseBase	the training set (T)
//	 * @param theInputDesc	the input attribute (a)
//	 * @param theInputValue1	the 1st input value (x)
//	 * @param theInputValue2	the 2nd input value (y)
//	 * @param theOutputDesc	the output attribute (C)
//	 * @return VDM with exponent q=2 and a factor of C=NumberOfOutputClasses
//	 */
//	public static double getVDM_Q3(DefaultCaseBase theCaseBase,
//	SymbolDesc theInputDesc, String theInputValue1, String theInputValue2, 
//	SymbolDesc theOutputDesc){
//		
//		int numberOfOutputClasses = theOutputDesc.getAllowedValues().size();
//		
//		return numberOfOutputClasses * getVDM(theCaseBase, theInputDesc, theInputValue1, theInputValue2, theOutputDesc, 2);
//	}
	
	
	
	
	
	/**
	 * Calculates the Look-Up-Table (LUT) for the local similarity metric "VDM".
	 * 
	 * @param theDiscretizedSymbolFct target LUT for the local similarity metric
	 * @param theSymbolSimilarityConfig VDM variation to be used for calculation
	 * @param theDiscretizedCaseBase case base with discretized values
	 * @param theVDMConfig VDM configuration
	 * @throws CaseBaseException Error while access to case base
	 * 
	 */
	public static void setSimilarityMatrix_VDM(
			SymbolFct theDiscretizedSymbolFct, 
			SymbolSimilarityConfig theSymbolSimilarityConfig, 
			DefaultCaseBase theDiscretizedCaseBase, 
			VDMConfig theVDMConfig
			) throws CaseBaseException{
		
		double aCaluclatedSimilarity;
		double aCaluclatedDistance;
		
		SymbolDesc aSymbolDesc = theDiscretizedSymbolFct.getDesc();
		
		for (String anAllowedValue1 : aSymbolDesc.getAllowedValues()) {
			for (String anAllowedValue2 : aSymbolDesc.getAllowedValues()) {
				
				switch (theSymbolSimilarityConfig) {
					case VDM_N1:
						aCaluclatedDistance = getVDM_N1(theDiscretizedCaseBase, aSymbolDesc, anAllowedValue1, anAllowedValue2, theVDMConfig);
						break;
					case VDM_N2:
						aCaluclatedDistance = getVDM_N2(theDiscretizedCaseBase, aSymbolDesc, anAllowedValue1, anAllowedValue2, theVDMConfig);
						break;
					case VDM_N3:
						aCaluclatedDistance = getVDM_N3(theDiscretizedCaseBase, aSymbolDesc, anAllowedValue1, anAllowedValue2, theVDMConfig);
						break;
					case VDM_N1_Q2:
						aCaluclatedDistance = getVDM_N1_Q2(theDiscretizedCaseBase, aSymbolDesc, anAllowedValue1, anAllowedValue2, theVDMConfig);
						break;
					default:
						System.err.println("ERROR: Not a VDM SymbolSimilarityConfig: " + theSymbolSimilarityConfig.getName());
						aCaluclatedDistance = Double.NaN;
						break;
				}
				
				aCaluclatedSimilarity = 1 - aCaluclatedDistance;
				
				// FIXME Achtung: Anders berechnen! Theoretisch darf die Distanz > 1 sein...
				if (aCaluclatedSimilarity < 0){
					aCaluclatedSimilarity = 0;
				}
				
				System.out.println("DEBUG: Calculated Similarity of '" + theDiscretizedSymbolFct.getName() + "' (" + anAllowedValue1 + ";" + anAllowedValue2 + ") = " + aCaluclatedSimilarity);
				theDiscretizedSymbolFct.setSimilarity(anAllowedValue1, anAllowedValue2, aCaluclatedSimilarity);
			}
		}
	}
	



}
