/**
 * (2019) Heidelberg University, Institute of Medical Biometry and Informatics (IMBI), Germany
 * 
 * This file is part of myCbrBuilder.
 *  
 * myCbrBuilder is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * myCbrBuilder is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with vivaGen. If not, see <http://www.gnu.org/licenses/>.
 * 
 **/
package similarity.computation;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;

import org.cliommics.vivagen.Instances;

import application.javafx.charts.KMChart;
import core.MyCbrBuilderSession;
import core.connectors.CsvConnector;
import de.dfki.mycbr.core.DefaultCaseBase;
import exceptions.CaseBaseException;
import similarity.computation.config.KaplanMeierTargetConfig;
import similarity.computation.config.KaplanMeierTargetConfig.SURVIVALTARGET;

/**
 * Class for testing purposes of "ComputationKM"
 * 
 * @author Christian Karmen
 *
 */
public class ComputationKMtest {
	

	// FIXME All lines below are only for debugging / testing => better to write unit tests
	
	private static ArrayList<String> getHD4harmonizedWhiteList(){
		ArrayList<String> anCaseDescriptionList = new ArrayList<>();
		ArrayList<String> anSolutionDescriptionList = new ArrayList<>();
		
		anCaseDescriptionList.clear();
		anCaseDescriptionList.add("00 - Main Data \\ Age");					// 5er	// 3er
		anCaseDescriptionList.add("00 - Main Data \\ Sex");					// 5er	// 3er		
		anCaseDescriptionList.add("02 - ON STUDY \\ Weight [kg]");			// 5er	// 3er
		anCaseDescriptionList.add("00 - Main Data \\ ISS");					// 5er
		anCaseDescriptionList.add("00 - Main Data \\ Stage of disease");	// 5er
//		anCaseDescriptionList.add("02 - ON STUDY \\ Height [cm]");
//		anCaseDescriptionList.add("02 - ON STUDY \\ Surface area [m^2]");
//		anCaseDescriptionList.add("02 - ON STUDY \\ ALAT [U/l]");
//		anCaseDescriptionList.add("02 - ON STUDY \\ Albumin [g/l]");
//		anCaseDescriptionList.add("02 - ON STUDY \\ Calcium [mmol/l] to");
//		anCaseDescriptionList.add("02 - ON STUDY \\ Creatinine [mg/dl]");
//		anCaseDescriptionList.add("02 - ON STUDY \\ CRP [mg/l]");
//		anCaseDescriptionList.add("02 - ON STUDY \\ LDH [U/l]");
//		anCaseDescriptionList.add("02 - ON STUDY \\ Total proteins [g/l]");
//		anCaseDescriptionList.add("02 - ON STUDY \\ Hemoglobin [mmol/l]");
//		anCaseDescriptionList.add("02 - ON STUDY \\ Platelets [x10^9/l]");
//		anCaseDescriptionList.add("02 - ON STUDY \\ Immunofixation serum");
//		anCaseDescriptionList.add("02 - ON STUDY \\ Immunofixation urine");
//		anCaseDescriptionList.add("02 - ON STUDY \\ Serum M-protein [g/l]");
//		anCaseDescriptionList.add("02 - ON STUDY \\ M-protein heavy chain");
//		anCaseDescriptionList.add("02 - ON STUDY \\ M-protein light chain");
//		anCaseDescriptionList.add("02 - ON STUDY \\ Number of bone plasmacytoma");
//		anCaseDescriptionList.add("02 - ON STUDY \\ Diffuse osteoporosis");
//		anCaseDescriptionList.add("02 - ON STUDY \\ Number of lytic bone lesions");
		Collections.sort(anCaseDescriptionList);

		anSolutionDescriptionList.clear();
		anSolutionDescriptionList.add("00 - Main Data \\ Randomization arm");
//		anSolutionDescriptionList.add("00 - Main Data \\ RESPONSE AFTER 1ST INDUCTION");
//		anSolutionDescriptionList.add("00 - Main Data \\ RESPONSE AFTER 2ND INDUCTION");
//		anSolutionDescriptionList.add("00 - Main Data \\ RESPONSE FOLLOW-UP");
//		anSolutionDescriptionList.add("00 - Main Data \\ PFS indicator (censored at alloSCT)");
//		anSolutionDescriptionList.add("00 - Main Data \\ PFS. [mo] (censored at alloSCT)");
		anSolutionDescriptionList.add("00 - Main Data \\ Overall survival [m]");
		anSolutionDescriptionList.add("00 - Main Data \\ Survival status indicator");
//		anSolutionDescriptionList.add("00 - Main Data \\ Registration hospital");
		
		// creating white-list for csv import
		ArrayList<String> aWhiteList = new ArrayList<String>();
		aWhiteList.addAll(anCaseDescriptionList); 
		aWhiteList.addAll(anSolutionDescriptionList);
		
		return aWhiteList;
	}
	

	/**  
	 * Runs the Calculations for CBR with HD4 dataset. 
	 * 
	 * @param args Will be ignored for the moment
	 */
	public static void main(String[] args) {	
//		ComputationKMtest aComputationKMtest = new ComputationKMtest();
		
		String aCaseBaseDirectory = "Z:\\CBR\\Harmonized\\";
    	if (System.getProperty("os.name").toLowerCase().contains("mac os")){
    		aCaseBaseDirectory = "/Users/rikerck/MM/";
    	}
//    	File aCaseBaseFile = new File(aCaseBaseDirectory + "HARMONIZED main t2 delined dot selected HD4 null clean.csv");
    	File aCaseBaseFile = new File(aCaseBaseDirectory + "HARMONIZED main t2 delined dot selected HD4 mini231 relevant5.csv");

    	
    	MyCbrBuilderSession aSession = new MyCbrBuilderSession("foo");  
    	CsvConnector aConnector = new CsvConnector(aSession.getConcept(), aCaseBaseFile, ";", getHD4harmonizedWhiteList());
		try {
			aConnector.fetchCaseBase();
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(-1);
		}
    	DefaultCaseBase aCaseBase = aSession.getCaseBase();
    	//ComputationKM aComputationKM = new ComputationKM(aCaseBase, KaplanMeierTargetConfig.getDefaultTargetConfig());
    	
//		ComputationKM aComputationKM = new ComputationKM(aCaseBaseFile, ";");
		//aComputationKM.setSurvivalTarget(SURVIVALTARGET.PROGRESSION_FREE_SURVIVAL);
    	
    	boolean aStepShapeFlag = true;
//    	boolean aFileLoaderFlag = false;
    	KaplanMeierTargetConfig aKMTargetConfig = new KaplanMeierTargetConfig(SURVIVALTARGET.HD4_OVERALL_SURVIVAL);
    	Instances anInstances = null;
		try {
			anInstances = ComputationKM.getWekaInstancesFromCaseBase(aCaseBase, aKMTargetConfig);
		} catch (CaseBaseException e1) {
			e1.printStackTrace();
			System.exit(-1);
		}
    	try {
			//anInstances = SurvivalCutOffDiscretizedFct.makeNominal(anInstances, aKMTargetConfig, aFileLoaderFlag, "", I2b2Connector.getI2b2FactsMetaDataList());
    		anInstances = ComputationDiscretize.makeNominal(anInstances, aKMTargetConfig);
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(-1);
		}
    	
    	KMChart aKMChart = new KMChart(anInstances, aKMTargetConfig);
		String PARAMETER_COLUMN;
		
//		PARAMETER_COLUMN = "00 - Main Data \\ Sex";
//		PARAMETER_COLUMN = "00 - Main Data \\ M-protein heavy chain";
//		PARAMETER_COLUMN = "02 - ON STUDY \\ Stage of disease at diagnosis (at diagnosis)";
//		PARAMETER_COLUMN = "02 - ON STUDY \\ Stage of disease";
//		PARAMETER_COLUMN = "00 - Main Data \\ ISS";
//		aComputationKM.drawKM(PARAMETER_COLUMN);
		
//		PARAMETER_COLUMN = "00 - Main Data \\ ISS"; String PARAMETER_VALUE = "I";
//		PARAMETER_COLUMN = "00 - Main Data \\ ISS"; String PARAMETER_VALUE = "II";
//		PARAMETER_COLUMN = "00 - Main Data \\ ISS"; String PARAMETER_VALUE = "III";
//		aComputationKM.drawKM__oneAgainstOthers(PARAMETER_COLUMN, PARAMETER_VALUE);
		
//		aComputationKM.drawKM__SurvivalTarget_ReferenceArea();
		
//		PARAMETER_COLUMN = "00 - Main Data \\ Age";
//		aComputationKM.drawKM(PARAMETER_COLUMN);
		
//		double aWeight = aComputationKM.calculateImpactKM(PARAMETER_COLUMN);
		
		String VALUE1, VALUE2;
		PARAMETER_COLUMN = "00 - Main Data \\ ISS"; VALUE1="I"; VALUE2="I"; 
		ComputationKM.calculateLocalSimilarityKM(anInstances, aStepShapeFlag, aKMTargetConfig, PARAMETER_COLUMN, VALUE1, VALUE2);
		PARAMETER_COLUMN = "00 - Main Data \\ ISS"; VALUE1="I"; VALUE2="II"; 
		ComputationKM.calculateLocalSimilarityKM(anInstances, aStepShapeFlag, aKMTargetConfig, PARAMETER_COLUMN, VALUE1, VALUE2);
		PARAMETER_COLUMN = "00 - Main Data \\ ISS"; VALUE1="I"; VALUE2="III"; 
		ComputationKM.calculateLocalSimilarityKM(anInstances, aStepShapeFlag, aKMTargetConfig, PARAMETER_COLUMN, VALUE1, VALUE2);
		aKMChart.drawKM(PARAMETER_COLUMN);
		
//		aComputationKM.computeSurvivalMeasures(PARAMETER_COLUMN);
	}

}
