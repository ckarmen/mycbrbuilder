/**
 * (2019) Heidelberg University, Institute of Medical Biometry and Informatics (IMBI), Germany
 * 
 * This file is part of myCbrBuilder.
 *  
 * myCbrBuilder is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * myCbrBuilder is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with vivaGen. If not, see <http://www.gnu.org/licenses/>.
 * 
 **/
package similarity.computation;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Set;

import org.cliommics.vivagen.Instances;
import org.cliommics.vivagen.survivalanalysis.DiscretizeSurvival;
import org.cliommics.vivagen.survivalanalysis.SurvivalCurve;
import org.cliommics.vivagen.survivalanalysis.SurvivalList;

import core.CaseBaseUtils;
import core.Utils;
import core.Utils.CUSTOM_DATAPATH;
import de.dfki.mycbr.core.DefaultCaseBase;
import de.dfki.mycbr.core.Project;
import de.dfki.mycbr.core.casebase.Instance;
import de.dfki.mycbr.core.model.AttributeDesc;
import de.dfki.mycbr.core.model.Concept;
import de.dfki.mycbr.core.model.DoubleDesc;
import de.dfki.mycbr.core.model.FloatDesc;
import de.dfki.mycbr.core.model.IntegerDesc;
import de.dfki.mycbr.core.model.SymbolDesc;
import de.dfki.mycbr.core.similarity.SymbolFct;
import de.dfki.mycbr.core.similarity.config.AmalgamationConfig;
import exceptions.CaseBaseException;
import exceptions.SimilarityMeasureException;
import similarity.computation.config.KaplanMeierConfig;
import similarity.computation.config.KaplanMeierTargetConfig;
import similarity.computation.config.KaplanMeierTargetConfig.SURVIVALTARGET;
import similarity.config.DiscretizationConfig;
import similarity.config.SymbolSimilarityConfig;
import similarity.desc.ExtendedDoubleDesc;
import similarity.desc.ExtendedIntegerDesc;
import similarity.desc.ExtendedSymbolDesc;
import weka.core.Attribute;
import weka.core.DenseInstance;
import weka.core.ProtectedProperties;


/**
 * Supports Kaplan-Meier calculations and a graphical representation for CBR applications.
 * 
 * @author Christian Karmen
 *
 */
public class ComputationKM {

	private final static String itsNominalFilePrefix = "nominalized_";
	private final static String itsNominalFilePostfix = "nominalized.csv";
	
   
    
    
//    private final static File getNominalFile(){
//    	return getNominalFile("");
//    }
    
    public final static File getNominalFile(String theID){
//    	String theFileNameSeperator = ".";
    	
//    	if (theID.equals("")){
//    		theFileNameSeperator = "";
//    	}
    	
    	String theID_noSeraratorChar = theID.replace(File.separator, "");
    	String theID_noWhiteSpaces = theID_noSeraratorChar.replace(" ", "");
    	
    	return new File(
    			Utils.getCustomDataPath(CUSTOM_DATAPATH.TEMP)
    			+ itsNominalFilePrefix
//    			+ File.separator 
    			+ theID_noWhiteSpaces 
//    			+ theFileNameSeperator 
    			+ itsNominalFilePostfix);
    }
    
    
    
    /**
     * Exports cases from a myCBR case base to weka instances.
     * 
     * @param theCaseBase Case Base of interest
     * 
     * @return case base in weka instances format
     * @throws CaseBaseException Error while access the case base
     */
    public static Instances getWekaInstancesFromCaseBase(DefaultCaseBase theCaseBase, KaplanMeierTargetConfig theKMTargetConfig) throws CaseBaseException{
    	ArrayList<String> anAttributeLabelList = CaseBaseUtils.getCaseBaseAttributeLabelList(theCaseBase);
    	return getWekaInstancesFromCaseBase(theCaseBase, anAttributeLabelList, theKMTargetConfig);
    }

    
    /**
     * Exports cases from a myCBR case base to weka instances.
     * 
     * @param theCaseBase Case Base of interest
     * @param theAttributeLabelList List of attribute labels that should be exported
     * 
     * @return case base in weka instances format
     * @throws CaseBaseException Error while access the case base
     */
    public static Instances getWekaInstancesFromCaseBase(DefaultCaseBase theCaseBase, ArrayList<String> theAttributeLabelList, KaplanMeierTargetConfig theKMTargetConfig) throws CaseBaseException{
    	Instances aReturnValue;
    	
    	final String aCaseIdLabel = "caseID";
    	final String anAttributeLabelPrefix = ""; //SimilarityMeasure.KAPLANMEIER.getName() + "_";
//    	System.out.println("DEBUG: anAttributeLabelPrefix = " + anAttributeLabelPrefix);    	
    	
//    	String anAttributeName;
    	String anAttributeValue;
    	weka.core.Attribute aWekaAttribute;
    	ArrayList<weka.core.Attribute> aWekaAttributeList = new ArrayList<>();
    	
    	int aNumberOfInstances = theCaseBase.getCases().size();   	
    	if (aNumberOfInstances < 1){
    		return null;
    	}
    	
    	System.out.println("DEBUG: Converting CaseBase into weka format...");
    	    	
    	// Creating the weka case base data structure    	
    	// add caseID at first...
    	aWekaAttribute = new Attribute(aCaseIdLabel, CaseBaseUtils.getCaseIdList(theCaseBase), new ProtectedProperties(new Properties()));
//    	System.out.println("DEBUG: CaseID type = " + Attribute.typeToString(aWekaAttribute.type()));
    	aWekaAttributeList.add(aWekaAttribute); 
    	
    	// ...then all other allowed values 		
    	for (String anAttributeLabel : theAttributeLabelList) {
    		AttributeDesc anAttributeDesc = CaseBaseUtils.getCaseBaseAttributeDescByLabel(theCaseBase, anAttributeLabel);
    		if (anAttributeDesc instanceof ExtendedSymbolDesc){
    			Set<String> allowed = ((ExtendedSymbolDesc)anAttributeDesc).getAllowedValues();
    			aWekaAttribute = new Attribute(anAttributeLabelPrefix + anAttributeDesc.getName(), new ArrayList<String>(allowed), new ProtectedProperties(new Properties()));
    			
    			for (String anAllowedValue : ((ExtendedSymbolDesc)anAttributeDesc).getAllowedValues()) {
    				aWekaAttribute.setStringValue(anAllowedValue);
//    				System.out.println("DEBUG: Adding '" + anAllowedValue + "' to weka attribute '" + aWekaAttribute.name() + "'");
				}    			
    		} else if (anAttributeDesc instanceof ExtendedDoubleDesc || anAttributeDesc instanceof ExtendedIntegerDesc){
    			aWekaAttribute = new Attribute (anAttributeDesc.getName(), new ProtectedProperties(new Properties()));
    			//System.out.println("DEBUG: Metadata of Attribute'" + anAttributeDesc.getName() + "': " + aWekaAttribute.getLowerNumericBound());
    		} else {
    			System.err.println("ERROR: Unsupported Desc-Type in Attribute: " + anAttributeDesc.getName());
    			continue;
    		}
    		
			aWekaAttributeList.add(aWekaAttribute); 
		}
    	weka.core.Instances aWekaInstances = new weka.core.Instances("Imported from myCBR CaseBase", aWekaAttributeList, aNumberOfInstances);
    	
    	// filling weka data structure with instances from mycbr casebase
    	int aNumberOfAttributes = theAttributeLabelList.size() + 1; // + caseID
//    	System.out.println("DEBUG: Number of found attributes = " + aNumberOfAttributes);
    	
    	DenseInstance aWekaInstance;
    	String aCaseId;
    	for (Instance aCase : theCaseBase.getCases()) {
    		aWekaInstance = (new DenseInstance(aNumberOfAttributes));
    		
    		// setting caseID at first
    		aCaseId = aCase.getName();
//    		MyCbrUtils.printLine();
//    		System.out.println("DEBUG: CaseID = " + aCaseId);
    		aWekaAttribute = aWekaAttributeList.get(getWekaAttributeIndexByName(aWekaAttributeList, aCaseIdLabel));	
    		aWekaInstance.setValue(aWekaAttribute, aCaseId);    		
    		
    		for (String anAttributeLabel : theAttributeLabelList) {		
				anAttributeValue = CaseBaseUtils.getCaseAttributeValue(aCase, anAttributeLabel).getValueAsString();
				
				aWekaAttribute = aWekaAttributeList.get(getWekaAttributeIndexByName(aWekaAttributeList, anAttributeLabel));
//				System.out.println("DEBUG: Attribute Name: " + aWekaAttribute.name());
//				System.out.println("DEBUG: Attribute Type: " + Attribute.typeToString(aWekaAttribute));
				
				// Skipping undefined values
				if (anAttributeValue.equals(Project.UNDEFINED_SPECIAL_VALUE)){
//					System.out.println("DEBUG: Skipping undefined value!");
					break;
				}
				
				// Skipping unknown values
				if (anAttributeValue.equals(Project.UNKNOWN_SPECIAL_VALUE)){
//					System.out.println("DEBUG: Skipping unknown value!");
					break;
				}
				
//				System.out.println("DEBUG: Trying to add attribute '" + anAttributeLabel + "' = " + anAttributeValue);				
				if (aWekaAttribute.isNominal()){
					aWekaInstance.setValue(aWekaAttribute, anAttributeValue);					
//					System.out.println("DEBUG: isNominal");
				}else if (aWekaAttribute.isNumeric()){
//					System.out.println("DEBUG: isNumeric");
					aWekaInstance.setValue(aWekaAttribute, Double.parseDouble(anAttributeValue));					
				} else{
					System.err.println("ERROR: Unhandled weka attribute type: " + aWekaAttribute.name() + " and it's value: " + anAttributeValue);
				}
			}
			aWekaInstances.add(aWekaInstance);
		}

    	aReturnValue = new Instances(aWekaInstances);    	
    	aReturnValue.sort(getWekaAttributeIndexByName(aWekaAttributeList, theKMTargetConfig.TargetSurvivalColumn));

    	// DEBUG: writing newly created weka instances (=casebase) to file
//        try {
//        	File aDebugFile = new File(getNominalFileBase() + "debug.casebase2wekaImport.csv");
//            System.out.print("DEBUG: Storing CaseBase->weka import results to \"" + aDebugFile.getAbsolutePath() + "\" ...");
//            CSVSaver saver = new CSVSaver();
//            saver.setInstances(aReturnValue);
//			saver.setFile(aDebugFile);
//	        saver.setFieldSeparator(itsNominalDelimiter);
//	        saver.writeBatch();
//	        System.out.println("done.");
//		} catch (IOException e) {
//			System.err.println("ERROR OCCURED:");
//			e.printStackTrace();
//		}

    	System.out.println("DEBUG: ...Converting done.");
        return aReturnValue;
    }
    
    
    
    
    private static int getWekaAttributeIndexByName(ArrayList<weka.core.Attribute> theAttributeList, String theAttributeLabel){
    	
    	for (int aIndex=0; aIndex<theAttributeList.size(); aIndex++) {
			if (theAttributeList.get(aIndex).name().equals(theAttributeLabel)){
				return aIndex;
			}
		}
    	return -1;
    }

    
    /**
     * Calculates the Survival List of a given nominal attribute.
     * 
     * @param theInstances case instances of interest
     * @param theKMTargetConfig target config of interest
     * @param theNominalAttribute
     * 
     * @return Survival List of given Nominal Attribute
     */
    public static SurvivalList getSurvivalList(
    		Instances theInstances, 
    		KaplanMeierTargetConfig theKMTargetConfig, 
    		NominalAttribute theNominalAttribute){
    	
    	SurvivalCurve aKaplanMeierCurve = new SurvivalCurve(theInstances);
        aKaplanMeierCurve.setParameters(
        		null,	//armColumn
        		null, 	//armValue
        		theNominalAttribute.KEY, 
        		theNominalAttribute.VALUE, 
        		theNominalAttribute.CRITERION,  
        		theKMTargetConfig.TargetSurvivalColumn, 
        		theKMTargetConfig.TargetStatusColumn, 
        		theKMTargetConfig.TargetStatusIndicator,
        		true); 
        return aKaplanMeierCurve.getSurvivalList();
    }
    
    
    /**
     * Computes area between two Kaplan-Meier-curves. 
     * CAUTION: Result is negative when AUC of value1 is below AUC of value2. 
     * 
     * @param theInstances weka instances to learn from
     * @param theKMTargetConfig Kaplan-Meier target config
     * @param theStepShapeFlag step shape flag
     * @param theNominalAttribute1 attribute value a
     * @param theNominalAttribute2 attribute value b
     *  
     * @return Area Between Survival Curves. Result can be negative!
     */
    public static double getAreaBetweenSurvivalCurves(
    		Instances theInstances, 
    		KaplanMeierTargetConfig theKMTargetConfig, 
    		boolean theStepShapeFlag, 
    		NominalAttribute theNominalAttribute1, 
    		NominalAttribute theNominalAttribute2){
    	
    	double aResult;
        SurvivalList s1 = getSurvivalList(theInstances, theKMTargetConfig, theNominalAttribute1);
        SurvivalList s2 = getSurvivalList(theInstances, theKMTargetConfig, theNominalAttribute2);
        
        if (theStepShapeFlag)
        	aResult =  s1.getStepShapedAreaBetweenSurvivalCurves(s2);
        else
        	aResult = s1.getTrapezoidShapedAreaBetweenSurvivalCurves(s2);
        
        //System.out.println("DEBUG: Calculating AreaBetweenSurvivalCurves (" + theNominalAttribute1.VALUE + " <> " + theNominalAttribute2.VALUE + "); Result = " + aResult);

        //return Math.abs(aResult);
        return aResult;
    }
    
    
    
//    /**
//     * todo
//     * 
//     * @param theNominalAttribute
//     */
//    private void computeSurvivalMeasures(String theNominalAttribute){
//        HashMap<SurvivalList, Double> attributeMap2 = 
//        		itsParameterSelection.computeSurvivalMeasures(
//        				includeStudyDocuments, 
//        				excludeStudyDocuments, 
//        				null,
//        				null,
////        				"HD4 \\ 01 - REGISTRATION & RANDOMIZATION \\ Randomization arm", //null, //Configuration.RANDOMIZATION_ARM, 
////        				"'a: vad'", //null, //Configuration.RANDOMIZATION_ARM_A, 
//        				itsTargetSurvivalColumn, 
//        				itsTargetStatusColumn, 
//        				itsTargetStatusIndicator);
//        MapValueComparator<SurvivalList, Double> comparator2 = new MapValueComparator<>(attributeMap2);
//        TreeMap<SurvivalList, Double> sortedAttributeMap2 = new TreeMap<>(comparator2);
//        sortedAttributeMap2.putAll(attributeMap2);
//        for(SurvivalList key : sortedAttributeMap2.keySet())
//        {
//        	if(key.getParameterColumn().contains(theNominalAttribute))
//        	{
//        		for(String label : itsInstances.getAttributeLabels(key.getParameterColumn()).keySet())
//        		{
//                    System.out.println("computeSurvivalMeasures:  " + attributeMap2.get(key) + " " + key.getParameterColumn() + " " + label);
//        		}
//        	}
//        }
//    }
    
    
    /**
     * Gets all possible values of a given parameter.
     * 
     * @param theInstances case instances of interest
     * @param theAttribute attribute of interest
     * @param theSortedFlag	true=sorted; false=unsorted
     * @return List of all possible values
     */
    public static ArrayList<String> getAttributeLabels(Instances theInstances, String theAttribute, boolean theSortedFlag){
    	ArrayList<String> anAttributeLabelList = new ArrayList<>();
    	
    	for (Map.Entry<String, Integer> anEntry : theInstances.getAttributeLabels(theAttribute).entrySet()) {
			anAttributeLabelList.add(anEntry.getKey());
//			System.out.println("DEBUG: Found Attribute Label: " + anEntry.getKey());
		}
    	
    	if (theSortedFlag){
    		Collections.sort(anAttributeLabelList);
    	}
    	
    	return anAttributeLabelList;
    }
    



	
	
	/**
	 * Calculates the survival impact of a given attribute (with KM similarity measure technique). 
	 * 
     * @param theWekaTrainingInstances weka instances to learn from
     * @param theKMTargetConfig Kaplan-Meier target config
     * @param theStepShapeFlag step shape flag
	 * @param theAttributeLabel the attribute of interest
	 * 
	 * @return survival impact factor (used for weight)
	 */
	public static double calculateImpactKM(
			Instances theWekaTrainingInstances, 
			boolean theStepShapeFlag, 
			KaplanMeierTargetConfig theKMTargetConfig, 
			String theAttributeLabel){
		
		long startTime = System.currentTimeMillis();
		
		// calculating the reference area in KMs of survival status (e.g. "dead" against "not dead")
		double aReferenceArea = getAreaBetweenValueAndNotValue(
				theWekaTrainingInstances, 
				theStepShapeFlag, 
				theKMTargetConfig, 
				theKMTargetConfig.TargetStatusColumn, 
				theKMTargetConfig.TargetStatusIndicator);
		System.out.println("DEBUG: Reference Area = " + aReferenceArea);
		
		double aMaxAreaDiff = getMaxAreaDiff(theWekaTrainingInstances, theStepShapeFlag, theKMTargetConfig, theAttributeLabel);		
	
        long aTimeInMS = System.currentTimeMillis() - startTime;
        System.out.println("DEBUG: calculateImpactKM() time consumption = " + aTimeInMS + "ms");
		
		// Impact is the ratio between maxArea and referenceArea 
		double anImpact = Math.abs(aMaxAreaDiff / aReferenceArea);
		System.out.println("DEBUG: Impact of attribute \"" + theAttributeLabel + "\" = " + anImpact);
	
//		return anImpact;
		return anImpact * 100;	// FIXME better normalize with max area within observed time frame
	}
	
	
	/**
	 * Calculates the area between two values in a KM diagram. 
	 * CAUTION: Result is negative when AUC of value is below AUC of others. 
	 * 
     * @param theInstances weka instances to learn from
     * @param theKMTargetConfig Kaplan-Meier target config
     * @param theStepShapeFlag step shape flag
	 * @param theAttributeLabel
	 * @param theAttributeValue
	 * 
	 * @return Area between two values in a KM diagram. Result can be negative!
	 */
	public static double getAreaBetweenValueAndNotValue(
			Instances theInstances, 
			boolean theStepShapeFlag, 
			KaplanMeierTargetConfig theKMTargetConfig,
			String theAttributeLabel,
			String theAttributeValue){
		
		NominalAttribute aNARef1 = new NominalAttribute(theAttributeLabel, theAttributeValue, true);
		NominalAttribute aNARef2 = new NominalAttribute(theAttributeLabel, theAttributeValue, false);
		
		return getAreaBetweenSurvivalCurves(theInstances, theKMTargetConfig, theStepShapeFlag, aNARef1, aNARef2);
	}
	
	
	
	/**
	 * Calculates the AUC differences for each possible value pair and returns the maximum.
	 * 
     * @param theInstances weka instances to learn from
     * @param theStepShapeFlag step shape flag
     * @param theKMTargetConfig Kaplan-Meier target config
	 * @param theAttributeLabel attribute of interest
	 * 
	 * @return maximum AUC difference
	 */
	private static double getMaxAreaDiff(
			Instances theInstances, 
			boolean theStepShapeFlag, 
			KaplanMeierTargetConfig theKMTargetConfig, 
			String theAttributeLabel){
		
		NominalAttribute aNA1, aNA2;
		double anAreaDiff;
		double aMaxAreaDiff = 0;
		ArrayList<String> theValueList = getAttributeLabels(theInstances, theAttributeLabel, true);
		
		for (int i = 0; i < theValueList.size()-1; i++) {
			for (int j = i+1; j < theValueList.size(); j++) {
				aNA1 = new NominalAttribute(theAttributeLabel, theValueList.get(i), true);
				aNA2 = new NominalAttribute(theAttributeLabel, theValueList.get(j), true);
				anAreaDiff = Math.abs(getAreaBetweenSurvivalCurves(theInstances, theKMTargetConfig, theStepShapeFlag, aNA1, aNA2));
//				System.out.println("DEBUG: diff(" + theValueList.get(i) + ";" + theValueList.get(j) +") = " + anAreaDiff);
				if (anAreaDiff > aMaxAreaDiff){
					aMaxAreaDiff = anAreaDiff;
				}
			}
		}
//		System.out.println("DEBUG: Max Area Diff= " + aMaxAreaDiff);
		
		return aMaxAreaDiff;
	}
	
	
	
	/**
	 * Calculates the local similarity using the KM similarity measure.
	 * 
	 * @param theInstances weka instances to learn from
     * @param theKMTargetConfig Kaplan-Meier target config
     * @param theStepShapeFlag step shape flag
	 * @param theAttributeLabel attribute of interest
	 * @param theValuelabel1 first value for comparison
	 * @param theValuelabel2 other value for comparison
	 * @return calculated local similarity
	 */
	public static double calculateLocalSimilarityKM(
			Instances theInstances, 
			boolean theStepShapeFlag, 
			KaplanMeierTargetConfig theKMTargetConfig, 
			String theAttributeLabel, 
			String theValuelabel1, 
			String theValuelabel2){
		
		double aReturnValue;
		
		// FIXME Special Values berücksichtigen (unknown, undefined)
		if (theValuelabel1 == theValuelabel2){
			aReturnValue = 1;
			System.out.println("DEBUG: Calculated local similarity (" + theValuelabel1 + ";" + theValuelabel2 + ") = " + aReturnValue);
			return aReturnValue;
		}
		
		if (theInstances.getAttributeIndex(theAttributeLabel) < 0){
			System.err.println("ERROR: Attribute '" + theAttributeLabel + "' not found!");
			return -1;
		}
		
		NominalAttribute aNA1 = new NominalAttribute(theAttributeLabel, theValuelabel1, true);
		NominalAttribute aNA2 = new NominalAttribute(theAttributeLabel, theValuelabel2, true);
		double anAreaDiff = Math.abs(getAreaBetweenSurvivalCurves(theInstances, theKMTargetConfig, theStepShapeFlag, aNA1, aNA2));
		
		double aMaxAreaDiff = getMaxAreaDiff(theInstances, theStepShapeFlag, theKMTargetConfig, theAttributeLabel);
		
		aReturnValue = 1 - (anAreaDiff / aMaxAreaDiff);
		System.out.println("DEBUG: Calculated local similarity (" + theValuelabel1 + ";" + theValuelabel2 + ") = " + aReturnValue);
		
		return aReturnValue;
	}

	
	
	/**
	 * Calculates the Look-Up-Table (LUT) for the local similarity metric "KAPLANMEIER".
	 * 
     * @param theCaseInstances weka instances to learn from
     * @param theKMTargetConfig Kaplan-Meier target config
     * @param theStepShapeFlag step shape flag
	 * @param theDiscretizedSymbolFct The target LUT for the local similarity metric
	 */
	public static void setSimilarityMatrix_KM(
			Instances theCaseInstances,
			boolean theStepShapeFlag, 
			KaplanMeierTargetConfig theKMTargetConfig, 
			SymbolFct theDiscretizedSymbolFct){
		
		double aCaluclatedSimilarity;		
		SymbolDesc aSymbolDesc = theDiscretizedSymbolFct.getDesc();
		
		for (String anAllowedValue1 : theDiscretizedSymbolFct.getDesc().getAllowedValues()) {
			for (String anAllowedValue2 : theDiscretizedSymbolFct.getDesc().getAllowedValues()) {						
				aCaluclatedSimilarity = ComputationKM.calculateLocalSimilarityKM(
						theCaseInstances, 
						theStepShapeFlag, 
						theKMTargetConfig, 
						aSymbolDesc.getName(),
						anAllowedValue1, 
						anAllowedValue2);

//				System.out.println("DEBUG: Calculated local Similarity (" + anAllowedValue1 + ";" + anAllowedValue2 + ") = " + aCaluclatedSimilarity);
				theDiscretizedSymbolFct.setSimilarity(anAllowedValue1, anAllowedValue2, aCaluclatedSimilarity);
			}
		}
	}
	
	
	
	/**
	 * Calculates the Look-Up-Table (LUT) for the local similarity metric "KAPLANMEIER".
	 * 
	 * @param theCaseInstances weka instances to learn from
	 * @param theKMConfig Kaplan-Meier configuration
	 * @param theSymbolFct The target LUT for the local similarity metric
	 */
	public static void setSimilarityMatrix_KM(
			Instances theCaseInstances,
			KaplanMeierConfig theKMConfig, 
			SymbolFct theSymbolFct){
		
		setSimilarityMatrix_KM(theCaseInstances, theKMConfig.StepShapeFlag, theKMConfig.KMTargetConfig, theSymbolFct);
	}
	
	
	
	/**
	 * Generates an Amalgam function (global similarity function) for the KAPLAN-MEIER metric. 
	 * 
	 * @param theConcept Concept of interest
	 * @param theCaseDescList case description list
	 * @param theCaseSolutionList case solution list
	 * @param theTrainingCaseBase training case base
	 * @param theKMConfig Kaplan-Meier configuration
	 * @throws CaseBaseException Error while access the case base
	 * @throws SimilarityMeasureException Error while preparing similarity measure
	 */
	public static void generateAmalgamFct_KAPLANMEIER(
			Concept theConcept, 
			ArrayList<String> theCaseDescList, 
			ArrayList<String> theCaseSolutionList, 
			DefaultCaseBase theTrainingCaseBase,
			KaplanMeierConfig theKMConfig) 
					throws CaseBaseException, SimilarityMeasureException{
		
		Computation.generateAmalgamFct(
				"KAPLANMEIER", 
				theConcept, 
				theCaseDescList, 
				theCaseSolutionList,
				DiscretizationConfig.SURVIVAL_CUTOFF, 
				SymbolSimilarityConfig.KAPLANMEIER, 
				theTrainingCaseBase, 
				AmalgamationConfig.SQUARED_SUM, 
				theKMConfig);
	}

	  /**
	   * Performs a survival analysis for all attributes of the given nominal attribute
	   * and return the value with the highest survival curve. 
	   * 
	   * @param theCaseBase Case base to analyze
	   * @param theAttributeLabel Attribute to analyze 
	   * @param theSurvivaltarget 
	   * @return value with the highest survival curve
	   * 
	   * @throws CaseBaseException Error while access the case base. 
	   */
	  public static String getValueWithHighestKMCurve(DefaultCaseBase theCaseBase, String theAttributeLabel, SURVIVALTARGET theSurvivaltarget) throws CaseBaseException{
		  String anAttributeLabel = theAttributeLabel;
		  KaplanMeierTargetConfig aKMTargetConfig = new KaplanMeierTargetConfig(theSurvivaltarget);
		  ArrayList<String> anAttributeList = new ArrayList<>();
		  anAttributeList.add(anAttributeLabel);
		  anAttributeList.add(aKMTargetConfig.TargetStatusColumn);
		  anAttributeList.add(aKMTargetConfig.TargetSurvivalColumn);
		  
		  Instances aWekaInstances = ComputationKM.getWekaInstancesFromCaseBase(theCaseBase, anAttributeList, aKMTargetConfig);		  
		  
		  // check if attribute is numeric
		  boolean isNumeric = CaseBaseUtils.getCaseBaseAttributeDescByLabel(theCaseBase, anAttributeLabel) instanceof DoubleDesc
				  || CaseBaseUtils.getCaseBaseAttributeDescByLabel(theCaseBase, anAttributeLabel) instanceof FloatDesc
				  || CaseBaseUtils.getCaseBaseAttributeDescByLabel(theCaseBase, anAttributeLabel) instanceof IntegerDesc;
		  
		  boolean isnominal = CaseBaseUtils.getCaseBaseAttributeDescByLabel(theCaseBase, anAttributeLabel) instanceof SymbolDesc;
		  
		  // only numeric and nominal attributes are allowed. 
		  if (!isnominal && !isNumeric) {	
			  System.err.println("ERROR: getLabelOfHighestKMCurve() used with unsupported AttributeDesc for '" + anAttributeLabel + "':  " 
					  + CaseBaseUtils.getCaseBaseAttributeDescByLabel(theCaseBase, anAttributeLabel).getClass());
			  return null;
		  }
		  
		// Numeric attributes needs to be discretized in advance
		  if (isNumeric){	
//			  Instances aDescretizedInstances;
//			  double aCutOff;
			  try {
				  DiscretizeSurvival aDiscretizer = new DiscretizeSurvival(
						  aKMTargetConfig.TargetSurvivalColumn, 
						  aKMTargetConfig.TargetStatusColumn, 
						  aKMTargetConfig.TargetStatusIndicator, 
						  true);
				  aWekaInstances = aDiscretizer.makeNominal(aWekaInstances);
				  System.out.println("Cut-off: " + aDiscretizer.getCutPoints()[1][0]); 
//				  aCutOff = aDiscretizer.getCutPoints()[aWekaInstances.attribute(theAttributeLabel).index()][0];
//				  anAttributeLabel = "[DISCRETIZED]" + theAttributeLabel + "[DESC]";
			  } catch (Exception e) {
				  e.printStackTrace();
				  return null;
			  }
		  }
		  
		  // Generate list of allowed values
		  ArrayList<String> anAllowedValueList = new ArrayList<>();
		  for (Entry<String, Integer> anAttributeValue : aWekaInstances.getAttributeLabels(anAttributeLabel).entrySet()) {
			  anAllowedValueList.add(anAttributeValue.getKey());
		  }
//		  System.out.println("DEBUG: List of allowed values for attribute ' " + anAttributeLabel + "': " + anAllowedValueList.toString());
		  
		  String aValueWithHighestKMCurve = null;
		  double aMaxAST = -1;
		  double aCurrentAST;
		  //KaplanMeierCurve aKMCurve;
		  NominalAttribute aNominalAttribute;
		  for (String anAllowedValue : anAllowedValueList) {
			  aNominalAttribute = new NominalAttribute(anAttributeLabel, anAllowedValue, true);
			  //aKMCurve = new KaplanMeierCurve(aWekaInstances);
			  aCurrentAST = Math.abs(ComputationKM.getSurvivalList(aWekaInstances, aKMTargetConfig, aNominalAttribute).getStepShapedAreaBetweenSurvivalCurveAndTimeAxis()); 
			  if (aCurrentAST > aMaxAST){
				  aMaxAST = aCurrentAST;
				  aValueWithHighestKMCurve = anAllowedValue;
			  }
		  }
		  
		  return aValueWithHighestKMCurve;
	  }
	
}
