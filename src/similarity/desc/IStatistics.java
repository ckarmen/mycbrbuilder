/**
 * (2019) Heidelberg University, Institute of Medical Biometry and Informatics (IMBI), Germany
 * 
 * This file is part of myCbrBuilder.
 *  
 * myCbrBuilder is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * myCbrBuilder is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with vivaGen. If not, see <http://www.gnu.org/licenses/>.
 * 
 **/
package similarity.desc;

/**
 * Interface for statistical functions.
 * 
 * @author Christian Karmen
 *
 */
public interface IStatistics {
	
	/**
	 * Gets the standard variation.
	 * 
	 * @return standard variation
	 */
	public double getStandardDeviation();
	
	/**
	 * Sets the standard variation.
	 * 
	 * @param theStandardDeviation standard variation
	 */
	public void setStandardDeviation(double theStandardDeviation);
	
	/**
	 * Gets the mean value.
	 * 
	 * @return mean value
	 */
	public double getMeanValue();
	
	
	/**
	 * Sets the mean value.
	 * 
	 * @param theMeanValue mean value
	 */
	public void setMeanValue(double theMeanValue);
	
	
	/**
	 * Gets the median value.
	 * 
	 * @return median value
	 */
	public double getMedianValue();
	
	/**
	 * Sets the median value.
	 * 
	 * @param theMedianValue median value
	 */
	public void setMedianValue(double theMedianValue);
	
	
//	/**
//	 * Sets the minimal value which may appear as Number specified for this
//	 * description to min. Does nothing if min >= max.
//	 * 
//	 * @param min
//	 *            minimal value appearing as Number for this description
//	 */
//	public void setMin(double min);
//
//	/**
//	 * Gets the minimal Number that can be used as value for this description
//	 * 
//	 * @return minimal value for attributes of this description
//	 */
//	public double getMin();
//
//	/**
//	 * Sets the max value which may appear as Number specified for this
//	 * description to max.
//	 * Does nothing if max <=min
//	 * 
//	 * @param max
//	 *            max value appearing as Number for this description
//	 */
//	public void setMax(double max);
//	
//	/**
//	 * Gets the maximal Number that can be used as value for this description
//	 * 
//	 * @return maximal value for attributes of this description
//	 */
//	public double getMax();
	
}
