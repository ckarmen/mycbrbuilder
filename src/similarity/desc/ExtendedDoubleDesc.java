/**
 * (2019) Heidelberg University, Institute of Medical Biometry and Informatics (IMBI), Germany
 * 
 * This file is part of myCbrBuilder.
 *  
 * myCbrBuilder is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * myCbrBuilder is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with vivaGen. If not, see <http://www.gnu.org/licenses/>.
 * 
 **/
package similarity.desc;

import core.Utils;
import de.dfki.mycbr.core.DefaultCaseBase;
import de.dfki.mycbr.core.model.Concept;
import de.dfki.mycbr.core.model.DoubleDesc;
import exceptions.CaseBaseException;
import similarity.computation.config.KaplanMeierConfig;
import similarity.computation.config.VDMConfig;
import similarity.config.NormalizationConfig;
import similarity.config.SymbolSimilarityConfig;
import similarity.fct.DVDMDiscretizedFct;
import similarity.fct.IVDMDiscretizedFct;
import similarity.fct.NormalizedNumberFct;
import similarity.fct.RandomNumberFct;
import similarity.fct.SurvivalCutOffDiscretizedFct;
import similarity.fct.WVDMDiscretizedFct;

/**
 * Class for an attribute description for decimal values. Implements additional similarity functions.  
 * 
 * @author Christian Karmen
 *
 */
public class ExtendedDoubleDesc extends DoubleDesc implements IStatistics{
	
	protected double itsMeanValue = Double.NaN;
	protected double itsMedianValue = Double.NaN;
	protected double itsStandardDeviation = Double.NaN;
	
	protected Object itsMetadata;

	/**
	 * Class for an extended attribute description for decimal values. Implements additional similarity functions.
	 * 
	 * @param owner Concept to assign the attribute
	 * @param name Label of the attribute
	 * @param min minimum allowed value
	 * @param max maximum allowed value
	 * 
	 * @throws Exception General error while creating attribute 
	 */
	public ExtendedDoubleDesc(
			Concept owner, 
			String name, 
			double min, 
			double max) 
					throws Exception {
		
		super(owner, name, min, max);
	}
	
	
	/**
	 * Adds a local similarity function based on randomization. 
	 * CAUTION: Only for validation purpose!
	 * 
	 * @param name Label of local similarity function
	 * @param active true=enables similarity function; false=disables similarity function
	 * 
	 * @return created local similarity function
	 */
    public final RandomNumberFct addRandomNumberFct(
    		final String name, 
    		final boolean active) {
    	
    	Utils.printTitle("Adding Random Similarity to attribute '" + getName() + "'");
    	
    	RandomNumberFct f = new RandomNumberFct(owner.getProject(), this, name);
        addFunction(f, active);
        return f;
    }
	
	
	/**
	 * Adds a local similarity function based on normalization. 
	 * 
	 * @param name Label of local similarity function
	 * @param active true=enables similarity function; false=disables similarity function
	 * @param theNormalizationConfig Normalization type
	 * 
	 * @return created local similarity function
	 */
    public final NormalizedNumberFct addNormalizedNumberFct(
    		final String name, 
    		final boolean active,
    		NormalizationConfig theNormalizationConfig) {
    	
    	Utils.printTitle("Adding Normalization to attribute '" + getName() + "'");
    	
    	NormalizedNumberFct f = new NormalizedNumberFct(owner.getProject(), this, name, theNormalizationConfig);
        addFunction(f, active);
        return f;
    }
    
    
	/**
	 * Adds a local similarity function based on "Discretized VDM" (DVDM). 
	 * 
	 * @param name Label of local similarity function
	 * @param active true=enables similarity function; false=disables similarity function
	 * @param theTrainingCaseBase Case Base to lean from
	 * @param theVDMConfig VDM specific configuration
	 * @param theSymbolSimilarityConfig VDM type configuration
	 * 
	 * @return created local similarity function
	 * @throws CaseBaseException Error while access to case base
	 */
    public final DVDMDiscretizedFct addDVDMFct(
    		final String name, 
    		final boolean active, 
    		DefaultCaseBase theTrainingCaseBase, 
    		VDMConfig theVDMConfig,
    		SymbolSimilarityConfig theSymbolSimilarityConfig) throws CaseBaseException {
    	
    	Utils.printTitle("Adding DVDM to attribute '" + getName() + "'");
    	
    	// FIXME anzahl von kategorien in GUI vorgeben lassen
    	int aNumberOfDiscreteIntervals = Math.max(theVDMConfig.OutputClassValueList.size(), DVDMDiscretizedFct.getNumberOfMinIntervals());
    	DVDMDiscretizedFct f = new DVDMDiscretizedFct(
    			owner.getProject(), 
    			this, 
    			name, 
    			theTrainingCaseBase, 
    			theVDMConfig, 
    			aNumberOfDiscreteIntervals, 
    			theSymbolSimilarityConfig);
        addFunction(f, active);
        return f;
    }
    
    
	/**
	 * Adds a local similarity function based on "Interpolated VDM" (IVDM). 
	 * 
	 * @param name Label of local similarity function
	 * @param active true=enables similarity function; false=disables similarity function
	 * @param theTrainingCaseBase Case Base to lean from
	 * @param theVDMConfig VDM specific configuration
	 * @param theSymbolSimilarityConfig VDM type configuration
	 * 
	 * @return created local similarity function
	 * @throws CaseBaseException Error while access to case base
	 */
    public final IVDMDiscretizedFct addIVDMFct(
    		final String name, 
    		final boolean active, 
    		DefaultCaseBase theTrainingCaseBase,
    		VDMConfig theVDMConfig,
    		SymbolSimilarityConfig theSymbolSimilarityConfig) throws CaseBaseException {
    	
    	Utils.printTitle("Adding IVDM to attribute '" + getName() + "'");
    	
    	// FIXME anzahl von kategorien in GUI vorgeben lassen
    	int aNumberOfDiscreteIntervals = Math.max(theVDMConfig.OutputClassValueList.size(), IVDMDiscretizedFct.getNumberOfMinIntervals());
    	IVDMDiscretizedFct f = new IVDMDiscretizedFct(
    			owner.getProject(), 
    			this, 
    			name, 
    			theTrainingCaseBase, 
    			theVDMConfig, 
    			aNumberOfDiscreteIntervals, 
    			theSymbolSimilarityConfig);
        addFunction(f, active);
        return f;
    }
    
    
	/**
	 * Adds a local similarity function based on "Windowed VDM" (WVDM). 
	 * 
	 * @param name Label of local similarity function
	 * @param active true=enables similarity function; false=disables similarity function
	 * @param theTrainingCaseBase Case Base to lean from
	 * @param theVDMConfig VDM specific configuration
	 * @param theSymbolSimilarityConfig VDM type configuration
	 * 
	 * @return created local similarity function
	 * @throws CaseBaseException Error while access to case base
	 */
    public final WVDMDiscretizedFct addWVDMFct(
    		final String name, 
    		final boolean active, 
    		DefaultCaseBase theTrainingCaseBase, 
    		VDMConfig theVDMConfig,
    		SymbolSimilarityConfig theSymbolSimilarityConfig) throws CaseBaseException {
    	
    	Utils.printTitle("Adding WVDM to attribute '" + getName() + "'");
    	
    	// FIXME anzahl von kategorien in GUI vorgeben lassen
    	int aNumberOfDiscreteIntervals = Math.max(theVDMConfig.OutputClassValueList.size(), WVDMDiscretizedFct.getNumberOfMinIntervals());
    	WVDMDiscretizedFct f = new WVDMDiscretizedFct(
    			owner.getProject(), 
    			this, 
    			name, 
    			theTrainingCaseBase, 
    			theVDMConfig, 
    			aNumberOfDiscreteIntervals, 
    			theSymbolSimilarityConfig);
        addFunction(f, active);
        return f;
    }
    
    
	/**
	 * Adds a local similarity function based on "Windowed VDM" (WVDM). 
	 * 
	 * @param name Label of local similarity function
	 * @param active true=enables similarity function; false=disables similarity function
	 * @param theTrainingCaseBase Case Base to lean from
	 * @param theKMConfig Kaplan-Meier specific configuration
	 * 
	 * @return created local similarity function
	 */
    public final SurvivalCutOffDiscretizedFct addSurvivalCutOffDiscretizedFct(
    		final String name, 
    		final boolean active, 
    		DefaultCaseBase theTrainingCaseBase, 
    		KaplanMeierConfig theKMConfig
    		) {
    	
    	Utils.printTitle("Adding SURVIAL CUT-OFF to attribute '" + getName() + "'");
    	
    	SurvivalCutOffDiscretizedFct f = new SurvivalCutOffDiscretizedFct(
    			owner.getProject(), 
    			this, 
    			name, 
    			theTrainingCaseBase,
    			theKMConfig
    			);
    	
        addFunction(f, active);
        return f;
    }

    
	public double getMeanValue(){
		return itsMeanValue;
	}
	
	
	public double getMedianValue() {
		return itsMedianValue;
	}
	
	
	public double getStandardDeviation() {
		return itsStandardDeviation;
	}


	public void setMedianValue(double theMedianValue) {
		this.itsMedianValue = theMedianValue;
		
//		owner.getProject().cleanInstances(owner, this);
//		setChanged();
//		notifyObservers();
	}
    

	public void setMeanValue(double theMeanValue) {
		this.itsMeanValue = theMeanValue;
		
//		owner.getProject().cleanInstances(owner, this);
//		setChanged();
//		notifyObservers();
	}


	public void setStandardDeviation(double theStandardDeviation) {
		this.itsStandardDeviation = theStandardDeviation;
		
//		owner.getProject().cleanInstances(owner, this);
//		setChanged();
//		notifyObservers();
	}



	public Object getMetadata() {
		return itsMetadata;
	}



	public void setItsMetadata(Object theMetadata) {
		this.itsMetadata = theMetadata;
	}
	
	
	

}
