/**
 * (2019) Heidelberg University, Institute of Medical Biometry and Informatics (IMBI), Germany
 * 
 * This file is part of myCbrBuilder.
 *  
 * myCbrBuilder is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * myCbrBuilder is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with vivaGen. If not, see <http://www.gnu.org/licenses/>.
 * 
 **/
package similarity.desc;

import java.util.Set;

import core.Utils;
import de.dfki.mycbr.core.model.Concept;
import de.dfki.mycbr.core.model.SymbolDesc;
import de.dfki.mycbr.core.similarity.SymbolFct;
import similarity.fct.DiscretizedSymbolFct;

/**
 * @author Christian Karmen
 *
 */
public class ExtendedSymbolDesc extends SymbolDesc{// implements IStatistics{
	
	Object itsMetadata;

	/**
	 * @param owner
	 * @param name
	 * @param allowedValues
	 * @throws Exception
	 */
	public ExtendedSymbolDesc(
			Concept owner, 
			String name, 
			Set<String> allowedValues) 
					throws Exception {
		
		super(owner, name, allowedValues);
		
//		MyCbrUtils.printTitle("Processing attribute '" + getName() + "'");
	}
	
	@Override
    public SymbolFct addSymbolFct(final String name,
            final boolean active) {
		
//		Utils.printTitle("Adding nominal attribute '" + getName() + "'");
		
		return super.addSymbolFct(name, active);
    }
	
    
    /**
     * 
     * @param name
     * @param theMinValue
     * @param theMaxValue
     * @param active
     * @return created local similarity function
     */
    public final DiscretizedSymbolFct addDiscretizedSymbolFct(
    		final String name, 
    		double theMinValue, 
    		double theMaxValue, 
    		final boolean active 
//    		SymbolSimilarityConfig theSymbolSimilarityConfig, 
//    		DefaultCaseBase theCaseBase, 
    		) {
    	
//    	MyCbrUtils.printTitle("Adding nominal (discretized) attribute '" + getName() + "'");
    	
//    	DefaultCaseBase aTrainingCaseBase = getOwner().getProject().getCaseBases().get(0);
    	DiscretizedSymbolFct f = new DiscretizedSymbolFct(
    			owner.getProject(), 
    			this, 
    			name, 
    			theMinValue, 
    			theMaxValue 
//    			theSymbolSimilarityConfig, 
//    			theCaseBase, 
    			);
        addFunction(f, active);
        return f; 
    }

	public Object getMetadata() {
		return itsMetadata;
	}

	public void setMetadata(Object itsMetadata) {
		this.itsMetadata = itsMetadata;
	}

    
    
    
    
    

/*
	@Override
	public double getStandardDeviation() {
		// TODO Auto-generated method stub
		return 0;
	}


	@Override
	public void setStandardDeviation(double theStandardDeviation) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public double getMeanValue() {
		// TODO Auto-generated method stub
		return 0;
	}


	@Override
	public void setMeanValue(double theMeanValue) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public double getMedianValue() {
		// TODO Auto-generated method stub
		return 0;
	}


	@Override
	public void setMedianValue(double theMedianValue) {
		// TODO Auto-generated method stub
		
	}


//	@Override
//	public void setMin(double min) {
//		// TODO Auto-generated method stub
//		
//	}
//
//
//	@Override
//	public double getMin() {
//		// TODO Auto-generated method stub
//		return 0;
//	}
//
//
//	@Override
//	public void setMax(double max) {
//		// TODO Auto-generated method stub
//		
//	}
//
//
//	@Override
//	public double getMax() {
//		// TODO Auto-generated method stub
//		return 0;
//	}
*/    

}
