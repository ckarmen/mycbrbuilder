/**
 * (2019) Heidelberg University, Institute of Medical Biometry and Informatics (IMBI), Germany
 * 
 * This file is part of myCbrBuilder.
 *  
 * myCbrBuilder is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * myCbrBuilder is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with vivaGen. If not, see <http://www.gnu.org/licenses/>.
 * 
 **/
package validation;

/**
 * JUnit test cases for class "CohortAnalysis".
 * 
 * @author Christian Karmen
 *
 */
public class TestCohortAnalysis {


//	@Test
//	public void testOutcomeAnalysis() {
//		fail("Not yet implemented");
//	}
//
//	@Test
//	public void testAddSimilarCaseOutcomeCaseDescriptionCaseOutcomeDescriptionCaseDescriptionCaseOutcomeDescriptionDouble() {
//		fail("Not yet implemented");
//	}
//
//	@Test
//	public void testAddSimilarCaseOutcomeSimilarCaseComparison() {
//		fail("Not yet implemented");
//	}
//
//	@Test
//	public void testAddSimilarCaseOutcomeList() {
//		fail("Not yet implemented");
//	}
//
//	@Test
//	public void testGetLabel() {
//		fail("Not yet implemented");
//	}
//
//	@Test
//	public void testSetLabel() {
//		fail("Not yet implemented");
//	}
//
//	@Test
//	public void testGetNumberOfCases() {
//		fail("Not yet implemented");
//	}
//
//	@Test
//	public void testGetNumberOfCasesWithArmLabel() {
//		fail("Not yet implemented");
//	}
//
//	@Test
//	public void testGetSimilarCaseOutcomeList() {
//		fail("Not yet implemented");
//	}
//
//	@Test
//	public void testGetSimilarCaseOutcomeListString() {
//		fail("Not yet implemented");
//	}
//
//	@Test
//	public void testGetArmRate() {
//		fail("Not yet implemented");
//	}
//
//	@Test
//	public void testGetSurvivalTypeRate() {
//		fail("Not yet implemented");
//	}
//
//	@Test
//	public void testGetAverageSimilarity() {
//		fail("Not yet implemented");
//	}
//
//	@Test
//	public void testGetAverageSurvivalTime() {
//		fail("Not yet implemented");
//	}
//
//	@Test
//	public void testGetMedianSurvivalTime() {
//		fail("Not yet implemented");
//	}
//
//	@Test
//	public void testGetMeanSurvivalTime() {
//		fail("Not yet implemented");
//	}
//
//	@Test
//	public void testGetStandardDeviationSurvivalTime() {
//		fail("Not yet implemented");
//	}
//
//	@Test
//	public void testGetVarianceSurvivalTime() {
//		fail("Not yet implemented");
//	}
//
//	@Test
//	public void testGetConfidenceIntervalSurvivalTime() {
//		fail("Not yet implemented");
//	}
//
//	@Test
//	public void testGetSurvivalTimeList() {
//		fail("Not yet implemented");
//	}
//
//	@Test
//	public void testPrintSimilarCaseOutcomeList() {
//		fail("Not yet implemented");
//	}


}
