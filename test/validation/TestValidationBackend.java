/**
 * (2019) Heidelberg University, Institute of Medical Biometry and Informatics (IMBI), Germany
 * 
 * This file is part of myCbrBuilder.
 *  
 * myCbrBuilder is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * myCbrBuilder is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with vivaGen. If not, see <http://www.gnu.org/licenses/>.
 * 
 **/
package validation;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.util.HashMap;

import org.junit.BeforeClass;
import org.junit.Test;

import core.connectors.WekaCsvConnector;
import de.dfki.mycbr.core.DefaultCaseBase;
import de.dfki.mycbr.core.Project;
import de.dfki.mycbr.core.model.Concept;
import similarity.computation.config.KaplanMeierTargetConfig;
import similarity.computation.config.KaplanMeierTargetConfig.SURVIVALTARGET;


/**
 * JUnit test cases for class "ValidationBackend".
 * 
 * @author Christian Karmen
 *
 */
public class TestValidationBackend {
	static DefaultCaseBase itsCaseBase;
	static Project itsProject;
	static Concept itsConcept;
	
	static KaplanMeierTargetConfig itsKMtargetConfig;
	static SURVIVALTARGET itsSurvivaltarget = SURVIVALTARGET.GENERATED;
	
//	SymbolDesc itsSurvivalStatusDesc;
//	SymbolDesc itsTherapyArmDesc;
//	ExtendedDoubleDesc itsSurvivalInMonthsDesc;
	
	static WekaCsvConnector itsWekaCsvConnector;
	
	static HashMap<String, Boolean> itsBiomarkerMap;
	
//	final static String DATASET_FILENAME = "250CASES_20STS_ValidationBackend.csv";
	final static String DATASET_FILENAME = "1000CASES_20STS_ValidationBackend.csv";
	
	
	/**
	 * Initialize test dataset
	 */
	@BeforeClass
	public static void initEnvironment(){
		try {
			itsProject = new Project();
			itsConcept = itsProject.createTopConcept("junit concept");
			
			itsKMtargetConfig = new KaplanMeierTargetConfig(itsSurvivaltarget);
			
			File aCsvFile = new File("datasets" + File.separatorChar + "unittests" + File.separatorChar + DATASET_FILENAME);
			itsWekaCsvConnector = new WekaCsvConnector(itsConcept, aCsvFile, ",", GeneratedDataset.getCaseAndSolutionDescriptionList());
			itsWekaCsvConnector.fetchCaseBase();
			itsCaseBase = itsWekaCsvConnector.getCaseBase();
			
			itsBiomarkerMap = ValidationBackend.buildBiomarkerMap(itsCaseBase, itsKMtargetConfig);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
//	@Test
//	public void testPerformLeaveOneOutValidation() {
//		fail("Not yet implemented");
//	}
//
//	@Test
//	public void testWriteValidationOverview() {
//		fail("Not yet implemented");
//	}
//
//	@Test
//	public void testCreateWeightsOverviewHeader() {
//		fail("Not yet implemented");
//	}
//
//	@Test
//	public void testAddWeightsOverview() {
//		fail("Not yet implemented");
//	}
//
//	@Test
//	public void testCreateSubCohortAnalysisHeader() {
//		fail("Not yet implemented");
//	}
//
//	@Test
//	public void testAddSubCohortAnalysis() {
//		fail("Not yet implemented");
//	}
	
	

	
	
//	
//	/**
//	 * Test if biomarker is set: nominal / armA
//	 * 
//	 * @throws Exception
//	 */
//	@Test
//	public void testBuildBiomarkerMap_nominal_armA() throws Exception {
//		boolean isNumericFlag = false;
//		boolean isArmA = true;			
//		
//		assertEquals(true,  itsBiomarkerMap.get("case1_" + ValidationBackend.getGeneratedBiomarkerAttributeLabel(isNumericFlag, isArmA)));
//		assertEquals(false, itsBiomarkerMap.get("case2_" + ValidationBackend.getGeneratedBiomarkerAttributeLabel(isNumericFlag, isArmA)));
//		assertEquals(false, itsBiomarkerMap.get("case3_" + ValidationBackend.getGeneratedBiomarkerAttributeLabel(isNumericFlag, isArmA)));
//		assertEquals(false, itsBiomarkerMap.get("case4_" + ValidationBackend.getGeneratedBiomarkerAttributeLabel(isNumericFlag, isArmA)));
//		assertEquals(false, itsBiomarkerMap.get("case5_" + ValidationBackend.getGeneratedBiomarkerAttributeLabel(isNumericFlag, isArmA)));
//		assertEquals(false, itsBiomarkerMap.get("case6_" + ValidationBackend.getGeneratedBiomarkerAttributeLabel(isNumericFlag, isArmA)));
//		assertEquals(true,  itsBiomarkerMap.get("case7_" + ValidationBackend.getGeneratedBiomarkerAttributeLabel(isNumericFlag, isArmA)));
//		assertEquals(false, itsBiomarkerMap.get("case8_" + ValidationBackend.getGeneratedBiomarkerAttributeLabel(isNumericFlag, isArmA)));
//		assertEquals(false, itsBiomarkerMap.get("case9_" + ValidationBackend.getGeneratedBiomarkerAttributeLabel(isNumericFlag, isArmA)));
//		assertEquals(false, itsBiomarkerMap.get("case10_"+ ValidationBackend.getGeneratedBiomarkerAttributeLabel(isNumericFlag, isArmA)));
//		
//		assertEquals(true, itsBiomarkerMap.get("case27_" + ValidationBackend.getGeneratedBiomarkerAttributeLabel(isNumericFlag, isArmA)));
//		assertEquals(true, itsBiomarkerMap.get("case31_" + ValidationBackend.getGeneratedBiomarkerAttributeLabel(isNumericFlag, isArmA)));
//		assertEquals(true, itsBiomarkerMap.get("case32_" + ValidationBackend.getGeneratedBiomarkerAttributeLabel(isNumericFlag, isArmA)));
//	}
//	
//	
//	/**
//	 * Test if biomarker is set: nominal / armB
//	 * 
//	 * @throws Exception
//	 */
//	@Test
//	public void testBuildBiomarkerMap_nominal_armB() throws Exception {
//		boolean isNumericFlag = false;
//		boolean isArmA = false;	
//		
//		assertEquals(false, itsBiomarkerMap.get("case1_" + ValidationBackend.getGeneratedBiomarkerAttributeLabel(isNumericFlag, isArmA)));
//		assertEquals(false, itsBiomarkerMap.get("case2_" + ValidationBackend.getGeneratedBiomarkerAttributeLabel(isNumericFlag, isArmA)));
//		assertEquals(false, itsBiomarkerMap.get("case3_" + ValidationBackend.getGeneratedBiomarkerAttributeLabel(isNumericFlag, isArmA)));
//		assertEquals(false, itsBiomarkerMap.get("case4_" + ValidationBackend.getGeneratedBiomarkerAttributeLabel(isNumericFlag, isArmA)));
//		assertEquals(false, itsBiomarkerMap.get("case5_" + ValidationBackend.getGeneratedBiomarkerAttributeLabel(isNumericFlag, isArmA)));
//		assertEquals(false, itsBiomarkerMap.get("case6_" + ValidationBackend.getGeneratedBiomarkerAttributeLabel(isNumericFlag, isArmA)));
//		assertEquals(false, itsBiomarkerMap.get("case7_" + ValidationBackend.getGeneratedBiomarkerAttributeLabel(isNumericFlag, isArmA)));
//		assertEquals(false, itsBiomarkerMap.get("case8_" + ValidationBackend.getGeneratedBiomarkerAttributeLabel(isNumericFlag, isArmA)));
//		assertEquals(true,  itsBiomarkerMap.get("case9_" + ValidationBackend.getGeneratedBiomarkerAttributeLabel(isNumericFlag, isArmA)));
//		assertEquals(false, itsBiomarkerMap.get("case10_"+ ValidationBackend.getGeneratedBiomarkerAttributeLabel(isNumericFlag, isArmA)));
//		
//		assertEquals(true, itsBiomarkerMap.get("case24_" + ValidationBackend.getGeneratedBiomarkerAttributeLabel(isNumericFlag, isArmA)));
//		assertEquals(true, itsBiomarkerMap.get("case25_" + ValidationBackend.getGeneratedBiomarkerAttributeLabel(isNumericFlag, isArmA)));
//		assertEquals(true, itsBiomarkerMap.get("case32_" + ValidationBackend.getGeneratedBiomarkerAttributeLabel(isNumericFlag, isArmA)));
//	}
//	
//	
//	/**
//	 * Test if biomarker is set: numeric / armA
//	 * @throws Exception 
//	 */
//	@Test
//	public void testHasBiomarker_numeric_armA() throws Exception {
//		boolean isNumericFlag = true;
//		boolean isArmA = true;			
//		
//		assertEquals(false, itsBiomarkerMap.get("case1_" + ValidationBackend.getGeneratedBiomarkerAttributeLabel(isNumericFlag, isArmA)));
//		assertEquals(true,  itsBiomarkerMap.get("case2_" + ValidationBackend.getGeneratedBiomarkerAttributeLabel(isNumericFlag, isArmA)));
//		assertEquals(true,  itsBiomarkerMap.get("case3_" + ValidationBackend.getGeneratedBiomarkerAttributeLabel(isNumericFlag, isArmA)));
//		assertEquals(false, itsBiomarkerMap.get("case4_" + ValidationBackend.getGeneratedBiomarkerAttributeLabel(isNumericFlag, isArmA)));
//		assertEquals(true,  itsBiomarkerMap.get("case5_" + ValidationBackend.getGeneratedBiomarkerAttributeLabel(isNumericFlag, isArmA)));
//		assertEquals(true,  itsBiomarkerMap.get("case6_" + ValidationBackend.getGeneratedBiomarkerAttributeLabel(isNumericFlag, isArmA)));
//		assertEquals(false, itsBiomarkerMap.get("case7_" + ValidationBackend.getGeneratedBiomarkerAttributeLabel(isNumericFlag, isArmA)));
//		assertEquals(false, itsBiomarkerMap.get("case8_" + ValidationBackend.getGeneratedBiomarkerAttributeLabel(isNumericFlag, isArmA)));
//		assertEquals(false, itsBiomarkerMap.get("case9_" + ValidationBackend.getGeneratedBiomarkerAttributeLabel(isNumericFlag, isArmA)));
//		assertEquals(false, itsBiomarkerMap.get("case10_"+ ValidationBackend.getGeneratedBiomarkerAttributeLabel(isNumericFlag, isArmA)));
//		
//		assertEquals(true, itsBiomarkerMap.get("case18_" + ValidationBackend.getGeneratedBiomarkerAttributeLabel(isNumericFlag, isArmA)));
//		assertEquals(true, itsBiomarkerMap.get("case30_" + ValidationBackend.getGeneratedBiomarkerAttributeLabel(isNumericFlag, isArmA)));
//		assertEquals(true, itsBiomarkerMap.get("case63_" + ValidationBackend.getGeneratedBiomarkerAttributeLabel(isNumericFlag, isArmA)));
//	}
//	
//	
//	
//	/**
//	 * Test if biomarker is set: numeric / armB
//	 * @throws Exception 
//	 */
//	@Test
//	public void testHasBiomarker_numeric_armB() throws Exception {
//		boolean isNumericFlag = true;
//		boolean isArmA = false;			
//		
//		assertEquals(true,  itsBiomarkerMap.get("case1_" + ValidationBackend.getGeneratedBiomarkerAttributeLabel(isNumericFlag, isArmA)));
//		assertEquals(false, itsBiomarkerMap.get("case2_" + ValidationBackend.getGeneratedBiomarkerAttributeLabel(isNumericFlag, isArmA)));
//		assertEquals(false, itsBiomarkerMap.get("case3_" + ValidationBackend.getGeneratedBiomarkerAttributeLabel(isNumericFlag, isArmA)));
//		assertEquals(false, itsBiomarkerMap.get("case4_" + ValidationBackend.getGeneratedBiomarkerAttributeLabel(isNumericFlag, isArmA)));
//		assertEquals(false, itsBiomarkerMap.get("case5_" + ValidationBackend.getGeneratedBiomarkerAttributeLabel(isNumericFlag, isArmA)));
//		assertEquals(false, itsBiomarkerMap.get("case6_" + ValidationBackend.getGeneratedBiomarkerAttributeLabel(isNumericFlag, isArmA)));
//		assertEquals(false, itsBiomarkerMap.get("case7_" + ValidationBackend.getGeneratedBiomarkerAttributeLabel(isNumericFlag, isArmA)));
//		assertEquals(false, itsBiomarkerMap.get("case8_" + ValidationBackend.getGeneratedBiomarkerAttributeLabel(isNumericFlag, isArmA)));
//		assertEquals(true,  itsBiomarkerMap.get("case9_" + ValidationBackend.getGeneratedBiomarkerAttributeLabel(isNumericFlag, isArmA)));
//		assertEquals(true,  itsBiomarkerMap.get("case10_"+ ValidationBackend.getGeneratedBiomarkerAttributeLabel(isNumericFlag, isArmA)));
//		
//		assertEquals(true, itsBiomarkerMap.get("case16_" + ValidationBackend.getGeneratedBiomarkerAttributeLabel(isNumericFlag, isArmA)));
//		assertEquals(true, itsBiomarkerMap.get("case21_" + ValidationBackend.getGeneratedBiomarkerAttributeLabel(isNumericFlag, isArmA)));
//		assertEquals(true, itsBiomarkerMap.get("case41_" + ValidationBackend.getGeneratedBiomarkerAttributeLabel(isNumericFlag, isArmA)));
//	}
	
	
	/**
	 * Test if biomarker is set: numeric / armA
	 * 
	 * @throws Exception
	 */
	@Test
	public void testBuildBiomarkerMap_numeric_armA() throws Exception {
		boolean isNumericFlag = true;
		boolean isArmA = true;			
		
		assertEquals(true, itsBiomarkerMap.get("case217_" + ValidationBackend.getGeneratedBiomarkerAttributeLabel(isNumericFlag, isArmA)));
		assertEquals(true, itsBiomarkerMap.get("case239_" + ValidationBackend.getGeneratedBiomarkerAttributeLabel(isNumericFlag, isArmA)));
		assertEquals(true, itsBiomarkerMap.get("case312_" + ValidationBackend.getGeneratedBiomarkerAttributeLabel(isNumericFlag, isArmA)));
		assertEquals(true, itsBiomarkerMap.get("case630_" + ValidationBackend.getGeneratedBiomarkerAttributeLabel(isNumericFlag, isArmA)));
		assertEquals(true, itsBiomarkerMap.get("case819_" + ValidationBackend.getGeneratedBiomarkerAttributeLabel(isNumericFlag, isArmA)));
		assertEquals(false, itsBiomarkerMap.get("case1_" + ValidationBackend.getGeneratedBiomarkerAttributeLabel(isNumericFlag, isArmA)));
		assertEquals(false, itsBiomarkerMap.get("case90_" + ValidationBackend.getGeneratedBiomarkerAttributeLabel(isNumericFlag, isArmA)));
		assertEquals(false, itsBiomarkerMap.get("case180_" + ValidationBackend.getGeneratedBiomarkerAttributeLabel(isNumericFlag, isArmA)));
		assertEquals(false, itsBiomarkerMap.get("case227_" + ValidationBackend.getGeneratedBiomarkerAttributeLabel(isNumericFlag, isArmA)));
		assertEquals(false, itsBiomarkerMap.get("case663_"+ ValidationBackend.getGeneratedBiomarkerAttributeLabel(isNumericFlag, isArmA)));
	}
	
	/**
	 * Test if biomarker is set: numeric / armB
	 * 
	 * @throws Exception
	 */
	@Test
	public void testBuildBiomarkerMap_numeric_armB() throws Exception {
		boolean isNumericFlag = true;
		boolean isArmA = false;			
		
		assertEquals(true, itsBiomarkerMap.get("case113_" + ValidationBackend.getGeneratedBiomarkerAttributeLabel(isNumericFlag, isArmA)));
		assertEquals(true, itsBiomarkerMap.get("case199_" + ValidationBackend.getGeneratedBiomarkerAttributeLabel(isNumericFlag, isArmA)));
		assertEquals(true, itsBiomarkerMap.get("case471_" + ValidationBackend.getGeneratedBiomarkerAttributeLabel(isNumericFlag, isArmA)));
		assertEquals(true, itsBiomarkerMap.get("case660_" + ValidationBackend.getGeneratedBiomarkerAttributeLabel(isNumericFlag, isArmA)));
		assertEquals(true, itsBiomarkerMap.get("case830_" + ValidationBackend.getGeneratedBiomarkerAttributeLabel(isNumericFlag, isArmA)));
		assertEquals(false, itsBiomarkerMap.get("case4_" + ValidationBackend.getGeneratedBiomarkerAttributeLabel(isNumericFlag, isArmA)));
		assertEquals(false, itsBiomarkerMap.get("case179_" + ValidationBackend.getGeneratedBiomarkerAttributeLabel(isNumericFlag, isArmA)));
		assertEquals(false, itsBiomarkerMap.get("case270_" + ValidationBackend.getGeneratedBiomarkerAttributeLabel(isNumericFlag, isArmA)));
		assertEquals(false, itsBiomarkerMap.get("case603_" + ValidationBackend.getGeneratedBiomarkerAttributeLabel(isNumericFlag, isArmA)));
		assertEquals(false, itsBiomarkerMap.get("case922_"+ ValidationBackend.getGeneratedBiomarkerAttributeLabel(isNumericFlag, isArmA)));
	}
	
	
	
	/**
	 * Test if biomarker is set: nominal / armA
	 * 
	 * @throws Exception
	 */
	@Test
	public void testBuildBiomarkerMap_nominal_armA() throws Exception {
		boolean isNumericFlag = false;
		boolean isArmA = true;			
		
		assertEquals(true, itsBiomarkerMap.get("case10_" + ValidationBackend.getGeneratedBiomarkerAttributeLabel(isNumericFlag, isArmA)));
		assertEquals(true, itsBiomarkerMap.get("case203_" + ValidationBackend.getGeneratedBiomarkerAttributeLabel(isNumericFlag, isArmA)));
		assertEquals(true, itsBiomarkerMap.get("case403_" + ValidationBackend.getGeneratedBiomarkerAttributeLabel(isNumericFlag, isArmA)));
		assertEquals(true, itsBiomarkerMap.get("case777_" + ValidationBackend.getGeneratedBiomarkerAttributeLabel(isNumericFlag, isArmA)));
		assertEquals(true, itsBiomarkerMap.get("case899_" + ValidationBackend.getGeneratedBiomarkerAttributeLabel(isNumericFlag, isArmA)));
		assertEquals(false, itsBiomarkerMap.get("case3_" + ValidationBackend.getGeneratedBiomarkerAttributeLabel(isNumericFlag, isArmA)));
		assertEquals(false, itsBiomarkerMap.get("case150_" + ValidationBackend.getGeneratedBiomarkerAttributeLabel(isNumericFlag, isArmA)));
		assertEquals(false, itsBiomarkerMap.get("case620_" + ValidationBackend.getGeneratedBiomarkerAttributeLabel(isNumericFlag, isArmA)));
		assertEquals(false, itsBiomarkerMap.get("case760_" + ValidationBackend.getGeneratedBiomarkerAttributeLabel(isNumericFlag, isArmA)));
		assertEquals(false, itsBiomarkerMap.get("case919_"+ ValidationBackend.getGeneratedBiomarkerAttributeLabel(isNumericFlag, isArmA)));
	}
	
	
	/**
	 * Test if biomarker is set: nominal / armB
	 * 
	 * @throws Exception
	 */
	@Test
	public void testBuildBiomarkerMap_nominal_armB() throws Exception {
		boolean isNumericFlag = false;
		boolean isArmA = false;			
		
		assertEquals(true, itsBiomarkerMap.get("case6_" + ValidationBackend.getGeneratedBiomarkerAttributeLabel(isNumericFlag, isArmA)));
		assertEquals(true, itsBiomarkerMap.get("case150_" + ValidationBackend.getGeneratedBiomarkerAttributeLabel(isNumericFlag, isArmA)));
		assertEquals(true, itsBiomarkerMap.get("case433_" + ValidationBackend.getGeneratedBiomarkerAttributeLabel(isNumericFlag, isArmA)));
		assertEquals(true, itsBiomarkerMap.get("case710_" + ValidationBackend.getGeneratedBiomarkerAttributeLabel(isNumericFlag, isArmA)));
		assertEquals(true, itsBiomarkerMap.get("case830_" + ValidationBackend.getGeneratedBiomarkerAttributeLabel(isNumericFlag, isArmA)));
		assertEquals(false, itsBiomarkerMap.get("case8_" + ValidationBackend.getGeneratedBiomarkerAttributeLabel(isNumericFlag, isArmA)));
		assertEquals(false, itsBiomarkerMap.get("case180_" + ValidationBackend.getGeneratedBiomarkerAttributeLabel(isNumericFlag, isArmA)));
		assertEquals(false, itsBiomarkerMap.get("case416_" + ValidationBackend.getGeneratedBiomarkerAttributeLabel(isNumericFlag, isArmA)));
		assertEquals(false, itsBiomarkerMap.get("case631_" + ValidationBackend.getGeneratedBiomarkerAttributeLabel(isNumericFlag, isArmA)));
		assertEquals(false, itsBiomarkerMap.get("case916_"+ ValidationBackend.getGeneratedBiomarkerAttributeLabel(isNumericFlag, isArmA)));
	}
	
	@Test
	public void testCountBiomarkerOccurences() throws Exception {		
//		assertEquals(517, ValidationBackend.countBiomarkerOccurences(itsBiomarkerMap, true, true), 0.01);	// numeric; armA	//510
		assertEquals(550, ValidationBackend.countBiomarkerOccurences(itsBiomarkerMap, true, false), 0.01);	// numeric; armB	//576
		assertEquals(302, ValidationBackend.countBiomarkerOccurences(itsBiomarkerMap, false, true), 0.01);	// nominal; armA
		assertEquals(292, ValidationBackend.countBiomarkerOccurences(itsBiomarkerMap, false, false), 0.01);	// nominal; armB
	}
	

}
