/**
 * (2019) Heidelberg University, Institute of Medical Biometry and Informatics (IMBI), Germany
 * 
 * This file is part of myCbrBuilder.
 *  
 * myCbrBuilder is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * myCbrBuilder is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with vivaGen. If not, see <http://www.gnu.org/licenses/>.
 * 
 **/
package similarity.computation;

import static org.junit.Assert.*;

import org.junit.Test;

public class TestComputationDiscretize {

//	@Test
//	public void testGenerateIntervalLabels() {
//		fail("Not yet implemented");
//	}
//
//	@Test
//	public void testGetDiscretizedIndex() {
//		fail("Not yet implemented");
//	}
//
//	@Test
//	public void testGetDiscretizedIndexAsString() {
//		fail("Not yet implemented");
//	}
//
//	@Test
//	public void testMakeNominal() {
//		fail("Not yet implemented");
//	}
//
//	@Test
//	public void testCalculateCutOff() {
//		fail("Not yet implemented");
//	}
//
//	@Test
//	public void testParseCutOffValue() {
//		fail("Not yet implemented");
//	}

	
	/**
	 * Test function: isRangeAboveOrBelowCutOff()
	 */
	@Test
	public void testIsRangeAboveOrBelowCutOff(){
		// TODO Testfall verschieben nach TestComputationDiscretize
		
		assertEquals(true, ComputationDiscretize.isRangeAboveCutOff("'\\'(0.13702-inf)\\''"));
		assertEquals(false, ComputationDiscretize.isRangeAboveCutOff("'\\'(-inf-1.150722]\\''"));
	}

}
