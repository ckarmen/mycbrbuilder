/**
 * (2019) Heidelberg University, Institute of Medical Biometry and Informatics (IMBI), Germany
 * 
 * This file is part of myCbrBuilder.
 *  
 * myCbrBuilder is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * myCbrBuilder is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with vivaGen. If not, see <http://www.gnu.org/licenses/>.
 * 
 **/
package similarity.computation;

import static org.junit.Assert.*;

import org.junit.Test;

public class TestComputationVDM {

//	@Test
//	public void testGenerateAmalgamFct_HVDM1() {
//		fail("Not yet implemented");
//	}
//
//	@Test
//	public void testGenerateAmalgamFct_HVDM2() {
//		fail("Not yet implemented");
//	}
//
//	@Test
//	public void testGenerateAmalgamFct_HVDM3() {
//		fail("Not yet implemented");
//	}
//
//	@Test
//	public void testGenerateAmalgamFct_DVDM() {
//		fail("Not yet implemented");
//	}
//
//	@Test
//	public void testGenerateAmalgamFct_IVDM() {
//		fail("Not yet implemented");
//	}
//
//	@Test
//	public void testGenerateAmalgamFct_WVDM() {
//		fail("Not yet implemented");
//	}
//
//	@Test
//	public void testGetNaxc() {
//		fail("Not yet implemented");
//	}
//
//	@Test
//	public void testGetNax() {
//		fail("Not yet implemented");
//	}
//
//	@Test
//	public void testGetPaxc() {
//		fail("Not yet implemented");
//	}
//
//	@Test
//	public void testGetVDM() {
//		fail("Not yet implemented");
//	}
//
//	@Test
//	public void testGetVDM_N1() {
//		fail("Not yet implemented");
//	}
//
//	@Test
//	public void testGetVDM_N2() {
//		fail("Not yet implemented");
//	}
//
//	@Test
//	public void testGetVDM_N3() {
//		fail("Not yet implemented");
//	}
//
//	@Test
//	public void testGetVDM_N1_Q2() {
//		fail("Not yet implemented");
//	}
//
//	@Test
//	public void testSetSimilarityMatrix_VDM() {
//		fail("Not yet implemented");
//	}

}
