/**
 * (2019) Heidelberg University, Institute of Medical Biometry and Informatics (IMBI), Germany
 * 
 * This file is part of myCbrBuilder.
 *  
 * myCbrBuilder is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * myCbrBuilder is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with vivaGen. If not, see <http://www.gnu.org/licenses/>.
 * 
 **/
package similarity.computation;

import static org.junit.Assert.assertEquals;

import java.io.File;

import org.junit.Before;
import org.junit.Test;

import core.connectors.WekaCsvConnector;
import de.dfki.mycbr.core.DefaultCaseBase;
import de.dfki.mycbr.core.Project;
import de.dfki.mycbr.core.model.Concept;
import de.dfki.mycbr.core.model.SymbolDesc;
import exceptions.CaseBaseException;
import similarity.computation.config.KaplanMeierTargetConfig;
import similarity.computation.config.KaplanMeierTargetConfig.SURVIVALTARGET;
import similarity.desc.ExtendedDoubleDesc;
import validation.GeneratedDataset;

/**
 * JUnit tests for class 'ComputationKM'
 * 
 * @author Christian Karmen
 *
 */
public class TestComputationKM {
	DefaultCaseBase itsCaseBase;
	Project itsProject;
	Concept itsConcept;
	
	KaplanMeierTargetConfig itsKMtargetConfig;
	SURVIVALTARGET itsSurvivaltarget = SURVIVALTARGET.GENERATED;
	
	SymbolDesc itsSurvivalStatusDesc;
	SymbolDesc itsTherapyArmDesc;
	ExtendedDoubleDesc itsSurvivalInMonthsDesc;
	
	WekaCsvConnector itsWekaCsvConnector;
	
	final private String DATASET_FILENAME = "250CASES_20STS_OutcomeAnalysis.csv";
	
	
	/**
	 * Initialize test dataset
	 */
	@Before
	public void initEnvironment(){
		try {
			itsProject = new Project();
			itsConcept = itsProject.createTopConcept("junit concept");
			
			itsKMtargetConfig = new KaplanMeierTargetConfig(itsSurvivaltarget);
			
			File aCsvFile = new File("datasets" + File.separatorChar + "unittests" + File.separatorChar + DATASET_FILENAME);
			itsWekaCsvConnector = new WekaCsvConnector(itsConcept, aCsvFile, ",", GeneratedDataset.getCaseAndSolutionDescriptionList());
			itsWekaCsvConnector.fetchCaseBase();
			itsCaseBase = itsWekaCsvConnector.getCaseBase();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

//	@Test
//	public void testGetNominalFile() {
//		fail("Not yet implemented");
//	}
//
//	@Test
//	public void testGetWekaInstancesFromCaseBaseDefaultCaseBaseKaplanMeierTargetConfig() {
//		fail("Not yet implemented");
//	}
//
//	@Test
//	public void testGetWekaInstancesFromCaseBaseDefaultCaseBaseArrayListOfStringKaplanMeierTargetConfig() {
//		fail("Not yet implemented");
//	}
//
//	@Test
//	public void testGetSurvivalList() {
//		fail("Not yet implemented");
//	}
//
//	@Test
//	public void testGetAreaBetweenSurvivalCurves() {
//		fail("Not yet implemented");
//	}
//
//	@Test
//	public void testGetAttributeLabels() {
//		fail("Not yet implemented");
//	}
//
//	@Test
//	public void testCalculateImpactKM() {
//		fail("Not yet implemented");
//	}
//
//	@Test
//	public void testGetAreaBetweenValueAndNotValue() {
//		fail("Not yet implemented");
//	}
//
//	@Test
//	public void testCalculateLocalSimilarityKM() {
//		fail("Not yet implemented");
//	}
//
//	@Test
//	public void testSetSimilarityMatrix_KMInstancesBooleanKaplanMeierTargetConfigSymbolFct() {
//		fail("Not yet implemented");
//	}
//
//	@Test
//	public void testSetSimilarityMatrix_KMInstancesKaplanMeierConfigSymbolFct() {
//		fail("Not yet implemented");
//	}
//
//	@Test
//	public void testGenerateAmalgamFct_KAPLANMEIER() {
//		fail("Not yet implemented");
//	}
	

	/**
	 * Test function: getValueWithHighestKMCurve() 
	 * @throws CaseBaseException 
	 */
	@Test
	public void testGetValueWithHighestKMCurve() throws CaseBaseException{	
		assertEquals("'\\'(0.13702-inf)\\''", ComputationKM.getValueWithHighestKMCurve(itsCaseBase, "ExpDistr1", itsSurvivaltarget));
		assertEquals("'\\'(-inf-1.150722]\\''", ComputationKM.getValueWithHighestKMCurve(itsCaseBase, "ExpDistr2", itsSurvivaltarget));
		assertEquals("'\\'(-inf-0.291102]\\''", ComputationKM.getValueWithHighestKMCurve(itsCaseBase, "ExpDistr3", itsSurvivaltarget));
		
		assertEquals("'\\'(7.33715-inf)\\''", ComputationKM.getValueWithHighestKMCurve(itsCaseBase, "NormDistr1", itsSurvivaltarget));
		assertEquals("'\\'(9.450684-inf)\\''", ComputationKM.getValueWithHighestKMCurve(itsCaseBase, "NormDistr2", itsSurvivaltarget));
		assertEquals("'\\'(6.143797-inf)\\''", ComputationKM.getValueWithHighestKMCurve(itsCaseBase, "NormDistr3", itsSurvivaltarget)); // wirklich?
		
		assertEquals("'\\'(-inf-0.544509]\\''", ComputationKM.getValueWithHighestKMCurve(itsCaseBase, "UniformDistr1", itsSurvivaltarget));
		assertEquals("'\\'(0.560501-inf)\\''", ComputationKM.getValueWithHighestKMCurve(itsCaseBase, "UniformDistr2", itsSurvivaltarget));
		assertEquals("'\\'(-inf-0.352084]\\''", ComputationKM.getValueWithHighestKMCurve(itsCaseBase, "UniformDistr3", itsSurvivaltarget));
		
		assertEquals("'\\'(0.511332-inf)\\''", ComputationKM.getValueWithHighestKMCurve(itsCaseBase, "WeibullDistr1", itsSurvivaltarget));
		assertEquals("'\\'(5.612738-inf)\\''", ComputationKM.getValueWithHighestKMCurve(itsCaseBase, "WeibullDistr2", itsSurvivaltarget)); // wirklich?
		assertEquals("'\\'(7.808014-inf)\\''", ComputationKM.getValueWithHighestKMCurve(itsCaseBase, "WeibullDistr3", itsSurvivaltarget));
		
		assertEquals("label2", ComputationKM.getValueWithHighestKMCurve(itsCaseBase, "UniformStrings1", itsSurvivaltarget));
		assertEquals("label1", ComputationKM.getValueWithHighestKMCurve(itsCaseBase, "UniformStrings2", itsSurvivaltarget));
		assertEquals("label1", ComputationKM.getValueWithHighestKMCurve(itsCaseBase, "UniformStrings3", itsSurvivaltarget));
		
		assertEquals("label1", ComputationKM.getValueWithHighestKMCurve(itsCaseBase, "NomBiomarkerArmA", itsSurvivaltarget));
		assertEquals("label1", ComputationKM.getValueWithHighestKMCurve(itsCaseBase, "NomBiomarkerArmB", itsSurvivaltarget));
		
		assertEquals("'\\'(-inf-7.556913]\\''", ComputationKM.getValueWithHighestKMCurve(itsCaseBase, "NumBiomarkerArmA", itsSurvivaltarget));
		assertEquals("'\\'(8.092199-inf)\\''", ComputationKM.getValueWithHighestKMCurve(itsCaseBase, "NumBiomarkerArmB", itsSurvivaltarget));
	}

}
