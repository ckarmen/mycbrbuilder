/**
 * (2019) Heidelberg University, Institute of Medical Biometry and Informatics (IMBI), Germany
 * 
 * This file is part of myCbrBuilder.
 *  
 * myCbrBuilder is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * myCbrBuilder is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with vivaGen. If not, see <http://www.gnu.org/licenses/>.
 * 
 **/
package similarity.computation;

import static org.junit.Assert.assertEquals;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.TreeSet;

import org.junit.Before;
import org.junit.Test;

import de.dfki.mycbr.core.DefaultCaseBase;
import de.dfki.mycbr.core.Project;
import de.dfki.mycbr.core.casebase.Instance;
import de.dfki.mycbr.core.model.Concept;
import de.dfki.mycbr.core.model.DoubleDesc;
import de.dfki.mycbr.core.model.SymbolDesc;
import similarity.desc.ExtendedDoubleDesc;
import similarity.desc.ExtendedIntegerDesc;
import similarity.desc.ExtendedSymbolDesc;

/**
 * 
 * JUnit Tests for the Class "Computation".
 * 
 * @author Christian Karmen
 *
 */
public class TestComputation {
	
	Project itsProject;	
	Concept itsConcept;
	DefaultCaseBase itsCaseBase;
	
	SymbolDesc itsSymbolDesc;
	DoubleDesc itsDoubleDesc;
	ExtendedDoubleDesc itsExtendedDoubleDesc;
	ExtendedSymbolDesc itsExtendedSymbolDesc;
	ExtendedIntegerDesc itsExtendedIntegerDesc;
	
	TreeSet<String> itsAllowedValueList;
	
	double MAX_DELTA = 0.00000001;


	
	/**
	 * Initialize a minimal cbr system
	 * 
	 * @throws Exception
	 */
	@Before
	public void initProject() throws Exception{
		itsProject = new Project();		
		itsConcept = itsProject.createTopConcept("foo");
		itsCaseBase = new DefaultCaseBase(itsProject, "casebase", 999);
		
		itsAllowedValueList = new TreeSet<>();
		itsAllowedValueList.add("value1");
		itsAllowedValueList.add("value2");
		itsAllowedValueList.add("value3");		
		
		itsExtendedSymbolDesc = new ExtendedSymbolDesc(itsConcept, "nominal attribute", itsAllowedValueList);
		itsExtendedDoubleDesc = new ExtendedDoubleDesc(itsConcept, "decimal attribute", 0, 100);
		itsExtendedIntegerDesc = new ExtendedIntegerDesc(itsConcept, "integer attribute", 0, 100);
	}

	@Test
	public void testSetStatistics() throws ParseException {
		Instance aCase1 = new Instance(itsConcept, "case1");
		Instance aCase2 = new Instance(itsConcept, "case2");
		Instance aCase3 = new Instance(itsConcept, "case3");
		Instance aCase4 = new Instance(itsConcept, "case4");
		Instance aCase5 = new Instance(itsConcept, "case5");
		Instance aCase6 = new Instance(itsConcept, "case6");
		
		aCase1.addAttribute(itsExtendedDoubleDesc, 1d);
		aCase2.addAttribute(itsExtendedDoubleDesc, 2d);
		aCase3.addAttribute(itsExtendedDoubleDesc, 3d);
		aCase4.addAttribute(itsExtendedDoubleDesc, 4d);
		aCase5.addAttribute(itsExtendedDoubleDesc, 5d);
		aCase6.addAttribute(itsExtendedDoubleDesc, 9d);
		
		aCase1.addAttribute(itsExtendedIntegerDesc, 1);
		aCase2.addAttribute(itsExtendedIntegerDesc, 2);
		aCase3.addAttribute(itsExtendedIntegerDesc, 3);
		aCase4.addAttribute(itsExtendedIntegerDesc, 4);
		aCase5.addAttribute(itsExtendedIntegerDesc, 5);
		aCase6.addAttribute(itsExtendedIntegerDesc, 9);
		
		Computation.setStatistics(itsExtendedDoubleDesc);				
		assertEquals(1d, itsExtendedDoubleDesc.getMin(), MAX_DELTA);
		assertEquals(9d, itsExtendedDoubleDesc.getMax(), MAX_DELTA);
		assertEquals(4d, itsExtendedDoubleDesc.getMeanValue(), MAX_DELTA);
		assertEquals(3.5, itsExtendedDoubleDesc.getMedianValue(), MAX_DELTA);
		assertEquals(Math.sqrt(40d/6d) , itsExtendedDoubleDesc.getStandardDeviation(), MAX_DELTA);		
		
		Computation.setStatistics(itsExtendedIntegerDesc);
		assertEquals(1, itsExtendedIntegerDesc.getMin(), MAX_DELTA);
		assertEquals(9, itsExtendedIntegerDesc.getMax(), MAX_DELTA);
		assertEquals(4, itsExtendedIntegerDesc.getMeanValue(), MAX_DELTA);
		assertEquals(3.5, itsExtendedIntegerDesc.getMedianValue(), MAX_DELTA);
		assertEquals(Math.sqrt(40d/6d) , itsExtendedIntegerDesc.getStandardDeviation(), MAX_DELTA);	
	}



	@Test
	public void testGetRandomNumber() {
		int aRepeats = 100;
		runRandom(aRepeats, 100);
		runRandom(aRepeats, 1);		
		runRandom(aRepeats, 0);
		runRandom(aRepeats, -1);
		runRandom(aRepeats, Integer.MAX_VALUE);
		runRandom(aRepeats, Integer.MIN_VALUE);
	}
	private void runRandom(int theNUmberOfRepeats, int theUpperLimit){
		for (int i = 0; i < theNUmberOfRepeats; i++) {
			if (theUpperLimit>0){
				assertEquals(true, Computation.getRandomNumber(theUpperLimit) <= theUpperLimit); 
				assertEquals(true, Computation.getRandomNumber(theUpperLimit) >= 0);
			} else {
				assertEquals(true, Computation.getRandomNumber(theUpperLimit) >= theUpperLimit); 
				assertEquals(true, Computation.getRandomNumber(theUpperLimit) <= 0);
			}
		}
	}

	@Test
	public void testGetNumberOfUniqueValues() throws Exception {		
		assertEquals(0, Computation.getNumberOfUniqueValues(itsExtendedSymbolDesc));		
		assertEquals(0, Computation.getNumberOfUniqueValues(itsExtendedDoubleDesc));
		
		Instance aCase1 = new Instance(itsConcept, "case1");
		Instance aCase2 = new Instance(itsConcept, "case2");
		Instance aCase3 = new Instance(itsConcept, "case3");
		
		aCase1.addAttribute(itsExtendedSymbolDesc, "value1");
		aCase2.addAttribute(itsExtendedSymbolDesc, "value1");
		aCase3.addAttribute(itsExtendedSymbolDesc, "value1");
		aCase1.addAttribute(itsExtendedDoubleDesc, 1.1);
		aCase2.addAttribute(itsExtendedDoubleDesc, 12.2);
		aCase3.addAttribute(itsExtendedDoubleDesc, 12.2);
		aCase1.addAttribute(itsExtendedIntegerDesc, 11);
		aCase2.addAttribute(itsExtendedIntegerDesc, 22);
		aCase3.addAttribute(itsExtendedIntegerDesc, 55);
		
		itsCaseBase.addCase(aCase1);
		itsCaseBase.addCase(aCase2);
		itsCaseBase.addCase(aCase3);

		assertEquals(1, Computation.getNumberOfUniqueValues(itsExtendedSymbolDesc));
		assertEquals(2, Computation.getNumberOfUniqueValues(itsExtendedDoubleDesc));
		assertEquals(3, Computation.getNumberOfUniqueValues(itsExtendedIntegerDesc));
	}

	
	@Test
	public void testGetMeanValueSimpleAttDesc() throws ParseException {
		Instance aCase1 = new Instance(itsConcept, "case1");
		Instance aCase2 = new Instance(itsConcept, "case2");
		Instance aCase3 = new Instance(itsConcept, "case3");
		Instance aCase4 = new Instance(itsConcept, "case4");
		Instance aCase5 = new Instance(itsConcept, "case5");
		
		aCase1.addAttribute(itsExtendedDoubleDesc, 15.1);
		aCase2.addAttribute(itsExtendedDoubleDesc, 11.3);
		aCase3.addAttribute(itsExtendedDoubleDesc, 54.3);
		aCase4.addAttribute(itsExtendedDoubleDesc, 43.33);
		aCase5.addAttribute(itsExtendedDoubleDesc, 45.76);
		
		assertEquals(33.958, Computation.getMeanValue(itsExtendedDoubleDesc), MAX_DELTA);
	}

	@Test
	public void testGetMeanValueArrayListOfDouble() {
		ArrayList<Double> aDoubleList = new ArrayList<>();
		aDoubleList.add(15.1);
		aDoubleList.add(11.3);
		aDoubleList.add(54.3);
		aDoubleList.add(43.33);
		aDoubleList.add(45.76);
		
		assertEquals(33.958, Computation.getMeanValue(aDoubleList), MAX_DELTA);
	}

//	@Test
//	public void testGetConfidenceIntervalSimpleAttDescDouble() {
//		fail("Not yet implemented");
//	}
//
//	@Test
//	public void testGetConfidenceIntervalArrayListOfDoubleDouble() {
//		fail("Not yet implemented");
//	}

	@Test
	public void testGetMin() throws ParseException {
		Instance aCase1 = new Instance(itsConcept, "case1");
		Instance aCase2 = new Instance(itsConcept, "case2");
		Instance aCase3 = new Instance(itsConcept, "case3");
		Instance aCase4 = new Instance(itsConcept, "case4");
		Instance aCase5 = new Instance(itsConcept, "case5");
		
		aCase1.addAttribute(itsExtendedDoubleDesc, 15.1);
		aCase2.addAttribute(itsExtendedDoubleDesc, 11.3);
		aCase3.addAttribute(itsExtendedDoubleDesc, 54.3);
		aCase4.addAttribute(itsExtendedDoubleDesc, 43.33);
		aCase5.addAttribute(itsExtendedDoubleDesc, 45.76);
		
		assertEquals(11.3, Computation.getMin(itsExtendedDoubleDesc), MAX_DELTA);
		
		aCase1.addAttribute(itsExtendedIntegerDesc, 15);
		aCase2.addAttribute(itsExtendedIntegerDesc, 11);
		aCase3.addAttribute(itsExtendedIntegerDesc, 54);
		aCase4.addAttribute(itsExtendedIntegerDesc, 43);
		aCase5.addAttribute(itsExtendedIntegerDesc, 45);
		
		assertEquals(11, Computation.getMin(itsExtendedIntegerDesc), MAX_DELTA);
	}

	@Test
	public void testGetMax() throws ParseException {
		Instance aCase1 = new Instance(itsConcept, "case1");
		Instance aCase2 = new Instance(itsConcept, "case2");
		Instance aCase3 = new Instance(itsConcept, "case3");
		Instance aCase4 = new Instance(itsConcept, "case4");
		Instance aCase5 = new Instance(itsConcept, "case5");
		
		aCase1.addAttribute(itsExtendedDoubleDesc, 15.1);
		aCase2.addAttribute(itsExtendedDoubleDesc, 11.3);
		aCase3.addAttribute(itsExtendedDoubleDesc, 54.3);
		aCase4.addAttribute(itsExtendedDoubleDesc, 43.33);
		aCase5.addAttribute(itsExtendedDoubleDesc, 45.76);
		
		assertEquals(54.3, Computation.getMax(itsExtendedDoubleDesc), MAX_DELTA);
		
		aCase1.addAttribute(itsExtendedIntegerDesc, 15);
		aCase2.addAttribute(itsExtendedIntegerDesc, 11);
		aCase3.addAttribute(itsExtendedIntegerDesc, 54);
		aCase4.addAttribute(itsExtendedIntegerDesc, 43);
		aCase5.addAttribute(itsExtendedIntegerDesc, 45);
		
		assertEquals(54, Computation.getMax(itsExtendedIntegerDesc), MAX_DELTA);
	}

	@Test
	public void testGetVarianceSimpleAttDesc() throws ParseException {
		Instance aCase1 = new Instance(itsConcept, "case1");
		Instance aCase2 = new Instance(itsConcept, "case2");
		Instance aCase3 = new Instance(itsConcept, "case3");
		Instance aCase4 = new Instance(itsConcept, "case4");
		Instance aCase5 = new Instance(itsConcept, "case5");
		
		aCase1.addAttribute(itsExtendedDoubleDesc, 1d);
		aCase2.addAttribute(itsExtendedDoubleDesc, 2d);
		aCase3.addAttribute(itsExtendedDoubleDesc, 3d);
		aCase4.addAttribute(itsExtendedDoubleDesc, 4d);
		aCase5.addAttribute(itsExtendedDoubleDesc, 5d);
		
		aCase1.addAttribute(itsExtendedIntegerDesc, 1);
		aCase2.addAttribute(itsExtendedIntegerDesc, 2);
		aCase3.addAttribute(itsExtendedIntegerDesc, 3);
		aCase4.addAttribute(itsExtendedIntegerDesc, 4);
		aCase5.addAttribute(itsExtendedIntegerDesc, 5);
		
		assertEquals(2d, Computation.getVariance(itsExtendedDoubleDesc), MAX_DELTA);
		assertEquals(2, Computation.getVariance(itsExtendedIntegerDesc), MAX_DELTA);
	}

	@Test
	public void testGetVarianceArrayListOfDouble() {
		ArrayList<Double> aDoubleList = new ArrayList<>();
		aDoubleList.add(1d);
		aDoubleList.add(2d);
		aDoubleList.add(3d);
		aDoubleList.add(4d);
		aDoubleList.add(5d);
		
		// sum = 1+2+3+4+5 = 15
		// mean = 15/5 = 3
		// variance = [ (1-3)^2 + (2-3)^2 + (3-3)^2 + (4-3)^2 + (5-3)^2 ] / 5
		//          = [    4    +    1    +    0    +    1    +     4   ] / 5
		//          = 10 / 5 = 2
		
		assertEquals(2d, Computation.getVariance(aDoubleList), MAX_DELTA);
	}

	@Test
	public void testGetStandardDeviationSimpleAttDesc() throws ParseException {
		Instance aCase1 = new Instance(itsConcept, "case1");
		Instance aCase2 = new Instance(itsConcept, "case2");
		Instance aCase3 = new Instance(itsConcept, "case3");
		Instance aCase4 = new Instance(itsConcept, "case4");
		Instance aCase5 = new Instance(itsConcept, "case5");
		
		aCase1.addAttribute(itsExtendedDoubleDesc, 1d);
		aCase2.addAttribute(itsExtendedDoubleDesc, 2d);
		aCase3.addAttribute(itsExtendedDoubleDesc, 3d);
		aCase4.addAttribute(itsExtendedDoubleDesc, 4d);
		aCase5.addAttribute(itsExtendedDoubleDesc, 5d);
		
		aCase1.addAttribute(itsExtendedIntegerDesc, 1);
		aCase2.addAttribute(itsExtendedIntegerDesc, 2);
		aCase3.addAttribute(itsExtendedIntegerDesc, 3);
		aCase4.addAttribute(itsExtendedIntegerDesc, 4);
		aCase5.addAttribute(itsExtendedIntegerDesc, 5);
		
		// var = 2		
		// stddev = sqrt(var)
		
		assertEquals(Math.sqrt(2d), Computation.getStandardDeviation(itsExtendedDoubleDesc), MAX_DELTA);
		assertEquals(Math.sqrt(2), Computation.getStandardDeviation(itsExtendedIntegerDesc), MAX_DELTA);
	}

	@Test
	public void testGetStandardDeviationArrayListOfDouble() {
		ArrayList<Double> aDoubleList = new ArrayList<>();
		aDoubleList.add(1d);
		aDoubleList.add(2d);
		aDoubleList.add(3d);
		aDoubleList.add(4d);
		aDoubleList.add(5d);
		// var = 2		
		// stddev = sqrt(var)
		
		assertEquals(Math.sqrt(2d), Computation.getStandardDeviation(aDoubleList), MAX_DELTA);
	}

	@Test
	public void testGetMedianValueArrayListOfDouble() {
		ArrayList<Double> aDoubleList = new ArrayList<>();
		aDoubleList.add(0.1);
		aDoubleList.add(0.2);
		aDoubleList.add(99.1);
		aDoubleList.add(99.2);
		aDoubleList.add(5.5);
		
		assertEquals(5.5, Computation.getMedianValue(aDoubleList), MAX_DELTA);
		
		aDoubleList.add(4.5);
		assertEquals(5.0, Computation.getMedianValue(aDoubleList), MAX_DELTA);		
		
		ArrayList<Double> anIntegerList = new ArrayList<>();
		anIntegerList.add(1d);
		anIntegerList.add(1d);
		anIntegerList.add(99d);
		anIntegerList.add(99d);
		anIntegerList.add(6d);
		
		assertEquals(6d, Computation.getMedianValue(anIntegerList), MAX_DELTA);
		
		anIntegerList.add(4d);
		assertEquals(5d, Computation.getMedianValue(anIntegerList), MAX_DELTA);
	}

	@Test
	public void testGetMedianValueSimpleAttDesc() throws ParseException {
		Instance aCase1 = new Instance(itsConcept, "case1");
		Instance aCase2 = new Instance(itsConcept, "case2");
		Instance aCase3 = new Instance(itsConcept, "case3");
		Instance aCase4 = new Instance(itsConcept, "case4");
		Instance aCase5 = new Instance(itsConcept, "case5");
		
		aCase1.addAttribute(itsExtendedDoubleDesc, 0.1);
		aCase2.addAttribute(itsExtendedDoubleDesc, 0.2);
		aCase3.addAttribute(itsExtendedDoubleDesc, 99.1);
		aCase4.addAttribute(itsExtendedDoubleDesc, 99.2);
		aCase5.addAttribute(itsExtendedDoubleDesc, 5.5);
		
		assertEquals(5.5, Computation.getMedianValue(itsExtendedDoubleDesc), MAX_DELTA);
		
		Instance aCase6 = new Instance(itsConcept, "case6");
		aCase6.addAttribute(itsExtendedDoubleDesc, 4.5);
		assertEquals(5.0, Computation.getMedianValue(itsExtendedDoubleDesc), MAX_DELTA);
	}

//	@Test
//	public void testSetSimilarityMatrix_OVERLAP() {
//		fail("Not yet implemented");
//	}

}
