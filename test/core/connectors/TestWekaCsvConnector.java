/**
 * (2019) Heidelberg University, Institute of Medical Biometry and Informatics (IMBI), Germany
 * 
 * This file is part of myCbrBuilder.
 *  
 * myCbrBuilder is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * myCbrBuilder is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with vivaGen. If not, see <http://www.gnu.org/licenses/>.
 * 
 **/
package core.connectors;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import core.CaseBaseUtils;
import de.dfki.mycbr.core.DefaultCaseBase;
import de.dfki.mycbr.core.Project;
import de.dfki.mycbr.core.model.AttributeDesc;
import de.dfki.mycbr.core.model.Concept;
import de.dfki.mycbr.core.model.DoubleDesc;
import de.dfki.mycbr.core.model.SymbolDesc;
import exceptions.CaseBaseException;
import exceptions.ConnectorException;
import validation.GeneratedDataset;
import weka.core.Instance;

/**
 * Class to test the WekaCsvConnector.
 * 
 * @author Christian Karmen
 */
public class TestWekaCsvConnector {
	
	static DefaultCaseBase itsCaseBase;
	static Project itsProject;
	static Concept itsConcept;
	static WekaCsvConnector itsWekaCsvConnector;
	static ArrayList<String> itsWhiteList;
	
	final static String DATASET_FILENAME = "250CASES_CsvConnector.csv";
	final static String DATASET_DELIMITER = ",";
	
	final static File itsCsvFile = new File("datasets" + File.separatorChar + "unittests" + File.separatorChar + DATASET_FILENAME);;
	
	

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		
	}
	

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		itsProject = new Project();
		itsConcept = itsProject.createTopConcept("junit_concept");
		itsWhiteList = GeneratedDataset.getCaseAndSolutionDescriptionList();
	}
	

	/**
	 * Test method for {@link connectors.TestWekaCsvConnector#testFetchCaseBase()}.
	 * @throws ConnectorException 
	 */
	@Test
	public void testFetchCaseBase() throws ConnectorException {
		itsWekaCsvConnector = new WekaCsvConnector(itsConcept, itsCsvFile, DATASET_DELIMITER, itsWhiteList);
		itsWekaCsvConnector.fetchCaseBase();
		itsCaseBase = itsWekaCsvConnector.getCaseBase();
		
		assertEquals(250, itsCaseBase.getCases().size());
	}
	

	/**
	 * Test method for {@link connectors.TestWekaCsvConnector#testWekaCsvConnector()}.
	 * @throws ConnectorException 
	 */
	@Test(expected = ConnectorException.class)
	public void testWekaCsvConnector_FileNotExist() throws ConnectorException {
		WekaCsvConnector aConnector = new WekaCsvConnector(
				itsConcept, 
				new File("foo"), 
				DATASET_DELIMITER, 
				GeneratedDataset.getCaseAndSolutionDescriptionList());
		aConnector.fetchCaseBase();
	}
	
	
	/**
	 * Test method for {@link connectors.TestWekaCsvConnector#testWekaCsvConnector()}.
	 * @throws ConnectorException 
	 */
	@Test
	public void testWekaCsvConnector_EmtpyWhiteList() throws ConnectorException {
		WekaCsvConnector aConnector = new WekaCsvConnector(itsConcept, itsCsvFile, DATASET_DELIMITER, new ArrayList<String>());
		
		aConnector.fetchCaseBase();
		
		itsCaseBase = itsWekaCsvConnector.getCaseBase();
		assertEquals(250, itsCaseBase.getCases().size());
		
		int aNumerOfAttributes = itsConcept.getAllAttributeDescs().size();
		assertEquals(0, aNumerOfAttributes);
	}
	
	
	/**
	 * Test method for {@link connectors.TestWekaCsvConnector#testWekaCsvConnector()}.
	 * @throws ConnectorException 
	 */
	@Test
	public void testWekaCsvConnector_DefinedWhiteList_Symbol() throws ConnectorException {
		String anAttributeLabel = "RandomArm";
		
		itsWhiteList = new ArrayList<>();
		itsWhiteList.add(anAttributeLabel);
		itsWekaCsvConnector = new WekaCsvConnector(itsConcept, itsCsvFile, DATASET_DELIMITER, itsWhiteList);
		
		itsWekaCsvConnector.fetchCaseBase();
		
		itsCaseBase = itsWekaCsvConnector.getCaseBase();
		assertEquals(250, itsCaseBase.getCases().size());
		
		int aNumerOfAttributes = itsConcept.getAllAttributeDescs().size();
		assertEquals(1, aNumerOfAttributes);
		
		AttributeDesc anAttributeDesc = itsConcept.getAttributeDesc(anAttributeLabel);
		assertTrue(anAttributeDesc instanceof SymbolDesc);
		
		String anAttributeDescLabel = ((SymbolDesc)anAttributeDesc).getName();
		assertEquals(anAttributeLabel, anAttributeDescLabel);
	}
	
	
	/**
	 * Test method for {@link connectors.TestWekaCsvConnector#testWekaCsvConnector()}.
	 * @throws ConnectorException 
	 */
	@Test
	public void testWekaCsvConnector_DefinedWhiteList_Double() throws ConnectorException {
		String anAttributeLabel = "SurvivalTime";
		
		itsWhiteList = new ArrayList<>();
		itsWhiteList.add(anAttributeLabel);
		itsWekaCsvConnector = new WekaCsvConnector(itsConcept, itsCsvFile, DATASET_DELIMITER, itsWhiteList);
		
		itsWekaCsvConnector.fetchCaseBase();
		
		itsCaseBase = itsWekaCsvConnector.getCaseBase();
		assertEquals(250, itsCaseBase.getCases().size());
		
		int aNumerOfAttributes = itsConcept.getAllAttributeDescs().size();
		assertEquals(1, aNumerOfAttributes);
		
		AttributeDesc anAttributeDesc = itsConcept.getAttributeDesc(anAttributeLabel);
		assertTrue(anAttributeDesc instanceof DoubleDesc);
		
		String anAttributeDescLabel = ((DoubleDesc)anAttributeDesc).getName();
		assertEquals(anAttributeLabel, anAttributeDescLabel);
	}
	

	/**
	 * Test method for {@link connectors.TestWekaCsvConnector#testFetchCaseBaseHashMapOfStringString()}.
	 * @throws ConnectorException 
	 */
	@Test
	public void testFetchCaseBaseHashMapOfStringString_OneFilter() throws ConnectorException {
		String aFilterAttributeLabel = "RandomArm";
		String aFilterAttributeValue = "arm A";
		String anotherAttributeValue = "arm B";
		HashMap<String, String> aFilter = new HashMap<>();
		aFilter.put(aFilterAttributeLabel, "'"+aFilterAttributeValue+"'");	// "'" because weka probably needs them for values with whitespaces
		
		itsWhiteList = new ArrayList<>();
		itsWhiteList.add(aFilterAttributeLabel);
		
		itsWekaCsvConnector = new WekaCsvConnector(itsConcept, itsCsvFile, DATASET_DELIMITER, itsWhiteList);
		itsWekaCsvConnector.fetchCaseBase(aFilter);
		
		itsCaseBase = itsWekaCsvConnector.getCaseBase();
		assertEquals(125, itsCaseBase.getCases().size());
		
		int aNumerOfAttributes = itsConcept.getAllAttributeDescs().size();
		assertEquals(itsWhiteList.size(), aNumerOfAttributes);
		
		AttributeDesc anAttributeDesc = itsConcept.getAttributeDesc(aFilterAttributeLabel);
		assertTrue(anAttributeDesc instanceof SymbolDesc);
		
		String anAttributeDescLabel = ((SymbolDesc)anAttributeDesc).getName();
		assertEquals(aFilterAttributeLabel, anAttributeDescLabel);
		
		Set<String> anAllowedValuesSet = ((SymbolDesc)anAttributeDesc).getAllowedValues();
		assertTrue(anAllowedValuesSet.contains(aFilterAttributeValue));
		
		// FIXME TRUE is ok here, FALSE would be the cleaner solution  
		assertTrue(anAllowedValuesSet.contains(anotherAttributeValue));
	}
	
	
	/**
	 * Test method for {@link connectors.TestWekaCsvConnector#testFetchCaseBaseHashMapOfStringString()}.
	 * @throws ConnectorException 
	 */
	@Test
	public void testFetchCaseBaseHashMapOfStringString_TwoFilters() throws ConnectorException {
		String aFilterAttributeLabel1 = "RandomArm";
		String aFilterAttributeValue1 = "arm A";
		
		String aFilterAttributeLabel2 = "SurvivalType";
		String aFilterAttributeValue2 = "LTS";
		
		HashMap<String, String> aFilter = new HashMap<>();
		aFilter.put(aFilterAttributeLabel1, "'"+aFilterAttributeValue1+"'");	// "'" because weka probably needs them for values with whitespaces
		aFilter.put(aFilterAttributeLabel2, aFilterAttributeValue2);
		
		itsWhiteList = new ArrayList<>();
		itsWhiteList.add(aFilterAttributeLabel1);
		itsWhiteList.add(aFilterAttributeLabel2);
		
		itsWekaCsvConnector = new WekaCsvConnector(itsConcept, itsCsvFile, DATASET_DELIMITER, itsWhiteList);
		itsWekaCsvConnector.fetchCaseBase(aFilter);
		
		itsCaseBase = itsWekaCsvConnector.getCaseBase();
		assertEquals(100, itsCaseBase.getCases().size());
		
		int aNumerOfAttributes = itsConcept.getAllAttributeDescs().size();
		assertEquals(itsWhiteList.size(), aNumerOfAttributes);
		
		AttributeDesc anAttributeDesc = itsConcept.getAttributeDesc(aFilterAttributeLabel1);
		assertTrue(anAttributeDesc instanceof SymbolDesc);
		
		AttributeDesc anAttributeDesc2 = itsConcept.getAttributeDesc(aFilterAttributeLabel2);
		assertTrue(anAttributeDesc2 instanceof SymbolDesc);
		
		String anAttributeDescLabel = ((SymbolDesc)anAttributeDesc).getName();
		assertEquals(aFilterAttributeLabel1, anAttributeDescLabel);
		
		String anAttributeDescLabel2 = ((SymbolDesc)anAttributeDesc2).getName();
		assertEquals(aFilterAttributeLabel2, anAttributeDescLabel2);
		
		Set<String> anAllowedValuesSet = ((SymbolDesc)anAttributeDesc).getAllowedValues();
		assertTrue(anAllowedValuesSet.contains(aFilterAttributeValue1));
		
		Set<String> anAllowedValuesSet2 = ((SymbolDesc)anAttributeDesc2).getAllowedValues();
		assertTrue(anAllowedValuesSet2.contains(aFilterAttributeValue2));
	}
	
	
	/**
	 * Test method for {@link connectors.TestWekaCsvConnector#testFetchCaseBaseInt()}.
	 * @throws ConnectorException 
	 * @throws IOException 
	 */
	@Test
	public void testFetchCaseBaseInt_TooHighCaseNumber() throws IOException, ConnectorException {
		int aCaseLimit = 10000;
		
		itsWekaCsvConnector = new WekaCsvConnector(itsConcept, itsCsvFile, DATASET_DELIMITER, itsWhiteList);
		itsWekaCsvConnector.fetchCaseBase(aCaseLimit);
		
		itsCaseBase = itsWekaCsvConnector.getCaseBase();
		assertEquals(250, itsCaseBase.getCases().size());
	}
	
	/**
	 * Test method for {@link connectors.TestWekaCsvConnector#testFetchCaseBaseInt()}.
	 * @throws ConnectorException 
	 * @throws IOException 
	 */
	@Test
	public void testFetchCaseBaseInt_SomeCaseNumber() throws IOException, ConnectorException {
		int aCaseLimit = 100;
		
		itsWekaCsvConnector = new WekaCsvConnector(itsConcept, itsCsvFile, DATASET_DELIMITER, itsWhiteList);
		itsWekaCsvConnector.fetchCaseBase(aCaseLimit);
		
		itsCaseBase = itsWekaCsvConnector.getCaseBase();
		assertEquals(aCaseLimit, itsCaseBase.getCases().size());
	}
	
	/**
	 * Test method for {@link connectors.TestWekaCsvConnector#testFetchCaseBaseInt()}.
	 * @throws ConnectorException 
	 * @throws IOException 
	 */
	@Test(expected = ConnectorException.class)
	public void testFetchCaseBaseInt_ZeroCases() throws IOException, ConnectorException {
		int aCaseLimit = 0;
		
		itsWekaCsvConnector = new WekaCsvConnector(itsConcept, itsCsvFile, DATASET_DELIMITER, itsWhiteList);
		itsWekaCsvConnector.fetchCaseBase(aCaseLimit);
	}
	
	/**
	 * Test method for {@link connectors.TestWekaCsvConnector#testFetchCaseBaseInt()}.
	 * @throws ConnectorException 
	 * @throws IOException 
	 */
	@Test(expected = ConnectorException.class)
	public void testFetchCaseBaseInt_NegativeCaseNumber() throws IOException, ConnectorException {
		int aCaseLimit = -100;
		
		itsWekaCsvConnector = new WekaCsvConnector(itsConcept, itsCsvFile, DATASET_DELIMITER, itsWhiteList);
		itsWekaCsvConnector.fetchCaseBase(aCaseLimit);
		
		itsCaseBase = itsWekaCsvConnector.getCaseBase();
		assertEquals(0, itsCaseBase.getCases().size());
	}

	
	/**
	 * Test method for {@link connectors.TestWekaCsvConnector#testFetchCaseBaseInt()}.
	 * @throws ConnectorException 
	 * @throws CaseBaseException 
	 */
	@Test
	public void testFetchCaseBase_IdenticalData_Symbol() throws ConnectorException, CaseBaseException {
		String aSymbolAttributeLabel = "RandomArm";
		
		itsWekaCsvConnector = new WekaCsvConnector(itsConcept, itsCsvFile, DATASET_DELIMITER, itsWhiteList);
		itsWekaCsvConnector.fetchCaseBase();
		
		itsCaseBase = itsWekaCsvConnector.getCaseBase();
		assertEquals(250, itsCaseBase.getCases().size());
		
		int anAttributeIndex = itsWekaCsvConnector.itsWekaInstances.getAttributeIndex(aSymbolAttributeLabel);
		Instance aWekaCase;
		de.dfki.mycbr.core.casebase.Instance aCaseBaseInstance;
		int aCaseId;
		String aWekaValue;
		String aCaseBaseValue;
		
		for (int i = 0; i < itsWekaCsvConnector.itsWekaInstances.size(); i++) {
			aWekaCase = itsWekaCsvConnector.itsWekaInstances.get(i);
			aCaseId = itsWekaCsvConnector.itsWekaInstances.getIntegerValueByAttributeName(i, "Instance"); //aWekaCase.attribute(0).value(0);
			aWekaValue = aWekaCase.stringValue(anAttributeIndex);
			
			aCaseBaseInstance = CaseBaseUtils.getCaseBaseInstanceById(itsCaseBase, "case"+aCaseId);
			aCaseBaseValue = CaseBaseUtils.getCaseAttributeValue(aCaseBaseInstance, aSymbolAttributeLabel).getValueAsString();
			
//			System.out.println("DEBUG: " + aWekaValue + " <> " + aCaseBaseValue);
			assertTrue(aWekaValue.equals(aCaseBaseValue));
		}
	}
	
	
	/**
	 * Test method for {@link connectors.TestWekaCsvConnector#testFetchCaseBaseInt()}.
	 * @throws ConnectorException 
	 * @throws CaseBaseException 
	 */
	@Test
	public void testFetchCaseBase_IdenticalData_Double() throws ConnectorException, CaseBaseException {
		String aSymbolAttributeLabel = "SurvivalTime";
		
		itsWekaCsvConnector = new WekaCsvConnector(itsConcept, itsCsvFile, DATASET_DELIMITER, itsWhiteList);
		itsWekaCsvConnector.fetchCaseBase();
		
		itsCaseBase = itsWekaCsvConnector.getCaseBase();
		assertEquals(250, itsCaseBase.getCases().size());
		
		int anAttributeIndex = itsWekaCsvConnector.itsWekaInstances.getAttributeIndex(aSymbolAttributeLabel);
		Instance aWekaCase;
		de.dfki.mycbr.core.casebase.Instance aCaseBaseInstance;
		int aCaseId;
		double aWekaValue;
		double aCaseBaseValue;
		
		for (int i = 0; i < itsWekaCsvConnector.itsWekaInstances.size(); i++) {
			aWekaCase = itsWekaCsvConnector.itsWekaInstances.get(i);
			aCaseId = itsWekaCsvConnector.itsWekaInstances.getIntegerValueByAttributeName(i, "Instance"); //aWekaCase.attribute(0).value(0);
			aWekaValue = aWekaCase.value(anAttributeIndex);
			
			aCaseBaseInstance = CaseBaseUtils.getCaseBaseInstanceById(itsCaseBase, "case"+aCaseId);
			aCaseBaseValue = Double.parseDouble(CaseBaseUtils.getCaseAttributeValue(aCaseBaseInstance, aSymbolAttributeLabel).getValueAsString());
			
//			System.out.println("DEBUG: " + aWekaValue + " <> " + aCaseBaseValue);
			assertEquals(aWekaValue, aCaseBaseValue, 0.000001);
		}
	}
	
}
