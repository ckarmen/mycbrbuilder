/**
 * (2019) Heidelberg University, Institute of Medical Biometry and Informatics (IMBI), Germany
 * 
 * This file is part of myCbrBuilder.
 *  
 * myCbrBuilder is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * myCbrBuilder is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with vivaGen. If not, see <http://www.gnu.org/licenses/>.
 * 
 **/
package core;

import static org.junit.Assert.*;

import org.junit.Test;

public class TestCaseBaseUtils {

	@Test
	public void testGetCaseBaseAttributeDescList() {
		fail("Not yet implemented");
	}

	@Test
	public void testGetCaseBaseAttributeLabelList() {
		fail("Not yet implemented");
	}

	@Test
	public void testGetCaseBaseAttributeDescByLabel() {
		fail("Not yet implemented");
	}

	@Test
	public void testGetFirstCase() {
		fail("Not yet implemented");
	}

	@Test
	public void testGetCaseIdList() {
		fail("Not yet implemented");
	}

	@Test
	public void testGetCaseBaseInstanceById() {
		fail("Not yet implemented");
	}

	@Test
	public void testGetAllAttributes() {
		fail("Not yet implemented");
	}

	@Test
	public void testGetAllAttributeValues() {
		fail("Not yet implemented");
	}

	@Test
	public void testGetAllCaseBaseAttributesAsDouble() {
		fail("Not yet implemented");
	}

	@Test
	public void testGetRandomCase() {
		fail("Not yet implemented");
	}

	@Test
	public void testPrintCaseBaseData() {
		fail("Not yet implemented");
	}

	@Test
	public void testAddInstanceToCaseBase() {
		fail("Not yet implemented");
	}

	@Test
	public void testGetClonedCaseBaseDefaultCaseBase() {
		fail("Not yet implemented");
	}

	@Test
	public void testGetClonedCaseBaseDefaultCaseBaseStringString() {
		fail("Not yet implemented");
	}

	@Test
	public void testGetFilteredCaseBase() {
		fail("Not yet implemented");
	}

	@Test
	public void testPrintCaseAttributes() {
		fail("Not yet implemented");
	}

	@Test
	public void testPrintCase() {
		fail("Not yet implemented");
	}

}
