/**
 * (2019) Heidelberg University, Institute of Medical Biometry and Informatics (IMBI), Germany
 * 
 * This file is part of myCbrBuilder.
 *  
 * myCbrBuilder is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * myCbrBuilder is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with vivaGen. If not, see <http://www.gnu.org/licenses/>.
 * 
 **/
package core;

import static org.junit.Assert.*;

import org.junit.Test;

public class TestMyCbrUtils {

	@Test
	public void testPrintRetrievalResults() {
		fail("Not yet implemented");
	}

	@Test
	public void testPrintConceptDescription() {
		fail("Not yet implemented");
	}

	@Test
	public void testPrintLine() {
		fail("Not yet implemented");
	}

	@Test
	public void testPrintLineStringInt() {
		fail("Not yet implemented");
	}

	@Test
	public void testPrintTitle() {
		fail("Not yet implemented");
	}

	@Test
	public void testGenerateFctNameString() {
		fail("Not yet implemented");
	}

	@Test
	public void testGenerateFctNameStringString() {
		fail("Not yet implemented");
	}

	@Test
	public void testGenerateFctNameStringStringString() {
		fail("Not yet implemented");
	}

	@Test
	public void testGetConceptShortName() {
		fail("Not yet implemented");
	}

	@Test
	public void testGetAttributeDescByName() {
		fail("Not yet implemented");
	}

	@Test
	public void testGetActiveAmalgamFct() {
		fail("Not yet implemented");
	}

	@Test
	public void testGetActiveFct() {
		fail("Not yet implemented");
	}

	@Test
	public void testGetFctListOfActiveAmalgamFct() {
		fail("Not yet implemented");
	}

	@Test
	public void testGetCaseAttribute() {
		fail("Not yet implemented");
	}

	@Test
	public void testIsSymbolDesc() {
		fail("Not yet implemented");
	}

	@Test
	public void testGetCaseDescList() {
		fail("Not yet implemented");
	}

	@Test
	public void testGetLocalSimilarity() {
		fail("Not yet implemented");
	}

	@Test
	public void testPerformRetrieval() {
		fail("Not yet implemented");
	}

	@Test
	public void testAddSymbolDescription() {
		fail("Not yet implemented");
	}

	@Test
	public void testAddDoubleDescription() {
		fail("Not yet implemented");
	}

	@Test
	public void testAddAdvancedDoubleDescription() {
		fail("Not yet implemented");
	}

	@Test
	public void testAddIntegerDescription() {
		fail("Not yet implemented");
	}

	@Test
	public void testAddAdvancedIntegerDescription() {
		fail("Not yet implemented");
	}

	@Test
	public void testSetAttributeActivity() {
		fail("Not yet implemented");
	}

	@Test
	public void testGetRestrictedValuesList() {
		fail("Not yet implemented");
	}

	@Test
	public void testZipFile() {
		fail("Not yet implemented");
	}

	@Test
	public void testArchiveFiles() {
		fail("Not yet implemented");
	}

	@Test
	public void testGetFiles() {
		fail("Not yet implemented");
	}

	@Test
	public void testRemoveFiles() {
		fail("Not yet implemented");
	}

}
